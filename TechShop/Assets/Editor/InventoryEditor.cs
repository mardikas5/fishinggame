﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Inventory))]
public class InventoryEditor : Editor
{
    public override void OnInspectorGUI()
    {

        Custom();

        base.OnInspectorGUI();
    }

    public void Custom()
    {
        Inventory inv = (Inventory)target;

        HashSet<Int2> used = new HashSet<Int2>();

        EditorGUILayout.Space();

        if (inv.Items?.data == null)
        {
            return;
        }

        for (int i = 0; i < inv.Items.data.Length; i++)
        {
            if (inv.Items.data[i] == null)
            {
                continue;
            }

            if (used.Contains(inv.Items.data[i].CornerPos))
            {
                continue;
            }

            EditorGUILayout.BeginHorizontal("Box");
            EditorGUILayout.LabelField(inv.Items.data[i].Name, GUILayout.MaxWidth(150f));
            EditorGUILayout.LabelField(inv.Items.data[i].CornerPos.ToString(), GUILayout.MaxWidth(50f));
            EditorGUILayout.LabelField(inv.Items.data[i].Rotation.ToString(), GUILayout.MaxWidth(150f));
            EditorGUILayout.EndHorizontal();

            used.Add(inv.Items.data[i].CornerPos);
        }
    }

}
