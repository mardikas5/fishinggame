﻿using UnityEngine;
using UnityEditor;
using Fishy;

public class ScriptableCreators
{
    [MenuItem("Assets/Create/Recipe/SimpleRecipe", priority = 0)]
    public static void CreateSimpleRecipe()
    {
        ScriptableObjectUtility.CreateAsset<Recipe>();
    }

    [MenuItem("Assets/Create/ScriptableCollection", priority = 0)]
    public static void CreateCollection()
    {
        ScriptableObjectUtility.CreateAsset<ScriptableObjectCollection>();
    }

    [MenuItem("Assets/Create/Recipe/RecipeBase", priority = 0)]
    public static void CreateRecipeBase()
    {
        //ScriptableObjectUtility.CreateAsset<Recipe>();
    }


    [MenuItem("Assets/Create/RecipeMaterial/MaterialBase", priority = 0)]
    public static void CreateRecipeMaterialBase()
    {
        ScriptableObjectUtility.CreateAsset<MaterialBase>();
    }

    [MenuItem("Assets/Create/Item/ItemTag(deprec)", priority = 0)]
    public static void CreateItemTag()
    {
        //ScriptableObjectUtility.CreateAsset<ItemTag>();
    }

    [MenuItem("Assets/Create/Fish/FishBase", priority = 0)]
    public static void CreateFishBase()
    {
        ScriptableObjectUtility.CreateAsset<FishBase>();
    }

    [MenuItem("Assets/Create/Fish/FishAIParams", priority = 0)]
    public static void CreateFishAIParams()
    {
        ScriptableObjectUtility.CreateAsset<FishAIParams>();
    }

    [MenuItem("Assets/Create/Entity/Base", priority = 0)]
    public static void CreateEntityBase()
    {
        ScriptableObjectUtility.CreateAsset<EntityBase>();
    }

    [MenuItem("Assets/Create/Entity/ItemBase", priority = 0)]
    public static void CreateItem()
    {
        ScriptableObjectUtility.CreateAsset<ItemBase>();
    }

    [MenuItem("Assets/Create/Fish/WaterBodyBase", priority = 0)]
    public static void CreateWaterBodyBase()
    {
        ScriptableObjectUtility.CreateAsset<WaterBodyBase>();
    }

    [MenuItem("Assets/Create/DayNight/DayNightPreset", priority = 0)]
    public static void CreatePreset()
    {
        ScriptableObjectUtility.CreateAsset<DayNightPreset>();
    }

}