﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/SimpleMask"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_CutOff("CutOff",Range(0,1)) = 0
		_Radius("Radius",Range(0,1)) = 0.2
		_Mask("maskInt", float ) = 1
	}
		SubShader
		{
			LOD 100
			//Blend One One
			Tags { "Queue" = "Geometry" }  // Write to the stencil buffer before drawing any geometry to the screen
			ColorMask 0 // Don't write to any colour channels
			ZWrite off // Don't write to the Depth buffer
			ZTest always
			
			Stencil
			{
				Ref [_Mask]
				Comp Always
				Pass Replace
				//Fail Replace
			}

			Pass
			{
				//Tags {"LightMode" = "ShadowCaster"}

				CGPROGRAM
				
				#pragma vertex vert
				#pragma fragment frag
				// make fog work

				#pragma multi_compile_fog
				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
				float _CutOff;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);

					o.uv = TRANSFORM_TEX(v.uv, _MainTex);

					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, i.uv);

					float dissolve = step(col,1 - _CutOff);

					clip(_CutOff - dissolve);
					//discard;
					return col;
				}

				ENDCG
			}
			Pass
			{
				Tags {"LightMode" = "ShadowCaster"}
				
				CGPROGRAM
				
				#pragma vertex vert
				#pragma fragment frag
					// make fog work
				
					#pragma multi_compile_fog
					#include "UnityCG.cginc"
				
					struct appdata
					{
						float4 vertex : POSITION;
						float2 uv : TEXCOORD0;
					};
				
					struct v2f
					{
						float2 uv : TEXCOORD0;
						UNITY_FOG_COORDS(1)
						float4 vertex : SV_POSITION;
					};
				
					sampler2D _MainTex;
					float4 _MainTex_ST;
					float _CutOff;
				
					v2f vert(appdata v)
					{
						v2f o;
						o.vertex = UnityObjectToClipPos(v.vertex);
				
						o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
						UNITY_TRANSFER_FOG(o,o.vertex);
						return o;
					}
				
					fixed4 frag(v2f i) : SV_Target
					{
						fixed4 col = tex2D(_MainTex, i.uv);
				
						float dissolve = step(col,1 - _CutOff);
				
						clip(_CutOff - dissolve);
						
						return col;
					}
				
					ENDCG
			}
		}
}