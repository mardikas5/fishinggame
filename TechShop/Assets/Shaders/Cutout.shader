﻿Shader "Cutout"
{
	Properties
	{
		_MainTex("Base (RGBA)", 2D) = "white" {}
		_Radius("HoleRadius", Range(-1,1)) = 0
	}
		SubShader
		{
			LOD 100
			Blend One One
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
			//ZTest Always
			//ZWrite off
			 
			CGPROGRAM
			#pragma surface surf Lambert fullforwardshadows alpha:fade

			struct Input
			{
				float2 uv_MainTex;
				float3 worldPos;
			};

			sampler2D _MainTex;
			uniform float _Radius;

			void surf(Input IN, inout SurfaceOutput o)
			{
				half4 col = tex2D(_MainTex, IN.uv_MainTex).rgba;
				clip(col.r + _Radius);
				o.Albedo = half4(1, 1, 1, 1);
				
			}
			ENDCG
		}
			FallBack "Diffuse"
}