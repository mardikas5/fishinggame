﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

	Shader "Custom/CutAwayWorldCutoff" 
	{
		Properties{
			_Color("Color", Color) = (1,1,1,1)
			_EmissionColor("EmissionColor",Color) = (1,0,0,1)
			_Emission("Emission",Range(0,1)) = 0.0
			_MainTex("Albedo (RGB)", 2D) = "white" {}
	
			_GlossMap("GlossMap",2D) = "white"{}
			_Glossiness("Smoothness", Float) = 0.5
			_Metallic("Metallic", Float) = 0.0
			_AO("AO",2D) = "white"{}
			_NormalMap("NormalMap",2D) = "bump"{}
			_NormalMapIntensity("NormalMapIntensity",Range(-2,2)) = 0.0
			
			_CutAwayTex("CutAwayTexture",2D) = "white"{}
			_HeightOffset("HeightOffset", float) = 0
			_CutOffActive("activeCutoff", float) = 0
			_Cull("Culling", float) = 0
		}
	
		SubShader
		{
			Tags {"Queue" = "Geometry" "RenderType" = "Opaque" }

				ZWrite On
				Cull [_Cull]
				ZTest LEqual
				ColorMask rgba

				Stencil
				{
					//Ref 1
					//Comp NotEqual
					//Pass Replace
				}

				CGPROGRAM
				#include "UnityCG.cginc"
				#pragma surface surf Standard fullforwardshadows addshadow// alpha
				#pragma target 3.0



				struct Input {
					float2 uv_MainTex;
					float2 uv_Gloss;
					float3 worldPos;
					float2 uv_CutAwayTex;
					float4 screenPos;
				};
	
				sampler2D _MainTex;
	
				half _Glossiness;
				half _Metallic;
				fixed4 _Color;
				float4 _EmissionColor;
				float _NormalMapIntensity;
				float _Emission;
				sampler2D _GlossMap,_AO,_NormalMap;

				sampler2D _CutAwayTex;
				float _GlobalWorldYCutoff;
				float _WorldCutoffActive;
				float _CutOffActive;
				float _HeightOffset;
				UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_INSTANCING_BUFFER_END(Props)
	
				void surf(Input IN, inout SurfaceOutputStandard o) {
					//float2 screenUV2 = -IN.screenPos.xy / IN.screenPos.w;
					//float2 screenPos = float2(sin(screenUV2.x * 3.14),sin(screenUV2.y * 3.14));

					float2 screenUV = (IN.worldPos.xz + IN.worldPos.xy + IN.worldPos.zy) * .03;//( +
					screenUV += sin(_Time.xx / 5);
					//screenUV /= 2;
					fixed4 c2 = tex2D(_CutAwayTex, screenUV);
					
					float val = (IN.worldPos.y + _HeightOffset) - _GlobalWorldYCutoff;
					val -= c2.rgb;
					float sValue = smoothstep(0, 2, val);
					
					sValue = 1 - min(_WorldCutoffActive, sValue);//min(min(_WorldCutoffActive, _CutOffActive), sValue);
					clip(sValue - 1);
					
					fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	
					o.Albedo = c.rgb;
					o.Metallic = _Metallic;
					o.Smoothness = _Glossiness * tex2D(_GlossMap,IN.uv_MainTex);
	
					o.Alpha = c.a;

					clip(c.a - 0.01f);

					o.Occlusion = tex2D(_AO,IN.uv_MainTex);
					o.Emission = _Emission * _EmissionColor;
					fixed3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex)).rgb;
					n.x *= _NormalMapIntensity;
					n.y *= _NormalMapIntensity;
					o.Normal = normalize(n);
					
				}
				ENDCG


				//end pass1
				//Lighting On
				//blend One One


			
		}
				//FallBack "Diffuse"
	}