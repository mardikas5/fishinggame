﻿Shader "Unlit/Fishy"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_EmissionColor("EmissionColor",Color) = (1,0,0,1)
		_Emission("Emission",Range(0,1)) = 0.0
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_GlossMap("GlossMap",2D) = "white"{}
		_Glossiness("Smoothness", Float) = 0.5
		_Metallic("Metallic", Float) = 0.0
		_AO("AO",2D) = "white"{}
		_NormalMap("NormalMap",2D) = "bump"{}
		_NormalMapIntensity("NormalMapIntensity",Range(1,5)) = 1.0

		_Modifier("Modifier", Vector) = (1,1,1,1)
		_Amplitude("Amplitude", Float) = 1
		_Speed("Speed", Float) = 1
	}
		SubShader
		{
			Tags
			{
				"RenderType" = "Opaque"
			}


			CGPROGRAM
			#pragma vertex vert
			#pragma surface surf Standard fullforwardshadows addshadow

			#pragma target 3.0
			#include "UnityCG.cginc"


			struct Input
			{
				float4 vertex : POSITION;
				float2 uv_MainTex : TEXCOORD0;
			};

			sampler2D _MainTex;

			half _Glossiness;
			half _Metallic;
			fixed4 _Color;
			float4 _EmissionColor;
			float _NormalMapIntensity;
			float _Emission;
			sampler2D _GlossMap, _AO, _NormalMap;

			uniform float4 _Modifier;
			uniform float _Speed;
			uniform float _Amplitude;

			void vert(inout appdata_full v)
			{
				float positiveDivider = min(.4, v.vertex.z);
				float movementModifier = 1 + abs(v.vertex.y * .5);
				float movement = (sin((v.vertex.z * _Amplitude * _Modifier.z) + (_Time.w * _Speed) + (_Modifier.x * _Modifier.y)) * (1 - cos(max(1, v.vertex.z) * 1.57079))) / (_Amplitude * _Modifier.w);
				movement *= movementModifier;
				movement *= positiveDivider;
				v.vertex.x += movement;
			}

			void surf(Input IN, inout SurfaceOutputStandard o) {
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness * tex2D(_GlossMap, IN.uv_MainTex);
				o.Alpha = c.a;
				o.Occlusion = tex2D(_AO, IN.uv_MainTex);
				o.Emission = _Emission * _EmissionColor;
				fixed3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex)).rgb;
				n.x *= _NormalMapIntensity;
				n.y *= _NormalMapIntensity;
				o.Normal = normalize(n);
			}
			ENDCG
			}
					FallBack "Diffuse"
		}
