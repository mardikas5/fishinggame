﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalShaderSettings : Singleton<GlobalShaderSettings>
{
    public const string _WorldCutoff = "_GlobalWorldYCutoff";
    public const string _WorldCutoffActive = "_WorldCutoffActive";

    public int globalCutoffDistInt;
    public int globalCutoffEnabledInt;

    public bool startWorldCutOff;

    public float cutoffDist = 0f;

    public void Start()
    {
        DontDestroyOnLoad(this);
        globalCutoffDistInt = Shader.PropertyToID(_WorldCutoff);
        globalCutoffEnabledInt = Shader.PropertyToID(_WorldCutoffActive);
        SetCutoffActive(startWorldCutOff);
    }


    public void UpdateShaderCutoffHeight(float value)
    {
        cutoffDist = value;
        Shader.SetGlobalFloat(globalCutoffDistInt, value);
    }

    public void SetCutoffActive(bool value)
    {
        float _intValue = value ? 1f : 0f;
        Shader.SetGlobalFloat(globalCutoffEnabledInt, _intValue);
    }
}
