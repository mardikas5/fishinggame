﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CharWalkAnimEventHandler : MonoBehaviour
{
    public Transform LeftFoot, RightFoot;

    public GameObject spawnOnStep;

    public AudioSource audioSource;

    public List<AudioClip> stepSounds;

    public bool IsDrunk;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Update()
    {
        
    }

    public void FootL()
    {
        HandleStep(LeftFoot);
    }

    public void FootR()
    {
        HandleStep(RightFoot);
    }

    private void HandleStep(Transform foot)
    {
        GameObject obj = Instantiate(spawnOnStep);
        obj.transform.position = foot.transform.position;
        audioSource.PlayOneShot(stepSounds.Random());
    }
}
