﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[ExecuteInEditMode]
public class ParticleToTarget : MonoBehaviour
{
    public Transform Target;

    private ParticleSystem system;

    private ParticleSystem.Particle[] particles = new ParticleSystem.Particle[1000];
    private ParticleSystem.Particle[] StoredValues = new ParticleSystem.Particle[1000];

    public UnityEvent onParticleDied;
    private int count;
    public int deadParticles = 0;

    public float timeChange = 0f;

    private void Start()
    {
        if (system == null) { system = GetComponent<ParticleSystem>(); }
    }

    private void Update()
    {
        if (deadParticles > 0f)
        {
            onParticleDied?.Invoke();
            deadParticles = 0;
        }

        Vector3 endPos = Target.transform.position;
        count = system.GetParticles(particles);

        if (system.time <= 0.05f)
        {
            system.GetParticles(StoredValues);
        }

        for (int i = 0; i < count; i++)
        {
            ParticleSystem.Particle particle = particles[i];
            float nTime = 1f - (particle.remainingLifetime / particle.startLifetime);

            Vector3 initialPos = StoredValues[i].position;

            Vector3 moveVector = (endPos - initialPos) * nTime;

            if (particle.remainingLifetime < 0.05f)
            {
                OnParticleDied();
            }

            particle.position = initialPos + moveVector;

            particles[i] = particle;
        }

        system.SetParticles(particles, count);
    }

    private void OnParticleDied()
    {
        deadParticles++;
    }
}
