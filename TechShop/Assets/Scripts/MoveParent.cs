﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveParent : MonoBehaviour
{
    public Transform parent;
    public Vector3 lastFrameMove;
    public Animator animator;

    public CharacterController controller;

    private void Start()
    {
        animator = GetComponent<Animator>();

        if (parent == null)
        {
            return;
        }

        if (controller == null)
        {
            controller = parent.GetComponentInChildren<CharacterController>();
        }
    }

    private void OnAnimatorMove()
    {
        //shit breaks because of scene change?
        //animator.ApplyBuiltinRootMotion();

        if (parent == null)
        {
            return;
        }

        Vector3 animPos = transform.position - parent.transform.position;

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        if (animPos == Vector3.zero)
        {
            return;
        }

        controller.Move(animPos);
    }
}
