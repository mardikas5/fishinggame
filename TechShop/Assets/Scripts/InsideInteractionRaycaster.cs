﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InsideInteractionRaycaster : MonoBehaviour
{
    private InteractionHighlightController highlighter;

    public Transform compareTo;

    public List<Interactable> currentRequests = new List<Interactable>();

    private Dictionary<Interactable, bool> rayHits = new Dictionary<Interactable, bool>();

    private void Awake()
    {
        if (compareTo == null)
        {
            compareTo = GameLogic.Instance.Player.transform;
        }
        highlighter = GameLogic.Instance.interactionHighlight;
    }

    public void raycastAll(ref Dictionary<Interactable, bool> rayhitCasts)
    {
        rayhitCasts.Clear();
        if (Camera.main == null)
        {
            return;
        }

        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(r, 100f);
        bool hitwall = false;
        foreach (RaycastHit h in hits)
        {
            Interactable interactable = h.transform.GetComponent<Interactable>();
            if (interactable != null)
            {
                if (rayhitCasts.ContainsKey(interactable))
                {
                    continue;
                }
                rayhitCasts.Add(interactable, hitwall);
            }
            else
            {
                hitwall = true;
            }
        }
    }

    // Update is called once per frame
    private void Update()
    {
        RayCastInteractions();
    }

    private void RayCastInteractions()
    {
        if (UI.Instance.MouseOnUI)
        {
            return;
        }

        raycastAll(ref rayHits);

        ClearCurrent();
        SetAsCurrent(rayHits);
        if (rayHits.Keys.Count == 0)
        {
            return;
        }

        if (Input.GetMouseButton(0))
        {
            if (currentRequests.Count <= 0)
            {
                return;
            }
            currentRequests[0]?.OnPointerClick(null);
        }
    }

    private void SetAsCurrent(Dictionary<Interactable, bool> rayCastHits)
    {
        foreach (Interactable key in rayCastHits.Keys)
        {
            float distTo = Mathf.Abs(compareTo.transform.position.y - key.transform.position.y);
            if (distTo > 1f && rayCastHits[key])
            {
                return;
            }
            key.OnPointerEnter(null);
        }

        currentRequests = rayCastHits.Keys.ToList();
    }

    private void ClearCurrent()
    {
        Action<Interactable> k = (x) =>
        {
            if (x == null || x.gameObject == null)
            {
                return;
            }
            x.OnPointerExit(null);
            //highlighter.mouseInputHandler.RemovePossible(new MouseInputController.InputRequestUser(x.MousePriority, x.interactionObject));
        };

        currentRequests.ForEach(k);
    }
}
