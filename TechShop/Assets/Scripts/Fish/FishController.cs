﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FishController : MonoBehaviour
{
    public SpriteRenderer FishRenderer;

    public void Start()
    {
        transform.SetParent(GameLogic.Instance.FishParent);
    }
}
