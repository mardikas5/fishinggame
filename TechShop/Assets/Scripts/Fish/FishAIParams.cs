﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishAIParams : ScriptableObject
{
    public float preferredDepth;

    public float Smartness;
}
