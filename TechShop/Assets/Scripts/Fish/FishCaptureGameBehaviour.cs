﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Behaviour of the fish during the capture game -> how it moves on the 1D or 2D fishing game.
/// </summary>
public class FishCaptureGameBehaviour : MonoBehaviour
{
    public enum FishSwimState
    {
        Resting,
        Evading,
        BeingPulled,
        None,
    }

    private float TimeToUpdate = 0f;
    private Vector2 FishTarget;

    private Fish Fish;
    private FishBase FishBase => Fish.FishBase;

    protected Vector2 normalizedPos;

    public Vector2 NormalizedPos => normalizedPos;

    public FishSwimState State { get; private set; }

    /// <summary>
    /// Capture progress for this fish.
    /// </summary>
    public float Progress;

    public void Awake()
    {
        Init(GetComponent<Fish>());
    }

    public void Init(Fish fish)
    {
        Fish = fish;
    }

    public void UpdateBehaviour()
    {
        TimeToUpdate -= Time.deltaTime;

        if (TimeToUpdate < 0f)
        {
            FishTarget = updateTarget();
            TimeToUpdate = FishBase.UpdatePosInterval + Random.Range(-FishBase.UpdatePosRandomness, FishBase.UpdatePosRandomness);
        }

        MoveFish();
    }

    private Vector2 updateTarget()
    {
        Vector2 limits = new Vector2(FishBase.MinJumpDistance, FishBase.MaxJumpDistance);

        return new Vector2(
            GetAxisTarget(limits, normalizedPos.x),
            GetAxisTarget(limits, normalizedPos.y)
        );
    }

    private float GetAxisTarget(Vector2 minMax, float currentPos)
    {
        float newTarget = Random.Range(minMax.x, minMax.y);
        float dir = Random.Range(-1f, 1f);

        float randomSign = Mathf.Sign(dir);

        float addedMove = newTarget * randomSign;

        float rawTarget = currentPos + addedMove;

        float clampedTarget = Mathf.Clamp01(rawTarget);
        float returnValue = clampedTarget;

        if (rawTarget != clampedTarget)
        {
            if (FishBase.bounceFishFromWalls)
            {
                returnValue = bounceBackFish(rawTarget);
            }
        }

        return returnValue;
    }

    private void MoveFish()
    {
        normalizedPos.x += AxisMove(FishTarget.x, normalizedPos.x);
        normalizedPos.y += AxisMove(FishTarget.y, normalizedPos.y);

        //float MoveTowards = FishTarget.x - normalizedPos.x;
        //float MoveNormalized = Mathf.Sign(MoveTowards);
        //float MoveBy = MoveNormalized * Time.deltaTime * FishBase.SwimSpeedTension * Mathf.Pow(.8f + Mathf.Abs(MoveTowards), 2);
        //MoveBy = Mathf.Clamp(MoveBy, -Mathf.Abs(MoveTowards), Mathf.Abs(MoveTowards));
        //normalizedPos.x += (fishNormalizedPosX - normalizedPos.x) * Time.deltaTime * Base.SwimSpeedVisual;
    }

    /// <summary>
    /// Moves the fish for a single axis.
    /// </summary>
    /// <param name="target"></param>
    /// <param name="currentPos"></param>
    /// <returns></returns>
    private float AxisMove(float target, float currentPos)
    {
        float MoveTowards = target - currentPos;
        float MoveNormalized = Mathf.Sign(MoveTowards);

        float MoveBy = MoveNormalized * Time.deltaTime * FishBase.SwimSpeedTension * Mathf.Pow(.8f + Mathf.Abs(MoveTowards), 2);
        MoveBy = Mathf.Clamp(MoveBy, -Mathf.Abs(MoveTowards), Mathf.Abs(MoveTowards));

        return MoveBy;
    }

    /// <summary>
    /// Bounces back fish if it goes over the normalized bounds. (0 or 1)
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    private float bounceBackFish(float target)
    {
        float overBounds = target;
        float clamped = Mathf.Clamp01(target);
        overBounds -= clamped;

        float distToBounced = Mathf.Abs((clamped - overBounds) - normalizedPos.x);
        float distToNormal = Mathf.Abs(clamped - normalizedPos.x);

        if (distToBounced > distToNormal)
        {
            clamped -= overBounds;
        }

        return clamped;
    }

    public void SetState(FishSwimState state)
    {
        State = state;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="strength"></param>
    public void Pull(float strength)
    {
        float pullValue = Time.deltaTime * strength;

        Progress -= pullValue;

        SetState(FishSwimState.BeingPulled);
    }

    public void Evade()
    {
        Progress += Time.deltaTime * FishBase.FishEscapeSpeed;

        SetState(FishSwimState.Evading);
    }
}
