﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Random = UnityEngine.Random;

public class FishAI : MonoBehaviour
{
    [Serializable]
    public class AIAction
    {
        public Func<bool> IsSuccess;
        public Func<bool> IsFail;
        public Action UpdateLoop;

        public event Action<bool> OnComplete;

        public event Action OnFail;

        public bool isSuccess = false;
        public bool isComplete = false;

        public string debug_desc;

        public AIAction(Func<bool> isSuccess, Func<bool> isFail, Action UpdateLoop)
        {
            this.UpdateLoop = UpdateLoop;
            isComplete = false;
            IsSuccess = isSuccess;
            IsFail = isFail;
        }

        public void UpdateBehaviour()
        {
            if (isComplete)
            {
                return;
            }

            UpdateLoop?.Invoke();

            if (IsFail())
            {
                OnComplete?.Invoke(false);
                isSuccess = false;
                isComplete = true;

            }
            if (IsSuccess())
            {
                OnComplete?.Invoke(true);
                isSuccess = true;
                isComplete = true;

            }

        }
    }

    //
    public FishAIParams AIParams;

    public Vector3 activeTarget;
    public float currentSpeed = 0f;
    public float maxSpeed = 2f;
    public float minSpeed = .1f;
    public float acceleration;

    public float targetSpeed;

    private List<Vector3> pathTargets = new List<Vector3>();
    public float turnAngleSpeed = .5f;

    public event Action<float> OnSpeedChanged;

    private float rayReactTime = .4f;
    private float nextUpdateTime = 0f;

    public float debugDistAngle, debugDist;

    public AIAction currentAction;

    void Awake()
    {
        currentAction = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentAction = null;

        SetSpeed(minSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBehaviour();

        FishMovement();
    }

    public void UpdateBehaviour()
    {
        if (currentAction == null)
        {
            currentAction = GetNewAction();
        }
        else
        {
            currentAction.UpdateBehaviour();
        }

        if (currentAction.isComplete)
        {
            currentAction = GetNewAction();
        }
    }

    private AIAction MoveToTarget(Vector3 target)
    {
        Func<bool> success = () => Vector3.Distance(transform.position, target) < 1f;
        Func<bool> fail = () => false; //cant really fail ritenow.

        Action UpdateLoop = () => SetTarget(target);

        AIAction moveToTarget = new AIAction(success, fail, UpdateLoop);

        moveToTarget.debug_desc = "move to: " + target;

        return moveToTarget;
    }

    private AIAction GetNewAction()
    {
        return MoveToTarget(Wander());
    }

    public Vector3 GetRandomTarget(float maxDist)
    {
        float x = Random.Range(-maxDist, maxDist);
        float y = 0f;
        float z = Random.Range(-maxDist, maxDist);

        return new Vector3(x, y, z);
    }

    public float getDepth()
    {
        return AIParams.preferredDepth;
    }

    public Vector3 Wander()
    {
        Vector3 target = GetRandomTarget(25f);
        target.y = 0f;

        float preferDepth = -Random.Range(getDepth() - 2f, getDepth() + 2f);
        Vector3 targetPos = transform.position + target;
        targetPos.y = preferDepth;

        return targetPos;
    }

    public void SetTarget(Vector3 pos)
    {
        activeTarget = pos;
    }

    private Vector3 GetForward(Vector3 pos)
    {
        Vector3 moveVector = pos - transform.position;
        Vector3 turnTo =  moveVector.normalized - transform.forward;
        return turnTo;
    }

    private void FishMovement()
    {
        TurnTowards(activeTarget - transform.position, out Quaternion rot, out Quaternion wantedAngle);

        float angleDist = Quaternion.Angle(transform.rotation, wantedAngle);

        transform.rotation = rot;

        float dist = Vector3.Distance(activeTarget, transform.position);

        debugDist = dist;
        debugDistAngle = angleDist;

        if (dist > (angleDist / (turnAngleSpeed / currentSpeed)))
        {
            targetSpeed = maxSpeed; 
        }
        else
        {
            targetSpeed = minSpeed;
        }

        targetSpeed = Mathf.Clamp(targetSpeed, minSpeed, maxSpeed);

        float speedDiff = targetSpeed - currentSpeed;

        if (speedDiff >= 0f)
        {
            SpeedUp();
        }

        if (speedDiff < 0f)
        {
            SlowDown();
        }

        Move(transform.forward * currentSpeed * Time.deltaTime);
    }

    private void Move(Vector3 delta)
    {
        RayCastForwards();

        transform.position += delta;
    }

    private void RayCastForwards()
    {
        Ray forwards = new Ray(transform.position, transform.forward);
        //Ray downWards = new Ray(transform.position, -transform.up);

        RaycastHit[] hits = Physics.RaycastAll(forwards, 5f);

        if (hits == null || hits.Length == 0)
        {
            //hits = Physics.RaycastAll(downWards, 10f);
        }

        float checkHeight = (transform.position + transform.forward * 5f).y;
        if (checkHeight > -2f)
        {
            RayHitBehaviour(transform.position + (Vector3.up *2f) + transform.forward);
            nextUpdateTime = Time.time + rayReactTime;
            return;
        }

        if (hits != null && hits.Length > 0)
        {
            if (nextUpdateTime > Time.time)
            {
                return;
            }

            foreach (RaycastHit k in hits)
            {
                RayHitBehaviour(k.point);
                nextUpdateTime = Time.time + rayReactTime;
                return;
            }
        }
    }

    private void SpeedUp()
    {
        SetSpeed(currentSpeed + (1f * acceleration * Time.deltaTime));
    }

    private void SlowDown()
    {
        SetSpeed(currentSpeed - (1f * acceleration * Time.deltaTime));
    }

    private void TurnTowards(Vector3 turnTo, out Quaternion rot, out Quaternion wantedAngle)
    {
        wantedAngle = Quaternion.LookRotation(turnTo, Vector3.up);
        float magnitude = turnAngleSpeed * Time.deltaTime;
        rot = Quaternion.RotateTowards(transform.rotation, wantedAngle, magnitude);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Vector3 hitpoint = collision.GetContact(0).point;

        RayHitBehaviour(hitpoint);
    }

    private void RayHitBehaviour(Vector3 hitPoint)
    {
        if (nextUpdateTime > Time.time)
        {
            return;
        }

        Vector3 moveAway = transform.position - hitPoint;

        SlowDown();

        currentAction = MoveAway(moveAway.normalized * 3f);
    }

    private AIAction MoveAway(Vector3 p)
    {
        Vector3 pos = transform.position + p;

        Func<bool> success = () =>
        {
            return Vector3.Distance(transform.position, pos) < .3f;
        };

        Func<bool> fail = () => false;

        Action Update = () => { SetTarget(pos); };

        AIAction action = new AIAction(success, fail, Update);
        action.debug_desc = "turn to: " + p;

        return action;
    }

    private void SetSpeed(float newSpeed)
    {
        newSpeed = Mathf.Clamp(newSpeed, minSpeed, maxSpeed);

        currentSpeed = newSpeed;

        OnSpeedChanged?.Invoke(newSpeed);
    }
}
