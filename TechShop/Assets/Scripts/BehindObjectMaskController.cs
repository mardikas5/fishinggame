﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehindObjectMaskController : MonoBehaviour
{
    public Material CutAwayMask;
    public Material RemoveShadowsMask;
    public MeshRenderer removeShadowsRenderer;
    public Camera castRayFrom;
    public Transform player;
    public Transform removeShadowsTransform;

    public int propertyId;

    public float EnabledAlpha;
    public float DisabledAlpha;
    public Vector3 EnabledSize;

    public bool isActive = false;
    public bool TweenState = false;
    
    public LayerMask rayCast;

    public float TweenLength;
    private float TweenValue = 0f;
    private TweenerCore<float, float, FloatOptions> tween;
    private TweenerCore<float, float, FloatOptions> vibrateEdgeTween;

    // Start is called before the first frame update
    private void Start()
    {
        propertyId = Shader.PropertyToID("_CutOff");

        RemoveMask();
    }

    private void UpdateMaskState()
    {
        float dist = Vector3.Distance(player.transform.position, castRayFrom.transform.position) - .4f;
        Ray r = new Ray(castRayFrom.transform.position, player.transform.position - castRayFrom.transform.position);
        RaycastHit[] hits = Physics.SphereCastAll(r, .3f, dist, rayCast.value);

        Debug.DrawRay(r.origin, r.direction, Color.red, 2f);

        foreach (RaycastHit t in hits)
        {
            if (t.transform != player.transform)
            {
                isActive = true;
                return;
            }
        }

        isActive = false;
    }

    // Update is called once per frame
    private void Update()
    {
        UpdateMaskState();
        UpdateMaskTween();
    }

    private void UpdateMaskTween()
    {
        if (TweenState != isActive)
        {
            TweenState = isActive;
            if (isActive)
            {
                ShowMask();
            }
            else
            {
                RemoveMask();
            }
        }
    }

    private void ShowMask()
    {
        tween?.Kill(false);
        float actual = TweenLength * (1 - TweenValue);
        tween = DOTween.To(() => TweenValue, SetNormalizedOpenValue, 1f, actual);
        vibrateEdgeTween = DOTween.To(() => transform.localScale.x, (x) => transform.localScale = Vector3.one * x , .5f + Mathf.Sin(Time.time) * .01f, .2f).SetLoops(-1, LoopType.Yoyo);
    }

    private void RemoveMask()
    {
        vibrateEdgeTween?.Kill(false);
        tween?.Kill(false);
        float actual = TweenLength * TweenValue;
        tween = DOTween.To(() => TweenValue, SetNormalizedOpenValue, 0f, actual);
    }

    private void SetNormalizedOpenValue(float x)
    {
        CutAwayMask.SetFloat(propertyId, Mathf.Lerp(DisabledAlpha, EnabledAlpha, x));
        removeShadowsTransform.transform.localScale = EnabledSize * x;
        TweenValue = x;
    }
}
