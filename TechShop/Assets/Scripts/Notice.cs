﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Notice
{
    public NoticeBoard Board;
    public NoticeReward Reward;

    public string Title;
    public string Description;
}
