﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBeeper : MonoBehaviour
{
    public float averageWordTimeMS = 450;

    public List<AudioClip> bepSounds = new List<AudioClip>();

    public float PitchMin, PitchMax;

    [Serializable]
    public class TimedSound
    {
        public AudioClip clip;
        public float delay;
        internal float pitchMin;
        internal float pitchMax;
    }

    public List<TimedSound> getSentenceSounds(string sentence)
    {
        float totalDelay = 0f;
        List<TimedSound> sounds = new List<TimedSound>();
        string[] words = sentence.Split(' ');

        for (int i = 0; i < words.Length; i++)
        {
            float wordTime = averageWordTimeMS;
            int wordLength = words[i].Length;
            wordTime *= (float)wordLength / 5f;

            sounds.Add(new TimedSound()
            {
                clip = bepSounds.Random(),
                delay = totalDelay,
                pitchMin = PitchMin,
                pitchMax = PitchMax
            });
            //convert to ms.
            totalDelay += (wordTime / 1000f);
        }

        return sounds;
    }

    // Start is called before the first frame update
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {

    }
}
