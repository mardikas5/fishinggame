﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioOnItemsExchanged : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        GameEvents.Instance.OnItemsExchanged += OnItemsExchanged;
    }

    private void OnItemsExchanged(List<Item> obj)
    {
        Debug.LogError("on items exchanged. " + obj?.Count);
        if (obj.Count > 0)
        {
            GetComponent<AudioClipContainer>()?.PlayRandom();
        }
    }
}
