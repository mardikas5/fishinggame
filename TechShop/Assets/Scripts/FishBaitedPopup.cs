﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static FishingBaitGame;

public class FishBaitedPopup : MonoBehaviour, ICallbackHandler<OnFishBaitedHandler>
{
    public Func<OnFishBaitedHandler> Handler => () => FishBaitedHandler;

    public GameObject UIPopUp;

    private void FishBaitedHandler(bool success, Action onComplete)
    {
        gameObject.SetActive(true);

        CanvasGroup cg = UIPopUp.GetComponent<CanvasGroup>();
        RectTransform rectT = transform.GetComponent<RectTransform>();

        rectT.anchoredPosition = new Vector2(0, Screen.height / 2f);
        rectT.DOAnchorPosY(0f, 1f);

        transform.localScale = Vector3.right;
        cg.alpha = 0f;
        Sequence seq = DOTween.Sequence();

        seq.Append(transform.DOScale(1f, .4f));

        seq.Append(cg.DOFade(.3f, .2f));
        seq.Append(cg.DOFade(.95f, .7f));
        seq.AppendInterval(1f);
        seq.Append(cg.DOFade(0f, .2f));
        seq.AppendCallback(() =>
        {
            gameObject.SetActive(false);
            onComplete?.Invoke();
        });
    }
}
