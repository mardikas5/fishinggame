﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Have to manually keep adding state enums, could be used for setting states -> dict, state in controller.
public enum StateEnum
{
    Run,
    Walk,
    Stand,
    Crouch,
    Dodge,
    Attack,
    Grounded,
    UseTool,
    InConversation,
    Unset,
    Resting,
    GenericAnim,
}

public abstract class HumanStateBehaviour : GenericState
{
    public abstract StateEnum State { get; }

    public HumanStateController Controller { get; private set; }

    /// <summary>
    /// Init is called by the controller at awake of said controller.
    /// </summary>
    /// <param name="controller"></param>
    public override void Init(GenericStateController controller)
    {
        base.Init(controller);
        Controller = controller as HumanStateController;
    }
}