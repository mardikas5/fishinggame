﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class HumanAttackState : HumanStateBehaviour
//{
//    public override StateEnum State => StateEnum.Attack;

//    public HumanGroundedState groundedState;

//    public bool IsAttacking = false;

//    public float TimeoutInterval = .3f;
//    public float TimeoutTimer = 0f;

//    //ignore
//    private bool nextAttackStacked = false;
//    private AttackAction.Params stackedParams;

//    public override void Init(HumanStateController humanStateController)
//    {
//        base.Init(humanStateController);

//        humanStateController.RegisterTrigger<AttackAction.Signature, AttackAction>(OnAttack);

//        humanStateController.RegisterTrigger<AttackAction.Signature, AttackAction>(OnAttackReleased);

//    }

//    public override void EnterState()
//    {
//        base.EnterState();

//        IsAttacking = false;
//    }

//    public override void UpdateBehaviour()
//    {
//        base.UpdateBehaviour();

//        if (IsAttacking || nextAttackStacked)
//        {
//            TimeoutTimer = TimeoutInterval;
//        }
//        else
//        {
//            TimeoutTimer -= Time.deltaTime;
//            if (TimeoutTimer < 0f)
//            {
//                TimeOut();
//            }
//        }
//    }

//    private void TimeOut()
//    {
//        Controller.SetState(groundedState);
//    }

//    public override void ExitState()
//    {
//        base.ExitState();
//        nextAttackStacked = false;
//        Controller.HumanController.animController.SetAnimTagFloat(CharAnimController.AnimTags.Attack, -1);
//    }

//    private bool OnAttack(AttackAction.Params param)
//    {
//        if (param.Release)
//        {
//            return true;
//        }

//        WeaponAttack atk = Controller.WeaponUser.AttackSignal(param.inputType, out bool Busy);
//        Debug.Log(" attack cmd, is busy: " + Busy);

//        if (Busy)
//        {
//            nextAttackStacked = true;
//            stackedParams = param;
//            return false;
//        }

//        if (atk == null)
//        {
//            return false;
//        }

//        Controller.HumanController.animController.SetAnimTagFloat(CharAnimController.AnimTags.ChargeAttack, 1f);

//        Controller.HumanController.animController.SetAnimTagFloat(CharAnimController.AnimTags.Attack, atk.AnimTypeParam);

//        IsAttacking = true;

//        return true;
//    }

//    private bool OnAttackReleased(AttackAction.Params param)
//    {
//        if (!param.Release)
//        {
//            return true;
//        }

//        nextAttackStacked = false;

//        Controller.HumanController.animController.SetAnimTagFloat(CharAnimController.AnimTags.ChargeAttack, 0f);

//        Controller.WeaponUser.AttackReleaseSignal(param.inputType, OnAttackComplete);

//        return true;
//    }

//    private void OnAttackComplete()
//    {
//        Controller.HumanController.animController.SetAnimTagFloat(CharAnimController.AnimTags.Attack, -1);

//        IsAttacking = false;

//        if (nextAttackStacked)
//        {
//            //next frame.
//            this.InvokeDelayed(0, () => OnAttack(stackedParams));
//        }
//    }
//}
