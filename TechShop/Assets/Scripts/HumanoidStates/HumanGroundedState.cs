﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//stand, walk, run for now.
//can go to > dodge
/// <summary>
/// Grounded state for a human, usually also the default state.
/// </summary>
public class HumanGroundedState : HumanStateBehaviour
{
    public override StateEnum State => StateEnum.Grounded;

    public float Acceleration;
    public float MaxSpeed;
    public float AccumulatedSpeed;

    //set from inspector.
    public HumanStateBehaviour dodgeState;
    public HumanStateBehaviour useToolState;

    //calc for animation movement speeds.
    private float[] lastMoveDistances = new float[7];
    private int nrOfFramesToCheckForMovement = 7;
    private int moveIndex = 0;
    private float currentSpeed = 0;

    [SerializeField]
    private bool IsDrunk; // Little out of place to swap movement anim.

    private float lastMoveTime;

    /// <summary>
    /// Has the character been moved this frame.
    /// </summary>
    private bool isMoving = false;

    private Tweener FaceCharacterTweener;

    private Vector3 lastTransformPosition = Vector3.zero;
    private float moveCheckTime = .1f;


    public override void EnterState()
    {
        base.EnterState();
    }

    public override void ExitState() {
        base.ExitState();

        SetCharacterForwardTween(null);

        Controller.HumanController.animController.SetAnimTagBool(CharAnimController.AnimTags.Walk, false);
    }

    protected override void Update()
    {
        base.Update();

        // this seems way out of place? -> just do a drunk state? -> 
        Controller.HumanController.animator.SetBool("IsDrunk", IsDrunk);
    }


    public override void Init(GenericStateController humanStateController)
    {
        base.Init(humanStateController);

        RegisterTrigger<GenericAnimActionSignal.Signature, GenericAnimActionSignal>(EnterAnimation);

        RegisterTrigger<DodgeAction.Signature, DodgeAction>(TryDodge);

        RegisterTrigger<MoveAction.Signature, MoveAction>(MoveCharacter);

        RegisterTrigger<AttackAction.Signature, AttackAction>(Attack);

        RegisterTrigger<FaceDirection.Signature, FaceDirection>(FaceDirection);
    }


    private bool EnterAnimation(GenericAnimActionSignal.AnimParams param)
    {
        if (!param.enabled)
        {
            return false;
        }

        HumanGenericAnimState animState = Controller.HumanController.GenericAnimState as HumanGenericAnimState;

        animState.animationString = param.name;
        animState.enabledValue = true;

        SwitchState(animState, true);

        return true;
    }


    private bool Attack(AttackAction.Params param)
    {
        //singleton reference.
        if (UI.Instance.MouseOnUI)
        {
            return false;
        }

        SwitchState(useToolState, true);

        return true;
    }


    private bool TryDodge(Vector3 param)
    {
        SwitchState(dodgeState);

        return true;
    }


    public override void UpdateBehaviour()
    {
        base.UpdateBehaviour();

        UpdateMovement();
    }


    private void UpdateMovement()
    {
        AccumulatedSpeed *= .98f; // slow down a bit each frame.
        AccumulatedSpeed -= Time.deltaTime * 3f;

        //if not moving the character, slow down faster.
        if (!isMoving)
        {
            AccumulatedSpeed *= .6f;
        }
        
        AccumulatedSpeed = Mathf.Max(AccumulatedSpeed, 0f);

        //reset character movement this frame.
        isMoving = false;

        UpdateMoveAnim();
    }


    private void UpdateMoveAnim() {
        //negated comes from a platform moving under the character or a boat etc.
        Vector3 negated = Controller.HumanController.negatedMovement;
        Vector3 currentPosition = transform.position - negated;
        Debug.DrawLine(currentPosition, lastTransformPosition, Color.red, 1f);

        float dist = Vector3.Distance(lastTransformPosition.x0z(), currentPosition.x0z());

        lastTransformPosition = transform.position;

        float distMoveInFrame = dist / Time.deltaTime;

        if (distMoveInFrame > 1000f) {
            return;
        }

        lastMoveDistances[moveIndex] = distMoveInFrame;
        moveIndex++;

        if (moveIndex >= nrOfFramesToCheckForMovement) {
            moveIndex = 0;
        }

        float totalMoved = 0f;

        for (int i = 0; i < lastMoveDistances.Length; i++) {
            totalMoved += lastMoveDistances[i];
        }

        if (totalMoved != 0f) {
            currentSpeed = totalMoved / nrOfFramesToCheckForMovement;

            //hard typed anim string in code.
            Controller.HumanController.animator.SetFloat("WalkSpeed", currentSpeed);
            Controller.HumanController.animController.SetAnimTagBool(CharAnimController.AnimTags.Walk, true);
            return;
        }

        if ((Time.time - (lastMoveTime + moveCheckTime)) > 0f) {
            Controller.HumanController.animController.SetAnimTagBool(CharAnimController.AnimTags.Walk, false);
        }
    }


    private bool MoveCharacter(Vector3 dir)
    {
        isMoving = true;

        if (dir.sqrMagnitude <= .0001f)
        {
            return true;
        }

        lastMoveTime = Time.time;

        AccumulatedSpeed += Acceleration * Time.deltaTime;
        AccumulatedSpeed = Mathf.Min(MaxSpeed, AccumulatedSpeed);

        Controller.AbsMove(dir * Time.deltaTime * AccumulatedSpeed);

        return true;
    }


    private void SetCharacterForwardTween(Tweener tweener)
    {
        //this could be generic for a tween / tweener.
        if (FaceCharacterTweener != null)
        {
            FaceCharacterTweener.Kill();
            FaceCharacterTweener = null; //?
        }

        FaceCharacterTweener = tweener;
    }


    private bool FaceDirection(Vector3 dir)
    {
        Tweener lookAt;
        //if the character has not moved in a while
        if (Time.time - lastMoveTime > .1f)
        {
            lookAt = Controller.HumanController.visuals.transform.DOLookAt(transform.position + dir, .2f, AxisConstraint.None);
            SetCharacterForwardTween(lookAt);
            return true;
        }

        if (Controller.lastMoveDir == Vector3.zero)
        {
            return true;
        }

        Quaternion q = Quaternion.LookRotation(Controller.lastMoveDir, Vector3.up);
        Vector3 v3Euler = q.eulerAngles;

        lookAt = Controller.HumanController.visuals.transform.DORotate(v3Euler, .1f);
        SetCharacterForwardTween(lookAt);
        return true;
    }
}
