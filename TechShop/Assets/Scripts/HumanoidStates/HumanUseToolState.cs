﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanUseToolState : HumanStateBehaviour {
    public override StateEnum State => StateEnum.UseTool;

    //set in the inspector.
    public HumanGroundedState groundedState;

    [SerializeField] //for inspector debug
    private bool IsToolBusy = false;
    [SerializeField] //for inspector debug
    private bool Interrupted = false;

    private ToolInteraction activeInteraction;

    public override void Init(GenericStateController humanStateController) {
        base.Init(humanStateController);

        EntryConditions.Add(() => Controller.ToolUser.activeTool != null);

        RegisterTrigger<AttackAction.Signature, AttackAction>(OnUseTool);
    }

    public override void EnterState() {
        base.EnterState();

        Interrupted = false;

        Debug.LogError("entered tool use state");

        IsToolBusy = false;

        if (Controller.ToolUser.activeTool == null) {
            SwitchState(groundedState);
        }

        this.InvokeDelayed(.1f, () => { if (!IsToolBusy) { InterruptUse(); } });
    }


    public override void UpdateBehaviour() {
        base.UpdateBehaviour();

        //if (IsToolBusy)
        //{
        if (Input.GetKeyDown(KeyCode.Escape)) {
            InterruptUse();
        }
        //}
    }

    private void InterruptUse() {
        if (Interrupted) {
            return;
        }

        Interrupted = true;

        if (activeInteraction == null) {
            OnUseToolComplete();
            return;
        }

        activeInteraction.Stop();
        activeInteraction.Interrupt(() => OnUseToolComplete());
    }

    public override void ExitState() {
        base.ExitState();
        Controller.HumanController.animController.SetAnimTagFloat(CharAnimController.AnimTags.Attack, -1);
    }

    private bool OnUseTool(AttackAction.Params param) {
        if (IsToolBusy) {
            return false;
        }

        IsToolBusy = true;

        if (!TryUseTool(param)) {
            InterruptUse();
            return false;
        }

        Controller.ToolUser.activeTool.OnToolEquipped.AttachOneShot(OnToolEquipped);

        return true;
    }

    private void OnToolEquipped() {
        //hmm what
        if (Controller.ToolUser.activeTool.User != Controller.ToolUser) {
            InterruptUse();
            return;
        }

        Controller.ToolUser.activeTool.OnToolEquipped.AttachOneShot(OnToolEquipped);
    }

    private bool TryUseTool(AttackAction.Params param) {
        bool canUseTool = Controller.ToolUser.UseTool(param.inputType, out ToolInteraction interaction);

        if (!canUseTool) {
            Debug.LogError("cannot use tool");
            return false;
        }

        if (interaction == null) {
            Debug.LogError(" tool use returned a null action. ");
            return false;
        }

        activeInteraction = interaction;

        interaction.Execute(OnUseToolComplete);

        return true;
    }

    private void OnUseToolComplete() {
        Debug.Log("done using tool.");

        Controller.HumanController.animController.SetAnimTagFloat(CharAnimController.AnimTags.Attack, -1);

        //this.InvokeDelayed(.01f, () =>
        //{
        IsToolBusy = false;
        //});

        Controller.SetState(groundedState);
    }
}
