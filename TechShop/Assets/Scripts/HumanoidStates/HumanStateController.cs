﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static HumanStateBehaviour;


/// <summary>
/// Statemachine specifically for human types.
/// </summary>
public class HumanStateController : GenericStateController
{
    // only takes humanstatebehaviours.
    public new HumanStateBehaviour ActiveState => _activeState as HumanStateBehaviour;

    public ToolUser ToolUser; //slightly out of place

    public HumanController HumanController;

    public CharacterController CharacterController;

    public Vector3 lastMoveDir;
    public Vector3 lastMoveForward;

    public void Dodge()
    {
        SignalAction<DodgeAction>(lastMoveDir);
    }

    public void Move(Vector3 dir)
    {
        SignalAction<MoveAction>(dir);
    }

    public void AbsMove(Vector3 vector3)
    {
        CharacterController.Move(vector3);

        lastMoveDir = vector3;
    }

    public void Attack(InputEnum attackInput)
    {
         SignalAction<AttackAction>(new AttackAction.Params(attackInput, false));
    }

    public void AttackRelease(InputEnum attackInput)
    {
        SignalAction<AttackAction>(new AttackAction.Params(attackInput, true));
    }

    public void FaceDirection(Vector3 forward)
    {
        SignalAction<FaceDirection>(forward);

        lastMoveForward = forward;
    }
}
