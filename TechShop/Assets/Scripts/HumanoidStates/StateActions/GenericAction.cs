﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GenericAction : StateAction<bool, object>
{
    public GenericAction(bool returnValue, object param) : base(returnValue, param)
    {
    }
}

public class GenericAnimActionSignal : StateAction<bool, GenericAnimActionSignal.AnimParams>
{
    [Serializable]
    public class AnimParams
    {
        public string name;
        public bool enabled;

        public AnimParams(string name, bool enabled)
        {
            this.name = name;
            this.enabled = enabled;
        }
    }

    public GenericAnimActionSignal(bool returnValue, AnimParams animParams) : base(returnValue, animParams) { }
}

public class TargetPosAction : StateAction<bool, Transform>
{
    public TargetPosAction(bool returnValue, Transform param) : base(returnValue, param)
    {
    }
}
