﻿using System;

/// <summary>
/// Generic action with T return value and T1 type params.
/// </summary>
/// <typeparam name="T">Return type</typeparam>
/// <typeparam name="T1">Parameter type</typeparam>
public class StateAction<T,T1>
{
    public delegate T Signature(T1 param);
    
    public T returnValue;
    public T1 param;

    public StateAction(T returnValue, T1 param)
    {
        this.returnValue = returnValue;
        this.param = param;
    }
}

