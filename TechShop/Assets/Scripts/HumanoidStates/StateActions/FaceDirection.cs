﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceDirection : StateAction<bool, Vector3>
{
    public FaceDirection(bool returnValue, Vector3 param) : base(returnValue, param)
    {
    }
}
