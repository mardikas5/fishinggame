﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AttackAction : StateAction<bool, AttackAction.Params>
{
    public class Params
    {
        public InputEnum inputType;
        public bool Release;

        public Params(InputEnum inputType, bool release)
        {
            this.inputType = inputType;
            Release = release;
        }
    }

    public AttackAction(bool returnValue, Params param) : base(returnValue, param)
    {

    }
}
