﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DodgeAction : StateAction<bool, Vector3>
{
    public DodgeAction(bool returnValue, Vector3 param) : base(returnValue, param)
    {
    }
}
