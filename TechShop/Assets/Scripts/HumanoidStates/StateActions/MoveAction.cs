﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAction : StateAction<bool, Vector3>
{
    public MoveAction(bool returnValue, Vector3 param) : base(returnValue, param)
    {
    }
}
