﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanGenericAnimState : HumanStateBehaviour
{
    public override StateEnum State => StateEnum.GenericAnim;

    public HumanStateBehaviour groundedState;

    public Vector3 targetPosition;

    public string animationString;

    public float animExitDelay = 3f;
    public float smoothMoveTime = .5f;

    /// <summary>
    /// Value of the animation bool when it is enabled. In some cases this may be inversed.
    /// </summary>
    public bool enabledValue;


    public override void Init(GenericStateController controller)
    {
        base.Init(controller);

        RegisterTrigger<GenericAnimActionSignal.Signature, GenericAnimActionSignal>(UpdateAnimation);
        RegisterTrigger<TargetPosAction.Signature, TargetPosAction>(moveCharToPos);
    }

    private bool moveCharToPos(Transform param)
    {
        Transform moveT = Controller.CharacterController.transform;

        moveT.DOMove(param.transform.position, smoothMoveTime);

        moveT.DOLookAt(param.transform.position + param.transform.forward, smoothMoveTime);

        Controller.HumanController.visuals.DORotate(Vector3.zero, smoothMoveTime);

        return true;
    }

    public override void EnterState()
    {
        base.EnterState();

        Debug.Log("enter anim: " + Controller.transform.parent.name + ", " + animationString);

        Controller.HumanController.charController.enabled = false;

        Controller.HumanController.animator.SetBool(animationString, enabledValue);
    }

    public override void ExitState()
    {
        base.ExitState();

        Controller.HumanController.charController.enabled = true;

        Controller.HumanController.animator.SetBool(animationString, !enabledValue);
    }

    private bool UpdateAnimation(GenericAnimActionSignal.AnimParams param)
    {
        Debug.Log("anim signal: " + param.name + ", " + param.enabled);

        Controller.HumanController.animator.SetBool(param.name, param.enabled);

        if (param.name == animationString && !param.enabled)
        {
            this.InvokeDelayed(animExitDelay, () =>
            {
                Controller.SetState(groundedState);
            });
        }

        return true;
    }
}
