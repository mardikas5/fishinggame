﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanDodgeState : HumanStateBehaviour
{
    public override StateEnum State => StateEnum.Dodge;

    public HumanStateBehaviour defaultReturnState;

    //open for the inspector.
    public float dodgeSpeed;
    public float dodgeTime;

    public event Action OnDodgeComplete;

    //anim string reference..
    public string DodgeBool = "Dodge";

    //todo use speedcurve for speed.
    public AnimationCurve speedCurve;

    protected bool IsDodging = false;

    public override void EnterState()
    {
        base.EnterState();

        Dodge(OnDodgeCompleteHandler);
    }

    private void OnDodgeCompleteHandler()
    {
        Controller.HumanController.animController.SetAnimTagBool(CharAnimController.AnimTags.Dodge, false);

        Controller.SetState(defaultReturnState);
    }

    public override void ExitState()
    {
        base.ExitState();

        Controller.HumanController.animController.SetAnimTagBool(CharAnimController.AnimTags.Dodge, false);
    }

    public override void UpdateBehaviour()
    {
        Controller.HumanController.visuals.transform.forward = Controller.lastMoveDir;

        Controller.AbsMove(Controller.lastMoveDir.normalized * Time.deltaTime * dodgeSpeed); //TODO: speedcurve evaluate:
    }

    public bool Dodge(Action onCompleted = null)
    {
        if (!Active)
        {   //this is probably generic as well -> cannot enter a state or get signal without state being active.
            Debug.Log("State not active, cant dodge.");
            return false;
        }

        if (IsDodging)
        {
            return false;
        }

        Controller.HumanController.animController.SetAnimTagBool(CharAnimController.AnimTags.Dodge, true);

        IsDodging = true;

        this.InvokeDelayed(dodgeTime, () => dodgeActionComplete(onCompleted));

        return true;
    }

    private void dodgeActionComplete(Action onCompleted)
    {
        //rotate.position -= new Vector3(0, .5f, 0f);

        IsDodging = false;
        onCompleted?.Invoke();
    }
}
