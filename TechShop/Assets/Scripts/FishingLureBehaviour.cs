﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingLureBehaviour : MonoBehaviour
{
    public FishingRod Controller;
    public float freezeUntil;
    public float freezeTime = .3f;
    public MartRope rope;
    public SimplePointBoat floater;

    // Start is called before the first frame update
    private void Awake()
    {
        floater = GetComponent<SimplePointBoat>();
        Controller.OnToolEquipped += FreezeToOrigin;
    }

    private void FreezeToOrigin()
    {
        freezeUntil = Time.time + freezeTime;
        SetPosToOrigin();
    }

    private void Update()
    {
        if (Time.time < freezeUntil)
        {
            SetPosToOrigin();
        }
    }

    private void FixedUpdate()
    {
        UnderWaterBehaviour();
    }

    private void UnderWaterBehaviour()
    {
        if (rope == null)
        {
            return;
        }
        if (floater.underWater)
        {
            foreach (GameObject t in rope.getJoints())
            {
                if (t.transform.position.y > transform.position.y)
                {
                    continue;
                }
            
                t.transform.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.up * floater.forceMp * Time.fixedDeltaTime, t.transform.position, ForceMode.Acceleration);
            }
        }
    }

    private void SetPosToOrigin()
    {
        transform.position = Controller.transform.position;
    }
}
