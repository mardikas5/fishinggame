﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GS_MoveToShip : MonoBehaviour
{
    public Transform EndPos;

    public Transform ShorePos;

    public PlayerController player;

    //fade to black?

    public void Start()
    {
        player = GameLogic.Instance.Player;    
    }

    public void TeleportToEndPos()
    {
        UI.Instance.FadeInPanel();
        this.InvokeDelayed(.5f, () =>
        {
            player.Teleport(EndPos.transform.position);
            UI.Instance.FadeOutPanel();
        });
    }

    public void TeleportToShore()
    {
        UI.Instance.FadeInPanel();
        this.InvokeDelayed(.5f, () =>
        {
            player.Teleport(ShorePos.transform.position);
            UI.Instance.FadeOutPanel();
        });
    }
}
