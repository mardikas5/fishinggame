﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GS_DeliverSchedule : MonoBehaviour
{
    //arrival date
    public DateTime arrivalDate;

    //next leave time
    public DateTime leavingDate;

    public string NoticeTitle;

    [TextArea]
    public string NoticeDescription;

    public float hoursBetweenDeliveries;
    public float hoursIdling;

    public UIWorldToolTip deliveryNoticeTooltip;

    public Inventory ItemSalesBox;
    public GameObject ItemSalesBoxVisuals;

    public HumanController SupplierCharacter;

    public TimeController Time;

    public SimplePointBoat boat;
    public BoatAI boatAI;

    public bool IsInPort = false;
    public bool IsMoving = false;

    public List<Transform> boatWaypoints = new List<Transform>();

    public List<Transform> boatWayPointsOut = new List<Transform>();

    public UnityEvent onArrive, onLeave;

    public TradeController tradeController;

    public string charPlayAnimString;

    public void Start()
    {
        IsInPort = false;
        Leave(false);
        Time = GameLogic.Instance.Time;
        arrivalDate = Time.GameTime.AddHours(8f);
        deliveryNoticeTooltip.textObj.text = GetNotice(arrivalDate);
    }

    public void Update()
    {
        CheckArrival();
    }

    public void CheckArrival()
    {
        if (!IsInPort)
        {
            if (Time.GameTime > arrivalDate.AddHours(-3f))
            {
                if (!IsMoving)
                {
                    StartArrivalSequence();
                    IsMoving = true;
                }
            }

            if (Time.GameTime > arrivalDate)
            {
                Debug.LogError("arrive");
                IsMoving = false;
                Arrive();
            }
        }
        else
        {
            if (Time.GameTime > leavingDate)
            {
                Debug.LogError("leave");
                Leave();
            }
        }
    }

    private void StartArrivalSequence()
    {
        GenerateMoveSequence(true);
    }

    public string GetNotice(DateTime nextDelivery)
    {
        string delivery = "~ Delivery notice ~ \n";
        if (IsInPort)
        {
            delivery += " This delivery be taken at: " + leavingDate.ToString("h tt dd. MMMM.");
        }
        delivery += " The next supply run for Greyrock lighthouse is scheduled " + arrivalDate.Day + ". of " + arrivalDate.ToString("MMMM ");
        delivery += arrivalDate.ToString("h tt") + ".";
        return delivery;
    }

    public void Arrive()
    {
        IsInPort = true;

        leavingDate = Time.GameTime.AddHours(hoursIdling);

        Vector3 endPoint = boatWaypoints[boatWaypoints.Count - 1].position;

        if (Vector3.Distance(endPoint, boat.transform.position) > 5f)
        {
            boat.transform.position = endPoint;
        }

        tradeController.Sell();

        SupplierCharacter.GetComponent<Interactable>().isEnabled = true;

        PlaySupplierCharAnims();

        arrivalDate = arrivalDate.AddHours(hoursBetweenDeliveries);

        deliveryNoticeTooltip.textObj.text = GetNotice(arrivalDate);

        onArrive?.Invoke();
    }


    public void Leave(bool useMovement = true)
    {
        IsInPort = false;

        SupplierCharacter.GetComponent<Interactable>().isEnabled = false;

        if (useMovement)
        {
            PlaySupplierCharAnims();
            GenerateMoveSequence(false);
            onLeave?.Invoke();
        }

        deliveryNoticeTooltip.textObj.text = GetNotice(arrivalDate);
    }

    public void PlaySupplierCharAnims()
    {
        SupplierCharacter.StateController.SignalAction<GenericAnimActionSignal>(new GenericAnimActionSignal.AnimParams(charPlayAnimString, true));

        this.InvokeDelayed(3.5f, () => SupplierCharacter.StateController.SignalAction<GenericAnimActionSignal>(new GenericAnimActionSignal.AnimParams(charPlayAnimString, false)));
    }

    public void GenerateMoveSequence(bool moveIn)
    {
        if (moveIn)
        {
            boatAI.TravelWaypoints(boatWaypoints);
        }
        else
        {
            boat.transform.DOMove(boatWayPointsOut[1].position, 5f);
            boat.transform.DORotate(boatWayPointsOut[1].eulerAngles, 5f);

            boatAI.TravelWaypoints(boatWayPointsOut);
        }
    }
}
