﻿using System.Collections;
using System.Collections.Generic;
using PixelCrushers.DialogueSystem;
using UnityEngine;

public class GS_PlayerInitialWakeUp : MonoBehaviour
{
    public Transform PlayerMarker;
    public Transform LighthouseManMarker;

    public GameObject PlayerObject;
    public GameObject LighthouseManObject;

    private CameraVignetteSequence vignetteFX;

    public DialogueSystemTrigger dialogueTrigger;

    public void Start()
    {
        
    }

    public void SignalStart()
    {
        LighthouseManObject.GetComponent<SimpleAI>().enabled = false;

        vignetteFX = GameLogic.Instance.CameraController.GetComponent<CameraVignetteSequence>();

        vignetteFX?.FadeIn(15f);
    }

    public void Play()
    {
        gameObject.SetActive(true);

        this.InvokeDelayed(.5f, StartPlay);
    }

    private void StartPlay()
    {
        PlayerObject.GetComponent<PlayerController>().Teleport(PlayerMarker.transform.position);
        LighthouseManObject.transform.position = LighthouseManMarker.transform.position;

        this.InvokeDelayed(2f, () => vignetteFX.FadeOut(25f));

        dialogueTrigger.TryStart(PlayerObject.transform);
    }

    private void OnEndConversation(Transform t)
    {
        LighthouseManObject.GetComponent<SimpleAI>().enabled = true;

        vignetteFX.FadeOut();
    }
}
