﻿using Fishy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class debug_UISpawnItem : MonoBehaviour
{
    public RectTransform contentPanel;
    public debug_UISpawnItemButton objectPrefab;
    public GameObject window;

    private bool isOpen = false;

    public void Open()
    {
        if (isOpen)
        {
            return;
        }
        isOpen = true;
        window.SetActive(true);
    }

    public void Close()
    {
        isOpen = false;
        window.SetActive(false);
    }

    // Start is called before the first frame update
    private void Start()
    {
        SpawnItemsList();
    }

    private void SpawnItem(EntityBase p)
    {
        EntityInstance inst = EntityFactory.Create(p);
        Item k = ItemFactory.Create(inst);
        if (!GameLogic.Instance.Player.InventoryController.Inventory.TryPlaceAnySlot(k))
        {
            EntityFactory.CreateInWorldStatic(k, GameLogic.Instance.Player.transform.position + Vector3.up);
        }
    }

    private void SpawnItemsList()
    {
        foreach (EntityBase k in Resources.FindObjectsOfTypeAll<EntityBase>())
        {
            debug_UISpawnItemButton obj = Instantiate(objectPrefab, contentPanel);
            obj.Init(k);
            obj.button.onClick.AddListener(() => SpawnItem(k));
        }

        foreach (EntityBase k in FindObjectsOfType<EntityBase>())
        {
            debug_UISpawnItemButton obj = Instantiate(objectPrefab, contentPanel);
            obj.Init(k);
            obj.button.onClick.AddListener(() => SpawnItem(k));
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Close();
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.K))
        {
            Open();
        }
    }
}
