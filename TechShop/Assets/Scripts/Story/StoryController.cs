﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class StoryController : MonoBehaviour
{
    public List<GameObject> orderedSlides = new List<GameObject>();

    private GameObject currentSlide;

    private int index;

    public float sleepTime = .5f;

    private float sleepTimer;

    public TweenFadeHandler fadeHandler;

    public UnityEvent OnStoryEnd;

    public bool IsOpen = false;

    private void Awake()
    {
        fadeHandler.Init(GetComponent<CanvasGroup>(), gameObject, .8f, .5f);
        NextSlide();
    }

    public void StartStoryTelling()
    {
        gameObject.SetActive(true);
        IsOpen = true;
        fadeHandler.Open();
    }

    // Update is called once per frame
    void Update()
    {
        if (sleepTimer > Time.time)
        {
            return;
        }

        if (!IsOpen)
        {
            return;
        }

        if (index == orderedSlides.Count)
        {
            if (Input.GetMouseButtonDown(0))
            {
                EndStory();
            }
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            sleepTimer = Time.time + sleepTime;
            NextSlide();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            index = orderedSlides.Count - 1;
            NextSlide();
        }
    }

    private void EndStory()
    {
        fadeHandler.Close().OnInvoke((x) => gameObject.SetActive(false));

        IsOpen = false;

        OnStoryEnd?.Invoke();
    }

    private void NextSlide()
    {
        if (currentSlide != null)
        {
            GameObject oldSlide = currentSlide;

            CanvasGroup oldGroup = currentSlide.GetComponent<CanvasGroup>();

            DOTween.To(() => oldGroup.alpha, (x) => oldGroup.alpha = x, 0f, .5f).OnComplete(() => oldSlide.SetActive(false));
        }

        orderedSlides[index].SetActive(true);

        currentSlide = orderedSlides[index];

        CanvasGroup currentGroup = currentSlide.GetComponent<CanvasGroup>();

        currentGroup.alpha = 0f;

        DOTween.To(() => currentGroup.alpha, (x) => currentGroup.alpha = x, 1f, .5f);

        index++;
    }
}
