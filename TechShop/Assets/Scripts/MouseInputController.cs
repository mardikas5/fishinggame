﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Handles mouse input, allows clicks from mouse users.
/// </summary>
public class MouseInputController : Singleton<MouseInputController>
{
    [Serializable]
    public class InputRequestUser
    {
        public bool interactable;
        public int Priority;
        public object obj;

        public InputRequestUser(int priority, object obj)
        {
            Priority = priority;
            this.obj = obj;
        }
    }

    //could just order this list...
    public List<InputRequestUser> ActiveInputRequest = new List<InputRequestUser>();

    public bool HasInputTarget => ActiveInputRequest.Count > 0;

    public bool InputActive = true;

    public void AddPossible(InputRequestUser obj)
    {
        if (!InputActive)
        {
            return;
        }

        ActiveInputRequest.Add(obj);
    }

    public void RemovePossible(InputRequestUser inReq)
    {
        var existing = ActiveInputRequest.FindAll((x) => x.obj == inReq.obj);
        existing.ForEach((x) => ActiveInputRequest.Remove(x));
    }

    public void EnableInput() {
        InputActive = true;
    }

    public void DisableInput() {
        InputActive = false;
    }


    public bool AllowedToUseClick(InputRequestUser inReq)
    {
        if (!InputActive)
        {
            return false;
        }

        bool allow = ActiveInputRequest.FirstOrDefault((x) => (x.Priority >= inReq.Priority && x.obj != inReq.obj)) == null;

        return allow;
    }
}

