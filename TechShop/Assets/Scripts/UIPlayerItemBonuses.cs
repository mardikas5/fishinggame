﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerItemBonuses : MonoBehaviour
{
    public UIItemStatBonus itemStatBonusPrefab;
    public RectTransform itemStatParent;

    public List<UIItemStatBonus> ActiveItems = new List<UIItemStatBonus>();

    public PlayerStats PlayerStats;



    // Start is called before the first frame update
    private void Start()
    {
        PlayerStats = GameLogic.Instance.Player.Stats;

        Generate(GameLogic.Instance.Player.EquipmentController);
    }

    public void Generate(PlayerEquipmentController equipment)
    {
        foreach (PlayerEquipmentController.PlayerEquipmentSlot t in equipment.EquipmentSlots)
        {
            t.Inventory.ItemRemoved += ItemRemoved;
            t.Inventory.ItemAdded += ItemAdded;

            if (t.Inventory.ItemsList.Count > 0)
            {
                GenerateModifiers(t.Inventory.ItemsList[0]);
            }
        }
    }


    private void Clear()
    {
        foreach (UIItemStatBonus t in ActiveItems)
        {
            Destroy(t.gameObject);
        }

        ActiveItems.Clear();
    }

    private void ItemAdded(Item slotItem)
    {
        slotItem.ItemChangedSignal += OnItemChanged;

        Clear();

        Generate(GameLogic.Instance.Player.EquipmentController);
    }

    private void OnItemChanged(Item slotItem)
    {
        ItemRemoved(slotItem);

        ItemAdded(slotItem);
    }

    private void ItemRemoved(Item slotItem)
    {
        slotItem.ItemChangedSignal -= OnItemChanged;

        Clear();

        Generate(GameLogic.Instance.Player.EquipmentController);
    }

    private void GenerateModifiers(Item slotItem)
    {
        List<ItemBase> allMods = new List<ItemBase>();
        ItemComponentSlots.GetNestedItems(slotItem, ref allMods);

        foreach (var t in allMods)
        {
            CreateModifierItem(slotItem.ItemBase, t);
        }
    }

    

    private void CreateModifierItem(ItemBase slotItem, ItemBase mods)
    {
        if (mods != null)
        {
            for (int p = 0; p < mods.Modifiers.Count; p++)
            {
                UIItemStatBonus itemStatBonus = Instantiate(itemStatBonusPrefab, itemStatParent);
                ActiveItems.Add(itemStatBonus);
                itemStatBonus.Init(slotItem.Name, mods.Modifiers[p]);
            }
        }
    }


}
