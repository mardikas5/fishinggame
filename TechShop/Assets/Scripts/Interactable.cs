﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

//"Fishing game interactable"
public class Interactable : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public bool isEnabled = true;

    public int MousePriority;

    public float InteractionDistance = 3f;

    public GameObject interactionObject;


    public UnityEvent OnClick, OnHoverEnter, OnHoverExit;

    private MouseInputController InputController;
    private GameLogic Logic;


    void Init() {
        InputController = MouseInputController.Instance;
        Logic = GameLogic.Instance;
    }

    public void Awake()
    {
        if (interactionObject == null)
        {
            interactionObject = gameObject;
        }

        this.InvokeDelayed(.1f, Init);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isEnabled)
        {
            return;
        }
        if (InteractionDistance >= 0f)
        {
            if (!IsInteractable())
            {
                return;
            }
        }
        if (!InputController.AllowedToUseClick(GetInput()))
        {
            return;
        }

        OnClick?.Invoke();
    }

    /// <summary>
    /// Handler to check if this object can be interacted with. -> default case the player has to be close to the object.
    /// </summary>
    /// <returns></returns>
    private bool IsInteractable()
    {
        if (!isEnabled) {
            return false;
        }
        if (Logic.Player == null)
        {
            return false;
        }
        
        float dist = Vector3.Distance(transform.position, Logic.Player.transform.position);
        return dist < InteractionDistance;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (InputController.ActiveInputRequest.Count > 0)
        { return; }
        if (!InputController.InputActive)
        { return;}

        OnHoverEnter?.Invoke();
        InputController.AddPossible(GetInput());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnHoverExit?.Invoke();
        InputController.RemovePossible(GetInput());
    }

    public void OnDestroy()
    {
        InputController.RemovePossible(GetInput());
    }

    /// <summary>
    /// Generates an inputrequest from the set mousepriority and gameobject as target.
    /// </summary>
    /// <returns></returns>
    private MouseInputController.InputRequestUser GetInput()
    {
        GameObject target = interactionObject;

        MouseInputController.InputRequestUser inputRequest = new MouseInputController.InputRequestUser(MousePriority, target)
        {
            interactable = IsInteractable()
        };

        return inputRequest;
    }

    private void OnDisable()
    {
        OnPointerExit(null);
    }
}
