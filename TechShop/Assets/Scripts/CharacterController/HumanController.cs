﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Generic controller for a human.
/// </summary>
public class HumanController : MonoBehaviour
{
    /// <summary>
    /// State controller.
    /// </summary>
    [SerializeField]
    public HumanStateController StateController;

    /// <summary>
    /// Transform that the character visuals are attached to.
    /// </summary>
    public Transform visuals;
    public Animator animator;
    public CharAnimController animController;
    public CharacterController charController;

    /// <summary>
    /// The transform which represents the face of the human. 
    /// </summary>
    public Transform Face;

    /// <summary>
    /// Generic animation state, can be used to play any animation based on string
    /// </summary>
    public HumanStateBehaviour GenericAnimState;

    /// <summary>
    /// Movement that occured as as a result of a platform or ship moving under.
    /// </summary>
    public Vector3 negatedMovement = Vector3.zero;

    /// <summary>
    /// Negated movement from previous frame
    /// </summary>
    public Vector3 lastFrameNegatedMovement = Vector3.zero;

    public void AddNegatedVelocity(Vector3 velo)
    {
        negatedMovement += velo;
    }

    public void LateUpdate()
    {
        lastFrameNegatedMovement = negatedMovement;
        negatedMovement = Vector3.zero;
    }


    public void FixedUpdate()
    {
        if (!charController.enabled)
        {
            return;
        }
        if (!charController.isGrounded)
        {
            charController.Move(Physics.gravity * Time.fixedDeltaTime);
        }
    }


    public void Awake()
    {
        if (charController == null)
        {
            charController = GetComponent<CharacterController>();
        }
    }

    public void LookAt(Transform t)
    {
        visuals.LookAt(t);
    }

    public void Attack(InputEnum attackInput)
    {
        StateController.Attack(attackInput);
    }

    public void AttackRelease(InputEnum attackInput)
    {
        StateController.AttackRelease(attackInput);
    }

    public void Dodge()
    {
        StateController.Dodge();
    }

    public void Move(Vector3 dir)
    {
        StateController.Move(dir);
    }

    public void FaceDirection(Vector3 forward)
    {
        StateController.FaceDirection(forward);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit) {
        hit.transform.GetComponent<PlatformListener>()?.OnControllerColliderHit(hit);
        hit.transform.GetComponent<PhysicsPlatform>()?.OnControllerColliderHit(hit);
    }

}
