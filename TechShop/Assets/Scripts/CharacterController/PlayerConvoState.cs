﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConvoState : HumanStateBehaviour
{
    public override StateEnum State => StateEnum.InConversation;

    public override void EnterState()
    {
        GameLogic.Instance.Player.MovementController.enabled = false;

        base.EnterState();
    }

    public override void ExitState()
    {
        GameLogic.Instance.Player.MovementController.enabled = true;

        base.ExitState();
    }
}
