﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public enum InputEnum {
    Primary,
    Secondary,
    Tetriary
}

//action controller for player.
public class PlayerMoveController : MonoBehaviour {

    public HumanController PlayerHuman;

    public LayerMask rayCastLayer; // mouse hit raycast layer.

    public int clickPriority; // click priority for attack.


    public void Update() {
        Vector3 inputDir = GetInputDirection();

        if (inputDir != Vector3.zero) {
            PlayerHuman.Move(inputDir);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            PlayerHuman.Dodge();
        }

        GetAttack();

        PlayerHuman.FaceDirection(GetCharacterFaceDirection());
    }


    public void GetAttack() {

        //Check if the click is meant for player attack or something has higher priority.
        if (!MouseInputController.Instance.AllowedToUseClick(mouseClickRequest())) {
            return;
        }
        //Check if click was on UI.
        if (UI.Instance.MouseOnUI) {
            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            PlayerHuman.Attack(InputEnum.Primary);
        }
        if (Input.GetMouseButtonDown(1)) {
            PlayerHuman.Attack(InputEnum.Secondary);
        }

        if (Input.GetMouseButtonUp(0)) {
            PlayerHuman.AttackRelease(InputEnum.Primary);
        }
        if (Input.GetMouseButtonUp(1)) {
            PlayerHuman.AttackRelease(InputEnum.Secondary);
        }
    }

    public Vector3 GetCharacterFaceDirection() {
        Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit[] hits = Physics.RaycastAll(screenRay, 200f, rayCastLayer.value);

        if (hits == null || hits.Length == 0) {
            //not really correct.
            return Vector3.forward;
        }

        Vector3 hitPos = hits.First().point;

        Vector3 planePos = hitPos.x0z();

        Vector3 charPlanePos = transform.position.x0z();

        Vector3 faceDir = (planePos - charPlanePos).normalized;

        return faceDir;
    }


    public Vector3 GetInputDirection() {
        // Get Horizontal and Vertical Input
        float horizontalInput = Input.GetAxis ("Horizontal");
        float verticalInput = Input.GetAxis ("Vertical");

        // Calculate the Direction to Move based on the tranform of the Player
        Vector3 moveDirectionForward = Vector3.forward * verticalInput;
        Vector3 moveDirectionSide = Vector3.right * horizontalInput;

        //find the direction
        Vector3 direction = (moveDirectionForward + moveDirectionSide).normalized;
        //find the distance
        Vector3 distance = direction;

        return distance;
    }

    public MouseInputController.InputRequestUser mouseClickRequest() {
        return new MouseInputController.InputRequestUser(clickPriority, this);
    }
}
