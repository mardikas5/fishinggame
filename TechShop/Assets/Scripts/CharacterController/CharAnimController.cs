﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Animation controller for a game character.
/// </summary>
public class CharAnimController : MonoBehaviour
{
    [Serializable]
    public class AnimTagStates
    {
        public AnimTags tag;
        public AnimParamType paramType;
        public string animatorString;
        public bool status;
        public float Value;
    }

    public enum AnimTags
    {
        Walk,
        Dodge,
        Grounded,
        Attack,
        ChargeAttack
    }

    public enum AnimParamType
    {
        Bool,
        Float
    }

    public List<AnimTagStates> animStates = new List<AnimTagStates>();
    public HumanController charController;

    public string Speed = "WalkSpeed";
    public string ForwardDir = "Forward";
    public string RightDir = "Right";


    public void Update()
    {
        updateStateAnimator();
    }

    //Exposed for inspector / unityevents.
    public void EnableBool(string name)
    {
        charController.animator.SetBool(name, true);
    }

    public void DisableBool(string name)
    {
        charController.animator.SetBool(name, false);
    }

    /// <summary>
    /// Set a predefined animator string to float value
    /// </summary>
    /// <param name="animTag"></param>
    /// <param name="state"></param>
    public void SetAnimTagFloat(AnimTags animTag, float value)
    {
        animStates.FirstOrDefault((x) => x.tag == animTag).Value = value;
    }

    /// <summary>
    /// Set a predefined animator string to bool value
    /// </summary>
    /// <param name="animTag"></param>
    /// <param name="state"></param>
    public void SetAnimTagBool(AnimTags animTag, bool state)
    {
        animStates.FirstOrDefault((x) => x.tag == animTag).status = state;
    }

    private void updateStateAnimator()
    {
        Animator animator = charController.animator;

        Vector3 lastMove = charController.StateController.lastMoveDir.normalized;
        Vector3 facingForward = charController.visuals.forward;
        Vector3 facingRight = charController.visuals.right;

        float dotProdForward = Vector3.Dot(lastMove, facingForward);
        float dotProdRight = Vector3.Dot(lastMove, facingRight);

        animator.SetFloat(ForwardDir, dotProdForward);
        animator.SetFloat(RightDir, dotProdRight);

        foreach (AnimTagStates animState in animStates)
        {
            if (animState.paramType == AnimParamType.Bool)
            {
                animator.SetBool(animState.animatorString, animState.status);
            }
            if (animState.paramType == AnimParamType.Float)
            {
                animator.SetFloat(animState.animatorString, animState.Value);
            }
        }
    }
}
