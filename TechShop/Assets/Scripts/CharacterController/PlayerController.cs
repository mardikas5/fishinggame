﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;
using System;

public class PlayerController : MonoBehaviour
{
    public PlayerDutyController DutyController;

    public HumanStateController StateController;
    public PlayerMoveController MovementController;
    public InventoryController InventoryController;

    public PlayerEnergyController EnergyController;
    public PlayerEquipmentController EquipmentController;
    public PlayerJournal PlayerJournal;
    public ToolUser ToolUser;
    public PlayerStats Stats;

    public HumanStateBehaviour conversationState;

    private GameLogic Logic;
    private UI UI;

    public void Start()
    {
        Logic = GameLogic.Instance;
        UI = UI.Instance;
        //DialogueManager.instance.conversationStarted += OnStartConversation;
        //DialogueManager.instance.conversationEnded += OnExitConversation;
    }

    public void Update()
    {
        ShortcutKeys();
    }

    //bit ugly direct reference to UI.
    public void ShortcutKeys()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            UI.PlayerOverViewUI.Toggle();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            UI.FishingMapPanelUI.Toggle();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            UI.PlayerJournalUI.Open();
        }
    }

    public void EnterConversation(Transform t)
    {
        OnStartConversation(t);
    }

    public void ExitConversation(Transform t)
    {
        OnExitConversation(t);
    }

    public void Teleport(Vector3 pos)
    {
        //dangerous.
        GetComponent<CharacterController>().enabled = false;
        transform.position = pos;
        GetComponent<CharacterController>().enabled = true;
    }


    public void OnStartConversation(Transform t)
    {
        // a lot of jumping over things

        Debug.LogError("Conversation started.");
        Logic.Time.TimeScale = .1f;
        Vector3 setPos = Logic.CameraController.GetConvoPos(t);
        Logic.CameraController.ConvoTransform.position = setPos;
        MouseInputController.Instance.InputActive = false;

        StateController.SetState(conversationState);
    }

    public void OnExitConversation(Transform t)
    {
        MouseInputController.Instance.InputActive = true;

        Logic.Time.TimeScale = 1f;//default timescale;
        StateController.Restart();
    }
}
