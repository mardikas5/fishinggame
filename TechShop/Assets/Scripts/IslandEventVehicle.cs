﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandEventVehicle : MonoBehaviour
{
    public UIWorldToolTip toolTip;
    public Interactable interactable;

    public string EventUnavailable;

    public void Start()
    {
        GameLogic.Instance.IslandEvents.OnEventUpdate += UpdateEventVehicle;
        UpdateEventVehicle();
    }


    private void UpdateEventVehicle()
    {
        bool hasEvent = GameLogic.Instance.IslandEvents.IsEventAvailable();
        interactable.isEnabled = hasEvent;

        if (!hasEvent)
        {
            toolTip.textObj.text = EventUnavailable;
        }
        else
        {
            toolTip.textObj.text = "Travel to: " + GameLogic.Instance.IslandEvents?.ActiveEvent?.Description;
        }
    }
}
