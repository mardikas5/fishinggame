﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//controlling the game loop.
public class GameController : MonoBehaviour
{
    public event Action OnStartNewDay;
    public bool enableDuties = false;
    public void Start()
    {
        GameLogic.Instance.PlayTimeController.OnDayEnded += OnDayEnded;
    }

    private void OnDayEnded()
    {
        if (enableDuties)
        {
            TrySolveDuties(GameLogic.Instance.Player.DutyController.DayDuties());
        }
    }

    private void TrySolveDuties(List<PlayerDuty> solveDuties)
    {
        //GameLogic.Instance.Time.PauseTime();

        //UI.Instance.FadeInPanel(() =>
        //{
        //    GameLogic.Instance.Player.DutyController.ResolveDuties(solveDuties, OnResolvedDuties);
        //});
    }

    private void OnResolvedDuties()
    {
        ShowNewDay(StartNewDay);
    }

    private void StartNewDay()
    {
        UI.Instance.FadeOutPanel(() =>
        {
            GameLogic.Instance.PlayTimeController.StartNewDay();

            //hmm, let inventorycontroller do this?
            GameLogic.Instance.Player.InventoryController.Transactions.Clear();

            SignalNewDay();
        });
    }

    public void SignalNewDay()
    {
        OnStartNewDay?.Invoke();
    }

    private void ShowNewDay(Action OnNewDayConfirmed)
    {
        UI.Instance.NewDayUI.Show();

        Action singleTime = null;
        singleTime = () =>
        {

            GameLogic.Instance.Time.ResumeTime();
            UI.Instance.NewDayUI.OnBegin -= singleTime;
            UI.Instance.NewDayUI.Hide();
            OnNewDayConfirmed?.Invoke();
        };

        UI.Instance.NewDayUI.OnBegin += singleTime;
    }
}
