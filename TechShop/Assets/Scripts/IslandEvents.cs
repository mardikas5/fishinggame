﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandEvents : MonoBehaviour
{
    [Serializable]
    public class EventSpawnChance
    {
        public VisitorEvent visitEvent;
        public float Chance;
    }

    public VisitorEvent ActiveEvent;

    public List<EventSpawnChance> spawnChances = new List<EventSpawnChance>();

    public VisitorEvent PresetEvent = null;

    public bool SpawnRandomEvents = true;

    public float CheckInterval = 2f;

    public DateTime nextCheckTime;

    public event Action OnEventUpdate;

    public bool IsEventAvailable() { return ActiveEvent != null; }


    public void Start()
    {
        nextCheckTime = GameLogic.Instance.Time.GameTime.AddHours(CheckInterval);
    }

    public void Update()
    {
        UpdateCurrentEvent();

        CheckNextEventSpawn();
    }

    public void CheckNextEventSpawn()
    {
        if (ActiveEvent != null)
        {
            return;
        }

        if (nextCheckTime > GameLogic.Instance.Time.GameTime)
        {
            return;
        }

        nextCheckTime = GameLogic.Instance.Time.GameTime.AddHours(CheckInterval);

        if (SpawnRandomEvents)
        {
            SpawnNewRandomEvent();
        }
    }

    public void UpdateCurrentEvent()
    {
        if (ActiveEvent != null)
        {
            if (!ActiveEvent.IsStarted)
            {
                return;
            }
            if (ActiveEvent.EndDate < GameLogic.Instance.Time.GameTime)
            {
                EndCurrentEvent();
            }
        }
    }

    private void EndCurrentEvent()
    {
        ActiveEvent.End();
        ActiveEvent = null;
        OnEventUpdate?.Invoke();
    }

    public EventSpawnChance GetRandomEvent()
    {
        EventSpawnChance k = GetWeightedRandom.GetObject(spawnChances, (x) => x.Chance);
        return k;
    }

    public void SpawnNewRandomEvent()
    {
        if (ActiveEvent != null)
        {
            EndCurrentEvent();
        }

        VisitorEvent randomEvent = GetRandomEvent().visitEvent;

        if (randomEvent == null)
        {
            return;
        }

        ActiveEvent = SpawnEvent(randomEvent);

        OnEventUpdate?.Invoke();
        ActiveEvent.BeginEvent();
    }

    private VisitorEvent SpawnEvent(VisitorEvent spawnEvent)
    {
        if (spawnEvent == null)
        {
            return null;
        }

        VisitorEvent newInstance = Instantiate(spawnEvent);
        newInstance.transform.position = Vector3.zero;
        return newInstance;
    }

    public void MoveToCurrentEvent()
    {
        if (ActiveEvent != null)
        {
            ActiveEvent.TeleportToEvent();
        }
    }
}
