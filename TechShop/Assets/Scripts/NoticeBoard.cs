﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoticeBoard : MonoBehaviour
{
    public List<Notice> notices = new List<Notice>();

    public void OnClickedHandler()
    {
        UI.Instance.NoticeBoardPanelUI.Init(this);
    }

    public void AddNotice(Notice n)
    {
        notices.Add(n);
    }
    //
}
