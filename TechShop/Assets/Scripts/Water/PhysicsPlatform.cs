﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PhysicsPlatform : MonoBehaviour
{
    [Serializable]
    public class PhysicsParentFollower
    {
        public Transform Transform;
        public Vector3 lastUpdatePos;

        public PhysicsParentFollower(Transform transform)
        {
            Transform = transform;
        }
    }


    public List<Transform> plantedTransforms = new List<Transform>();

    public List<CharacterController> plantedChars = new List<CharacterController>();

    public Dictionary<Transform, PhysicsParentFollower> ParentedMovementShadows = new Dictionary<Transform, PhysicsParentFollower>();

    public Vector3 lastOffset;
    public Vector3 lastRotation;

    public SimplePointBoat simpleboat;

    // Start is called before the first frame update
    private void Start()
    {
        simpleboat = GetComponent<SimplePointBoat>();
        ParentedMovementShadows.Clear();
    }

    // Update is called once per frame
    private void Update()
    {

    }

    public void Leave(CharacterController t)
    {
        plantedChars.Remove(t);
    }

    private void FixedUpdate()
    {
        lastOffset = simpleboat.lastOffset;

        foreach (CharacterController t in plantedChars)
        {
            ApplyMovementShadow(t);
        }

        foreach (Transform t in plantedTransforms)
        {
            //t.Translate(lastOffset, Space.World);
        }

        lastRotation = simpleboat.transform.eulerAngles;
    }

    private void ApplyMovementShadow(CharacterController CharController)
    {
        PhysicsParentFollower shadow = GetOrCreateShadow(CharController);
        Vector3 diff = shadow.Transform.position - shadow.lastUpdatePos;
        AddOffset(CharController, diff);
        shadow.Transform.position = CharController.transform.position;
        shadow.lastUpdatePos = shadow.Transform.position;
    }

    private PhysicsParentFollower GetOrCreateShadow(CharacterController CharController)
    {
        if (ParentedMovementShadows.TryGetValue(CharController.transform, out PhysicsParentFollower Follow))
        {
            return Follow;
        }
        else
        {
            Transform nt = new GameObject(CharController.name + "follower").transform;
            nt.transform.position = CharController.transform.position;
            PhysicsParentFollower newFollow = new PhysicsParentFollower(nt);
            newFollow.lastUpdatePos = nt.transform.position;
            nt.transform.parent = this.transform;

            ParentedMovementShadows.Add(CharController.transform, newFollow);
            return newFollow;
        }

        //return null;
    }

    private void AddOffset(CharacterController t, Vector3 offset)
    {
        t.GetComponent<HumanController>().AddNegatedVelocity(offset);

        if (t.TryGetComponent(out SimpleAI agento))
        {
            //agento.isOnFakeSurface = true;
            agento.Move(offset);
            return;
        }
        
        t.Move(offset);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.transform == this)
        {
            return;
        }
        if (collision.transform.TryGetComponent(out CharacterController controller))
        {
            HitController(controller);
            return;
        }
        //12 is tool.
        if (collision.gameObject.layer == 12)
        {
            return;
        }

        if (plantedTransforms.Contains(collision.transform))
        {
            return;
        }

        plantedTransforms.Add(collision.transform);
    }


    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        HitController(hit.controller);
    }

    private void HitController(CharacterController controller)
    {
        if (plantedChars.Contains(controller))
        {
            return;
        }

        plantedChars.Add(controller);
    }

    public void OnCollisionExit(Collision collision)
    {
        //Debug.LogError("exit: " + collision.transform.name);
    }
}
