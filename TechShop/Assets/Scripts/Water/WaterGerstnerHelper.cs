﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityStandardAssets.Water;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;

[Serializable]
public class GerstnerVars
{
    public Vector4 amplitude;
    public Vector4 frequency;
    public Vector4 steepness;
    public Vector4 speed;
    public Vector4 directionAB;
    public Vector4 directionCD;

    public void CopyFromMaterial(Material mat)
    {
        amplitude   = mat.GetVector("_GAmplitude");
        frequency   = mat.GetVector("_GFrequency");
        steepness   = mat.GetVector("_GSteepness");
        speed       = mat.GetVector("_GSpeed");
        directionAB = mat.GetVector("_GDirectionAB");
        directionCD = mat.GetVector("_GDirectionCD");
    }
}

public class WaterGerstnerHelper : Singleton<WaterGerstnerHelper>
{
    public float startTime = 0f;
    public Vector4 _TimeYYYY;
    public float planeHeight = .2f;

    public GerstnerVars gerstnerVars;

    public Material waterMat;

    public event Action<Transform, Vector3> WaterCollision;

    // Start is called before the first frame update
    private void Start()
    {
        startTime = Shader.GetGlobalVector("_Time").y;
    }
    
    [ContextMenu("copy mat")]
    public void CopyFromMaterial()
    {
        CopyMaterialValues(waterMat);
    }

    public void CopyMaterialValues(Material mat)
    {
        gerstnerVars.CopyFromMaterial(mat);
    }

    public void Update()
    {
        float time = startTime + Time.time;

        _TimeYYYY = new Vector4(time, time, time, time);
    }

    public void SignalHitWater(Transform obj, Vector3 force)
    {
        WaterCollision?.Invoke(obj, force);
    }

    public void GetGersntnerAtPosition(out Vector3 waterOffset, Vector3 planePos, out bool isUnderWater, out float submerged)
    {
        var vars = gerstnerVars;
        waterOffset = GerstnerOffset4(
            planePos.v2xz(),
            vars.steepness,
            vars.amplitude,
            vars.frequency,
            vars.speed,
            vars.directionAB,
            vars.directionCD);

        isUnderWater = false;
        submerged = (waterOffset.y + planeHeight) - planePos.y;
        if (submerged > 0f)
        {
            isUnderWater = true;
        }
    }

    private Vector3 GerstnerOffset4(Vector2 xzVtx, Vector4 steepness, Vector4 amp, Vector4 freq, Vector4 speed, Vector4 dirAB, Vector4 dirCD)
    {
        Vector3 offsets;

        Vector4 steepXXYY = new Vector4(steepness.x, steepness.x, steepness.y, steepness.y);
        Vector4 steepZZWW = new Vector4(steepness.z, steepness.z, steepness.w, steepness.w);

        Vector4 ampXXYY =  new Vector4(amp.x, amp.x, amp.y, amp.y);
        Vector4 ampZZWW =  new Vector4(amp.z, amp.z, amp.w, amp.w);

        //steepXXYY * ampXXYY *dirAB;
        Vector4 AB = steepXXYY;
        AB.Scale(ampXXYY);
        AB.Scale(dirAB);

        //steepZZWW * ampZZWW * dirCD;
        Vector4 CD = steepZZWW;
        CD.Scale(ampZZWW);
        CD.Scale(dirCD);

        //freq.xyzw * new Vector4(dot(dirAB.xy, xzVtx), dot(dirAB.zw, xzVtx), dot(dirCD.xy, xzVtx), dot(dirCD.zw, xzVtx));
        Vector4 dotProds = new Vector4(
            Vector2.Dot(new Vector2(dirAB.x,dirAB.y), xzVtx),
            Vector2.Dot(new Vector2(dirAB.z,dirAB.w), xzVtx),
            Vector2.Dot(new Vector2(dirCD.x,dirCD.y), xzVtx),
            Vector2.Dot(new Vector2(dirCD.z,dirCD.w), xzVtx));

        Vector4 dotABCD = freq;
        dotABCD.Scale(dotProds);

        Vector4 TIME = _TimeYYYY;
        TIME.Scale(speed);

        Vector4 cosGuy = dotABCD;
        cosGuy = cosGuy.add(TIME);

        Vector4 sinGuy = dotABCD;
        sinGuy = sinGuy.add(TIME);

        float[] cosValues = cosGuy.components();

        for (int i = 0; i < cosValues.Length; i++)
        {
            cosValues[i] = Mathf.Cos(cosValues[i]);
        }

        float[] sinValues = sinGuy.components();

        for (int i = 0; i < sinValues.Length; i++)
        {
            sinValues[i] = Mathf.Sin(sinValues[i]);
        }

        Vector4 COS = Vector4.zero.fromComponents(cosValues);
        Vector4 SIN = Vector4.zero.fromComponents(sinValues);

        offsets.x = Vector4.Dot(COS, new Vector4(AB.x, AB.z, CD.x, CD.z));
        offsets.z = Vector4.Dot(COS, new Vector4(AB.y, AB.w, CD.y, CD.w));
        offsets.y = Vector4.Dot(SIN, amp);

        return offsets;
    }

    private bool HeightPlane(int numPoints, Vector3[] points,
    out Complex barX, out Complex barY, out Complex barH, out Complex barA0, out Complex barA1)
    {
        // Compute t h e mean o f t h e p o i n t s .
        Vector3 mean = Vector3.zero;
        for (int i = 0; i < numPoints; ++i)
        {
            mean += points[i];
        }
        mean /= numPoints;
        // Compute t h e l i n e a r sy s tem m a t r i x and v e c t o r e l em e n t s .
        Complex xxSum = 0 , xySum = 0 , xhSum = 0 , yySum = 0 , yhSum = 0;
        for (int i = 0; i < numPoints; ++i)
        {
            Vector3 diff = points[i] - mean;
            xxSum += diff[0] * diff[0];
            xySum += diff[0] * diff[1];
            xhSum += diff[0] * diff[2];
            yySum += diff[1] * diff[1];
            yhSum += diff[1] * diff[2];
        }

        Complex det = xxSum * yySum - xySum * xySum;
        if (det != 0)
        {
            // Compute t h e f i t t e d p l a n e h ( x , y ) = barH + barA0 ∗ ( x − barX ) + barA1 ∗ ( y − barY ) .
            barX = mean[0];
            barY = mean[1];
            barH = mean[2];
            barA0 = (yySum * xhSum - xySum * yhSum) / det;
            barA1 = (xxSum * yhSum - xySum * xhSum) / det;
            return true;
        }
        else
        {
            // The o u t p u t i s i n v a l i d . The p o i n t s a r e a l l t h e same o r t h e y a r e c o l l i n e a r .
            barX = 0;
            barY = 0;
            barH = 0;
            barA0 = 0;
            barA1 = 0;
            return false;
        }
    }
}

//based from:
/*
    {
    half3 offsets;

    half4 AB = steepness.xxyy * amp.xxyy * dirAB.xyzw;
    half4 CD = steepness.zzww * amp.zzww * dirCD.xyzw;

    half4 dotABCD = freq.xyzw * half4(dot(dirAB.xy, xzVtx), dot(dirAB.zw, xzVtx), dot(dirCD.xy, xzVtx), dot(dirCD.zw, xzVtx));
    half4 TIME = _Time.yyyy * speed;

    half4 COS = cos (dotABCD + TIME);
    half4 SIN = sin (dotABCD + TIME);

    offsets.x = dot(COS, half4(AB.xz, CD.xz));
    offsets.z = dot(COS, half4(AB.yw, CD.yw));
    offsets.y = dot(SIN, amp);

    return offsets;			
}	

*/


