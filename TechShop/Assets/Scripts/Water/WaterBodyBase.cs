﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class WaterBodyBase : ScriptableObject
{
    [Serializable]
    public class FishGroup
    {
        public float amount;
        public EntityBase fish;
    }

    [Serializable]
    public class RandomPossibleItems
    {
        float amount;
        public EntityBase item;
    }

    public float randomItemChance = .01f;

    public List<RandomPossibleItems> RandomLootItems = new List<RandomPossibleItems>();
    public List<FishGroup> Fishes = new List<FishGroup>();
}
