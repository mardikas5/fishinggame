﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGerstnerSplashHandler : MonoBehaviour
{
    public WaterGerstnerHelper water;
    public GameObject SplashObject;
    public float magnitudeToSpawn = 1f;

    // Start is called before the first frame update
    private void Start()
    {
        water = GetComponent<WaterGerstnerHelper>();
        water.WaterCollision += OnWaterCollisionEnter;
    }

    private void OnWaterCollisionEnter(Transform arg1, Vector3 arg2)
    {
        if (arg2.magnitude > magnitudeToSpawn)
        {
            Instantiate(SplashObject, arg1.position - (Vector3.up * .5f), Quaternion.identity, null);
        }
    }
}
