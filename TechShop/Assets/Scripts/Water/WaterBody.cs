﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBody : MonoBehaviour
{
    public WaterBodyBase Base;

    public Fish GetFish()
    {
        WaterBodyBase.FishGroup fishGroup = GetWeightedRandom.GetObject(Base.Fishes, (x) => x.amount);
        
        Fish instance = FishFactory.GenerateFishStatic(fishGroup.fish);

        return instance;
    }
}

