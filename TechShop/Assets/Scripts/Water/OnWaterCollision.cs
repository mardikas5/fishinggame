﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//on any layer index collision.
public class OnWaterCollision : MonoBehaviour
{
    public LayerMask hitLayerMask;

    public GameObject spawnOnCollision;

    public float magnitudeToSpawn;




    private void OnCollisionEnter(Collision collision)
    {
        if (hitLayerMask == (hitLayerMask | (1 << collision.gameObject.layer)))
        {
            if (collision.impulse.magnitude > magnitudeToSpawn)
            {
                Instantiate(spawnOnCollision, transform.position, Quaternion.identity, null);
            }
        }
    }
}
