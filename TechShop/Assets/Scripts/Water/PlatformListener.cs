﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformListener : MonoBehaviour
{
    public PhysicsPlatform parent;

    private void OnCollisionEnter(Collision collision)
    {
        parent.OnCollisionEnter(collision);
    }

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        parent.OnControllerColliderHit(hit);
    }

    private void OnCollisionExit(Collision collision)
    {
        parent.OnCollisionExit(collision);
    }
}
