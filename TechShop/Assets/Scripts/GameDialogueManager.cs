﻿using PixelCrushers.DialogueSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDialogueManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        DialogueManager.instance.conversationStarted += OnConvoStarted;
        DialogueManager.instance.conversationEnded += OnConvoEnded;
    }

    private void OnConvoStarted(Transform t)
    {
        UI.Instance.HUDPanel.SetActive(false);
        MouseInputController.Instance.InputActive = false;
    }

    private void OnConvoEnded(Transform t)
    {
        UI.Instance.HUDPanel.SetActive(true);
        MouseInputController.Instance.InputActive = true;
    }

}
