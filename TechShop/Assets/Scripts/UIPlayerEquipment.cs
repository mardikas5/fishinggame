﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerEquipment : MonoBehaviour
{
    public UIInventory ToolSlot, UpperGarments, LowerGarments, Belt, Hat, Boots, Charms0, Charms1, Charms2, Charms3;

    public void Init(PlayerEquipmentController equipmentController)
    {
        ToolSlot.Generate(equipmentController.ToolSlot.Inventory).itemFrame = false;
        UpperGarments.Generate(equipmentController.UpperGarments.Inventory).itemFrame = false;
        LowerGarments.Generate(equipmentController.LowerGarments.Inventory).itemFrame = false;
        Belt.Generate(equipmentController.Belt.Inventory).itemFrame = false;
        Hat.Generate(equipmentController.Hat.Inventory).itemFrame = false;
        Boots.Generate(equipmentController.Boots.Inventory).itemFrame = false;
        Charms0.Generate(equipmentController.Charms0.Inventory).itemFrame = false;
        Charms1.Generate(equipmentController.Charms1.Inventory).itemFrame = false;
        Charms2.Generate(equipmentController.Charms2.Inventory).itemFrame = false;
        Charms3.Generate(equipmentController.Charms3.Inventory).itemFrame = false;
    }

    void Awake()
    {
        Init(GameLogic.Instance.Player.EquipmentController);
    }
}
