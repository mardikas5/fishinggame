﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Merchant : MonoBehaviour
{
    public float playerDisposition;

    public Inventory Inventory;

    public event Action<float> AmountUpdated;

    public float shopMoney = 1000f;

    //how much does it cost to buy item
    public float ItemBuyValue(Item i)
    {
        if (i == null)
        {
            return 0f;
        }
        //todo disposition.
        return i.Entity.Value() * 1.3f;
    }

    //how much merchant will pay for item
    public float ItemSellValue(Item i)
    {
        if (i == null)
        {
            return 0f;
        }

        //todo disposition.
        return i.Entity.Value() * .7f;
    }
}
