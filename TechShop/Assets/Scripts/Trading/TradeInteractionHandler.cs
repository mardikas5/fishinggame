﻿using PixelCrushers.DialogueSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TradeInteractionHandler : MonoBehaviour
{
    public TradeController tradeController;

    public UnityEvent OnTradeOpened;
    public UnityEvent OnTradeClosed;

    public void Start()
    {
        Lua.RegisterFunction("Trade", this, typeof(TradeInteractionHandler).GetMethod("Trade"));
        tradeController = GetComponent<TradeController>();

        tradeController.OnClosed += OnClosedHandler;
    }

    private void OnClosedHandler()
    {
        OnTradeClosed?.Invoke();
    }

    public void Trade(string test)
    {
        Transform currentTalker = DialogueManager.CurrentConversant;

        OpenTradeMenu(currentTalker.GetComponent<Merchant>());
    }

    public void OpenTradeMenu(Merchant merchant)
    {
        if (merchant == null)
        {
            Debug.LogError(" trying to start a trade with non merchant character, add a merchant component first! ");
            return;
        }

        PlayerController player = GameLogic.Instance.Player;

        tradeController.StartTrade(player, merchant);

        UI.Instance.TradePanelUI.Open(
            tradeController,
            merchant.Inventory,
            player.InventoryController.Inventory,
            tradeController.PlayerSellInventory,
            tradeController.StoreBuyInventory);

        OnTradeOpened?.Invoke();
    }
}
