﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//player specific.
public class TradeController : MonoBehaviour
{
    public event Action<Item> SellItemAdded;
    public event Action<Item> BuyItemAdded;

    public event Action<List<Item>> SellCompleted;
    public event Action<List<Item>> BuyCompleted;

    public event Action MerchantNotEnoughMoneySignal;
    public event Action NotEnoughMoneySignal;

    public Inventory PlayerInventory;
    public Inventory StoreInventory;

    public Inventory PlayerSellInventory;
    public Inventory StoreBuyInventory;

    public PlayerController Player;
    public Merchant Merchant;

    public event Action OnClosed;

    public void Start()
    {
        SellCompleted += (x) => GameEvents.Instance.DynamicInvokeEvent(GameEvents.EventEnum.OnItemsExchanged, x);
        BuyCompleted += (x) => GameEvents.Instance.DynamicInvokeEvent(GameEvents.EventEnum.OnItemsExchanged, x);
    }

    public void StartTrade(PlayerController player, Merchant merchant)
    {
        Player = player;
        Merchant = merchant;

        StoreInventory = merchant.Inventory;
        PlayerInventory = player.InventoryController.Inventory;
    }


    public float CurrentBuyValue()
    {
        return BuyValue(StoreBuyInventory.ItemsList);
    }

    public float CurrentSellValue()
    {
        return SellValue(PlayerSellInventory.ItemsList);
    }

    public float BuyValue(List<Item> items)
    {
        float amount = 0f;
        foreach (Item i in items)
        {
            amount += Merchant.ItemBuyValue(i);
        }
        return amount;
    }

    public float SellValue(List<Item> items)
    {
        float amount = 0f;
        foreach (Item i in items)
        {
            amount += Merchant.ItemSellValue(i);
        }
        return amount;
    }

    public void Sell()
    {
        List<Item> movedItems = new List<Item>();

        PlayerSellInventory.ItemsList.ForEach((x) =>
        {
            StoreInventory.TryPlaceAnySlot(x);
            movedItems.Add(x);
        });


        float totalValueItems = 0f;

        foreach (Item i in movedItems)
        { 
            float itemValue = Merchant.ItemSellValue(i);
            totalValueItems += itemValue;

            //todo this transaction stuff.
            GameLogic.Instance.Player.InventoryController.Transactions.Add(new Transaction(Transaction.Type.Selling, i.Entity, itemValue));
            //
        }

        if (totalValueItems > Merchant.shopMoney)
        {
            MerchantNotEnoughMoneySignal?.Invoke();
            return;
        }

        Merchant.shopMoney -= totalValueItems;

        GameLogic.Instance.playerMoney.AddAmount(totalValueItems);

        SellCompleted?.Invoke(movedItems);
    }

    public bool Buy()
    {
        List<Item> movedItems = new List<Item>();
        float totalCost = 0f;

        foreach (Item i in StoreBuyInventory.ItemsList)
        {
            totalCost += Merchant.ItemBuyValue(i);
        }

        if (GameLogic.Instance.playerMoney.Amount < totalCost)
        {
            NotEnoughMoneySignal?.Invoke();
            return false;
        }
        
        foreach (Item i in StoreBuyInventory.ItemsList)
        {
            movedItems.Add(i);
            if (!PlayerInventory.TryPlaceAnySlot(i))
            {
                SendItems(movedItems, StoreInventory);
                return false;
            }
        }

        if (!GameLogic.Instance.playerMoney.RemoveAmount(totalCost))
        {
            Debug.LogError(" did the player just get free items? ");
        }

        BuyCompleted?.Invoke(movedItems);

        return true;
    }

    public void Close()
    {
        SendItems(PlayerSellInventory.ItemsList, PlayerInventory);
        SendItems(StoreBuyInventory.ItemsList, StoreInventory);

        OnClosed.Invoke();
    }

    private void SendItems(List<Item> items, Inventory inventory)
    {
        items.ForEach((x) =>
        {
            inventory.TryPlaceAnySlot(x);
        });
    }
}
