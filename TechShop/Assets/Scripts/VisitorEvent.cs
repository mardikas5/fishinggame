﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorEvent : MonoBehaviour
{
    public bool IsPlayerInside = false;
    public bool IsEnded = false;

    public Transform EventPosition;

    public Transform ShorePos => GameLogic.Instance.LightHouseIslandPos;

    public string Description;

    public bool IsStarted;

    public DateTime EndDate;

    public PlayerController player;

    public float EventDurationInHours;

    public DateTime StartDate;

    public event Action OnEventBegin;
    public event Action OnEventEnd;


    public void Awake()
    {
        player = GameLogic.Instance.Player;
    }

    public DelayedAction TeleportToEvent()
    {
        IsPlayerInside = true;
        UI.Instance.FadeInPanel();
        DelayedAction r = new DelayedAction();

        this.InvokeDelayed(.5f, () =>
        {
            player.Teleport(EventPosition.transform.position);
            UI.Instance.FadeOutPanel();
            r.Invoke();
        });

        return r;
    }

    public void MoveToShore()
    {
        TeleportToShore();
    }

    public DelayedAction TeleportToShore()
    {
        IsPlayerInside = false;
        UI.Instance.FadeInPanel();
        DelayedAction r = new DelayedAction();

        this.InvokeDelayed(.5f, () =>
        {
            r.Invoke();
            player.Teleport(ShorePos.transform.position);
            UI.Instance.FadeOutPanel();
        });

        return r;
    }

    public void BeginEvent()
    {
        OnEventBegin?.Invoke();
        IsStarted = true;
        StartDate = GameLogic.Instance.Time.GameTime;
        EndDate = StartDate.AddHours(EventDurationInHours);
    }

    public void End()
    {
        OnEventEnd?.Invoke();
        if (IsPlayerInside)
        {
            TeleportToShore().Callback += () => Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
