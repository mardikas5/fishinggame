﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UITimePanel : MonoBehaviour
{
    public TextMeshProUGUI TimeText;
    public TextMeshProUGUI DateText;

    public Color TimePausedColor;

    public Color TimeRunningColor;


    private TimeController TimeController;

    // Start is called before the first frame update
    private void Start()
    {
        //Save the reference so instance is not looked up every frame (update).
        TimeController = GameLogic.Instance.Time;
    }

    // Update is called once per frame
    private void Update()
    {
        if (GameLogic.Instance.Time.TimeStopped)
        {
            TimeText.color = TimePausedColor;
        }
        else
        {
            TimeText.color = TimeRunningColor;
        }

        TimeText.text = TimeController.CurrentTimeString();

        DateText.text = TimeController.LongGameTimeString;
    }
}
