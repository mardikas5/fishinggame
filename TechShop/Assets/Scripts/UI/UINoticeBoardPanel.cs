﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UINoticeBoardPanel : MonoBehaviour, IGenericUIPanel
{
    public Transform Content;

    public NoticeBoard noticeBoard;

    public UINoticeSmall noticeListObjectPrefab;

    public TextMeshProUGUI noticeText;
    public TextMeshProUGUI noticeTitle;

    public RectTransform mainNoticeObject;

    public event Action<IGenericUIPanel> PanelClosed;
    public event Action<IGenericUIPanel> PanelOpened;

    public List<UINoticeSmall> activeNotices = new List<UINoticeSmall>();

    public TweenFadeHandler fadeHandler;
    public TweenFadeHandler mainNoticeFadeHandler;

    public Notice activeNotice;

    public Sequence activeSequence;

    public void Awake()
    {
        fadeHandler.Init(GetComponent<CanvasGroup>(), gameObject, .8f, .5f);
        mainNoticeFadeHandler.Init(mainNoticeObject.GetComponent<CanvasGroup>(), mainNoticeObject.gameObject, 1f, .5f);
    }

    public void Init(NoticeBoard board, bool open = true)
    {
        noticeBoard = board;

        ClearElements();

        GenerateElements(board);

        if (open)
        {
            Open();
        }
    }

    private void GenerateElements(NoticeBoard board)
    {
        foreach (Notice t in board.notices)
        {
            Notice localRef = t;
            UINoticeSmall smallNotice = Instantiate(noticeListObjectPrefab, Content);
            smallNotice.Init(localRef);
            smallNotice.OnClicked += () => SetActiveNotice(localRef);
            activeNotices.Add(smallNotice);
        }

        if (board.notices.Count == 0)
        {
            UINoticeSmall smallNotice = Instantiate(noticeListObjectPrefab, Content);
            smallNotice.Init(new Notice() { Title = "There are no notices posted on this board." });
            activeNotices.Add(smallNotice);
        }
    }

    private void ClearElements()
    {
        foreach (UINoticeSmall t in activeNotices)
        {
            Destroy(t.gameObject);
        }

        activeNotices.Clear();
    }

    public void SetActiveNotice(Notice n)
    {
        if (n == null)
        {
            mainNoticeFadeHandler.Close();
            activeNotice = null;
            return;
        }

        mainNoticeFadeHandler.Open();

        noticeTitle.text = n.Title;
        noticeText.text = n.Description;
        activeNotice = n;
    }

    public void OnClickTakeWith()
    {
        
    }

    public void Start()
    {

    }

    public void Open()
    {
        PanelOpened?.Invoke(this);
        gameObject.SetActive(true);
        fadeHandler.Open();
    }

    public void Close()
    {
        SetActiveNotice(null);
        PanelClosed?.Invoke(this);
        fadeHandler.Close();
    }
}
