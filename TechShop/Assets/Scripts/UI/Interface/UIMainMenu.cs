﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening.Plugins.Options;
using DG.Tweening.Core;

public class UIMainMenu : MonoBehaviour
{
    public RectTransform content;
    public TweenFadeHandler UIFadeInOutHandler;
    public CanvasGroup mainPanelCanvas;

    public CanvasGroup anyKeyToStart;

    private Tween anyKeyFade;

    private bool IsOpen = false;

    public void Start()
    {
        UIFadeInOutHandler.Init(mainPanelCanvas, mainPanelCanvas.gameObject, .3f, .5f);
        Close();
    }

    public void Update()
    {
        if (Input.anyKeyDown)
        {
            Open();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Close();
        }

        UpdateAnyKeyMessage();
    }

    public void UpdateAnyKeyMessage()
    {
        if (!IsOpen)
        {
            if (anyKeyFade != null)
            {
                return;
            }

            anyKeyFade = anyKeyToStart.DOFade(1f, 2f);
            anyKeyFade.OnComplete(
                () => anyKeyFade = anyKeyToStart.DOFade(.5f, 2f).OnComplete(
                    () => anyKeyFade = null));
        }
    }

    public void Close()
    {
        IsOpen = false;
        anyKeyToStart?.DOFade(1f, .3f);
        UIFadeInOutHandler.Close();
        content.anchoredPosition = new Vector2(0, Screen.height + content.rect.height);
    }

    public void Open()
    {
        if (IsOpen)
        {
            return;
        }

        IsOpen = true;

        anyKeyToStart?.DOFade(0f, .3f);
        anyKeyFade.Kill();
        anyKeyFade = null;

        content.DOAnchorPos(Vector2.zero, 1.5f).SetEase(Ease.OutBack).easeOvershootOrAmplitude = .3f;
        UIFadeInOutHandler.Open();
    }
}
