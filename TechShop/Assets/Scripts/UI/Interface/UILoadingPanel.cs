﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoadingPanel : MonoBehaviour
{
    public RectTransform fishRect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveFish();
    }

    public void MoveFish()
    {
        Vector3 wanted = fishRect.eulerAngles;
        //wigglin.
        wanted += new Vector3(0f, 0f, Mathf.Sin(Time.time * 15f) * 5f);

        fishRect.DOLocalRotate(wanted, .3f);
    }
}
