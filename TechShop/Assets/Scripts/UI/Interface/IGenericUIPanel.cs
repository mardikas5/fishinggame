﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGenericUIPanel
{
    event Action<IGenericUIPanel> PanelClosed;

    event Action<IGenericUIPanel> PanelOpened;

    void Close();
}
