﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIPlayerStat : MonoBehaviour
{
    public TextMeshProUGUI statName;
    public TextMeshProUGUI statValue;

    public void SetStat( PlayerStats.StatValue t)
    {
        t.ValueChanged += OnValueChanged;
        statName.text = t.Name;
        OnValueChanged(t.FinalValue);
    }

    private void OnValueChanged(float obj)
    {
        statValue.text = (obj).ToString("F1");
    }
}
