﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIItemEquipmentPanel : MonoBehaviour, IGenericUIPanel
{
    public GridLayoutGroup layoutGroup;
    public PercentCellSizeHandler cellSizeHandler;
    public RectTransform itemsParent;
    public RectTransform bonusesParent;

    public Item activeObject;

    public UIItemAttribute itemAttributePrefab;
    public TextMeshProUGUI itemDescription;
    public TextMeshProUGUI itemValue;
    public Image itemImage;
    public TextMeshProUGUI itemTitle;

    public List<Inventory> listenInventories = new List<Inventory>();

    public List<Slot> Slots = new List<Slot>();
    public List<UIInventory> UISlotInventories = new List<UIInventory>();
    public List<UIItemAttribute> UIItemAttributes = new List<UIItemAttribute>();

    public UIInventory SlotInventoryPrefab;

    public event Action<IGenericUIPanel> PanelOpened;
    public event Action<IGenericUIPanel> PanelClosed;
    //
    private PlayerStats playerStats;

    public void Close()
    {
        Clear();

        gameObject.SetActive(false);
        PanelClosed?.Invoke(this);
        activeObject = null;
    }

    public void Clear()
    {
        foreach (UIInventory t in UISlotInventories)
        {
            t.Close(true);
        }

        foreach (UIItemAttribute attrib in UIItemAttributes)
        {
            Destroy(attrib.gameObject);
        }

        UIItemAttributes.Clear();
        UISlotInventories.Clear();
    }

    public void Open(Item item, bool interactable = true)
    {
        Clear();

        activeObject = item;

        gameObject.SetActive(true);
        ItemBase itemBase = item.ItemBase;

        itemDescription.text = itemBase.Description;
        itemValue.text = item.Entity.Value().ToString("F2");
        itemTitle.text = itemBase.Name;
        itemImage.sprite = itemBase.Sprite;

        ItemComponentSlots itemSlots = item.Entity.GetComponent<ItemComponentSlots>();

        PanelOpened?.Invoke(this);

        if (itemSlots == null)
        {
            return;
        }

        GenerateItemBonuses(item);

        GenerateSlots(item, itemSlots, interactable);
    }

    private void GenerateItemBonuses(Item item)
    {
        List<ItemBase> nestedItems = new List<ItemBase>();
        ItemComponentSlots.GetNestedItems(item, ref nestedItems);

        playerStats = GameLogic.Instance.Player.GetComponent<PlayerStats>();

        if (nestedItems != null)
        {
            for (int k = 0; k < nestedItems.Count; k++)
            {
                for (int p = 0; p < nestedItems[k].Modifiers.Count; p++)
                {
                    UIItemAttribute inst = Instantiate(itemAttributePrefab, bonusesParent);

                    inst.bonusText.text = nestedItems[k].Name + ":" + PlayerStats.StatValue.GetModifierString(nestedItems[k].Modifiers[p].mod) + " " + nestedItems[k].Modifiers[p].stat.ToString();

                    UIItemAttributes.Add(inst);
                }
            }
        }
    }

    private void GenerateSlots(Item item, ItemComponentSlots slotContainer, bool interactable)
    {
        if (item == null)
        {
            return;
        }

        for (int k = 0; k < slotContainer.ComponentSlots.Count; k++)
        {
            Inventory targetInventory = slotContainer.ComponentSlotInventories[k];

            AddListenInventory(targetInventory);

            UIInventory UIInv = Instantiate(SlotInventoryPrefab, layoutGroup.transform);
            UIInv.name = ("slot_" + k);
            UIInv.UIItemParent = itemsParent;
            UIInv.Generate(targetInventory);

            UIInv.Interactable = interactable;
            string text = slotContainer.ComponentSlots[k].SlotDefinition?.tags[0]?.Tag;
            this.InvokeDelayed(.01f, () => UIInv?.Slots[new Vector2i(0, 0)]?.SetOverlay(text));

            UISlotInventories.Add(UIInv);

            if (targetInventory.ItemsList?.Count > 0)
            {
                if (targetInventory.ItemsList[0] != null)
                {
                    GenerateSlots(targetInventory.ItemsList[0], targetInventory.ItemsList[0].Entity.GetComponent<ItemComponentSlots>(), interactable);
                }
            }
        }
    }



    private void AddListenInventory(Inventory inventory)
    {
        listenInventories.Add(inventory);
        inventory.ItemRemoved += RemakeSlots;
        inventory.ItemAdded += RemakeSlots;
    }

    private void RemoveListenInventory(Inventory inventory)
    {
        inventory.ItemRemoved -= RemakeSlots;
        inventory.ItemAdded -= RemakeSlots;
    }

    private void RemakeSlots(Item obj)
    {
        foreach (Inventory i in listenInventories)
        {
            RemoveListenInventory(i);
        }

        foreach (UIItemAttribute attrib in UIItemAttributes)
        {
            Destroy(attrib.gameObject);
        }

        UIItemAttributes.Clear();

        listenInventories.Clear();

        Open(activeObject);
    }
}
