﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIItemDescriptionPanel : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI valueText;

    public LayoutElement descriptionLayout;
    public TweenFadeHandler fadeHandler = new TweenFadeHandler();

    public RectTransform sizeTransform;
    public float maxSize = 700;

    public UIItem ActiveItem;

    public void Awake()
    {
        fadeHandler.Init(GetComponent<CanvasGroup>(), gameObject, .2f, .2f);
    }

    public void UpdateSize()
    {
        if (descriptionText.GetComponent<RectTransform>().sizeDelta.x > maxSize)
        {
            EnableLimitedSize();
        }
        else
        {
            descriptionLayout.enabled = false;
        }
    }

    public void EnableLimitedSize()
    {
        descriptionLayout.enabled = true;
        descriptionLayout.preferredWidth = maxSize;
    }

    public void LateUpdate()
    {
        if (ActiveItem != null)
        {
            if (ActiveItem.IsDragging)
            {
                Close();
            }
        }

        SetPanelPos();
       
        CheckMouseInside();
    }

    private void SetPanelPos()
    {
        float scale = UI.Instance.Canvas.scaleFactor;
        //transform.position = Input.mousePosition;
        Vector3 pos = Input.mousePosition;

        if ((sizeTransform.sizeDelta.x * scale) + pos.x > Screen.width)
        {
            pos.x -= sizeTransform.sizeDelta.x * scale;
        }

        //if (sizeTransform.sizeDelta.y + transform.position.y > Screen.height)
        //{
        //    pos.y -= sizeTransform.sizeDelta.y;
        //}

        if (pos.y - (sizeTransform.sizeDelta.x * scale) < 0)
        {
            pos.y += sizeTransform.sizeDelta.y * scale;
        }

        transform.position = pos;
    }

    public void Init(UIItem i)
    {
        ActiveItem = i;

        titleText.text = i.Item.ItemBase.Name;
        descriptionText.text = i.Item.ItemBase.Description;
        valueText.text = i.Item.Entity.ValueString();

        GetComponent<CanvasGroup>().alpha = 0f;
        Close();
        descriptionLayout.enabled = false;
        UI.Instance.InvokeDelayed(.0f, () => { UpdateSize(); Open(); });
    }

    public void CheckMouseInside()
    {
        if (ActiveItem == null)
        {
            return;
        }

        Vector3 pos = ActiveItem.Moveable.InverseTransformPoint(Input.mousePosition);
        bool IsMouseInside = ActiveItem.Moveable.rect.Contains(pos);

        if (!IsMouseInside)
        {
            Close();
        }
    }

    public void Open()
    {
        gameObject.SetActive(true);

        fadeHandler.Open(false);
    }

    public void Close()
    {
        fadeHandler.Close();
    }
}
