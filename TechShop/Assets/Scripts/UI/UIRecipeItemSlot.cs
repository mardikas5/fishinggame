﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class UIRecipeItemSlot : MonoBehaviour
{
    public TMPro.TextMeshProUGUI tagsTxt;
    public Recipe.Requirement currentReq;
    public UIInventory uiInventory;

    public void Awake()
    {
        currentReq = null;
        uiInventory = GetComponent<UIInventory>();
    }

    public void SetRequirement(Recipe.Requirement r)
    {
        RemovePreviousCondition();

        currentReq = r;
        uiInventory.Container.InsertConditions.Add(Condition);

        string s = "";
        for (int i = 0; i < r.tags.Length; i++)
        {
            if (i > 0)
            {
                s += ", ";
            }
            s += r.tags[i];
        }

        tagsTxt.text = s;
    }

    private bool Condition(Item i)
    {
        if (currentReq == null)
        {
            Debug.LogError(" no condition set. ");
            return true;
        }

        return currentReq.Match(i.Entity);
    }

    public void RemovePreviousCondition()
    {
        uiInventory.Container.InsertConditions.Remove(Condition);
    }
}
