﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UINormalizedPositionKeeper : MonoBehaviour
{
    RectTransform rectTransform;

    public Vector2 currentPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    [ExecuteInEditMode]
    void Update()
    {
        if (rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }
    }


    public void SetAnchorsToCurrentSize()
    {
        GetComponent<RectTransform>().SetAnchorsToCurrentSize();
    }
}
