﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static FishingBaitGame;
using Random = UnityEngine.Random;



public class UIFishingBaitGame : MonoBehaviour
{
    public FishingBaitGame Controller;


    public GameObject BaitGameTargetPrefab;

    public Transform targetTransform;
    public RectTransform matchCenterCircle;

    public Image FillBarImage;

    public float baitProgress = 0f;
    public float RotateSpeed = .3f;

    public List<UIBaitGameTarget> Targets = new List<UIBaitGameTarget>();

    [SerializeField, GenericType]
    public InterfaceContainer<ICallbackHandler<OnFishBaitedHandler>> baitedVisualsHandler = new InterfaceContainer<ICallbackHandler<OnFishBaitedHandler>>();

    [SerializeField, GenericType]
    public InterfaceContainer<ICallbackHandler<OnFishBaitedHandler>> baitFailedVisualsHandler = new InterfaceContainer<ICallbackHandler<OnFishBaitedHandler>>();

    public MonoRefContainer baitSuccess;
    public MonoRefContainer baitFailContainer;


    private Tween shakeTween;
    private TweenFadeHandler tweenUIFadeHandler;

    private void Awake()
    {
        tweenUIFadeHandler = new TweenFadeHandler();
        tweenUIFadeHandler.Init(GetComponent<CanvasGroup>(), gameObject, .3f, .5f);


    }

    public void Show(FishingBaitGame gameController) {

        Controller = gameController;

        gameObject.SetActive(true);

        OnProgressUpdate();

        AttachListeners(Controller);
        
        RotateTarget(2f);

        tweenUIFadeHandler.Open();
    }

    private void AttachListeners(FishingBaitGame baitGameController) {
        baitGameController.OnTargetPopped += PopClosestCircle;
        baitGameController.TargetAdded += AddTarget;
        baitGameController.OnProgressUpdate += OnProgressUpdate;
        baitGameController.UpdateGame += OnGameUpdate;
        baitGameController.TargetSizeChanged += UpdateTargetSize;
    }


    private void RemoveListeners(FishingBaitGame baitGameController) {
        baitGameController.OnTargetPopped -= PopClosestCircle;
        baitGameController.TargetAdded -= AddTarget;
        baitGameController.OnProgressUpdate -= OnProgressUpdate;
        baitGameController.UpdateGame -= OnGameUpdate;
        baitGameController.TargetSizeChanged -= UpdateTargetSize;
    }


    private void OnGameUpdate(float deltaTime) {
        Targets.ForEach((x) => x.UpdateTarget(deltaTime));
    }

    public void ShowBaitFailure(Action onComplete)
    {
        baitFailedVisualsHandler.Interface(baitFailContainer.serialRef).Handler().Invoke(false, onComplete);
    }

    public void ShowBaitSuccess(Action onComplete)
    {
        baitedVisualsHandler.Interface(baitSuccess.serialRef).Handler().Invoke(true, onComplete);
    }



    /// <summary>
    /// Handler for controller adding a new target.
    /// </summary>
    /// <param name="target"></param>
    private void AddTarget(Target target)
    {
        UIBaitGameTarget t_inst = Instantiate(BaitGameTargetPrefab, targetTransform).GetComponent<UIBaitGameTarget>();
        t_inst.transform.rotation = BaitGameTargetPrefab.transform.rotation;

        float speed = GetNextTargetSpeed();

        t_inst.rotAnglePerSecond = speed * 360f;

        t_inst.Init(this, target, 2f);

        Targets.Add(t_inst);
    }


    /// <summary>
    /// Handler for controller progress changed.
    /// </summary>
    public void OnProgressUpdate()
    {
        FillBarImage.transform.parent.GetComponent<RectTransform>().DOShakeScale(.5f, .05f, 15, 45);

        FillBarImage.DOFillAmount(Controller.NormalizedProgress, .4f);

        if (shakeTween != null)
        {
            shakeTween.Kill(true);
            shakeTween = null;
        }

        if (Controller.NormalizedProgress < .25f)
        {
            shakeTween = FillBarImage.transform.parent.GetComponent<RectTransform>().DOShakeAnchorPos(.3f, 15, 20, 45).SetLoops(-1);
        }
    }


    /// <summary>
    /// Handler for when controller pops the target.
    /// </summary>
    /// <param name="progress"></param>
    public void PopClosestCircle(float progress)
    {
        //instead of popping the closest one, the target object should be passed down and checked.
        UIBaitGameTarget closest = Targets.FirstOrDefault();

        if (closest == null)
        {
            return;
        }

        foreach (UIBaitGameTarget t in Targets)
        {
            if (t.percentAnchorSize < closest.percentAnchorSize)
            {
                closest = t;
            }
        }

        float diff = Mathf.Abs(Controller.CenterMatchSize - closest.percentAnchorSize);

        diff = Mathf.Clamp01(diff);

        Targets.Remove(closest);
        closest.Pop(progress);
    }


    private void RotateTarget(float rotationTime)
    {
        float rotAnglePerSecond = -720f;

        RectTransform rectT = matchCenterCircle;

        float value = 0f;

        Vector3 startAngles = rectT.localEulerAngles;

        DOTween.To(
            () => value,
            (x) =>
            { value = x; rectT.localEulerAngles = startAngles + new Vector3(0, 0, x); },
            rotAnglePerSecond, rotationTime).SetEase(Ease.Linear).OnComplete(() => RotateTarget(rotationTime));
    }

    public void End()
    {
        foreach (UIBaitGameTarget t in Targets)
        {
            t.Pop(1f, true);
        }

        Targets.Clear();
        Debug.LogError("bait game ended");
        tweenUIFadeHandler.Close();
    }



    private void UpdateTargetSize(float size)
    {
        float currentSize = (.5f - matchCenterCircle.anchorMin.x) * 2f;
        DOTween.To(
            () => currentSize,
            (x) => { currentSize = x; SetPercentSize(matchCenterCircle, currentSize); }, size, .3f);
    }


    public float GetNextTargetSpeed() {
        return Random.Range(RotateSpeed, RotateSpeed * 3f);
    }

    //Helpers
    public Vector2 pointOnCircle(float radius, float time) {
        return PointOnCircle(radius, time, new Vector2(Screen.width / 2f, Screen.height / 2f));
    }

    public static Vector2 PointOnCircle(float radius, float angleInDegrees, Vector2 origin)
    {
        // Convert from degrees to radians via multiplication by PI/180        
        float x = (float)(radius * Math.Cos(angleInDegrees * Math.PI / 180F)) + origin.x;
        float y = (float)(radius * Math.Sin(angleInDegrees * Math.PI / 180F)) + origin.y;

        return new Vector2(x, y);
    }

    public static void SetPercentSize(RectTransform rectT, float size)
    {
        Vector2 center = new Vector2(.5f,.5f);

        size *= .5f;

        rectT.anchorMin = center - (Vector2.one * size);
        rectT.anchorMax = center + (Vector2.one * size);

        rectT.anchoredPosition = Vector2.zero;
        rectT.sizeDelta = Vector2.zero;
    }
}
