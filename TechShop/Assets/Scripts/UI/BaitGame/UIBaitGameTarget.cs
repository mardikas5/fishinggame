﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static FishingBaitGame;

public class UIBaitGameTarget : MonoBehaviour
{
    public Target _target;

    public float percentAnchorSize;
    public RectTransform rectT;

    public GameObject spawnOnPop;

    public float rotAnglePerSecond;

    public UIFishingBaitGame UIController;

    public Image targetImage;

    public Color nonMatchColor;
    public Color matchColor;

    public bool isPopped = false;

    public void Start()
    {
        rectT = GetComponent<RectTransform>();
    }

    public void UpdateTarget(float deltaTime)
    {
        SetSize(_target.Size);

        if (isPopped)
        {
            return;
        }

        SetColor(percentAnchorSize, UIController.Controller.CenterMatchSize);
    }


    private void SetColor(float currentSize, float centerMatchSize)
    {
        float diff = Mathf.Abs(currentSize - centerMatchSize) * 10f;

        diff *= diff;

        diff = Mathf.Min(1f, diff);

        targetImage.color = (matchColor * (1f - diff)) + (nonMatchColor * diff);
    }

    public void Init( UIFishingBaitGame baitGame, Target t, float Size, float rotationTime = 1f)
    {
        isPopped = false;

        _target = t;

        UIController = baitGame;

        SetSize(Size);

        StartRotation(rotationTime);
    }

    private void StartRotation(float rotationTime)
    {
        float value = 0f;

        Vector3 startAngles = rectT.localEulerAngles;

        DOTween.To(
            () => value,
            (x) =>
            { value = x; rectT.localEulerAngles = startAngles + new Vector3(0, 0, x); },
            rotAnglePerSecond, rotationTime).SetEase(Ease.Linear).OnComplete(() => StartRotation(rotationTime));
    }

    public void SetSize(float normalizedSize)
    {
        UIFishingBaitGame.SetPercentSize(rectT, normalizedSize);

        percentAnchorSize = normalizedSize;
    }

    public void Pop(float accuracy, bool silent = false)
    {
        float colorMultiplier = 0f;

        if (gameObject == null)
        {
            return;
        }

        if (silent)
        {
            Destroy(gameObject);
            return;
        }

        if (accuracy > 0f)
        {
            colorMultiplier = Mathf.Clamp01(accuracy * 15f);
        }

        Image img = GetComponent<Image>();

        img.DOFade(0f, .2f).OnComplete(() => Destroy(gameObject));

        GameObject obj = Instantiate(spawnOnPop, transform.parent, false);
        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;

        ParticleSystem.MainModule p = obj.GetComponent<ParticleSystem>().main;
        p.startColor = Color.white * colorMultiplier;

        RectTransform rt = obj.GetComponent<RectTransform>();

        float normalizedSize = (rectT.anchorMax.x - .5f) * 2f;
        rt.transform.localScale = Vector3.one * normalizedSize;

        rt.anchorMax = rectT.anchorMax;
        rt.anchorMin = rectT.anchorMin;

        rt.anchoredPosition = rectT.anchoredPosition;
        rt.sizeDelta = rectT.sizeDelta;

        isPopped = true;
    }

}
