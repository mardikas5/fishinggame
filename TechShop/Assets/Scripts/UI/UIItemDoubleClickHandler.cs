﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIItemDoubleClickHandler : MonoBehaviour
{
    public UIInventory InventoryUI;
    public UIItemEquipmentPanel EquipmentPanelUI;
    public UIItemDescriptionPanel ItemDescriptionUI;

    public bool interactableEquipment = true;

    private UIItem activeItem;

    private void Awake()
    {
        if (InventoryUI == null)
        {
            InventoryUI = GetComponent<UIInventory>();
        }
        if (EquipmentPanelUI == null)
        {
            EquipmentPanelUI = UI.Instance.ItemEquipmentPanelUI;
        }
        if (ItemDescriptionUI == null)
        {
            ItemDescriptionUI = UI.Instance.ItemDescriptionPanelUI;
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        InventoryUI.OnItemDoubleClicked += OnItemDoubleClicked;
        InventoryUI.OnItemHovered += OnItemHovered;
        InventoryUI.OnItemHoverExit += OnItemHoverExit;
    }

    private void OnItemHoverExit(UIItem obj)
    {
        ItemDescriptionUI.Close();
    }

    private void Close()
    {
        EquipmentPanelUI.Close();
    }

    private void OnItemHovered(UIItem obj)
    {
        if (!obj.IsDragging)
        {
            ItemDescriptionUI.Open();
        }

        ItemDescriptionUI.Init(obj);
    }

    private void OnDisable()
    {
        ItemDescriptionUI.Close();
    }

    private void OnItemDoubleClicked(UIItem obj)
    {
        activeItem = obj;
        EquipmentPanelUI.Open(obj.Item, interactableEquipment);
    }
}
