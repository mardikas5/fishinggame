﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFishingMapPanel : MonoBehaviour, IGenericUIPanel
{
    public CanvasGroup canvasGroup;
    public UIFishingSpot FishingSpotPrefab;

    public TweenFadeHandler notEnoughMoneyHandler;
    public TweenFadeHandler fadeHandler;

    public RectTransform playerLocation;

    public bool IsOpen = false;

    public FishingMapController MapController;

    public float PlayerMoveSpeed = .5f;

    public UIFishingSpot target;

    public List<UIFishingSpot> uiFishingSpots = new List<UIFishingSpot>();
    public UIFishingSpot home;
    public UIFishingSpot currentSpot;

    public GameObject notEnoughMoneyPanel;

    public event Action<IGenericUIPanel> PanelOpened;
    public event Action<IGenericUIPanel> PanelClosed;

    public bool Locked = false;

    public void Close()
    {
        if (Locked)
        {
            return;
        }
        notEnoughMoneyHandler.Close();
        IsOpen = false;
        PanelClosed?.Invoke(this);
        fadeHandler.Close();
    }

    public void Open()
    {
        if (Locked)
        {
            return;
        }
        PanelOpened?.Invoke(this);
        IsOpen = true;
        fadeHandler.Open();
    }

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    private void Start()
    {
        currentSpot = home;

        canvasGroup.alpha = 0f;

        MapController = GameLogic.Instance.FishingMapController;

        fadeHandler = new TweenFadeHandler();
        fadeHandler.Init(canvasGroup, gameObject, 1f, .3f);

        notEnoughMoneyHandler = new TweenFadeHandler();
        notEnoughMoneyHandler.Init(notEnoughMoneyPanel.GetComponent<CanvasGroup>(), notEnoughMoneyPanel.gameObject, .2f, .5f);

        foreach (UIFishingSpot spot in uiFishingSpots)
        {
            UIFishingSpot localRef = spot;
            spot.GetComponent<Button>().onClick.AddListener(() => FishingSpotClicked(localRef));
        }

        home.fishingSpot.Known = true;
        home.fishingSpot.Unlocked = true;
        home.GetComponent<Button>().onClick.AddListener(() => FishingSpotClicked(home));
    }

    public void FishingSpotClicked(UIFishingSpot fishSpot)
    {
        if (Locked)
        {
            return;
        }

        FishingSpot spot = fishSpot.fishingSpot;

        if (fishSpot == currentSpot)
        {
            return;
        }

        if (!spot.Unlocked)
        {
            string cost = spot.CostToUnlock.ToString("F1");
            UI.Instance.PurchasePopupUI.ShowPopUp("Purchase spot for: " + cost + " ?",() => TryPurchase(spot), null);
            return;
        }
        else
        {
            string text = " Travel to fishing spot? ";
            if (!string.IsNullOrEmpty(fishSpot.CustomText))
            {
                text = fishSpot.CustomText;
            }
            UI.Instance.PurchasePopupUI.ShowPopUp(text, () => { target = fishSpot; UI.Instance.FadeInPanel(); Locked = true; }, null);
        }
    }

    private void TryPurchase(FishingSpot spot)
    {
        if (GameLogic.Instance.playerMoney.RemoveAmount(spot.CostToUnlock))
        {
            spot.Unlock();
        }
        else
        {
            notEnoughMoneyHandler.Open(false);
            this.InvokeDelayed(1f, () => notEnoughMoneyHandler.Close());
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (target != null)
        {
            Vector3 newPos = Vector3.MoveTowards(playerLocation.transform.position, target.transform.position, PlayerMoveSpeed);

            playerLocation.transform.position = newPos;

            if (Vector3.Distance(playerLocation.transform.position, target.transform.position) < .2f)
            {
                OnReachedTarget(target);
                target = null;
            }
        }
    }

    private void OnReachedTarget(UIFishingSpot target)
    {
        Locked = false;
        UI.Instance.FadeOutPanel();
        currentSpot = target;
        Close();
        GameLogic.Instance.FishingMapController.CurrentSpot = target.fishingSpot;

        if (target == home)
        {
            GameLogic.Instance.FishingMapController.SetPlayerOnCoast();
            return;
        }
        GameLogic.Instance.FishingMapController.SetPlayerFishing();
    }

    public void Toggle()
    {
        if (IsOpen)
        {
            Close();
            return;
        }
        Open();
    }
}
