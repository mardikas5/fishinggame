﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIFishCaptureResult : MonoBehaviour
{
    public TextMeshProUGUI fishTitleText;
    public TextMeshProUGUI fishDescriptionText;
    public Image fishSprite;

    public TextMeshProUGUI Rarity;
    public TextMeshProUGUI Size;
    public TextMeshProUGUI Value;

    public CanvasGroup canvasGroup;
    public TweenFadeHandler fadeHandler;

    private bool isInit = false;

    private void Init()
    {
        if (isInit)
        {
            return;
        }

        if (canvasGroup == null)
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }
        fadeHandler = new TweenFadeHandler();
        fadeHandler.Init(canvasGroup, gameObject, .3f, .5f);
        isInit = true;
    }

    public void Awake()
    {
        Init();
    }

    public void Start()
    {
        canvasGroup.alpha = 0f;
    }

    public void DisplayResult(Fish capturedFish, bool open = true)
    {
        Init();

        if (open)
        {
            Open();
        }

        string itemDesc = capturedFish.Base?.GetComponentSO<ItemBase>()?.Description;

        if (itemDesc == null)
        {
            itemDesc = capturedFish.Base.Description;
        }

        GetComponent<AudioClipContainer>().PlayRandom(GetComponent<AudioSource>());

        fishTitleText.text = capturedFish.Base.Name;
        fishSprite.sprite = capturedFish.FishBase.Sprite;
        fishDescriptionText.text = itemDesc;

        Rarity.text = capturedFish.FishBase.Rarity;
        Size.text = capturedFish.Size.ToString("F1");
        Value.text = capturedFish.Value().ToString("F");
    }

    public void Open()
    {
        fadeHandler.Open();
    }

    public void Close()
    {
        fadeHandler.Close();
    }
}
