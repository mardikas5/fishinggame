﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerEnergy : MonoBehaviour
{
    public Image EnergyBar;
    public TextMeshProUGUI EnergyText;

    private PlayerEnergyController Controller;

    private float CurrentAmount;

    private void OnEnable()
    {
        //eh.
        CurrentAmount = GameLogic.Instance.Player.EnergyController.CurrentEnergy;

        UpdateEnergyFill(CurrentAmount);
    }

    private void Awake()
    {
        Controller = GameLogic.Instance.Player.EnergyController;

        Controller.OnAmountUpdated += (x) => UpdateEnergyFill(x);

        UpdateEnergyFill(CurrentAmount);
    }

    public void UpdateEnergyFill(float newAmount)
    {
        DOTween.To(() => CurrentAmount, SetText, newAmount, .8f);

        EnergyBar.DOFillAmount(newAmount / Controller.MaxEnergy, .8f);
    }

    private void SetText(float pNewValue)
    {
        CurrentAmount = pNewValue;

        EnergyText.text = $"{(int)pNewValue} / {Controller.MaxEnergy}";
    }
}
