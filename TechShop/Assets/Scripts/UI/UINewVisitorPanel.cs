﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using System;

public class UINewVisitorPanel : MonoBehaviour
{
    public RectTransform visitorPanel;
    public TextMeshProUGUI visitorText;
    public IslandEvents islandEvents;
    [NonSerialized]
    private bool isOpen = true;

    private TweenFadeHandler fadeHandler = new TweenFadeHandler();

    public void Start()
    {
        fadeHandler.Init(GetComponent<CanvasGroup>(), gameObject, .5f, 2f);
        islandEvents = GameLogic.Instance.IslandEvents;
        islandEvents.OnEventUpdate += OnEventUpdate;
        Close();
    }

    private void OnEventUpdate()
    {
        if (islandEvents.ActiveEvent == null)
        {
            ShowMessage("A visitor has left the island...");
            return;
        }
        ShowMessage("A visitor is passing by...");
    }

    public void ShowMessage(string text, float time = 5f)
    {
        visitorText.text = text;
        Open();
        GameLogic.Instance.InvokeDelayed(time, Close);
    }

    public void Open()
    {
        if (isOpen)
        {
            return;
        }
        isOpen = true;
        fadeHandler.Open();
        visitorPanel.DOAnchorPos(Vector2.zero, .5f);
    }

    public void Close()
    {
        if (!isOpen)
        {
            return;
        }
        isOpen = false;
        fadeHandler.Close();
        visitorPanel.DOAnchorPos(new Vector2(visitorPanel.rect.width, 0f), .5f);
    }
}
