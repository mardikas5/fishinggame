﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class UIPlayerMoney : MonoBehaviour
{
    public TextMeshProUGUI amountText;

    public PlayerMoney money;

    private float UIMoneyAmount = 0f;

    public float updateValueInterval = .05f;

    private float lastUpdateTime;

    private TweenerCore<float, float, FloatOptions> activeTween;

    public UnityEvent OnMoneyValueChange;

    public event Action OnMoneyValueChanged;

    private void Awake()
    {
        OnMoneyValueChanged += () => OnMoneyValueChange?.Invoke();
        money.AmountUpdated += OnMoneyUpdated;
    }

    private void OnMoneyUpdated(float endValue)
    {
        Debug.Log("money update: " + endValue);

        float difference = Mathf.Abs(UIMoneyAmount - endValue);
        float duration = 1f + (4f * Mathf.Log10(difference));

        if (activeTween != null)
        {
            activeTween.Kill(false);
        }

        activeTween = DOTween.To(() => UIMoneyAmount, OnUpdateMoney, endValue, duration).SetEase(Ease.OutExpo).OnComplete(() =>
        {
            activeTween = null;
        });

    }

    private void OnUpdateMoney(float pNewValue)
    {
        UIMoneyAmount = pNewValue;
        string oldText = amountText.text;

        //Fixed point.
        amountText.text = UIMoneyAmount.ToString("F");

        if (oldText != amountText.text)
        {
            if (lastUpdateTime + updateValueInterval < Time.time)
            {
                OnMoneyValueChanged?.Invoke();
                lastUpdateTime = Time.time;
            }
        }
    }

    public void Start()
    {
        OnMoneyUpdated(money.Amount);
    }
}
