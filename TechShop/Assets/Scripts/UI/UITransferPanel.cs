﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UITransferPanel : MonoBehaviour
{
    public UIInventory UIPlayerInventory;
    public UIInventory UIChestInventory;

    private Inventory boxInv;
    private Inventory playerInv;

    public bool isOpen = false;

    public void SetInventoryBox(Inventory boxInventory)
    {
        boxInv = boxInventory;
    }

    public void SetInventoryPlayer(Inventory player)
    {
        playerInv = player;
    }

    public void Open()
    {
        if (boxInv != null && playerInv != null)
        {
            Open(playerInv, boxInv);
        }
    }

    public void Open(Inventory playerInventory, Inventory chestInventory)
    {
        gameObject.SetActive(true);

        if (isOpen) { return; }
        isOpen = true;

        UIPlayerInventory.Generate(playerInventory);
        UIChestInventory.Generate(chestInventory);

        UIPlayerInventory.OnItemClicked += OnItemClicked;
        UIChestInventory.OnItemClicked += OnItemClicked;
    }

    public void Close()
    {
        if (!isOpen) { return; }

        isOpen = false;

        UIPlayerInventory.OnItemClicked -= OnItemClicked;
        UIChestInventory.OnItemClicked -= OnItemClicked;

        gameObject.SetActive(false);
        
        UIPlayerInventory.Close(false);
        UIChestInventory?.Close(false);
    }

    private void OnItemClicked(UIItem arg1, PointerEventData arg2)
    {
        if (!Input.GetKey(KeyCode.LeftControl))
        {
            return;
        }

        if (arg1.UIContainer == UIChestInventory)
        {
            UIPlayerInventory.Container.TryPlaceAnySlot(arg1.Item);
        }
        else
        {
            UIChestInventory.Container.TryPlaceAnySlot(arg1.Item);
        }
    }
}
