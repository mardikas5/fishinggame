﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class UINoticeSmall : MonoBehaviour
{
    public Notice Notice;

    public TextMeshProUGUI smallObjText;

    public event Action OnClicked;

    public void Init(Notice n)
    {
        smallObjText.text = n.Title;
    }

    public void OnClick()
    {
        OnClicked?.Invoke();
    }
}
