﻿using Fishy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using static Recipe;

public class UIRecipeInputsPanel : MonoBehaviour
{
    public UIInventory slotInventoryPrefab;
    public RectTransform contentParent;

    public List<Inventory> activeSlots = new List<Inventory>();
    public List<UIInventory> activeUISlots = new List<UIInventory>();
    public List<Requirement> requirements = new List<Requirement>();

    public Recipe ActiveRecipe;
    public event Action<Recipe> RecipeStarted;

    public TextMeshProUGUI titleText;
    public ItemProcessor processor;

    public List<Item> GetOrderedItems()
    {
        List<Item> items = new List<Item>();
        for (int i = 0; i < activeSlots.Count; i++)
        {
            List<Item> iList = activeSlots[i].ItemsList;
            if (iList.Count > 0)
            {
                items.Add(iList[0]);
            }
        }
        return items;
    }

    public void Clear()
    {
        foreach (Inventory i in activeSlots)
        {
            Item item = i.ItemsList.FirstOrDefault((x) => x != null);
            if (item != null)
            {
                GiveUserItem(item);
            }
            Destroy(i);

        }
        foreach (UIInventory i in activeUISlots)
        {
            i.Close(true);
        }

        ActiveRecipe = null;
        activeSlots.Clear();
        requirements.Clear();
        activeUISlots.Clear();
    }

    private void GiveUserItem(Item item)
    {
        if (processor.User != null)
        {
            if (processor.User?.GetComponentInChildren<Inventory>()?.TryPlaceAnySlot(item) == true)
            {
                return;
            }
        }

        EntityFactory.CreateInWorldStatic(item, processor.transform.position + Vector3.up);
    }

    public void SetRecipe(Recipe recipe, ItemProcessor itemProcessor)
    {
        Clear();

        processor = itemProcessor;

        ActiveRecipe = recipe;

        titleText.text = "recipe: " + recipe.Name;

        for (int i = 0; i < recipe.Requirements.Length; i++)
        {
            GenerateSlot(recipe.Requirements[i]);
        }
    }

    private UIInventory GenerateSlot(Requirement req)
    {
        Inventory _inv = gameObject.AddComponent<Inventory>();
        _inv.Init(new Int2(1, 1));
        _inv.IgnoreItemSize = true;
        requirements.Add(req);

        activeSlots.Add(_inv);

        UIInventory _uiInv = Instantiate(slotInventoryPrefab, contentParent);
        _uiInv.IsSingleSlotInventory = true;
        _uiInv.Generate(_inv, true);

        _uiInv.GetComponent<UIRecipeItemSlot>()?.SetRequirement(req);

        activeUISlots.Add(_uiInv);
        return _uiInv;
    }

    public void SignalStart()
    {
        RecipeStarted?.Invoke(ActiveRecipe);
    }

    public void OnDisable()
    {
        Clear();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }
}
