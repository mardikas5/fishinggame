﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPlayerFish : MonoBehaviour
{
    public TextMeshProUGUI AmountText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        AmountText.text = GameLogic.Instance.Player.InventoryController.Inventory.ItemsList.Count.ToString();
    }
}
