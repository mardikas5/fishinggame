﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerStats : MonoBehaviour
{
    public UIPlayerStat PlayerStatPrefab;

    public RectTransform StatsContentParent;

    public List<UIPlayerStat> UIPlayerStatsObjects = new List<UIPlayerStat>();

    public PlayerStats PlayerStats;


    public void Generate()
    {
        foreach (PlayerStats.StatValue t in PlayerStats.AllStats)
        {
            UIPlayerStat _inst = Instantiate(PlayerStatPrefab, StatsContentParent);
            _inst.SetStat(t);
            UIPlayerStatsObjects.Add(_inst);
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        PlayerStats = GameLogic.Instance.Player.GetComponent<PlayerStats>();

        Generate();
    }
}
