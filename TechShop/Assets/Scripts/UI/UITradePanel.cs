﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class UITradePanel : MonoBehaviour, IGenericUIPanel
{
    public UIInventory UIPlayerInventory;
    public UIInventory UIStoreInventory;
    //items player is selling.
    public UIInventory UIPlayerSellInventory;
    //items player is buying.
    public UIInventory UIStoreBuyInventory;

    public TextMeshProUGUI sellAmount;
    public TextMeshProUGUI buyCostAmount;

    public TextMeshProUGUI playerFunds;
    public TextMeshProUGUI shopFunds;

    public TradeController TradeController;

    public GameObject notEnoughGoldPanel;

    public AudioClipContainer buyItemCompleteClips;

    private Coroutine closeDelay;
    private TweenFadeHandler fadeHandler = new TweenFadeHandler();

    private bool IsOpen = false;


    public event Action<IGenericUIPanel> PanelOpened;
    public event Action<IGenericUIPanel> PanelClosed;

    public void Open(TradeController tradeController, Inventory storeInventory, Inventory playerInventory, Inventory playerSellInventory, Inventory storeBuyInventory)
    {
        if (IsOpen)
        {
            Close();
        }

        gameObject.SetActive(true);


        TradeController = tradeController;

        UIPlayerInventory.Generate(playerInventory, false);
        UIStoreInventory.Generate(storeInventory, false);
        UIPlayerSellInventory.Generate(playerSellInventory, false);
        UIStoreBuyInventory.Generate(storeBuyInventory, false);

        SetItemValueDrawer(UIPlayerInventory, TradeController.Merchant.ItemSellValue);
        SetItemValueDrawer(UIPlayerSellInventory, TradeController.Merchant.ItemSellValue);
        SetItemValueDrawer(UIStoreInventory, TradeController.Merchant.ItemBuyValue);
        SetItemValueDrawer(UIStoreBuyInventory, TradeController.Merchant.ItemBuyValue);

        BuyingChanged(null);
        SellingChanged(null);

        OnShopMoneyUpdated(TradeController.Merchant.shopMoney);
        OnPlayerMoneyUpdated(GameLogic.Instance.playerMoney.Amount);

        IsOpen = true;

        PanelOpened?.Invoke(this);

        Sub();

        fadeHandler.Init(notEnoughGoldPanel.GetComponent<CanvasGroup>(), notEnoughGoldPanel.gameObject, .2f, .5f);
        fadeHandler.Close();
    }

    private void SetItemValueDrawer(UIInventory UIInventory, Func<Item, float> valueHandler)
    {
        UIInventoryItemValueHandler _UIInv = UIInventory.GetComponent<UIInventoryItemValueHandler>();
        if (_UIInv == null)
        {
            _UIInv = UIInventory.gameObject.AddComponent<UIInventoryItemValueHandler>();
        }

        _UIInv.SetValueHandler(UIInventory, valueHandler);
    }

    private void StoreBuyItemClicked(UIItem obj, PointerEventData eventArgs)
    {
        if (eventArgs.button == PointerEventData.InputButton.Right)
        {
            RightClickHandler(obj, false);
            return;
        }

        UIStoreInventory.Inventory.TryPlaceAnySlot(obj.Item);
    }

    private void StoreItemClicked(UIItem obj, PointerEventData eventArgs)
    {
        if (eventArgs.button == PointerEventData.InputButton.Right)
        {
            RightClickHandler(obj, false);
            return;
        }

        UIStoreBuyInventory.Inventory.TryPlaceAnySlot(obj.Item);
    }

    private void SellItemClicked(UIItem obj, PointerEventData eventArgs)
    {
        if (eventArgs.button == PointerEventData.InputButton.Right)
        {
            RightClickHandler(obj, true);
            return;
        }

        UIPlayerInventory.Inventory.TryPlaceAnySlot(obj.Item);
    }

    private void PlayerItemClicked(UIItem obj, PointerEventData eventArgs)
    {
        if (eventArgs.button == PointerEventData.InputButton.Right)
        {
            RightClickHandler(obj, true);
            return;
        }

        UIPlayerSellInventory.Inventory.TryPlaceAnySlot(obj.Item);
    }

    private void NotEnoughGoldHandler()
    {
        if (closeDelay != null)
        {
            StopCoroutine(closeDelay);
            closeDelay = null;
        }

        fadeHandler.Open(false);
        closeDelay = this.InvokeDelayed(1f,() => fadeHandler.Close());
    }



    private void Sub()
    {
        TradeController.Merchant.AmountUpdated += OnShopMoneyUpdated;
        TradeController.NotEnoughMoneySignal += NotEnoughGoldHandler;
        //TradeController.BuyCompleted += OnItemsBought;
        //TradeController.SellCompleted += OnItemsSold;

        UIPlayerSellInventory.Inventory.ItemAdded += SellingChanged;
        UIPlayerSellInventory.Inventory.ItemRemoved += SellingChanged;

        UIStoreBuyInventory.Inventory.ItemAdded += BuyingChanged;
        UIStoreBuyInventory.Inventory.ItemRemoved += BuyingChanged;

        GameLogic.Instance.playerMoney.AmountUpdated += OnPlayerMoneyUpdated;

        //items
        UIPlayerInventory.OnItemClicked += PlayerItemClicked;
        UIPlayerSellInventory.OnItemClicked += SellItemClicked;

        UIStoreInventory.OnItemClicked += StoreItemClicked;
        UIStoreBuyInventory.OnItemClicked += StoreBuyItemClicked;
    }



    private void UnSub()
    {
        TradeController.Merchant.AmountUpdated -= OnShopMoneyUpdated;
        TradeController.NotEnoughMoneySignal -= NotEnoughGoldHandler;
        //TradeController.BuyCompleted -= OnItemsBought;
        //TradeController.SellCompleted -= OnItemsSold;

        UIPlayerSellInventory.Inventory.ItemAdded -= SellingChanged;
        UIPlayerSellInventory.Inventory.ItemRemoved -= SellingChanged;

        UIStoreBuyInventory.Inventory.ItemAdded -= BuyingChanged;
        UIStoreBuyInventory.Inventory.ItemRemoved -= BuyingChanged;

        GameLogic.Instance.playerMoney.AmountUpdated -= OnPlayerMoneyUpdated;

        //items
        UIPlayerInventory.OnItemClicked -= PlayerItemClicked;
        UIPlayerSellInventory.OnItemClicked -= SellItemClicked;

        UIStoreInventory.OnItemClicked -= StoreItemClicked;
        UIStoreBuyInventory.OnItemClicked -= StoreBuyItemClicked;
    }

    private void RightClickHandler(UIItem obj, bool isPlayerItem)
    {
        UI.Instance.ItemEquipmentPanelUI.Open(obj.Item, isPlayerItem);
    }

    private void OnShopMoneyUpdated(float obj)
    {
        shopFunds.text = obj.ToString("F");
    }

    private void OnPlayerMoneyUpdated(float obj)
    {
        playerFunds.text = obj.ToString("F");
    }

    private void BuyingChanged(Item obj)
    {
        buyCostAmount.text = TradeController.CurrentBuyValue().ToString("F");
    }

    private void SellingChanged(Item obj)
    {
        sellAmount.text = TradeController.CurrentSellValue().ToString("F");
    }

    public void Buy()
    {
        TradeController.Buy();
    }

    public void Sell()
    {
        TradeController.Sell();
    }

    public void Close()
    {
        PanelClosed?.Invoke(this);

        UnSub();

        TradeController.Close();

        UIPlayerInventory.Close(false);
        UIStoreInventory.Close(false);
        UIPlayerSellInventory.Close(false);
        UIStoreBuyInventory.Close(false);

        gameObject.SetActive(false);
    }
}
