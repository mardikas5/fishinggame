﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFamilyTab : MonoBehaviour
{
    public UIFamilyAttribute FamilyNeedPrefab;

    public Transform needParentTransform;

    public FamilyController Controller;

    public List<UIFamilyAttribute> UINeeds = new List<UIFamilyAttribute>();

    public void Start()
    {
        Init(GameLogic.Instance.FamilyController);
    }

    public void Init(FamilyController controller)
    {
        Controller = controller;

        Generate();
    }

    public void Generate()
    {
        foreach (FamilyNeed t in Controller.Needs)
        {
            UIFamilyAttribute inst = Instantiate(FamilyNeedPrefab, needParentTransform);
            inst.Init(t);
        }
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
