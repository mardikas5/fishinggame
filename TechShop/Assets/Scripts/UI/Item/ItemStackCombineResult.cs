﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ItemStackCombineResult
{
    public Item StackIntoItem;
    public Item StackFromItem;

    public int StackIntoAmount;
    public int StackFromAmount;

}
