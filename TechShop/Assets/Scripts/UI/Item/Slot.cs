using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public static Color DefaultColor    = new Color(.2f,.2f,.2f,.9f);
    public static Color CanPlaceColor   = new Color(.1f,.5f,.1f,.7f);
    public static Color BlockedColor    = new Color(.5f, 0f, 0f,.8f);
    public static Color BlockingColor   = new Color(.5f, 0f,.2f,.7f);
    public static Color OccupiedColor   = new Color(.0f,.0f,.0f,.9f);

    public enum SlotState
    {
        None,
        Default,
        Placing,
        Blocking,
        Occupied,
        Blocked
    }

    private static Dictionary<SlotState, Color> States = new Dictionary<SlotState, Color>()
    {
        [SlotState.None]        = DefaultColor,
        [SlotState.Default]     = DefaultColor,
        [SlotState.Placing]     = CanPlaceColor,
        [SlotState.Blocking]    = BlockingColor,
        [SlotState.Occupied]    = OccupiedColor,
        [SlotState.Blocked]     = BlockedColor
    };


    public SlotState State { get => _state; set => SetState(value); }

    [SerializeField]
    private SlotState _state;

    private SlotState lastUpdateState;

    public Action OnMouseDownSignal;
    public Action OnMouseOverSignal;
    public Action OnDirty;

    [NonSerialized]
    public ItemBase item;

    public Int2 Position;

    public IUIItemContainer UIContainer { get; private set; }
    public bool IsSingleSlotInventory = false;

    public Image Image;
    public TextMeshProUGUI overlayText;

    public TweenerCore<Color, Color, ColorOptions> tween;


    public void Awake()
    {
        if (Image == null)
        {
            Image = GetComponent<Image>();
        }

        OnDirty += EvaluateDirty;
    }

    public virtual void Init(IUIItemContainer UII)
    {
        UIContainer = UII;
        Image.color = DefaultColor;
        SetState(SlotState.Default);
    }

    public void OnEnable()
    {
        EvaluateDirty();
    }

    public void SetState(SlotState State)
    {
        if (_state != State)
        {
            _state = State;
            OnDirty?.Invoke();
        }
    }

    protected void EvaluateDirty()
    {
        if (lastUpdateState != State)
        {
            lastUpdateState = State;
            TweenColors();
        }
    }

    private void TweenColors()
    {
        if (tween != null)
        {
            if (tween.endValue == States[State])
            {
                return;
            }

            tween.Kill();
        }

        tween = Image.DOColor(States[State], .35f);
    }

    public void SetOverlay(string text)
    {
        if (overlayText == null)
        {
            return;
        }

        if (string.IsNullOrEmpty(text))
        {
            overlayText.gameObject.SetActive(false);
            return;
        }

        overlayText.gameObject.SetActive(true);

        overlayText.text = text;
    }

    public void OnMouseOver()
    {
        OnMouseOverSignal?.Invoke();
    }

    public void OnMouseDown()
    {
        OnMouseDownSignal?.Invoke();
    }
}
