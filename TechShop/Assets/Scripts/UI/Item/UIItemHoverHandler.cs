﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIItemHoverHandler : MonoBehaviour
{
    public UIInventory UIInv;
    public AudioClipContainer ClipContainer;

    void Attach()
    {
        UIInv = GetComponent<UIInventory>();
        UIInv.OnItemHovered += OnItemHover;
    }

    private void OnItemHover(UIItem obj)
    {
        if (obj.IsDragging)
        {
            return;
        }

        ClipContainer.PlayRandom(GetComponent<AudioSource>());
    }

    // Start is called before the first frame update
    void Awake()
    {
        Attach();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
