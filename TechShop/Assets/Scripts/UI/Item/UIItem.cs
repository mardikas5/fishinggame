using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// UI for item.
/// </summary>
public class UIItem : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerClickHandler, IUIEntity, IPointerEnterHandler, IPointerExitHandler
{
    /// <summary>
    /// UI container for item.
    /// </summary>
    public UIInventory UIContainer;

    public RectTransform Moveable;

    public TextMeshProUGUI StackSizeText;

    public TextMeshProUGUI ItemCostText;

    public Item Item;

    public Image Image;

    public Image Frame;

    private float lastClickTime = 0f;

    public event Action<UIItem, PointerEventData> OnItemClicked;

    public event Action<UIItem> OnDoubleClicked;

    public event Action<UIItem> OnItemHovered;

    public event Action<UIItem> OnItemHoverExit;

    public Vector2 manualOffset = Vector2.zero;

    public TweenerCore<float, float, FloatOptions> sizeFade;

    public bool IsDragging;
    public bool showFrame;
    public bool ScalingEnabled;

    public Vector2 GetScreenOffset
    {
        get
        {
            Vector2 offset = Moveable.sizeDelta / 2f;
            if (Item.Rotation == Rotation.Horizontal)
            {
                return new Vector2(-offset.x, offset.y);
            }
            else
            {
                return new Vector2(-offset.x, offset.y);
            }
        }
    }

    public Vector2 GetDragOffset
    {
        get
        {
            Vector2 offset = new Vector2((float)UIInventory.GridSize.x / 2f, -(float)UIInventory.GridSize.y / 2f);
            Vector2 center = GetScreenOffset + offset;

            return center;
        }
    }


    public static Vector2 GetInventorySize(Int2 Size)
    {
        float height =  Size.y * UIInventory.GridSize.y;
        float width =   Size.x * UIInventory.GridSize.x;

        height += (Size.y - 1) * UIInventory.Spacing.y;
        width += (Size.x - 1) * UIInventory.Spacing.x;

        return new Vector2(width, height);
    }

    private void OnItemChangedHandler()
    {
        if (UIContainer?.Container != Item?.Container)
        {
            Destroy(gameObject);
            return;
        }

        Initialize(Item, UIContainer);
    }

    [ContextMenu("Debug container")]
    public void DebugContainer()
    {
        Debug.LogError(Item?.Container?.Title);
    }

    private void OnDestroy()
    {
        OnPointerExit(null);
        Item.ItemUpdated -= OnItemChangedHandler;
    }


    public void Initialize(Item t, UIInventory Container)
    {
        if (t == null)
        {
            Destroy(gameObject);
            return;
        }

        Item = t;

        t.ItemUpdated -= OnItemChangedHandler;
        t.ItemUpdated += OnItemChangedHandler;

        this.UIContainer = Container;

        Frame.gameObject.SetActive(showFrame);

        if (t.CurrentStack > 1)
        {
            StackSizeText.text = t.CurrentStack.ToString();
        }
        else
        {
            StackSizeText.text = "";
        }

        Moveable.anchorMax = new Vector2(0, 1);
        Moveable.anchorMin = new Vector2(0, 1);
        Moveable.pivot = new Vector2(.5f, .5f);

        Image.sprite = t.Sprite;// SpriteSheetLoader.LoadSingleFromSheet(t.Sprite);

        Moveable.sizeDelta = GetInventorySize(t.Size);

        RectTransform RT = Moveable.GetComponent<RectTransform>();
        RT.pivot = new Vector2(0.5f, 0.5f);
        RT.sizeDelta = new Vector2(RT.sizeDelta.x, RT.sizeDelta.y);

        //only turn the image, otherwise text components (stack and value of item) get flipped too.
        if (t.Rotation == Rotation.Vertical)
        {
            Image.rectTransform.eulerAngles = new Vector3(0, 0, -90f);
        }
        if (t.Rotation == Rotation.Horizontal)
        {
            Image.rectTransform.eulerAngles = new Vector3(0, 0, 0f);
        }
    }

    public PointerEventData SwapRotation()
    {
        if (Item.Rotation == Rotation.Horizontal)
        {
            Item.Rotation = Rotation.Vertical;
        }
        else
        {
            Item.Rotation = Rotation.Horizontal;
        }

        Initialize(Item, UIContainer);

        PointerEventData pointerData = new PointerEventData( EventSystem.current );
        pointerData.position = Input.mousePosition + new Vector3(1f, 1f, 1f);

        //Properly updates the location after a frame is complete.
        this.InvokeDelayed(Time.deltaTime, () => OnDrag(pointerData));

        return pointerData;
    }


    public void OnDrag(PointerEventData eventData)
    {
        if (!UIContainer.Interactable)
        {
            return;
        }

        IsDragging = true;
        Frame.gameObject.SetActive(true);
        UIInventoryDragHandler.Instance.SetCurrentlyDragging(this, eventData);
        UIInventoryDragHandler.Instance.OnDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!UIContainer.Interactable)
        {
            return;
        }

        IsDragging = false;
        Frame.gameObject.SetActive(showFrame);
        UIInventoryDragHandler.Instance.PlaceCurrentlyDragging(eventData);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (lastClickTime + UI.Instance.DoubleClickInterval > Time.time)
        {
            OnDoubleClicked?.Invoke(this);
            return;
        }

        lastClickTime = Time.time;

        OnItemClicked?.Invoke(this, eventData);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.SetAsLastSibling();
        OnItemHovered?.Invoke(this);
        ScaleUp();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnItemHoverExit?.Invoke(this);

        ScaleDown();
    }

    public void ScaleUp()
    {
        if (!ScalingEnabled)
        {
            return;
        }

        CheckFade();

        sizeFade = DOTween.To(() => transform.localScale.x, (x) => transform.localScale = Vector3.one * x, 1.1f, .1f);
    }

    public void ScaleDown()
    {
        CheckFade();

        sizeFade = DOTween.To(() => transform.localScale.x, (x) => transform.localScale = Vector3.one * x, 1f, .1f);
    }

    private void CheckFade()
    {
        if (sizeFade != null)
        {
            sizeFade.Kill();
            sizeFade = null;
        }
    }
}
