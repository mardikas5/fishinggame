﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ISlot
{
    Item ActiveItem { get; }

    //Equipment.SlotType Type { get; }

    event Action<Item> ActiveItemChanged;

    bool CanEquip(Item item);

    bool TryEquip(Item item);
}
