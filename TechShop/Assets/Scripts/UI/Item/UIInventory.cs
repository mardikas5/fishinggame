using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInventory : MonoBehaviour, IUIItemContainer
{
    public static Int2 GridSize = new Int2(75, 75);
    public static Int2 Padding  = new Int2(10, 10);
    public static Int2 Spacing  = new Int2(2, 2);
    public static Int2 MinSize  = new Int2(100, 100);

    public Inventory Container { get => Inventory; set => Inventory = value as Inventory; }

    public Inventory Inventory;
    public RectTransform Parent;
    public RectTransform View;
    public RectTransform UIItemParent;

    public TextMeshProUGUI TitleText;

    public UIItem ItemPrefab;
    public Slot SlotPrefab;

    [SerializeField]
    public LinearMap2D<Slot> _slots;

    public LinearMap2D<Slot> Slots => _slots;

    public bool Interactable = true;
    public bool itemFrame = true;
    [SerializeField]
    private List<UIItem> UIItems = new List<UIItem>();

    public ReadOnlyCollection<UIItem> Items => UIItems.AsReadOnly();

    private HashSet<Slot> dirtySlots = new HashSet<Slot>();

    /// <summary>
    /// Have the unity layoutgroups been allowed a frame to resize.
    /// </summary>
    public bool isUnityInited = false;

    public bool IsSingleSlotInventory = false;

    public bool ItemScaleSizeOnHover = true;

    public event Action OnClosed;

    public event Action<UIItem> OnItemDrawn;

    public event Action<UIItem, PointerEventData> OnItemClicked;

    public event Action<UIItem> OnItemDoubleClicked;

    public event Action<UIItem> OnItemHovered;

    public event Action<UIItem> OnItemHoverExit;

    public UIInventory Generate(Inventory inventory, bool interactable = true)
    {
        Inventory = inventory;

        Interactable = interactable;
        GridLayoutGroup group = Parent.GetComponent<GridLayoutGroup>();

        if (group != null)
        {
            group.cellSize = new Vector2(GridSize.x, GridSize.y);
        }

        UI.Instance.StartCoroutine(GenerateRoutine(inventory));

        Sub();

        return this;
    }

    public void Sub()
    {
        Inventory.ItemAdded += ItemAdded;
        Inventory.ItemRemoved += ItemRemoved;

        Inventory.Blocked += ItemBlocking;
        Inventory.UnBlocked += ItemBlockRemoved;
    }

    public void UnSub()
    {
        if (Inventory == null)
        {
            return;
        }

        Inventory.ItemAdded -= ItemAdded;
        Inventory.ItemRemoved -= ItemRemoved;

        Inventory.Blocked -= ItemBlocking;
        Inventory.UnBlocked -= ItemBlockRemoved;
    }

    public void UpdateSlot(Slot s)
    {
        s.SetState(GetState(s));
    }


    private void Update()
    {
        CheckUnityInited();

        foreach (Slot s in dirtySlots)
        {
            UpdateSlot(s);
        }

        dirtySlots.Clear();
    }

    private void CheckUnityInited()
    {
        if (isUnityInited)
        {
            return;
        }

        foreach (UIItem t in UIItems.ToArray())
        {
            Redraw(t);
        }

        isUnityInited = true;
    }

    public void OnDestroy()
    {
        if (Inventory == null)
        {
            return;
        }

        UnSub();
    }


    public Slot GetSlot(Vector2 position)
    {
        foreach (Slot s in _slots.data)
        {
            Rect actual = new Rect(s.GetComponent<RectTransform>().position, s.GetComponent<RectTransform>().rect.size);

            if (actual.Contains(position))
            {
                return s;
            }
        }
        return null;
    }


    private IEnumerator GenerateRoutine(Inventory i)
    {
        DestroyCurrentUIObjects();

        _slots = new LinearMap2D<Slot>(i.Size);

        GridLayoutGroup group = Parent.GetComponent<GridLayoutGroup>();
        if (group != null)
        {
            group.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            group.constraintCount = i.Size.x;
        }

        for (int y = 0; y < i.Size.y; y++)
        {
            for (int x = 0; x < i.Size.x; x++)
            {
                Slot t = Instantiate(SlotPrefab, Parent.transform);

                t.IsSingleSlotInventory = IsSingleSlotInventory;

                t.Position = new Int2(x, y);
                t.name = string.Format("Slot {0}", new Int2(x, y));

                t.Init(this);

                _slots.SetValue(new Int2(x, y), t);

                t.SetState(GetState(t));
            }
        }

        UIItemParent.SetAsLastSibling();

        yield return new WaitForEndOfFrame();

        //FitToContents();

        DrawAllItems();
    }

    public void FitToContents()
    {
        GetComponent<RectTransform>().sizeDelta = Parent.GetComponent<RectTransform>().sizeDelta + new Vector2(35, 35);
    }


    public void DrawAllItems()
    {
        RectangleFillIterator rectFill = new RectangleFillIterator(new Int2(0, 0), Inventory.Size);

        List<Item> DrawnItems = new List<Item>();

        foreach (Vector2i pos in rectFill)
        {
            Item item = Inventory[pos];

            if (item != null)
            {
                if (!DrawnItems.Contains(item))
                {
                    //need to wait one frame before drawing. Unity scaling broblems.
                    UI.Instance.InvokeDelayed(0f, () => ItemAdded(item));
                    DrawnItems.Add(item);
                }
            }
        }
    }


    private void ItemAdded(Item t)
    {
        UIItem item = Draw(t);
        if (item == null)
        {
            return;
        }

        item.OnItemHovered += OnItemHovered;
        item.OnItemHoverExit += OnItemHoverExit;
        item.OnItemClicked += OnItemClicked;
        item.OnDoubleClicked += OnItemDoubleClicked;

        t.OccupiedSlots.ForEach((x) => dirtySlots.Add(Slots[x]));
        t.OnDirty += Redraw;
    }


    private void ItemRemoved(Item t)
    {
        int index = UIItems.FindIndex((x) => x.Item == t);

        if (index < 0)
        {
            return;
        }

        UIItem uiItem = UIItems[index];

        uiItem.OnPointerExit(null);
        uiItem.OnItemHovered -= OnItemHovered;
        uiItem.OnItemHoverExit -= OnItemHoverExit;
        uiItem.OnItemClicked -= OnItemClicked;
        uiItem.OnDoubleClicked -= OnItemDoubleClicked;

        t.OccupiedSlots.ForEach((x) => dirtySlots.Add(Slots[x]));

        t.OnDirty -= Redraw;

        if (uiItem != null)
        {
            Destroy(uiItem.gameObject);
        }

        UIItems.RemoveAt(index);
    }

    public void Redraw(Item item)
    {
        ItemRemoved(item);

        ItemAdded(item);
    }

    public void Redraw(UIItem item)
    {
        Redraw(item?.Item);
    }


    private UIItem Draw(Item item)
    {
        if (_slots[item.CornerPos] == null)
        {
            return null;
        }

        UIItem uiItem = UIFactories.Instance.ItemFactory.CreateUIItem(item);

        uiItem.showFrame = itemFrame;
        uiItem.Initialize(item, this);
        uiItem.ScalingEnabled = ItemScaleSizeOnHover;

        uiItem.Moveable.SetParent(UIItemParent);

        uiItem.Moveable.localScale = Vector3.one;

        if (_slots[item.CornerPos].IsSingleSlotInventory)
        {
            ScaleToSlot(uiItem, _slots[item.CornerPos]);
        }
        else
        {
            //pos and size have to act diffrently if you put this in a single slot thing.
            Vector3 pos = _slots[item.CornerPos].GetComponent<RectTransform>().position
            - (new Vector3(uiItem.GetScreenOffset.x, uiItem.GetScreenOffset.y, 0)
            * UI.Instance.Canvas.transform.localScale.x);
            uiItem.Moveable.position = pos;
        }

        UIItems.Add(uiItem);

        OnItemDrawn?.Invoke(uiItem);

        return uiItem;
    }

    private void ScaleToSlot(UIItem uiItem, Slot slot)
    {
        if (slot == null)
        {
            return;
        }

        RectTransform slotRect = slot.GetComponent<RectTransform>();

        float width = slotRect.rect.width;
        float height =  slotRect.rect.height;

        uiItem.Moveable.sizeDelta = new Vector2(slotRect.rect.width, slotRect.rect.height);

        uiItem.Moveable.position = slotRect.position + (new Vector3(width / 2f, -height / 2f) * transform.lossyScale.x);
    }

    private void ItemBlockRemoved(List<Int2> slotpos)
    {
        foreach (Int2 pos in slotpos)
        {
            _slots[pos].SetState(GetState(_slots[pos]));
        }
    }

    public Slot.SlotState GetState(Slot s)
    {
        if (Inventory.Items[s.Position] == null)
        {
            return Slot.SlotState.Default;
        }

        return Slot.SlotState.Occupied;
    }

    private void ItemBlocking(List<Int2> slotpos)
    {
        foreach (Int2 pos in slotpos)
        {
            _slots[pos].SetState(Slot.SlotState.Blocking);
        }
    }


    public void DestroyCurrentUIObjects()
    {
        if (_slots == null || _slots.data == null)
        {
            return;
        }

        foreach (Slot r in _slots.data)
        {
            if (r == null)
            {
                continue;
            }

            Destroy(r.gameObject);
        }

        _slots.SetAll(null);
    }


    public void Close(bool destroy)
    {
        OnClosed?.Invoke();

        UIItems.ForEach((x) => x.Item.OnDirty -= Redraw);
        UIItems.ForEach((x) => Destroy(x.gameObject));
        UIItems.Clear();

        UnSub();
        DestroyCurrentUIObjects();
        isUnityInited = false;

        if (destroy)
        {
            Destroy(gameObject);
            return;
        }
    }

    public void Close()
    {
        Close(false);
    }
}

