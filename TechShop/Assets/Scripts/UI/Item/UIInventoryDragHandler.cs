using Fishy;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//specific to items and inventories.
/// <summary>
/// UIItem drag handler, used when moving around items via mouse in inventory.
/// </summary>
public class UIInventoryDragHandler : Singleton<UIInventoryDragHandler>
{
    private Vector3 lastPos = Vector3.zero;

    [Serializable]
    public struct DragData
    {
        public UIItem CurrentlyDragging;
        public Inventory ParentInventory;
        public Int2 StartSlot;
        public Vector3 Offset;
        public List<Slot> Highlighted;
    }

    [SerializeField]
    private DragData drag;

    private bool Reset = false;

    public Slot CurrentlyHovered;


    public Slot GetHoveredSlot(Vector3 offset)
    {
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        PointerEventData pointerData = new PointerEventData( EventSystem.current );

        pointerData.position = Input.mousePosition + offset;
        GetComponent<GraphicRaycaster>().Raycast(pointerData, raycastResults);

        foreach (RaycastResult r in raycastResults)
        {
            Slot slot = r.gameObject.GetComponent<Slot>();

            if (slot != null)
            {
                if (slot.IsSingleSlotInventory)
                {
                    if (offset != Vector3.zero)
                    {
                        return GetHoveredSlot(Vector3.zero);
                    }
                }

                return slot;
            }
        }

        //eh..
        if (offset != Vector3.zero)
        {
            Slot nonOffsetslot = GetHoveredSlot(Vector3.zero);
            if (nonOffsetslot?.IsSingleSlotInventory == true)
            {
                return nonOffsetslot;
            }
        }

        return null;
    }


    public void OnDrag(PointerEventData eventData)
    {
        if (drag.CurrentlyDragging == null)
        {
            return;
        }

        //dependent on ui object scale
        drag.CurrentlyDragging.Moveable.position = eventData.position;// += (eventData.delta / UI.Instance.transform.root.localScale);

        //reset all back to default state.
        ResetHighlights();

        if (CurrentlyHovered == null)
        {
            return;
        }

        TryPlaceInInventory();
    }


    //try place in Icontainer
    private void TryPlaceInInventory()
    {
        ItemPlacementResult result = new ItemPlacementResult();

        List<Int2> newOccupiedSlots = new List<Int2>();

        bool validPlacement =
            CurrentlyHovered?.UIContainer?.Container?.ValidPlacement(
            drag.CurrentlyDragging.Item,
            CurrentlyHovered.Position,
            out result) == true;

        newOccupiedSlots = result.OccupiedSlots;

        foreach (Int2 s in newOccupiedSlots)
        {
            if (CurrentlyHovered.UIContainer.Slots.IsInside(s))
            {
                drag.Highlighted.Add(CurrentlyHovered.UIContainer.Slots[s]);
            }
        }

        if (validPlacement)
        {
            drag.Highlighted.ForEach((x) => x.SetState(Slot.SlotState.Placing));
        }
        else
        {
            drag.Highlighted.ForEach((x) => x.SetState(Slot.SlotState.Blocked));
        }
    }


    private void ResetHighlights()
    {
        if (drag.Highlighted == null)
        {
            drag.Highlighted = new List<Slot>();
            return;
        }

        drag.Highlighted.ForEach((x) => x.SetState(x.UIContainer.GetState(x)));

        drag.Highlighted = new List<Slot>();
    }


    public bool SetCurrentlyDragging(UIItem UIItem, PointerEventData data)
    {
        if (UIItem == null)
        {
            drag = new DragData();
        }

        if (UIItem.UIContainer?.Container?.Interactable == false)
        {
            return false;
        }

        if (drag.CurrentlyDragging == UIItem && !Reset)
        {
            return false;
        }

        UIItem.Initialize(UIItem.Item, UIItem.UIContainer);

        Reset = false;

        UIItem.GetComponent<RectTransform>().SetParent(UI.Instance.drawArea);
        UIItem.GetComponent<RectTransform>().SetAsLastSibling();
        UIItem.GetComponent<RectTransform>().position = Input.mousePosition;//data.position;

        drag.Offset = UIItem.GetDragOffset;

        if (UIItem.Item.Size.y == 1)
        {
            drag.Offset.y = 0f;
        }

        if (UIItem.Item.Size.x == 1)
        {
            drag.Offset.x = 0f;
        }

        UIItem.Item.SaveStartState();

        drag.CurrentlyDragging = UIItem;

        UIItem.transform.localScale = Vector3.one;

        drag.StartSlot = UIItem.Item.CornerPos;

        return true;
    }


    //Placement entry
    public void PlaceCurrentlyDragging(PointerEventData pos)
    {
        ResetHighlights();

        if (drag.CurrentlyDragging == null)
        {
            return;
        }

        if (CurrentlyHovered == null)
        {
            if (IsDropped())
            {
                DropDraggedItem();
                return;
            }

            drag.CurrentlyDragging.Item.ApplyStartState();

            if (drag.CurrentlyDragging.UIContainer != null)
            {
                drag.CurrentlyDragging.UIContainer.Redraw(drag.CurrentlyDragging);
            }

            OnEndDrag();

            return;
        }

        TryPlaceDraggingItem(pos);
    }

    private bool IsDropped()
    {
        foreach (RaycastResult t in UI.Instance.MouseOnUIChecker.lastFrameHits)
        {
            if (t.gameObject.transform.IsChildOf(drag.CurrentlyDragging.transform))
            {
                continue;
            }

            if (t.gameObject.layer == LayerMask.NameToLayer("UI"))
            {
                return false;
            }
        }

        return true;
    }

    public void TryPlaceDraggingItem(PointerEventData pos)
    {
        if (CurrentlyHovered == null)
        {
            return;
        }

        Int2 newCorner = CurrentlyHovered.Position;

        if (!CurrentlyHovered.UIContainer.Container.Insert(
            drag.CurrentlyDragging.Item,
            CurrentlyHovered.Position,
            out ItemPlacementResult result))
        {
            ResetCurrentDragging();
        }
        else
        {
            drag.CurrentlyDragging.Item.FreeStartState();
        }

        OnEndDrag();
    }

    private void DropDraggedItem()
    {
        Vector3 pos = drag.CurrentlyDragging.Item.Container.transform.position;
        Inventory inv = drag.CurrentlyDragging.Item.Container;
        inv.Remove(drag.CurrentlyDragging.Item);

        EntityFactory.Instance.CreateInWorld(drag.CurrentlyDragging.Item, pos);
    }

    private void ResetCurrentDragging()
    {
        //if failed set it back to initial slot.
        drag.CurrentlyDragging.Item.ApplyStartState();
        drag.CurrentlyDragging.UIContainer.Container.Insert(drag.CurrentlyDragging.Item, drag.StartSlot, out ItemPlacementResult result);
    }

    private void OnEndDrag()
    {
        Destroy(drag.CurrentlyDragging?.gameObject);

        drag = new DragData();
    }

    // Update is called once per frame
    private void Update()
    {
        CurrentlyHovered = GetHoveredSlot(drag.Offset);


        if (Input.GetKeyDown(KeyCode.R))
        {
            if (drag.CurrentlyDragging != null)
            {
                Reset = true;
                drag.CurrentlyDragging.OnDrag(drag.CurrentlyDragging.SwapRotation());
            }
        }
    }
}
