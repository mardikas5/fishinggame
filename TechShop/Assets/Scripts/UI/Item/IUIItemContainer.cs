﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUIItemContainer
{
    Inventory Container { get; set; }

    //IContextHandler Handler { get; set; }

    LinearMap2D<Slot> Slots { get; }

    void Redraw(UIItem item);

    Slot.SlotState GetState(Slot x);
}
