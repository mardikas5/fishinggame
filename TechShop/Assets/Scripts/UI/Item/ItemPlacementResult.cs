﻿using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Result of trying to move and place an item in an inventory.
/// </summary>
public class ItemPlacementResult
{
    //Slots the item occupies.
    public List<Int2> OccupiedSlots = new List<Int2>();

    public Inventory Inventory;

    public Int2 CornerPosition;

    public bool IsCombiningStacks;

    public ItemStackCombineResult CombineStackResult;
}
