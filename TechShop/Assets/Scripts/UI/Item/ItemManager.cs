using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

/// <summary>
/// Item database...
/// </summary>
[Serializable, ExecuteInEditMode]
public class ItemManager : Singleton<ItemManager>
{
    public JsonSerializerSettings settings;

    public static string ItemDBPath => Application.dataPath + "/Items/";

    public static string ItemDBFile => "Items.txt";

    public static string ItemDBFullPath => ItemDBPath + ItemDBFile;



    public List<ItemBase> Items;




    public void AddItem(ItemBase t)
    {
        if (Items.Find((x) => x.Identifier == t.Identifier) != null)
        {
            return;
        }

        Items.Add(t);
    }


    public override void Awake()
    {
        base.Awake();

        settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };

        Items = new List<ItemBase>();

        AssureDirectoryExists(ItemDBPath, ItemDBFullPath);

        LoadItemDB();
    }

    private void AssureDirectoryExists(string dir, string file)
    {
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        if (!File.Exists(file))
        {
            File.Create(file);
        }
    }


    public static string SavePrefab(ItemBase i)
    {
        return JsonConvert.SerializeObject(i, Instance.settings);
    }


    public void SaveItemDB()
    {
        foreach (ItemBase p in Items)
        {
            Save(p);
        }
    }

    public void Save(ItemBase item)
    {
        string file = getFileName( item );
        AssureDirectoryExists(ItemDBPath, file);
        File.WriteAllText(ItemDBPath + file, SavePrefab(item));
    }

    public string getFileName(ItemBase item)
    {
        if (item == null)
        {
            return null;
        }
        return item.Identifier + ".item";
    }

    public string getFile(ItemBase item)
    {
        string file = getFileName( item );
        if (string.IsNullOrEmpty(file))
        {
            return null;
        }
        return ItemDBPath + file;
    }

    public static string CalculateMD5(string str)
    {
        using (MD5 md5 = MD5.Create())
        {
            byte[] bytes = Encoding.Default.GetBytes( str );
            return Encoding.Default.GetString(md5.ComputeHash(bytes));
        }
    }

    public static string CalculateMD5File(string filename)
    {
        if (!File.Exists(filename))
        {
            return null;
        }
        using (MD5 md5 = MD5.Create())
        {
            using (FileStream stream = File.OpenRead(filename))
            {
                return Encoding.Default.GetString(md5.ComputeHash(stream));
            }
        }
    }

    public ItemBase LoadItem(ItemBase p)
    {
        string file = getFileName( p );
        if (!File.Exists(file))
        {
            return null;
        }

        string text = File.ReadAllText( file );
        ItemBase i = JsonConvert.DeserializeObject<ItemBase>( text, Instance.settings );
        return i;
    }

    public void LoadItemDB()
    {

        if (Instance?.settings == null)
        {
            Debug.LogError("no settings set for json");
            return;
        }

        int fileCount = 0;
        Items = new List<ItemBase>();

        string[] files = Directory.GetFiles( ItemDBPath );
        foreach (string file in files)
        {
            if (!file.EndsWith(".item"))
            {
                continue;
            }
            string text = File.ReadAllText( file );
            fileCount++;

            if (string.IsNullOrEmpty(text))
            {
                Debug.LogError("read null file");
                continue;
            }


            ItemBase i = JsonConvert.DeserializeObject<ItemBase>( text, Instance.settings );
            Items.Add(i);
        }
    }
}
