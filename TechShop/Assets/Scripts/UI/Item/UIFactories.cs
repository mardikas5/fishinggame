using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


[Serializable]
public class UIFactories : Singleton<UIFactories>
{
    public UIInventoryFactory InventoryFactory;
    public UIItemFactory ItemFactory;

    public override void Awake()
    {
        base.Awake();
        InventoryFactory.SetRunner(this);
        ItemFactory.SetRunner(this);
    }
}


[Serializable]
public class UIFactory
{
    public GameObject Prefab;
    protected MonoBehaviour runner;

    public UIFactory(MonoBehaviour Runner)
    {
        runner = Runner;
    }

    public void SetRunner(MonoBehaviour Runner)
    {
        runner = Runner;
    }

    protected virtual void DefaultSetup(GameObject UIObject, bool move = true)
    {
        UIObject.transform.SetParent(UI.Instance.drawArea);

        UIObject.transform.localScale = new Vector3(1, 1, 1);

        if (move)
        {
            UIObject.transform.localPosition = Vector3.zero;
        }
    }

}
[DefaultExecutionOrder(-1000), Serializable]
public class UIItemFactory : UIFactory
{
    public GameObject DetailedViewPrefab;
    public GameObject InUsePrefab;

    public UIItemFactory(MonoBehaviour Runner) : base(Runner)
    {

    }

    public UIItem CreateUIItem(Item i)
    {
        UIItem t = GameObject.Instantiate(Prefab, new Vector3(-10000f, -10000f, 0f), Quaternion.identity, null).GetComponent<UIItem>();
        return t;
    }
}

[DefaultExecutionOrder(-1000), Serializable]
public class UIInventoryFactory : UIFactory
{
    public UIInventoryFactory(MonoBehaviour Runner) : base(Runner)
    {
    }

    public void CreateUIInventory(Inventory i, Action<UIInventory> onCreated = null)
    {
        runner.StartCoroutine(_CreateUIInventory(i, onCreated));
    }

    private IEnumerator _CreateUIInventory(Inventory i, Action<UIInventory> onCreated = null)
    {
        UIInventory uii = GameObject.Instantiate(Prefab, Prefab.transform.localPosition, Quaternion.identity, UI.Instance.drawArea).GetComponent<UIInventory>();

        RectTransform uiRect = uii.GetComponent<RectTransform>();
        uiRect.anchoredPosition = Prefab.GetComponent<RectTransform>().anchoredPosition;
        uiRect.sizeDelta = Prefab.GetComponent<RectTransform>().sizeDelta;

        uii?.TitleText?.SetText(i.Title);
        uii.Generate(i);

        yield return new WaitForEndOfFrame();

        DefaultSetup(uii.gameObject, false);

        yield return new WaitForEndOfFrame();

        onCreated?.Invoke(uii);
    }
}