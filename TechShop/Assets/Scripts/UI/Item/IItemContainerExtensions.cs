﻿//using Items;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public static class IItemContainerExtensions
//{
//    public static IItemContainer TopLevelContainer(this IItemContainer Container)
//    {
//        if (Container == null)
//        {
//            return null;
//        }

//        if (Container.Container == null)
//        {
//            return Container;
//        }

//        return Container.Container.TopLevelContainer();
//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <param name="inventory"></param>
//    /// <param name="item"></param>
//    /// <returns>false if recursion happened</returns>
//    public static bool IsNonRecursive(this IItemContainer inventory, Item item)
//    {
//        if (item == null || inventory == null)
//        {
//            return true;
//        }

//        IItemContainer itemInventory = item.GetComponentInterface<IItemContainer>();

//        int checklimit = 1000;
//        int limit = 0;

//        //if t.getcomponentInventory is at some point an inventory in the chain

//        while (itemInventory != null && limit < checklimit)
//        {
//            limit++;

//            if (inventory == itemInventory)
//            {
//                Debug.Log("recursion failed for inventory" + inventory.Name + ", " + itemInventory.Name);
//                return false;
//            }

//            if (inventory?.Container == null)
//            {
//                break;
//            }

//            inventory = inventory.Container;
//        }

//        if (limit >= checklimit)
//        {
//            Debug.Log("recursion check limit reached");
//        }

//        return true;
//    }
//}
