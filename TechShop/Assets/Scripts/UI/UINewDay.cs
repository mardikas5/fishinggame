﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UINewDay : MonoBehaviour
{
    public event Action OnBegin;

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void SignalBegin()
    {
        OnBegin?.Invoke();
    }
}
