﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIFamilyAttribute : MonoBehaviour
{
    public FamilyNeed Need;

    public TextMeshProUGUI needDescription;

    public TextMeshProUGUI needStatus;

    public void Init(FamilyNeed need)
    {
        Need = need;
    }

    public void Refresh()
    {
        if (Need == null)
        {
            needDescription.text = "NULL";
            return;
        }

        needDescription.text = Need.Description;

        needStatus.text = Need.currentAmount.ToString("F") + @" / " + Need.maxAmount.ToString("F");
    }

    public void Update()
    {
        Refresh();
    }
}
