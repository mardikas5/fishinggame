﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIRecipePanel : MonoBehaviour
{
    public UIRecipeComponent RequirementPrefab;

    public TextMeshProUGUI RecipeName;

    public Recipe recipe;

    public RectTransform requirementParent;

    public event Action OnRecipeClicked;

    public void Awake()
    {
        GetComponent<Button>().onClick.AddListener(() => OnRecipeClicked?.Invoke());
    }

    public void Init(Recipe Recipe)
    {
        if (recipe == null) { return; }

        recipe = Recipe;
        RecipeName.text = recipe.Name;

        foreach (Recipe.Requirement t in recipe.Requirements)
        {
            UIRecipeComponent p = Instantiate(RequirementPrefab, requirementParent);
            p.Init(t);
        }
    }
}
