﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI handler for the fish typing minigame.
/// </summary>
public class UIFishComboDrawer : MonoBehaviour {
    public FishComboGame comboGame; //could call an init function to pass this in. -> since its static, just assigned from inspector.

    public GameObject CombolinePrefab;

    public HorizontalLayoutGroup charLayoutGroup;

    public TextMeshProUGUI comboCharsPrefab;

    public Image timerImage;

    public Transform LineParentPanel;
    public Transform FishComboEntryPanel;

    public Transform ComboPanel;

    public List<TextMeshProUGUI> generatedComboChars = new List<TextMeshProUGUI>();

    private CanvasGroup canvasGroup;
    private TweenerCore<float, float, FloatOptions> canvasFade;

    private FishingComboTrigger activeTrigger;
    private List<GameObject> activeLines = new List<GameObject>();

    private Dictionary<FishingComboTrigger, GameObject> triggerLinePairs = new Dictionary<FishingComboTrigger, GameObject>();

    private int highlightIndex = -1;

    public void Awake() {
        activeTrigger = null;

        comboGame.onTriggersGenerated += GenerateComboLines;

        canvasGroup = ComboPanel.GetComponent<CanvasGroup>();

        canvasGroup.alpha = 0f;
    }

    public void Update() {
        UpdateUI(comboGame.activeTrigger);
    }

    /// <summary>
    /// Updates the ui to match the set trigger.
    /// </summary>
    /// <param name="trigger"></param>
    private void UpdateUI(FishingComboTrigger trigger) {
        if (trigger == activeTrigger) {
            if (trigger != null) {
                UpdateExistingTrigger(trigger);
            }
            return;
        }

        activeTrigger = trigger;

        ClearPrompt();

        if (trigger == null) {
            Hide();
            return;
        }

        ShowNewTrigger(trigger);
    }

    /// <summary>
    /// Clears old lines and generates new ones depending on the controller positions.
    /// </summary>
    private void GenerateComboLines() {
        ClearLines();

        foreach (FishingComboTrigger t in comboGame.activeComboTriggers) {
            DrawLine(t);
            t.onMistakeMade += OnMistake;
        }
    }

    private void OnMistake() {
        TextMeshProUGUI item =  generatedComboChars.ElementAtOrDefault(highlightIndex);
        if (item == null) {
            return;
        }

        item.DOColor(Color.red, .1f);
        item.transform.DOPunchScale(Vector3.one, .1f, 1, .2f);
    }


    /// <summary>
    /// Update and existing trigger, sets the correct highlight index.
    /// </summary>
    /// <param name="trigger"></param>
    private void UpdateExistingTrigger(FishingComboTrigger trigger) {
        SetHighlightIndex(trigger.ComboIndex);

        timerImage.DOFillAmount(1f - (comboGame.timeTaken / trigger.TimeToSolve), Time.deltaTime * 2f);
    }

    /// <summary>
    /// Set a new active trigger, spawn the needed characters, set highlight
    /// </summary>
    /// <param name="trigger"></param>
    private void ShowNewTrigger(FishingComboTrigger trigger) {
        Show();
        highlightIndex = -1;
        SpawnCharacters(trigger.keyCombo);

        SetHighlightIndex(trigger.ComboIndex);
    }

    private void Show() {
        FishComboEntryPanel.gameObject.SetActive(true);

        CheckFade();
        canvasFade = canvasGroup.DOFade(1f, .3f);
    }

    //Fade canvas
    private void CheckFade() {
        if (canvasFade != null) {
            canvasFade.Kill();
            canvasFade = null;
        }
    }

    private void Hide() {
        CheckFade();
        canvasFade = canvasGroup.DOFade(0f, .3f).OnComplete(() => {
            FishComboEntryPanel.gameObject.SetActive(false);
        });
    }

    /// <summary>
    /// Clear all UI characters
    /// </summary>
    private void ClearPrompt() {
        foreach (TextMeshProUGUI t in generatedComboChars) {
            Destroy(t.gameObject);
        }

        generatedComboChars.Clear();
    }

    /// <summary>
    /// Clears the lines that indicate where triggers are located on UI.
    /// </summary>
    private void ClearLines() {
        foreach (GameObject t in activeLines) {
            Destroy(t);
        }

        activeLines.Clear();
        triggerLinePairs.Clear();
    }

    /// <summary>
    /// Create the characters for this trigger.
    /// </summary>
    /// <param name="chars"></param>
    private void SpawnCharacters(char[] chars) {
        ClearPrompt();

        for (int i = 0; i < chars.Length; i++) {
            TextMeshProUGUI obj = Instantiate(comboCharsPrefab, charLayoutGroup.transform);
            obj.text = chars[i].ToString();
            generatedComboChars.Add(obj);
        }
    }

    /// <summary>
    /// Set the highlight of a character based on index.
    /// </summary>
    /// <param name="index"></param>
    private void SetHighlightIndex(int index) {
        if (highlightIndex == index) {
            return;
        }

        TextMeshProUGUI oldElement = generatedComboChars.ElementAtOrDefault(highlightIndex);
        if (oldElement != null) {
            oldElement.transform.DOScale(Vector3.one, .1f);
            oldElement.color = Color.green;
        }

        highlightIndex = index;
        TextMeshProUGUI newElement = generatedComboChars.ElementAtOrDefault(index);

        if (newElement != null) {
            newElement.color = Color.white;
            newElement.transform.DOScale(Vector3.one * 3f, .1f);
        }
    }

    private void DrawLine(FishingComboTrigger t) {
        RectTransform playArea = LineParentPanel.GetComponent<RectTransform>();
        GameObject Line = Instantiate(CombolinePrefab, playArea);
        Line.transform.SetAsFirstSibling();

        Line.GetComponent<RectTransform>().anchoredPosition = new Vector2(playArea.rect.size.x * t.NormalizedPos, 0f);
        activeLines.Add(Line);
        triggerLinePairs.Add(t, Line);
        t.ComboResultSignal += (x) => OnTriggerComplete(x, t);
    }

    private void OnTriggerComplete(bool success, FishingComboTrigger t) {
        if (success) //if the combo was successfully completed remove the ui line.
        {
            if (triggerLinePairs.TryGetValue(t, out GameObject lineObj)) {
                lineObj.transform.DOScale(Vector3.up, .3f);
            }
        }
    }
}
