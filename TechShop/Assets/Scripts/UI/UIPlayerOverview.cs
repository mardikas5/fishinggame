﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIPlayerOverview : MonoBehaviour, IGenericUIPanel
{
    public UIInventory playerInventory;
    public UIPlayerEquipment playerEquipment;

    public event Action<IGenericUIPanel> PanelOpened;
    public event Action<IGenericUIPanel> PanelClosed;

    public TweenFadeHandler UIFadeInOutHandler;
    public RectTransform content;

    public bool IsOpen;

    void Awake()
    {
        UIFadeInOutHandler.Init(GetComponent<CanvasGroup>(), gameObject, .2f, .15f);
    }
    // Start is called before the first frame update
    void Start()
    {
        playerInventory.Generate(GameLogic.Instance.Player.InventoryController.Inventory);
    }

    public void Toggle()
    {
        if (IsOpen)
        {
            Close();
            return;
        }
        Open();
    }

    public void Open()
    {
        gameObject.SetActive(true);
        content.anchoredPosition = new Vector2(0, 1000f);
        content.DOAnchorPos(Vector2.zero, .35f).SetEase(Ease.InOutQuad);
        UIFadeInOutHandler.Open();
        PanelOpened?.Invoke(this);
        IsOpen = true;
    }

    public void Close()
    {
        UIFadeInOutHandler.Close();
        PanelClosed?.Invoke(this);
        IsOpen = false;
        //gameObject.SetActive(false);
    }
}
