﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPurchaseLocation : MonoBehaviour, IGenericUIPanel
{
    public TweenFadeHandler fadeHandler;

    public Button yes;
    public Button no;
    public TextMeshProUGUI promptText;

    public event Action<IGenericUIPanel> PanelOpened;
    public event Action<IGenericUIPanel> PanelClosed;

    public void Awake()
    {
        fadeHandler.Init(GetComponent<CanvasGroup>(), gameObject, .3f, .5f);
    }

    public void Close()
    {
        PanelClosed?.Invoke(this);
        fadeHandler.Close();
    }

    public void ShowPopUp(string text, Action onYes, Action onNo)
    {
        PanelOpened?.Invoke(this);

        fadeHandler.Open();

        promptText.text = text;

        yes.onClick.RemoveAllListeners();
        no.onClick.RemoveAllListeners();

        yes.onClick.AddListener(() => {  onYes?.Invoke(); Close(); });
        no.onClick.AddListener(() => {  onNo?.Invoke(); Close(); });
    }
}
