﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Animations;

public class UIWorldToolTip : MonoBehaviour
{
    public TweenFadeHandler tweenHandler;

    public GameObject target;

    public Transform parentObj;

    private GameObject cameraObject;

    public TextMeshProUGUI textObj;

    public Vector3 offset = Vector3.up * 3f;

    private void Start()
    {
        tweenHandler = new TweenFadeHandler();
        tweenHandler.Init(GetComponent<CanvasGroup>(), gameObject, .3f, .5f);

        if (parentObj == null)
        {
            parentObj = transform?.parent?.parent;
        }
        if (UI.Instance == null)
        {
            Debug.LogError("aiote");
        }
        parentObj.parent = UI.Instance.drawArea;

        cameraObject = Camera.main.gameObject;

        Hide();
    }

    public void LateUpdate()
    {
        parentObj.transform.forward = Vector3.forward;
        parentObj.transform.localScale = Vector3.one;

        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        parentObj.position = Camera.main.WorldToScreenPoint(target.transform.position + offset);
    }

    public void Show()
    {
        tweenHandler.Open(false);
    }

    public void Hide()
    {
        tweenHandler.Close();
    }
}
