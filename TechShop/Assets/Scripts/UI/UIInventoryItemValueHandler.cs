﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventoryItemValueHandler : MonoBehaviour
{
    private Func<Item, float> costValueFunc;

    private UIInventory trackedInventory;

    public void SetValueHandler(UIInventory track, Func<Item, float> itemBuyValue)
    {
        UnSub();
        costValueFunc = itemBuyValue;
        Sub(track);

        foreach (UIItem t in track.Items)
        {
            OnDrawItem(t);
        }
    }

    private void OnDrawItem(UIItem obj)
    {
        if (costValueFunc == null)
        {
            obj.ItemCostText.text = "";
            return;
        }

        obj.ItemCostText.text = "¢ " + costValueFunc(obj?.Item).ToString("F2");
    }

    public void Sub(UIInventory inv)
    {
        inv.OnItemDrawn += OnDrawItem;
        trackedInventory = inv;
    }

    public void UnSub()
    {
        if (trackedInventory != null)
        {
            trackedInventory.OnItemDrawn -= OnDrawItem;
        }
    }

    public void Close()
    {
        UnSub();
    }
}
