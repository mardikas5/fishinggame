﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class UIRecipeComponent : MonoBehaviour
{
    public Recipe.Requirement requirement;
    public TextMeshProUGUI reqText;

    public void Init(Recipe.Requirement req)
    {
        requirement = req;
        reqText.text = req.tags.FirstOrDefault((x) => x != null);
    }
}
