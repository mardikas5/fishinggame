﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIRecievedItemPanel : MonoBehaviour, IGenericUIPanel
{
    public TextMeshProUGUI itemNameText;
    public Image ItemIcon;
    public TweenFadeHandler fadeHandler;
    public AudioClipContainer audioContainer;

    public event Action<IGenericUIPanel> PanelOpened;
    public event Action<IGenericUIPanel> PanelClosed;

    public void Open()
    {
        PanelOpened?.Invoke(this);
        fadeHandler.Open();
    }

    public void Close()
    {
        PanelClosed?.Invoke(this);
        fadeHandler.Close();
    }

    void Awake()
    {
        fadeHandler.Init(GetComponent<CanvasGroup>(), gameObject, .3f, .4f);
    }

    public void OnItemRecieved(ItemBase item)
    {
        if (gameObject.activeSelf == false)
        {
            gameObject.SetActive(true);
        }

        Open();
        itemNameText.text = item.Name;
        ItemIcon.sprite = item.Sprite;
        audioContainer.PlayRandom(GetComponent<AudioSource>());
    }
}
