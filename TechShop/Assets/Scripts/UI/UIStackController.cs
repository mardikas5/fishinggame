﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UIStackController : MonoBehaviour
{
    public List<IGenericUIPanel> StackedUIObjects = new List<IGenericUIPanel>();


    public void Start()
    {
        IGenericUIPanel[] objs = GetComponentsInChildren<IGenericUIPanel>(true);

        foreach (IGenericUIPanel t in objs)
        {
            t.PanelOpened += OnPanelOpened;
            t.PanelClosed += OnPanelClosed;
        }
    }

    private void OnPanelClosed(IGenericUIPanel panel)
    {
        StackedUIObjects.Remove(panel);
    }

    private void OnPanelOpened(IGenericUIPanel panel)
    {
        AddToStack(panel);
    }

    public void Update()
    {
        //Todo: central controller class for keycode use... where keycode events can be used up.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseNext();
        }
    }

    public void AddToStack(IGenericUIPanel stackObject)
    {
        StackedUIObjects.Add(stackObject);
    }

    public void CloseNext()
    {
        if (StackedUIObjects.Count == 0)
        {
            return;
        }

        int lastIndex = StackedUIObjects.Count - 1;
        StackedUIObjects[lastIndex].Close();
    }

}
