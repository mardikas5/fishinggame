﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFishingSpot : MonoBehaviour
{
    public FishingSpot fishingSpot;
    public RectTransform fishingBounds;
    public Image fishImage;
    public Sprite knownSprite;
    public Sprite isUnknownSprite;
    public string CustomText;

    public RectTransform rectTransform;

    private void Awake()
    {
        knownSprite = fishImage.sprite;
        rectTransform = GetComponent<RectTransform>();
    }

    public void Init(FishingSpot spot)
    {
        fishingSpot = spot;

        if (!spot.Known)
        {
            fishImage.sprite = isUnknownSprite;
        }

        spot.OnUnlocked += OnUnlockedHandler;
    }

    private void OnUnlockedHandler()
    {
        fishImage.sprite = knownSprite;
    }

    // Start is called before the first frame update
    private void Start()
    {
        Init(fishingSpot);
    }

    // Update is called once per frame
    private void Update()
    {

    }
}
