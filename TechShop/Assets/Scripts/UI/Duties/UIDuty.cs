﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class UIDuty : MonoBehaviour
{
    public TextMeshProUGUI descriptionText;

    public List<UIDutyComponent> uiDutyComponents = new List<UIDutyComponent>();

    public RectTransform ExtraInfoPanel;

    public PlayerDuty assignedDuty;

    public CanvasGroup canvasGroup;


    public virtual void Assign(PlayerDuty obj)
    {
        if (assignedDuty != null)
        {
            assignedDuty.OnValueUpdate -= OnValueUpdated;
        }

        assignedDuty = obj;

        SpawnTimeComponent();

        if (obj is MonetaryDuty)
        {
            SpawnMonetaryComponent();
        }


        assignedDuty.OnValueUpdate += OnValueUpdated;

        descriptionText.text = obj.Description;
    }

    private void SpawnTimeComponent()
    {
        GameObject mPrefab = UI.Instance.PlayerDutiesUI.dutyComponents.FirstOrDefault(
                (x) => x.GetComponent<UITimedDuty>())?.gameObject;

        if (mPrefab == null)
        {
            return;
        }

        UITimedDuty timedInstance = Instantiate(mPrefab, ExtraInfoPanel).GetComponent<UITimedDuty>();

        timedInstance.Assign(assignedDuty);

        uiDutyComponents.Add(timedInstance);
    }

    private void SpawnMonetaryComponent()
    {
        GameObject mPrefab = UI.Instance.PlayerDutiesUI.dutyComponents.FirstOrDefault(
                (x) => x.GetComponent<UIMonetaryDuty>())?.gameObject;

        if (mPrefab == null)
        {
            return;
        }

        UIMonetaryDuty moneyInstance = Instantiate(mPrefab, ExtraInfoPanel).GetComponent<UIMonetaryDuty>();

        moneyInstance.Assign(assignedDuty);

        uiDutyComponents.Add(moneyInstance);
    }

    private void OnDestroy()
    {
        if (gameObject != null)
        {
            if (assignedDuty != null)
            {
                assignedDuty.OnValueUpdate -= OnValueUpdated;
            }
        }
    }

    public virtual void OnValueUpdated()
    {
        //throw new NotImplementedException();
    }

    public void Resolve(Action onResolved)
    {
        transform.DOScale(1.5f, 1f);

        canvasGroup.DOFade(0f, 1.8f).OnComplete( () =>
        {
            onResolved?.Invoke();
            Destroy(gameObject);
        });
    }
}
