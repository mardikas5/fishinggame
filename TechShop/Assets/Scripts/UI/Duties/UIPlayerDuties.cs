﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerDuties : MonoBehaviour
{
    public UIDuty DutyPrefab;

    public PlayerDutyController dutyController;

    public Dictionary<PlayerDuty, UIDuty> uiPairs = new Dictionary<PlayerDuty, UIDuty>();

    public List<UIDutyComponent> dutyComponents = new List<UIDutyComponent>();


    public Transform ContentPanel;

    public Button CloseButton;

    private void Awake()
    {
        dutyController = GameLogic.Instance.Player.DutyController;

        dutyController.AddedDuty += OnAddedDuty;

        dutyController.RemovedDuty += OnRemovedDuty;

        foreach (PlayerDuty p in dutyController.duties)
        {
            OnAddedDuty(p);
        }
    }

    private void OnRemovedDuty(PlayerDuty obj)
    {
        if (uiPairs.TryGetValue(obj, out UIDuty uiObj))
        {
            uiPairs.Remove(obj);
            Destroy(uiObj);
        }
    }

    private void OnAddedDuty(PlayerDuty obj)
    {
        UIDuty uiDuty = null;

        uiDuty = Instantiate(DutyPrefab, ContentPanel);

        uiDuty.Assign(obj);

        uiPairs.Add(obj, uiDuty);
    }

    public void OnEnable()
    {
        foreach (UIDuty value in uiPairs.Values)
        {
            value.OnValueUpdated();
        }
    }

    public void EndDayEvaluation()
    {
        CloseButton.interactable = true;
    }

    public void StartEndOfDayEvaluation()
    {
        CloseButton.interactable = false;

        gameObject.SetActive(true);

        //TODO
    }

    public void ShowPanel()
    {

    }

    public void HidePanel()
    {

    }

}
