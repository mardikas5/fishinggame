﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDutyComponent : MonoBehaviour
{
    public PlayerDuty assignedDuty;

    public void Assign(PlayerDuty obj)
    {
        assignedDuty = obj;

        OnUpdateText();
    }

    protected virtual void OnUpdateText()
    {
        
    }
}
