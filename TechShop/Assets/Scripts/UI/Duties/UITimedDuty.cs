﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UITimedDuty : UIDutyComponent
{
    public TextMeshProUGUI timeText;

    protected override void OnUpdateText()
    {
        base.OnUpdateText();

        timeText.text = assignedDuty.dueDate.ToString(GameLogic.Instance.Time.StringFormatterShort);
    }
}
