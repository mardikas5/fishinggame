﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIMonetaryDuty : UIDutyComponent
{
    public TextMeshProUGUI amountText;


    protected override void OnUpdateText()
    {
        MonetaryDuty mDuty = assignedDuty as MonetaryDuty;

        if (mDuty == null)
        {
            amountText.text = "--.--";
            return;
        }

        amountText.text = mDuty.AmountRequired.ToString("F");
    }
}

