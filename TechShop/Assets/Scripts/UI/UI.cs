﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI : Singleton<UI>
{
    public UINewDay NewDayUI;

    public UIPlayerEnergy PlayerEnergyUI;
    public UIProcessPanel ProcessPanelUI;
    public UIPlayerDuties PlayerDutiesUI;
    public UITradePanel TradePanelUI;
    public UIFamilyTab FamilyPanelUI;
    public UITransferPanel TransferPanelUI;
    public UIInventory PlayerInventoryUI;
    public UIPlayerOverview PlayerOverViewUI;
    public UIFishingMapPanel FishingMapPanelUI;
    public UIRecievedItemPanel PlayerRecievedItemUI;
    public UISleepPromptPanel SleepPromptPanelUI;
    public UINoticeBoardPanel NoticeBoardPanelUI;
    public UINoticeBoardPanel PlayerJournalUI;
    public UINewVisitorPanel VisitorPanelUI;

    public GameObject HUDPanel;
    //More Like item info panel.
    public UIItemEquipmentPanel ItemEquipmentPanelUI;
    public UIPlayerEquipment PlayerEquipmentUI;

    public UIItemDescriptionPanel ItemDescriptionPanelUI;

    public GameObject demoCompleteObj;
    public GameObject tutorialTiredObj;

    public UIPurchaseLocation PurchasePopupUI;

    public Image fadePanel;

    public Transform drawArea;
    public Canvas Canvas;

    public float DoubleClickInterval = .2f;

    public MouseOnUIChecker MouseOnUIChecker;

    public bool MouseOnUI => MouseOnUIChecker.MouseOnUI();

    public void Update()
    {

    }

    public void ShowDemoCompleted()
    {
        demoCompleteObj.SetActive(true);
    }

    public void FadeInPanel(Action OnComplete = null)
    {
        fadePanel.GetComponent<CanvasGroup>().alpha = 0f;
        fadePanel.gameObject.SetActive(true);
        fadePanel.GetComponent<CanvasGroup>().DOFade(1f, 1f).OnComplete(() => OnComplete?.Invoke());
    }

    public void FadeOutPanel(Action OnComplete = null)
    {
        fadePanel.GetComponent<CanvasGroup>().alpha = 1f;
        fadePanel.GetComponent<CanvasGroup>().DOFade(0f, 1f).OnComplete(() =>
        {
            fadePanel.gameObject.SetActive(false);
            OnComplete?.Invoke();
        });
    }
}
