﻿using DG.Tweening;
using Fishy;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIProcessPanel : MonoBehaviour, IGenericUIPanel
{
    public ItemProcessor Processor;

    public ItemProcessor.ProcessState UIState;

    public UIInventory UIPlayerInventory;

    public UIRecipeInputsPanel RecipeInputsPanel;
    public RectTransform RecipiesParent;
    public UIInventory UIResultInventory;

    public Button btnStart;

    public GameObject result;
    public GameObject process;
    public GameObject progressBar;
    public Image progressFill;

    public Func<bool> OnStartHandler;
    public UIRecipePanel UIRecipePrefab;

    private TweenFadeHandler ProgressFadeHandler = new TweenFadeHandler();
    private TweenFadeHandler ResultInventoryFadeHandler = new TweenFadeHandler();

    private List<UIRecipePanel> recipeButtons = new List<UIRecipePanel>();

    private bool IsOpen = false;

    public event Action<IGenericUIPanel> PanelClosed;
    public event Action<IGenericUIPanel> PanelOpened;

    public void Awake()
    {
        RecipeInputsPanel.RecipeStarted += OnRecipeStarted;

        ResultInventoryFadeHandler.Init(result.GetComponent<CanvasGroup>(), result, .2f, .3f);
        ProgressFadeHandler.Init(progressBar.GetComponent<CanvasGroup>(), progressBar, .2f, .3f);

    }

    private void OnRecipeStarted(Recipe recipe)
    {
        Processor.StartProcessingRecipe(RecipeInputsPanel.GetOrderedItems(), recipe);
    }

    public void Update()
    {
        if (UIState == ItemProcessor.ProcessState.Processing)
        {
            OnProcessState();
        }
    }

    public void StateChangedHandler(ItemProcessor.ProcessState state)
    {
        if (state == ItemProcessor.ProcessState.Completed)
        {
            ResultInventoryFadeHandler.Open();
            process.SetActive(false);
            ProgressFadeHandler.Close();
            RecipeInputsPanel.Close();
            UIState = state;
        }

        if (state == ItemProcessor.ProcessState.Processing)
        {
            ResultInventoryFadeHandler.Close();
            process.SetActive(true);
            ProgressFadeHandler.Open();
            progressFill.DOFillAmount(Processor.normalizedProgress, .1f);
            UIState = state;
        }

        if (state == ItemProcessor.ProcessState.Waiting)
        {
            ResultInventoryFadeHandler.Close();
            process.SetActive(true);
            ProgressFadeHandler.Close();
            progressFill.fillAmount = 0f;
            RecipeInputsPanel.Close();
            UIState = state;
        }
    }

    public void OnProcessState()
    {
        progressFill.DOFillAmount(Processor.normalizedProgress, .1f);
    }

    public void Open(ItemProcessor Processor, Inventory playerInventory, Inventory resultInventory)
    {
        if (IsOpen)
        {
            Close();
        }

        IsOpen = true;

        this.Processor = Processor;

        Processor.stateChanged += StateChangedHandler;

        gameObject.SetActive(true);

        GenerateRecipeButtons(Processor);

        UIPlayerInventory.Generate(playerInventory);

        PanelOpened?.Invoke(this);

        if (resultInventory != null)
        {
            UIResultInventory?.Generate(resultInventory);
        }

        this.InvokeDelayed(.01f, () =>
        {
            StateChangedHandler(Processor.processorState);
        });
    }

    private void GenerateRecipeButtons(ItemProcessor processor)
    {
        for (int i = 0; i < processor.Recipies.Count; i++)
        {
            Recipe recipe = processor.Recipies[i];

            UIRecipePanel p = Instantiate(UIRecipePrefab, RecipiesParent);
            p.Init(recipe);
            p.OnRecipeClicked += () => OnRecipeClicked(recipe);
            recipeButtons.Add(p);
        }
    }

    private void OnRecipeClicked(Recipe recipe)
    {
        OpenRecipeInputsPanel(recipe);
    }

    private void OpenRecipeInputsPanel(Recipe recipe)
    {
        RecipeInputsPanel.Open();
        RecipeInputsPanel.SetRecipe(recipe, Processor);
    }

    public void ClearRecipes()
    {
        foreach (UIRecipePanel t in recipeButtons)
        {
            Destroy(t.gameObject);
        }

        recipeButtons.Clear();
    }

    public void Close()
    {
        ClearRecipes();
        RecipeInputsPanel.Close();

        IsOpen = false;

        OnStartHandler = null;

        UIPlayerInventory.Close(false);
        UIResultInventory?.Close(false);

        if (Processor != null)
        {
            Processor.stateChanged -= StateChangedHandler;
        }

        Processor = null;

        PanelClosed?.Invoke(this);

        gameObject.SetActive(false);
    }
}
