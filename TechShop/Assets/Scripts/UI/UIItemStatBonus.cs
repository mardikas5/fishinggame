﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIItemStatBonus : MonoBehaviour
{
    public TextMeshProUGUI ItemName;
    public TextMeshProUGUI StatName;
    public TextMeshProUGUI StatValue;

    public void Init(string item, PlayerStats.ModifierTypePair modifierTypePair)
    {
        ItemName.text = item;
        StatName.text = modifierTypePair.stat.ToString();
        StatValue.text = PlayerStats.StatValue.GetModifierString(modifierTypePair.mod);
    }
}
