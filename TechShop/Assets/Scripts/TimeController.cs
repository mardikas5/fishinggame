﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

//A lot of variables dont have to be public, easier to edit from inspector.

public class TimeController : MonoBehaviour {
    public DateTime GameTime;

    [TextArea]
    public string StringFormatterLong = "dddd \n d - MMMM - yyyy";

    [TextArea]
    public string StringFormatterShort = "ddd - d - m - yyyy";

    //Could move to gamesettings -> inject from there.
    public float DayNightLengthRealTimeSeconds;

    public bool TimeStopped = false;

    public float TimeScale = 1f;

    public string LongGameTimeString => GameTime.ToString(StringFormatterLong);

    public event Action<float> NormalizedHoursPassed;


    private float HourLengthRealTimeSeconds;

    public void Awake() {
        GameTime = new DateTime(1423, 4, 3, new GregorianCalendar());
    }

    public void Start() {
        HourLengthRealTimeSeconds = DayNightLengthRealTimeSeconds / 24f;
    }

    public float GameHoursToRealSeconds(float hours) {
        return HourLengthRealTimeSeconds * hours;
    }

    public int Hours() {
        return GameTime.Hour;
    }

    public int Minutes() {
        return GameTime.Minute;
    }

    public void PauseTime() {
        TimeStopped = true;
    }

    public void ResumeTime() {
        TimeStopped = false;
    }


    public float HoursPassedLastFrame() {
        if (TimeStopped) { return 0f; }
        return Time.deltaTime / HourLengthRealTimeSeconds;
    }


    public float NormalizedTimeOfDay() {
        float daysPassed = (GameTime.Hour + ((GameTime.Minute + (GameTime.Second / 60f)) / 60f)) / 24f;
        return daysPassed;
    }

    public string CurrentTimeString() {
        return GameTime.TimeOfDay.ToString(@"hh\:mm");
    }


    public void ForwardTime(float hours) {
        GameTime = GameTime.AddHours(hours);
        NormalizedHoursPassed?.Invoke(hours);
    }

    /// <summary>
    /// Forwards time to the set hour ( passing in 15.5f will make it 15h30min )
    /// </summary>
    /// <param name="timeOfDayInHours"></param>
    public void ForwardTimeToNext(float timeOfDayInHours) {
        TimeSpan targetTime = TimeSpan.FromHours(timeOfDayInHours);
        double hours = targetTime.Subtract(GameTime.TimeOfDay).TotalHours;
        if (hours < 0) { hours += 24f; }

        GameTime = GameTime.AddHours(hours);
        NormalizedHoursPassed?.Invoke((float)hours);
    }

    // Update is called once per frame
    private void Update() {
        TickTime();
    }

    public void TickTime() {
        if (TimeStopped) {
            return;
        }

        float hoursPassed = Time.deltaTime / HourLengthRealTimeSeconds;
        hoursPassed *= TimeScale;

        GameTime = GameTime.AddHours(hoursPassed);

        NormalizedHoursPassed?.Invoke(hoursPassed);
    }
}
