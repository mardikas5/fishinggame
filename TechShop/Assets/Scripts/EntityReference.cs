﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Visuals can be added to this object.
/// </summary>
public class EntityReference : MonoBehaviour
{
    public EntityInstance Entity;

    public void Init(EntityInstance entity)
    {
        Entity = entity;
    }
}

