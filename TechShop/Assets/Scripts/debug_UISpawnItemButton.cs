﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class debug_UISpawnItemButton : MonoBehaviour
{
    public Button button;
    public EntityBase item;
    public TextMeshProUGUI txtBox;

    public void Start()
    {
        //button.onClick 
    }

    public void Init(EntityBase k)
    {
        item = k;
        txtBox.text = item.name;
    }
}