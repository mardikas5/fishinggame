﻿//using Newtonsoft.Json;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

//Add modules for fish to get the games for a fish type?

[Serializable]
public class FishBase : ScriptableObject
{
    [JsonIgnore]
    public Sprite Sprite;

    //public string SpriteName;
    /// Capture game params.
    public float FishEscapeSpeed;
    /// <summary>
    /// How fast the fish moves on the 2d game horizontal line.
    /// </summary>
    public float SwimSpeedTension;

    public float UpdatePosInterval;
    public float UpdatePosRandomness;

    [JsonProperty("MinJumpDist")]
    public float MinJumpDistance;

    [JsonProperty("MaxJumpDist")]
    public float MaxJumpDistance;

    public float MinSize = 1f;
    public float MaxSize = 2f;

    public float BaitDifficulty = 1f;
    public float CaptureDifficulty = 1f;

    [Header("Letter combo game")]
    public int MinLines;
    public int MaxLines;

    [JsonProperty("Bounce")]
    public bool bounceFishFromWalls = false;

    public string Rarity = "Common";

#if UNITY_EDITOR
    [ContextMenu("JsonExample")]
    public void GetJson()
    {
        Debug.LogError("Newton: " + JsonConvert.SerializeObject(this));

        Debug.LogError(JsonUtility.ToJson(this));
    }
#endif
    /// End capture params.
}
