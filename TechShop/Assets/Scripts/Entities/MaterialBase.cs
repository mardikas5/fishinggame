﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fishy
{
    public class MaterialBase : ScriptableObject
    {
        public int Amount = 1;
    }
}
