﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class TryFindReferences : MonoBehaviour
{

#if UNITY_EDITOR
    [Serializable]
    public class TypePrefixPair
    {
        public string Type;
        public string Prefix;
    }

    public List<TypePrefixPair> Pairs = new List<TypePrefixPair>();

    public string lookPath = CSVToJsonReader.defaultScriptableLocation;

    public string[] GetFiles(string path)
    {
        string[] assetFiles = Directory.GetFiles(Application.dataPath, "*.asset", SearchOption.AllDirectories);
        for (int i = 0; i < assetFiles.Length; i++)
        {
            string assetPath = "Assets" + assetFiles[i].Replace(Application.dataPath, "").Replace('\\', '/');
            assetFiles[i] = assetPath;
        }

        return assetFiles;
    }

    [ContextMenu("TryGetEditorReferences")]
    public void TryGetEditorReferences()
    {
        List<EntityBase> EntityBases = new List<EntityBase>();

        string[] assets = GetFiles(Application.dataPath + "/" + CSVToJsonReader.defaultScriptableLocation);

        List<Object> objs = new List<Object>(assets.Length);

        foreach (string assetPath in assets)
        {
            Object foundObject = AssetDatabase.LoadAssetAtPath(assetPath, typeof(EntityBase));

            if (foundObject == null)
            {
                continue;
            }

            objs.Add(foundObject);
        }

        foreach (Object foundObj in objs)
        {
            EntityBase castEntity = foundObj as EntityBase;
            if (castEntity != null)
            {
                EntityBases.Add(castEntity);
            }
        }

        int removeIndex = "Entity_".Length;

        Debug.LogError($"{EntityBases.Count}, objs looking");

        foreach (EntityBase p in EntityBases)
        {
            string lookFor = p.name.Remove(0, removeIndex);

            foreach (TypePrefixPair t in Pairs)
            {
                Type type = Type.GetType(t.Type);

                if (type == null)
                {
                    Debug.LogError("couldnt find type: " + t.Type);
                    continue;
                }

                if (!CSVToJsonReader.CustomReader.TryGetID(t.Prefix + "_" + lookFor, out int id, out Object obj))
                {
                    continue;
                }

                p.Components.Add(obj as ScriptableObject);
                Debug.LogError("found component for: " + p.name + ", as: " + t.Type + ", name: " + obj.name);
            }

            p.Components.RemoveAll((x) => x == null);
            p.Components = p.Components.Distinct().ToList();
            EditorUtility.SetDirty(p as Object);
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
#endif
}
