﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WorldObjectItem : MonoBehaviour
{
    public Item itemInstance;
    public UIWorldToolTip tooltip;

    [SerializeField]
    private Rigidbody rigidBody;

    private float timeTillAllowed = 0f;

    private void Awake()
    {
        rigidBody = GetComponentInChildren<Rigidbody>();
    }

    public void Init(Item item)
    {
        itemInstance = item;
        tooltip.textObj.text = item.Name;
    }

    public void OnPlayerClick()
    {
        Pickup(GameLogic.Instance.Player.InventoryController.Inventory);
    }

    public bool Pickup(Inventory sendTo)
    {
        if (sendTo.TryPlaceAnySlot(itemInstance))
        {
            Destroy(gameObject);
            return true;
        }
        DoFlips();
        return false;
    }

    private void DoFlips()
    {
        if (timeTillAllowed > Time.time)
        {
            return;
        }

        timeTillAllowed = Time.time + 2f;

        rigidBody.AddForce(Vector3.up * Random.Range(200f, 300f), ForceMode.Acceleration);
        rigidBody.AddTorque(
            Vector3.right * Random.Range(200f, 300f) +
            Vector3.up * Random.Range(200f, 300f) +
            Vector3.forward * Random.Range(200f, 300f),
            ForceMode.Acceleration);
    }
}
