﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Fish : EntityInstance, IFishable
{
    public FishBase FishBase;

    public float Size;

    public Vector2 NormalizedSize = new Vector2(.2f,.2f);


    public void Start()
    {
        FishBase = Base.GetComponentSO<FishBase>();
    }

    public override float Value()
    {
        return Base.Value + (Size * .25f * Base.Value);
    }
}
