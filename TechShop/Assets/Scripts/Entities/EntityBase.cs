﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class EntityBase : ScriptableObject
{
    public List<ItemTag> Tags;

    public List<ScriptableObject> Components = new List<ScriptableObject>();

    public string Name;
    
    //object to copy if you want an instance of this entity.
    public EntityInstance WorldInstance;

    //object to copy if you want a visual, visible object for the player in the scene.
    public GameObject WorldEntity;

    [SerializeField]
    public float Value;

    [SerializeField, TextArea]
    public string Description;

    public bool HasTag(string tag)
    {
        return GetTag<ItemTag>(tag) != null;
    }

    public T GetTag<T>(string tag) where T : ItemTag
    {
        return Tags.FirstOrDefault((x) => x.Tag.ToLower() == tag.ToLower()) as T;
    }

    /// <summary>
    /// Get the scriptable object component.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetComponentSO<T>() where T : class
    {
        return Components.FirstOrDefault((x) => x is T) as T;
    }


    #region editor
#if UNITY_EDITOR
    [ContextMenu("Deparent")]
    public void Deparent()
    {
        var destPath = AssetDatabase.GetAssetPath(GetInstanceID());
        
        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.AddObjectToAsset(this, destPath + "_" + name + ".asset");
        
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    [ContextMenu("JsonExample")]
    public void GetJson()
    {
        Debug.LogError(JsonUtility.ToJson(this));
    }

#endif
    #endregion
}
