﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Instance of an entity -> premade scriptable object, could be knife, fish, fishing rod etc...
/// </summary>
public class EntityInstance : MonoBehaviour
{
    /// <summary>
    /// Static predefined data for this entity.
    /// </summary>
    public EntityBase Base;

    //Gameobject 
    public GameObject worldInstance;

    public event Action OnEntityUpdated;


    public EntityReference GetVisualEntity()
    {
        if (Base.WorldEntity == null)
        {
            return null;
        }

        if (worldInstance == null)
        {
            worldInstance = Instantiate(Base.WorldEntity);

            EntityReference reference = worldInstance.GetComponent<EntityReference>();

            if (reference == null)
            {
                reference = worldInstance.AddComponent<EntityReference>();
            }

            reference.Init(this);
            return reference;
        }

        return worldInstance.GetComponent<EntityReference>();
    }

    
    public virtual void SignalEntityUpdate()
    {
        OnEntityUpdated?.Invoke();
    }
    //
    public virtual float Weight()
    {
        return 1f;
    }

    //
    public virtual float Value()
    {
        return Base.Value;
    }

    public virtual string ValueString()
    {
        return Value().ToString("F2");
    }
}
