﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Specific to one item anyway no need to make it scriptable?
[Serializable]
public class ItemTag
{
    public string Tag;
    public float Amount;

    public ItemTag() { }

    public bool MatchesTag(ItemTag Other)
    {
        return Tag.ToLower() == Other.Tag.ToLower();
    }
}
