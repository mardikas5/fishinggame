﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fishy
{
    public class Material : EntityInstance
    {
        public MaterialBase MaterialBase;

        // Start is called before the first frame update
        void Start()
        {
            MaterialBase = Base.GetComponentSO<MaterialBase>();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
