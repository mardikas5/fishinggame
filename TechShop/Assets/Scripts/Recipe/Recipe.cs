﻿using Fishy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Recipe : ScriptableObject
{
    public string Name;

    [Serializable]
    public class Requirement
    {
        public bool matchAnyTag = true;
        public bool matchAllTags = false;
        public bool matchEntity = false;
        public bool consumeEntity = false;

        public string[] tags;
        public List<EntityBase> entityReq = new List<EntityBase>();

        public Requirement() { }

        [NonSerialized]
        public bool InUse = false;

        public bool Match(EntityInstance entity)
        {
            if (entity == null)
            {
                Debug.LogError("cannot match null entity.");
                return false;
            }

            if (matchEntity)
            {
                return MatchBaseItem(entity);
            }

            if (!matchEntity)
            {
                if (matchAllTags)
                {
                    return hasAllTags(entity);
                }
                if (matchAnyTag)
                {
                    return hasAnyTag(entity);
                }

                return hasAnyTag(entity);
            }

            return false;
        }

        private bool hasAllTags(EntityInstance entity)
        {
            foreach (string t in tags)
            {
                if (!entity.Base.HasTag(t))
                {
                    return false;
                }
            }

            if (tags.Length == 0)
            {
                Debug.LogError(" zero tags defined. ");
                return true;
            }

            return true;
        }

        private bool hasAnyTag(EntityInstance entity)
        {
            foreach (string t in tags)
            {
                if (entity.Base.HasTag(t))
                {
                    return true;
                }
            }

            if (tags.Length == 0)
            {
                Debug.LogError(" zero tags defined. ");
                return true;
            }

            return false;
        }

        private bool MatchBaseItem(EntityInstance i)
        {
            foreach (EntityBase p in entityReq)
            {
                if (i.Base == p)
                {
                    return true;
                }
            }

            return false;
        }
    }

    //energy needed from user. TODO add back
    public float EnergyCost;

    //time taken for the process
    public float ProductionTime;

    public List<ScriptableObject> Components;

    public List<EntityBase> OutputEntities;

    public Requirement[] Requirements;

    public bool ProduceOrdered(List<EntityInstance> orderedInputs, out List<EntityInstance> outputs, out List<EntityInstance> consumed)
    {
        outputs = new List<EntityInstance>();
        consumed = new List<EntityInstance>();

        if (orderedInputs.Count < Requirements.Length)
        {
            return false;
        }

        for (int i = 0; i < Requirements.Length; i++)
        {
            if (Requirements[i] == null)
            {
                Debug.LogError("null requirement.");
                continue;
            }

            if (!Requirements[i].Match(orderedInputs[i]))
            {
                return false;
            }
            if (Requirements[i].consumeEntity)
            {
                consumed.Add(orderedInputs[i]);
            }
        }

        foreach (EntityBase t in OutputEntities)
        {
            outputs.Add(EntityFactory.Create(t));
        }

        return true;
    }

    public bool Produce(List<EntityInstance> inputs, out List<EntityInstance> outputs)
    {
        HashSet<EntityInstance> usedItems = new HashSet<EntityInstance>();
        outputs = new List<EntityInstance>();

        foreach (Requirement req in Requirements)
        {
            if (!TryGetMatch(inputs, usedItems, req, out EntityInstance match))
            {
                return false;
            }
            usedItems.Add(match);
        }

        foreach (EntityBase t in OutputEntities)
        {
            outputs.Add(EntityFactory.Create(t));
        }

        return true;
    }

    public bool TryGetMatch(List<EntityInstance> entities, HashSet<EntityInstance> ignore, Requirement requirement, out EntityInstance match)
    {
        match = null;

        foreach (EntityInstance t in entities)
        {
            if (ignore.Contains(t))
            {
                continue;
            }

            if (requirement.Match(t))
            {
                match = t;
                return true;
            }
        }

        return false;
    }

    [ContextMenu("JsonExample")]
    public void GetJson()
    {
        Debug.LogError(JsonUtility.ToJson(this));
    }

    /// <summary>
    /// Get the scriptable object component.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetComponentSO<T>() where T : class
    {
        return Components.FirstOrDefault((x) => x is T) as T;
    }
}
