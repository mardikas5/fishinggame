﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SlashAttacksEnum
{
    Idle, //none
    FastRightLeft,
    FastLeftRight,
    Wide,
    Top
}

public class SlashAttack : WeaponAttack
{
    public SlashAttacksEnum attackType;


}

