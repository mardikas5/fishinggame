﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponTypeEnum
{
    OneHandedSword,
    TwoHandedSword,
    Pike,
    Bow
}

public class Weapon : MonoBehaviour
{
    [Serializable]
    public class EquipParams
    {
        public Vector3 localPos;
        public Vector3 localEulers;
        public Vector3 localScale = Vector3.one;
    }

    public WeaponAttack activeAttack;

    public event Action WeaponFired;
    public event Action<IHittable> OnHit;

    public WeaponTypeEnum WeaponType;

    public EquipParams equipParams;
    public GameObject weaponVisuals;
    public GameObject HitFX;

    public float ChainTimeout = .3f;
    public float TimeoutCounter = 0f;

    public bool isBusy = false;
    public bool isCharging = false;

    public WeaponAttackChain chainDefinition;

    public List<WeaponAttack> currentChain;
    private bool waitForRelease;

    private WeaponAttack.InputEnum lastInput;

    public void Start()
    {
        activeAttack = null;
    }

    public void Update()
    {
        UpdateWep();
    }

    private void UpdateWep()
    {
        if (activeAttack == null)
        {
            return;
        }

        if (!activeAttack.Charge(Time.deltaTime))
        {
            //release attack;
        }

        if (activeAttack != null && !activeAttack.IsBusy && !waitForRelease)
        {
            //if chain is waiting.
            TimeoutCounter += Time.deltaTime;
            if (TimeoutCounter > ChainTimeout)
            {
                Debug.LogError("reset chain");
                chainDefinition.ResetChain();
                activeAttack = null;
            }
        }
    }

    public WeaponAttack StartAttack(InputEnum attackInput, out bool Busy)
    {
        waitForRelease = true;

        Busy = false;

        if (activeAttack?.IsBusy == true)
        {
            Busy = true;
            return activeAttack;
        }

        activeAttack = chainDefinition.GoToNextAttack(attackInput);

        //if you've reached the end or theres no attack defined > try again once, will likely get start of new combo.
        if (activeAttack == null)
        {
            activeAttack = chainDefinition.GoToNextAttack(attackInput);
        }

        Debug.Log("next attack: " + activeAttack?.Identifier);

        TimeoutCounter = 0f;

        activeAttack?.Reset();

        return activeAttack;
    }

    public void ReleaseAttack(InputEnum released, Action onComplete = null, bool isInterrupted = false)
    {
        waitForRelease = false;

        if (activeAttack == null)
        {
            return;
        }

        if (activeAttack.IsBusy)
        {
            return;
        }

        TimeoutCounter = 0f;

        activeAttack.Execute(onComplete);
    }
}
