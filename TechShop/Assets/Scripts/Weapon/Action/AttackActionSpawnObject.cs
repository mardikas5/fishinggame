﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackActionSpawnObject : AttackActionHandler
{
    public GameObject spawnObject;
    public Transform attachTo;

    public float TimeToComplete = .2f;

    public override void Begin(Action onCompleted)
    {
        base.Begin(onCompleted);

        if (spawnObject != null)
        {
            GameObject spawned = Instantiate(spawnObject, attachTo);
            spawned.transform.localPosition = Vector3.zero;
        }

        this.InvokeDelayed(TimeToComplete, Complete);
    }
}
