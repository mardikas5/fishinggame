﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackActionHandler : MonoBehaviour
{
    public event Action Completed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Begin(Action onCompleted)
    {
        if (onCompleted == null)
        {
            return;
        }

        Action t = null;
        t += () => { onCompleted?.Invoke(); Completed -= t; };
        Completed += t;
    }

    public virtual void Complete()
    {
        Debug.LogError("completed attack");
        Completed?.Invoke();
    }
}
