﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class WeaponUser : MonoBehaviour
//{
//    public Transform AttachWeaponParent;

//    public GameObject activeVisuals;

//    public Weapon activeWeapon;

//    public bool IsImmobile = false;

//    public bool IsBlocked = false;

//    private List<WeaponAttack.InputEnum> activeInputs = new List<WeaponAttack.InputEnum>();

//    public void Start()
//    {
//        Equip(activeWeapon);
//    }

//    public void Interrupt()
//    {
//        //activeWeapon?.Interrupt();
//    }

//    public void SetBlock(bool blocked)
//    {
//        IsBlocked = blocked;
//    }

//    public WeaponAttack AttackSignal(InputEnum attackInput, out bool Busy)
//    {
//        Busy = false;

//        if (IsBlocked)
//        {
//            return null;
//        }

//        return activeWeapon.StartAttack(attackInput, out Busy);
//    }

//    public bool Equip(Weapon weapon)
//    {
//        if (weapon == null)
//        {
//            Debug.LogError("tried to equip null");
//            return false;
//        }

//        if (activeWeapon != null)
//        {
//            UnEquip(activeWeapon);
//        }

//        GameObject visuals = Instantiate(weapon.weaponVisuals);

//        visuals.transform.SetParent(AttachWeaponParent);
//        visuals.transform.localPosition = weapon.equipParams.localPos;
//        visuals.transform.localEulerAngles = weapon.equipParams.localEulers;
//        visuals.transform.localScale = weapon.equipParams.localScale;

//        activeVisuals = visuals;
//        activeWeapon = weapon;

//        return true;
//    }

//    public bool UnEquip(Weapon weapon)
//    {
//        if (weapon != activeWeapon)
//        {
//            return false;
//        }

//        if (activeVisuals != null)
//        {
//            Destroy(activeVisuals);
//        }

//        activeWeapon = null;
//        //remove wep.
//        return true;
//    }

//    public void AttackReleaseSignal(InputEnum released, Action onComplete = null, bool isInterrupted = false)
//    {
//        activeWeapon.ReleaseAttack(released, onComplete, isInterrupted);
//    }
//}
