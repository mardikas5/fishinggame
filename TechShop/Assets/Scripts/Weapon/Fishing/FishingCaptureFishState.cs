﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingCaptureFishState : FishingState
{
    public string failedCaptureString = " failed to capture ";

    public override FishingStates State => FishingStates.CaptureFish;

    public FishGameController FishGameController;

    //public FishCaptureGame captureGame;
    public FishGameResultHandler resultHandler;

    

    public override void EnterState()
    {
        base.EnterState();

        //if you have no fish on the line, end the game.
        if (Controller.activeFish == null)
        {
            CaptureEndHandler(false);
            return;
        }

        Controller.SetFailReason(failedCaptureString);
        FishGameController.StartGame(Controller.activeFish, Controller.activeRod, CaptureEndHandler);
    }

    public void CaptureEndHandler(bool isCaptured)
    {
        if (isCaptured)
        {
            Controller.caughtObjects.Add(Controller.activeFish);

            //risky.

            Item fishItem = ItemFactory.Create(Controller.activeFish);
            Controller.activeChar.GetComponentInChildren<PlayerController>().InventoryController.AddItem(fishItem);
        }

        resultHandler.Show(isCaptured, Controller.activeFish, () => Controller.SetState(NextState), Controller.failString);
    }

    public override void ExitState()
    {
        base.ExitState();

        FishGameController.EndGame();
    }
}
