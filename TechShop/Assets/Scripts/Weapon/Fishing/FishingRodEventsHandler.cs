﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRodEventsHandler : MonoBehaviour
{
    FishingRod rod;

    public AudioClipContainer throwSoundsContainer;


    private void Awake()
    {
        rod = GetComponent<FishingRod>();
        rod.OnLureThrow += OnLureThrown;
    }

    private void OnLureThrown()
    {
        throwSoundsContainer.PlayRandom(GetComponent<AudioSource>());
    }
}
