﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class FishingStrengthMeter : MonoBehaviour
{
    public Image fillImage;

    public float filledStrength;

    public float maxValue = 2f;

    public float FillMultiplier;

    public CanvasGroup cg;

    public void SetMaxValue(float max)
    {
        maxValue = max;
    }

    public void Fill(float valueIncrease)
    {
        filledStrength += valueIncrease * FillMultiplier;

        filledStrength = Mathf.Min(filledStrength, maxValue);

        fillImage.fillAmount = filledStrength / maxValue;
    }

    public void Awake()
    {
        cg = GetComponent<CanvasGroup>();
    }

    public void Reset()
    {
        filledStrength = 0f;

        Fill(filledStrength);
    }

    public float GetStrength()
    {
        return filledStrength;
    }

    public void Hide(float time = .3f, Action onComplete = null)
    {
        cg.DOFade(0f, time).OnComplete(() => onComplete?.Invoke());
    }

    public void Show(float time = .5f)
    {
        cg.alpha = 0f;
        cg.DOFade(1f, time);
    }
}