﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Random = UnityEngine.Random;


/// <summary>
/// (UI) Updates the visuals for the fishing capture game
/// </summary>
public class FishingPullInCatchMeter : MonoBehaviour
{
    //set from inspector.
    public RectTransform ComboGamePanel;

    public Image FishResolvePanel;

    /// <summary>
    /// Image for the fish
    /// </summary>
    public Image FishBox;

    /// <summary>
    /// Image for the player position
    /// </summary>
    public Image PlayerTensionBox;

    public Image TensionPanel;

    public RectTransform playArea, backGround;

    public Color OverlapColor, NoOverlapColor;

    //not set from inspector.
    public bool BoxesOverlap { get; private set; }

    public Vector2 OneDGameSizeY;
    public Vector2 TwoDGameSizeY;

    public Vector2 lastFrameFishPos;

    private bool Active;

    private RectTransform FishInnerRect;
    private FishBalanceGame BalanceGame;

    // play area size.
    private Vector2 PlayerMaxSize;
    // play area size.
    private Vector2 fishMaxSize;

    private Tweener shakeTween;

    public void Start()
    {
        FishInnerRect = FishBox.GetComponent<RectTransform>();
    }

    public void Update() {
        if (Active) {
            UpdateUI();
        }
        else {
            gameObject.SetActive(false);
        }
    }

    public void UpdateUI() {
        UpdateOverlapColors();

        UpdateBoxPositions();

        UpdateFishResolve();

        MoveVisualFish();

        UpdateFishBoxDir();
    }


    public void StartGame(FishBalanceGame fishCaptureGame, Action onStartSignal)
    {
        BalanceGame = fishCaptureGame;

        Active = true;

        ComboGamePanel.gameObject.SetActive(true);

        //opening anim
        gameObject.SetActive(true);

        gameObject.transform.DOScale(Vector3.one, .3f).SetEase(Ease.OutBack);

        gameObject.transform.localScale = Vector3.right + Vector3.up * .2f;

        FishBox.sprite = BalanceGame.ActiveFish.FishBase.Sprite;

        //game type
        if (fishCaptureGame.GameType == FishBalanceGame.BalanceGameType.twoDGame)
        {
            Open2DGame();
        }
        else
        {
            Open1DGame();
        }

        UpdateTensionBoxSizes();
        this.InvokeDelayed(.7f, () => { UpdateTensionBoxSizes(); onStartSignal(); });
    }

    //used to set up the game, set image sizes for player n fish, set player pos on fish.
    /// <summary>
    /// Updates the box sizes for player and fish.
    /// </summary>
    private void UpdateTensionBoxSizes()
    {
        PlayerTensionBox.rectTransform.sizeDelta = GetPlayerSize();
        FishBox.rectTransform.sizeDelta = GetFishSize();

        PlayerMaxSize = (TensionPanel.rectTransform.rect.size - PlayerTensionBox.rectTransform.sizeDelta);
        fishMaxSize = (TensionPanel.rectTransform.rect.size - FishBox.rectTransform.sizeDelta);

        BalanceGame.PlayerNormalizedPos = GetNormalizedPlayerPos(getPlayAreaPosFish());

        UpdateBoxPositions();
    }

    /// <summary>
    /// Update the way the fish is facing on the UI according to direction -> if it is swimming right to left, scale has to be inversed.
    /// </summary>
    private void UpdateFishBoxDir()
    {
        FishInnerRect.localScale = new Vector3(1f, 1f, 1f);
        Vector3 localEulers = FishInnerRect.localEulerAngles;
        float zero = 90f;
        float fishRectAngle = FishInnerRect.localEulerAngles.z + zero;
        fishRectAngle %= 360f;

        if (fishRectAngle < 0f)
        {
            FishInnerRect.localScale = new Vector3(1f, -1f, 1f);
        }
        if (fishRectAngle > 180f)
        {
            FishInnerRect.localScale = new Vector3(1f, -1f, 1f);
        }
    }

    private void Open2DGame()
    {
        Vector2 endSize = new Vector2(backGround.anchorMin.x, TwoDGameSizeY.x);
        playArea.DOAnchorMin(endSize, .5f);
        backGround.DOAnchorMin(endSize, .5f).OnUpdate(UpdateTensionBoxSizes);
    }

    private void Open1DGame()
    {
        Vector2 endSize = new Vector2(backGround.anchorMin.x, OneDGameSizeY.x);
        playArea.DOAnchorMin(endSize, .5f);
        backGround.DOAnchorMin(endSize, .5f).OnUpdate(UpdateTensionBoxSizes);
    }

    public Vector2 GetPlayerSize()
    {
        Vector2 playerSize = BalanceGame.playerNormalizedSize * TensionPanel.rectTransform.rect.size;
        return playerSize;
    }

    public Vector2 GetFishSize()
    {
        Vector2 fishSize = BalanceGame.fishNormalizedSize * TensionPanel.rectTransform.rect.size;
        return fishSize;
    }

    /// <summary>
    /// Updates the visual boxes of the player and fish.
    /// </summary>
    private void UpdateBoxPositions() {
        PlayerTensionBox.rectTransform.anchoredPosition = getPlayAreaPosPlayer();

        FishBox.rectTransform.anchoredPosition = getPlayAreaPosFish();

        BoxesOverlap = false;

        float distX = Mathf.Abs(PlayerTensionBox.rectTransform.anchoredPosition.x - FishBox.rectTransform.anchoredPosition.x);
        float distY = Mathf.Abs(PlayerTensionBox.rectTransform.anchoredPosition.y - FishBox.rectTransform.anchoredPosition.y);

        if (distX < PlayerTensionBox.rectTransform.rect.size.x / 2f) {
            if (distY < PlayerTensionBox.rectTransform.rect.size.y / 2f) {
                BoxesOverlap = true;
            }
        }
    }

    /// <summary>
    /// Does small animation and sets the forward of the fish image.
    /// </summary>
    private void MoveVisualFish()
    {
        Vector2 currentAnchor = getPlayAreaPosFish();
        Vector2 moveVector = lastFrameFishPos - currentAnchor;

        lastFrameFishPos = currentAnchor;

        Vector3 currentrot = FishInnerRect.eulerAngles;

        FishInnerRect.right = moveVector;

        Vector3 wanted = FishInnerRect.eulerAngles;

        //wigglin.
        wanted += new Vector3(Mathf.Sin(Time.time * 5f) * 25f, 0f, Mathf.Sin(Time.time * 5f) * 15f);

        FishInnerRect.eulerAngles = currentrot;

        FishInnerRect.DOLocalRotate(wanted, .3f);
    }

    /// <summary>
    /// Updates the progress bar aka fish resolve.
    /// </summary>
    private void UpdateFishResolve()
    {
        FishResolvePanel.fillAmount = BalanceGame.FishProgress;

        float dist = Mathf.Abs(.5f - BalanceGame.FishProgress) * .5f;

        Vector3 rand = new Vector3(
            Random.Range(-.1f, .1f),
            0f, 
            Random.Range(-.1f, .1f)
            );

        BalanceGame.Controller.ActiveRod.lure.transform.DOBlendableMoveBy(rand, .5f);

        if (shakeTween == null)
        {
            shakeTween = FishResolvePanel.transform.parent.GetComponent<RectTransform>()
                .DOShakeAnchorPos(.1f, 10 * dist, 15, 45)
                .OnComplete(() => shakeTween = null);
        }
    }


    public Vector2 GetNormalizedPlayerPos(Vector2 playAreaPos)
    {
        Vector2 anchorPosPlayer = PlayerTensionBox.rectTransform.anchorMin;
        
        Vector2 playerSize = PlayerMaxSize * anchorPosPlayer;

        if (PlayerMaxSize.y == 0f)
        {
            PlayerMaxSize.y = 0.01f;
        }

        Vector2 playerNormalizedPos = (playAreaPos + playerSize) / PlayerMaxSize;

        playerNormalizedPos.x = Mathf.Clamp01(playerNormalizedPos.x);
        playerNormalizedPos.y = Mathf.Clamp01(playerNormalizedPos.y);

        return playerNormalizedPos;
    }


    public Vector2 getPlayAreaPosFish()
    {
        Vector2 fishPos = BalanceGame.ActiveFishBehaviour.NormalizedPos * fishMaxSize;
        Vector2 anchorPosFish = FishBox.rectTransform.anchorMin;

        //at anchor 0.5f, 0.5f, half the play area is negative.
        fishPos -= fishMaxSize * anchorPosFish;
        return fishPos;
    }


    private Vector2 getPlayAreaPosPlayer()
    {
        Vector2 playerPos = BalanceGame.PlayerNormalizedPos * PlayerMaxSize;
        Vector2 anchorPosPlayer = PlayerTensionBox.rectTransform.anchorMin;

        playerPos -= PlayerMaxSize * anchorPosPlayer;
        return playerPos;
    }



    private void UpdateOverlapColors()
    {
        if (BoxesOverlap)
        {
            PlayerTensionBox.DOColor(OverlapColor, .1f);
        }
        else
        {
            PlayerTensionBox.DOColor(NoOverlapColor, .1f);
        }
    }


    public void EndGame(bool winner, Action onEndSignal)
    {
        gameObject.transform.DOScale(Vector3.right + (Vector3.up * .2f), .3f).SetEase(Ease.InBack).OnComplete(() =>
        {
            gameObject.SetActive(false);
            Active = false;
            onEndSignal?.Invoke();
        });
    }
}
