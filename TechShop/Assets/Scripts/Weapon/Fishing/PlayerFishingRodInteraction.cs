﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerFishingRodInteraction : ToolInteraction
{
    public enum FishingType
    {
        Beginner,
        Intermediate,
        Master,
    }

    public FishingType State;

    public IMiniGame activeGame;

    public FishingRod Rod;

    public PlayerFishingRodInteraction(ToolInteraction copyFrom, Tool tool, ToolUser user) : base(copyFrom, tool, user)
    {
        State = getFishingState(user);

        Rod = Tool as FishingRod;
    }

    //wip stats not done.
    private FishingType getFishingState(ToolUser user)
    {
        if (user.Stats.Fishing <= 0)
        {
            return FishingType.Beginner;
        }

        if (user.Stats.Fishing <= 4)
        {
            return FishingType.Intermediate;
        }

        return FishingType.Master;
    }

    public override void Stop()
    {
        activeGame.End();
    }

    public override void Interrupt(Action onComplete)
    {
        void finalize() { SignalComplete(); onComplete?.Invoke(); }

        if (Rod.isLureDeployed)
        {
            Rod.PullInLure(User.transform.root.GetComponentInChildren<HumanController>().animController, (x) => finalize());
        }
        else
        {
            finalize();
        }
    }

    public override void Execute(Action onComplete)
    {
        if (State == FishingType.Beginner)
        {
            activeGame = GameLogic.Instance.FishingGamesCollection.GetMiniGame(FishingGamesCollection.FishingGameEnum.Beginner);
            FishingMiniGame _g = activeGame as FishingMiniGame;
            _g.StartGame(Rod, (x) => onComplete?.Invoke(), User.transform.parent.GetComponentInChildren<HumanController>());
            _g.StageChanged += SetStage;
        }
    }
}
