﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingPullInLureState : FishingState
{
    public override FishingStates State => FishingStates.PullInLure;

    private bool Triggered = false;

    public bool waitForInput = false;

    public override void EnterState()
    {
        base.EnterState();

        Triggered = false;
    }

    public override void ExitState()
    {
        base.ExitState();

        Triggered = false;
    }

    public override void UpdateBehaviour()
    {
        base.UpdateBehaviour();

        if (Triggered)
        {
            return;
        }

        if (!UI.Instance.MouseOnUI)
        {
            if (!waitForInput || Input.GetMouseButtonDown(0))
            {
                Triggered = true;
                PullInLure();
            }
        }
    }

    private void PullInLure()
    {
        //Too specific? > this should be assigned from outside probably.
        Controller.activeRod.PullInLure(Controller.activeChar.animController, (x) => Controller.SignalFishingComplete(this));
    }
}
