﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FishingStates
{
    SetDirection,
    SetStrength,
    SetPos,
    ThrowOutLure,
    BaitFish,
    CaptureFish,
    PullInLure,
    Unset,
}

public abstract class FishingState : GenericState
{
    public FishingStateController Controller { get; private set; }

    public abstract FishingStates State { get; }

    public FishingState NextState;

    public int Stage;

    public override void Init(GenericStateController controller)
    {
        base.Init(controller);

        Controller = controller as FishingStateController;
    }
}
