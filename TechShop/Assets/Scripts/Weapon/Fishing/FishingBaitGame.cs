﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FishingBaitGame : MonoBehaviour, IMiniGame
{
    public class Target
    {
        public float Size;
        public float decreaseSizeSpeed;
        public float speedIncreaseDelta = .1f;

        public void Update(float deltaTime)
        {
            Size -= decreaseSizeSpeed * deltaTime;
            decreaseSizeSpeed += speedIncreaseDelta * deltaTime;
        }
    }


    public delegate void OnFishBaitedHandler(bool success, Action onCompleted);

    public UIFishingBaitGame baitGameUI;

    public FishingGamesCollection.FishingGameEnum GameEnum => FishingGamesCollection.FishingGameEnum.BaitGame;

    public float NormalizedProgress => Mathf.InverseLerp(RangeMin, RangeMax, Progress);
    public float Progress { get; internal set; }

    [Header("Baiting game circle movement values")]
    public float RangeMin = -.5f;
    public float RangeMax = 1f;

    public Vector2 MinMaxSpeed;
    public Vector2 MinMaxCenterSize;
    public Vector2 MinMaxSpawnDelay;
    public Vector2 MinMaxSpeedDelta;
    public Vector2 MinMaxClampProgress;

    public float CenterMatchSize = .1f;
    public float errorAllowed = .1f;

    public GameObject Lure;

    public bool isActive = false;

    private HumanController activeChar;

    public AudioClipContainer positive, negative;

    public event Action OnProgressUpdate;
    public event Action GameStarting;

    private event Action<bool> onEnd;

    public event Action<Target> TargetAdded;
    public event Action<float> OnTargetPopped;

    public event Action<float> UpdateGame;
    public event Action<float> TargetSizeChanged;

    public Target ActiveTarget;

    public void OnGameFinished(bool winner)
    {
        onEnd?.Invoke(winner);

        End(.5f);
    }

    public void SpawnNewTarget()
    {
        if (!isActive) { return; }

        ActiveTarget = new Target()
        {
            Size = 2f,
            decreaseSizeSpeed = Random.Range(MinMaxSpeed.x, MinMaxSpeed.y),
            speedIncreaseDelta = Random.Range(MinMaxSpeedDelta.x, MinMaxSpeedDelta.y)
        };

        TargetAdded?.Invoke(ActiveTarget);
    }

    public void ShowBaitGameResult(bool success, Action p)
    {
        if (!success)
        {
            baitGameUI.ShowBaitFailure(() => p?.Invoke());
            return;
        }

        baitGameUI.ShowBaitSuccess(() => p?.Invoke());
    }

    public void End(float delay)
    {
        isActive = false;

        if (delay > 0.01f)
        {
            this.InvokeDelayed(delay, baitGameUI.End);
            return;
        }

        baitGameUI.End();
    }

    public void End()
    {
        End(0f);
    }

    public void Update()
    {
        if (!isActive)
        {
            return;
        }

        TickGame();
    }

    private void TickGame()
    {
        UpdateGame?.Invoke(Time.deltaTime);

        if (Input.GetMouseButtonDown(0))
        {
            PopActiveTarget();
        }

        if (ActiveTarget == null)
        {
            return;
        }

        if ((ActiveTarget.Size + (errorAllowed + .2f)) < CenterMatchSize)
        {
            PopActiveTarget();
            return;
        }

        UpdateActiveTarget();
    }

    private void UpdateActiveTarget()
    {
        ActiveTarget.Update(Time.deltaTime);
    }

    private void PopActiveTarget()
    {
        if (ActiveTarget == null)
        {
            Debug.LogError("fish bait game: cannot pop when no activetarget is set.");
            return;
        }

        //random and not related?
        Lure.GetComponent<Rigidbody>().AddForce(Vector3.up * 2f, ForceMode.VelocityChange);

        PopTarget(ActiveTarget);

        ActiveTarget = null;

        SetCenterMatchSize(Random.Range(MinMaxCenterSize.x, MinMaxCenterSize.y));

        this.InvokeDelayed(Random.Range(MinMaxSpawnDelay.x, MinMaxSpawnDelay.y), SpawnNewTarget);
    }

    private void SetCenterMatchSize(float size)
    {
        CenterMatchSize = size;
        TargetSizeChanged?.Invoke(size);
    }

    public void StartGame(Action<bool> onBaitGameCompleteHandler, GameObject Lure)
    {
        Progress = 0f;

        isActive = true;

        onEnd = null;

        onEnd += onBaitGameCompleteHandler;

        this.Lure = Lure;

        this.InvokeDelayed(.3f, SpawnNewTarget);

        GameStarting?.Invoke();

        baitGameUI.Show(this);
    }

    public void PopTarget(Target target)
    {

        float diff = Mathf.Abs(CenterMatchSize - target.Size);

        float addedProgress = Mathf.Clamp(errorAllowed - diff, MinMaxClampProgress.x, MinMaxClampProgress.y);

        OnTargetPopped?.Invoke(addedProgress);

        //Look at this ? make it a ncalc formula?
        if (addedProgress > 0f)
        {
            positive.PlayRandom(GetComponent<AudioSource>());
            addedProgress += .02f;
            addedProgress *= 2f;
        }
        else
        {
            negative.PlayRandom(GetComponent<AudioSource>());
        }

        Progress += addedProgress;
        OnProgressUpdate?.Invoke();

        EvaluateWinLoss();
    }

    private void EvaluateWinLoss()
    {
        if (Progress >= 1f)
        {
            OnGameFinished(true);
        }

        if (Progress <= -.5f)
        {
            OnGameFinished(false);
        }
    }
}
