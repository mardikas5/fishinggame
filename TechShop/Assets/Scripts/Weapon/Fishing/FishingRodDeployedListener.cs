﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRodDeployedListener : MonoBehaviour
{
    public FishingRod rod;

    public bool isPlaying = false;

    // Update is called once per frame
    void Update()
    {
        if (rod.isLureDeployed)
        {
            PlaySounds();
        }
        else
        {
            StopSounds();
        }
    }

    private void StopSounds()
    {
        isPlaying = false;
        GetComponent<AudioSource>().enabled = false;
    }

    private void PlaySounds()
    {
        isPlaying = true;
        GetComponent<AudioSource>().enabled = true;
    }
}
