﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishGameLayer : MonoBehaviour
{
    public bool Completed = false;

    public Fish ActiveFish;
    public FishCaptureGameBehaviour ActiveFishBehaviour;
    public FishGameController Controller;

    public bool IsActive = false;

    public bool Success;

    public float FishProgress => Controller.FishProgress;


    public virtual void StartGame(Fish activeFish, FishGameController controller)
    {
        Completed = false;

        Success = false;

        Controller = controller;

        ActiveFishBehaviour = activeFish.GetComponent<FishCaptureGameBehaviour>();   
    }

    public virtual void EndGame(bool winner)
    {
        
    }
}
