﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FishingGamesCollection : MonoBehaviour
{
    public enum FishingGameEnum
    {
        Beginner,
        Advanced,
        BaitGame
    }

    [SerializeField]
    private List<MonoBehaviour> minis = new List<MonoBehaviour>();

    public List<IMiniGame> miniGames = new List<IMiniGame>();


    public void Start()
    {
        foreach (MonoBehaviour p in minis)
        {
            if (p is IMiniGame)
            {
                miniGames.Add(p as IMiniGame);
            }
        }
    }

    public IMiniGame GetMiniGame(FishingGameEnum gameType)
    {
        return miniGames.FirstOrDefault((x) => x.GameEnum == gameType);
    }
}
