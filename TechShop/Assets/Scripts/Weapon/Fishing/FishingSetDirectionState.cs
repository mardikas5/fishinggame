﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingSetDirectionState : FishingState
{
    public GameObject DirectionArrow;

    public override FishingStates State => FishingStates.SetDirection;

    public event Action<Vector3> OnGetDirection;


    public override void UpdateBehaviour()
    {
        base.UpdateBehaviour();
        ShowDirectionHandler();
    }

    public override void EnterState()
    {
        base.EnterState();

        if (Controller.enabled == false)
        {
            return;
        }

        if (Controller.activeChar?.transform == null)
        {
            return;
        }

        DirectionArrow.gameObject.transform.position = Controller.activeChar.transform.position;
        DirectionArrow.gameObject.SetActive(true);

        Controller.activeChar.animController.SetAnimTagFloat(CharAnimController.AnimTags.ChargeAttack, 1f);
        Controller.activeChar.animController.SetAnimTagFloat(CharAnimController.AnimTags.Attack, Controller.activeRod.baseInteraction.AnimTypeParam);
    }

    public override void ExitState()
    {
        base.ExitState();
        DirectionArrow.gameObject.SetActive(false);
    }

    private void ShowDirectionHandler()
    {
        if (UI.Instance.MouseOnUI)
        {
            return;
        }

        //danger.
        Vector3 wantedDir = Controller.activeChar.GetComponentInChildren<PlayerMoveController>().GetCharacterFaceDirection();

        Controller.activeChar.visuals.transform.DOLookAt(Controller.activeChar.visuals.transform.position + wantedDir, .2f);

        DirectionArrow.transform.forward = wantedDir;

        DirectionArrow.transform.position = Controller.activeChar.transform.position;

        if (Input.GetMouseButtonDown(0))
        {
            OnGetDirection?.Invoke(wantedDir);
            Controller.direction = wantedDir;
            Controller.SetState(NextState);
        }
    }
}
