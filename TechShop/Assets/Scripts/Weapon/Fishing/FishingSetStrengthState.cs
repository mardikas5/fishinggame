﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingSetStrengthState : FishingState
{
    public override FishingStates State => FishingStates.SetStrength;

    public event Action<float> OnStrengthSet;

    public FishingStrengthMeter StrengthMeter;

    public bool FillStarted;

    public float strengthMultiplier;

    public float startDelay = .3f;
    public float startTime = 0f;

    public override void EnterState()
    {
        base.EnterState();

        StrengthMeter.gameObject.SetActive(true);

        startTime = Time.time + startDelay;

        StrengthMeter.Show();

        StrengthMeter.Reset();
    }

    public override void ExitState()
    {
        base.ExitState();

        StrengthMeter.Hide(.4f, () => StrengthMeter.gameObject.SetActive(false));
    }

    public override void UpdateBehaviour()
    {
        base.UpdateBehaviour();


        if (startTime >= Time.time)
        {
            return;
        }

        if (Input.GetMouseButton(0))
        {
            FillStarted = true;
            StrengthMeter.Fill(Time.deltaTime);
        }
        if (Input.GetMouseButtonUp(0))
        {
            OnRelease();
        }
    }

    private void OnRelease()
    {
        if (!FillStarted)
        {
            return;
        }

        float str = StrengthMeter.GetStrength();

        OnStrengthSet?.Invoke(str);
        Controller.strength = str * strengthMultiplier; //multiply by some value?
        Controller.SetState(NextState);
    }
}
