﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Trigger for the typing minigame during fishing.
/// </summary>
[Serializable]
public class FishingComboTrigger
{
    public char[] keyCombo = new char[0];

    public float NormalizedPos;

    public float TimeToSolve;

    public int ComboIndex;

    public bool Completed;

    //Combo was complete
    public event Action onComboComplete;
    //Mistake was made.
    public event Action onMistakeMade;
    //Minigame completed -> was it fail or success
    public event Action<bool> ComboResultSignal;

    public int MistakesCount = 0;

    public bool Started = false;
    

    public void SignalResult(bool result)
    {
        ComboResultSignal?.Invoke(result);
    }

    public char[] GenerateCombo(int length, char[] allowedChars)
    {
        MistakesCount = 0;
        Completed = false;
        keyCombo = generateCombo(length, allowedChars);
        return keyCombo;
    }

    private char[] generateCombo(int length, char[] allowedChars)
    {
        char[] values = new char[length];

        for (int i = 0; i < length;i++)
        {
            int takeChar = Random.Range(0, allowedChars.Length);
            values[i] = allowedChars[takeChar];
        }

        return values;
    }

    public bool EnterCombo(KeyCode key)
    {
        if (key == KeyCode.None)
        {
            return true;
        }

        bool validKey = checkKey(key, ComboIndex);

        if (validKey)
        {
            ComboIndex++;
        }
        else
        {
            Debug.LogError("mistake made - expected: " + keyCombo[ComboIndex] +  " got: " + (char)key);
            MistakesCount++;
            onMistakeMade?.Invoke();
        }

        if (ComboIndex >= keyCombo.Length)
        {
            Completed = true;
            onComboComplete?.Invoke();
        }

        return validKey;
    }

    private bool checkKey(KeyCode key, int index)
    {
        if (index >= keyCombo.Length)
        {
            return true;
        }

        if (key == (KeyCode)keyCombo[index])
        {
            return true;
        }

        return false;
    }

    public void FailCombo()
    {
        MistakesCount += 1;
        onComboComplete?.Invoke();
    }
}
