﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRod : Tool
{
    private Vector3 activeDir;

    //serialized and assigned in editor.
    [SerializeField]
    public PlayerFishingRodInteraction baseInteraction;

    public GameObject lure;

    public MartRopeController ropeController;

    public MartRope rope;

    public bool isLureDeployed;

    private CollisionListener lureListener;

    public event Action<Collision> OnLureEndHit;
    public event Action OnLureThrow;
    

    private void Start()
    {
        isLureDeployed = false;
    }
    
    private void InitLure()
    {
        if (!lure.GetComponent<CollisionListener>())
        {
            lure.AddComponent<CollisionListener>();
        }

        lureListener = lure.GetComponent<CollisionListener>();

        lureListener.onCollisionEnter += (x) => OnLureEndHit?.Invoke(x);
    }


    public override void SetUser(ToolUser user)
    {
        base.SetUser(user);

        rope.CreateRope(rope.target);
    }

    private void Update()
    {
        float dist = GetLureDistance();
    }

    public float GetLureDistance()
    {
        return Vector3.Distance(lure.transform.position, transform.position);
    }

    public void PullInLure(CharAnimController anim, Action<bool> onComplete)
    {
        if (!isLureDeployed)
        {
            onComplete?.Invoke(false);
            return;
        }

        if (anim != null)
        {
            anim.SetAnimTagFloat(CharAnimController.AnimTags.ChargeAttack, 1f);
            anim.SetAnimTagFloat(CharAnimController.AnimTags.Attack, baseInteraction.AnimTypeParam);
            this.InvokeDelayed(.1f, () => anim.SetAnimTagFloat(CharAnimController.AnimTags.Attack, -1f));
        }

        ropeController.ReelInRope();
        this.InvokeDelayed(.6f, () => onComplete?.Invoke(true));

        isLureDeployed = false;
    }


    public void ThrowLureToPoint(float str, Action<bool> onComplete, CharAnimController anim = null)
    {
        if (isLureDeployed)
        {
            onComplete?.Invoke(false);
            return;
        }

        if (anim != null)
        {
            anim.SetAnimTagFloat(CharAnimController.AnimTags.ChargeAttack, 1f);
            anim.SetAnimTagFloat(CharAnimController.AnimTags.Attack, baseInteraction.AnimTypeParam);
            this.InvokeDelayed(.1f, () => anim.SetAnimTagFloat(CharAnimController.AnimTags.ChargeAttack, 0f));
        }

        OnLureThrow?.Invoke();
        ropeController.ThrowLure(str, (User.transform.parent.GetComponentInChildren<HumanController>().visuals.forward) + (Vector3.up * .2f));

        ///
        this.InvokeDelayed(3f, () => onComplete?.Invoke(true));

        isLureDeployed = true;
    }

    public void OnDestroy()
    {
        Destroy(lure);
    }

    public override bool ReleaseUseTool(InputEnum released, Action onComplete, bool isInterrupted)
    {
        return true;
    }

    public override bool UseTool(InputEnum attackInput, out ToolInteraction interaction)
    {
        interaction = new PlayerFishingRodInteraction(baseInteraction, this, User);

        SignalToolUse(interaction);

        if (!interaction.IsValid())
        {
            return false;
        }

        return true;
    }
}
