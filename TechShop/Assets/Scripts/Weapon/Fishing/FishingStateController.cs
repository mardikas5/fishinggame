﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingStateController : GenericStateController
{
    public FishingRod activeRod;
    public HumanController activeChar;

    public string failString;

    public Vector3 direction;
    public float strength;

    public Vector3 endPoint;

    public Fish activeFish;

    public List<IFishable> caughtObjects = new List<IFishable>();

    public WaterBody ActiveWaterBody { get; internal set; }

    public event Action onFishingComplete;

    public void ReturnToStart()
    {
        SetState(startState);
    }

    public void StartFishing(FishingState startState, Action<bool> onComplete)
    {
        caughtObjects.Clear();
        activeFish = null;

        Action signalOnce = null;

        signalOnce = () => { onFishingComplete -= signalOnce; onComplete?.Invoke(true); };
        onFishingComplete += signalOnce;

        SetState(startState);
    }

    public void SignalFishingComplete(FishingState finalState)
    {
        onFishingComplete?.Invoke();
    }

    public void OnEnable()
    {
        SetState(ActiveState);
    }

    public void OnDisable()
    {
        SetState(null);
    }

    public override void SetState(GenericState newState)
    {
        if (!enabled)
        {
            Debug.Log(" controller not enabled, ignoring set state. ");
            return;
        }
        base.SetState(newState);
    }

    public void SetFailReason(string FailString)
    {
        failString = FailString;
    }
}