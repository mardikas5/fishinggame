﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FishingBaitState : FishingState
{
    public string hitGroundString = " hit dry land ";
    public string failedBaitString = " failed to bait fish ";
    public int waterLayerIndex;

    private GameObject Lure;

    public override FishingStates State => FishingStates.BaitFish;

    public FishingBaitGame BaitingGame;


    public override void ExitState()
    {
        base.ExitState();

        BaitingGame.End();
    }

    public override void EnterState()
    {
        base.EnterState();

        Lure = Controller.activeRod.lure;

        if (!CheckLurePosition())
        {
            OnLureHitGround();
            return;
        }

        AssignFish();

        BaitingGame.StartGame(OnBaitGameCompleteHandler, Lure);
    }

    private bool CheckLurePosition()
    {
        WaterGerstnerHelper.Instance.GetGersntnerAtPosition(
            out Vector3 waterPos,
            transform.position,
            out bool isUnderWater,
            out float submerged);


        return (isUnderWater || submerged > -.2f);
        //Ray down = new Ray(Lure.transform.position, Vector3.down);
        //Ray downWards = new Ray(transform.position, -transform.up);

        //RaycastHit[] hits = Physics.RaycastAll(down, 1f);

        //if (hits != null && hits.Length > 0)
        //{
        //if (hits[0].transform.gameObject.layer != waterLayerIndex)
        //{
        //    return false;
        //}
        //}
        //return true;
    }

    private void AssignFish()
    {
        Controller.ActiveWaterBody = GameLogic.Instance.FishingMapController?.CurrentSpot?.waterBody;
        Controller.activeFish = Controller.ActiveWaterBody.GetFish();
    }

    private void OnLureHitGround()
    {
        Controller.activeFish = null;
        Controller.SetFailReason(hitGroundString);
        Controller.SetState(NextState);
    }

    private void OnBaitGameCompleteHandler(bool baitSuccess)
    {
        if (baitSuccess)
        {
            BaitingGame.ShowBaitGameResult(true, MoveToNextState);
            return;
        }

        Controller.SetFailReason(failedBaitString);
        Controller.activeFish = null;
        Controller.SetState(NextState);
    }

    public void MoveToNextState()
    {
        if (!Controller.enabled)
        {
            return;
        }

        Controller.SetState(NextState);
    }

    public override void UpdateBehaviour()
    {
        base.UpdateBehaviour();
    }
}
