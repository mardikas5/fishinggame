﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FishingThrowLureState : FishingState
{
    public override FishingStates State => FishingStates.ThrowOutLure;

    public event Action<float> OnLureThrown;

    public UnityEvent Unity_OnLureThrown;

    public virtual void Awake()
    {
        OnLureThrown += (x) => Unity_OnLureThrown?.Invoke();
    }

    public override void EnterState()
    {
        base.EnterState();
        ThrowLure(Controller.strength);
    }

    public override void UpdateBehaviour()
    {
        base.UpdateBehaviour();
    }

    public void ThrowLure(float strength)
    {
        //throw lure to point has a handler with a 3s delay.
        Controller.activeRod.ThrowLureToPoint(strength, (x) => OnLureThrownHandler(strength), Controller.activeChar.animController);
        OnLureThrown?.Invoke(strength);
    }

    public void OnLureThrownHandler(float str)
    {
        if (Controller.ActiveState != this)
        {
            //if state got changed for some reason (interrupted)
            return;
        }
        Controller.SetState(NextState);
    }

    public override void ExitState()
    {
        base.ExitState();
    }
}
