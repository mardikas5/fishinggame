﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//its like a state?
[Serializable]
public class WeaponAttack : MonoBehaviour
{
    public enum InputEnum
    {
        Primary,
        Secondary,
        Tetriary
    }

    public string Identifier;

    public float damageMultiplier;

    public float currentChangeTime;
    public float maxChargeTime;

    public bool IsChargeAttack;

    public event Action ExecuteSignal;
    public event Action InterruptSignal;
    public event Action CompleteSignal;

    public AttackActionHandler attackHandler;

    public bool IsBusy = false;

    private bool NeedsReset = false;

    public float AnimTypeParam = -1;

    public void Reset()
    {
        currentChangeTime = 0f;
        NeedsReset = false;
    }

    public bool Charge(float time)
    {
        currentChangeTime += time;

        if (currentChangeTime > maxChargeTime)
        {
            currentChangeTime = maxChargeTime;
            return false;
        }

        return true;
    }

    public void Execute(Action onComplete)
    {
        if (NeedsReset)
        {
            return;
        }

        Debug.Log(" executed: " + Identifier);
        ExecuteSignal?.Invoke();
        IsBusy = true;
        NeedsReset = true;

        if (attackHandler == null)
        {
            Debug.Log("attack for: " + name + " is null");
            return;
        }

        attackHandler.Begin(() => { onComplete(); Complete(); });
    }

    public void Complete()
    {
        IsBusy = false;
        CompleteSignal?.Invoke();
    }

    public void SignalInterrupt()
    {
        InterruptSignal?.Invoke();
    }
}
