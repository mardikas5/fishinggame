﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class WeaponAttackChain
{

    [Serializable]
    public class AttackComboNode
    {
        public string Identifier;
        public InputEnum inputType;
        public WeaponAttack nodeAttack;
        public List<AttackComboNode> nextNodes = new List<AttackComboNode>();

        public AttackComboNode GetNext(InputEnum Input)
        {
            AttackComboNode nextNode = nextNodes.FirstOrDefault((x) => x.inputType == Input);
            return nextNode;
        }
    }

    public List<AttackComboNode> chainDefinition = new List<AttackComboNode>();

    [NonSerialized]
    public AttackComboNode currentNode = null;

    public WeaponAttack previousAttack;

    //update?

    public void ResetChain()
    {
        
        currentNode = null;
    }

    public WeaponAttack GetNext(InputEnum inputType)
    {
        AttackComboNode nextNode = currentNode.GetNext(inputType);
        if (nextNode == null)
        {
            return chainDefinition.FirstOrDefault((x) => x.inputType == inputType)?.nodeAttack;
        }
        return nextNode?.nodeAttack;
    }

    public WeaponAttack GoToNextAttack(InputEnum inputType)
    {
        if (currentNode == null)
        {
            currentNode = chainDefinition.FirstOrDefault((x) => x.inputType == inputType);
            return currentNode?.nodeAttack;
        }

        AttackComboNode nextNode = currentNode.GetNext(inputType);
        currentNode = nextNode;

        return nextNode?.nodeAttack;
    }
}
