﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePointBoat : MonoBehaviour
{
    public Rigidbody rb;
    public List<Transform> checkPoints = new List<Transform>();

    public float planeHeight = .2f;
    public float forceMp = 0f;
    private WaterGerstnerHelper helper;

    public Vector3 lastOffset;

    public float rigidBodyUnderWaterDrag;
    public float rigidBodyUnderWaterAngleDrag;

    public float normalDrag;
    public float normalAngleDrag;

    private bool lastFrameUnder = false;
    public bool underWater => lastFrameUnder;

    public GameObject smallWakeObject;
    public Transform spawnWakePos;

    public float spawnWakeInterval = .5f;
    private float lastSpawnedWake = 0f;

    [Header("center & inertia")]
    public bool overwriteTensor;
    public Vector3 InertiaTensor;
    public bool overwriteCenterOfMass;
    public Vector3 localCenterOfMass;

    // Start is called before the first frame update
    private void Start()
    {
        normalDrag = rb.drag;
        normalAngleDrag = rb.angularDrag;
        helper = WaterGerstnerHelper.Instance;

    }

    [ContextMenu("forwards")]
    public void AddVelocityForwards()
    {
        rb.AddForce(transform.forward * 25f, ForceMode.Acceleration);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.G))
        {
            AddVelocityForwards();
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        UpdateBoat();
        UpdateRigidbody();
    }

    private void UpdateRigidbody()
    {
        if (overwriteTensor)
        {
            rb.inertiaTensor = InertiaTensor;
        }
        if (overwriteCenterOfMass)
        {
            rb.centerOfMass = localCenterOfMass;
        }
    }

    private void UpdateBoat()
    {
        float t = Time.fixedDeltaTime;
        bool isUnderWater = false;

        foreach (Transform trans in checkPoints)
        {
            ApplyForces(trans.position, rb, t, out bool underWater);
            if (underWater)
            {
                isUnderWater = true;
            }
        }

        if (!lastFrameUnder && isUnderWater)
        {
            helper.SignalHitWater(transform, rb.velocity);
        }

        lastFrameUnder = isUnderWater;

        if (isUnderWater)
        {
            rb.drag = rigidBodyUnderWaterDrag;
            rb.angularDrag = rigidBodyUnderWaterAngleDrag;
        }
        else
        {
            rb.drag = normalDrag;
            rb.angularDrag = normalAngleDrag;
        }

        lastOffset = rb.velocity * Time.fixedDeltaTime;

        if (rb.velocity.magnitude > 1f)
        {
            SpawnShipWake();
        }
    }


    public void GetWaterParams(Vector3 pos, out Vector3 waveOffset, out bool isUnderWater, out float submerged)
    {
        isUnderWater = false;
        helper.GetGersntnerAtPosition(out Vector3 offset, pos, out isUnderWater, out submerged);
        waveOffset = offset;
    }

    public void ApplyForces(Vector3 pos, Rigidbody p, float t, out bool isUnder)
    {
        GetWaterParams(pos, out Vector3 offset, out isUnder, out float submersion);

        if (isUnder)
        {
            submersion = Mathf.Clamp(submersion, 0f, 4f) / 1.5f;
            Vector3 offsetXZ = offset.x0z();
            Vector3 force = (Vector3.up + offsetXZ) * t * forceMp * submersion;
            Debug.DrawLine(pos, pos + (force / 4f), Color.green);
            //p.AddRelativeForce(force, ForceMode.acce)
            //force += offsetXZ;
            p.AddForceAtPosition(force, pos, ForceMode.Acceleration);
        }
    }

    private void SpawnShipWake()
    {
        if (spawnWakePos == null)
        {
            return;
        }

        if (Time.time > lastSpawnedWake + spawnWakeInterval)
        {
            lastSpawnedWake = Time.time;
            GameObject t = Instantiate(smallWakeObject, spawnWakePos.transform.position, Quaternion.identity, null);
        }
    }

}
