﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TimeController))]
public class PlayTimeController : MonoBehaviour
{
    public TimeController TimeController { get; private set; }

    public event Action OnDayEnded;
    public event Action OnDayStarted;

    public DateTime lastDay;

    public void Awake()
    {
        TimeController = GetComponent<TimeController>();
    }

    public void Start()
    {

    }

    public void Update()
    {
        CheckDayEnd();
    }

    private void CheckDayEnd()
    {
        if (TimeController.GameTime.Day != lastDay.Day)
        {
            EndDay();
        }

        lastDay = TimeController.GameTime;
    }

    public void EndDay()
    {
        OnDayEnded?.Invoke();
    }

    public void StartNewDay()
    {
        OnDayStarted?.Invoke();
    }
}
