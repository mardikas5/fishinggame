﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FadeAudio : MonoBehaviour
{
    public AudioSource source;

    private void Start()
    {
        if (source == null)
        {
            source = GetComponent<AudioSource>();
        }
    }

    public void FadeOut()
    {
        Debug.LogError("fade");
        source?.DOFade(0f, 10f).OnComplete(() => source.Stop());
    }
}
