﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fishy;
using UnityEngine;

namespace Fishy
{
    ///Only really meant for inventory items or entities which are not in the gameworld, contains data rather than visual representation.

    public class EntityFactory : Singleton<EntityFactory>
    {
        public static GameObject ItemsParent;

        public GameObject GenericWorldItem;

        public WorldObjectItem CreateInWorld(Item item, Vector3 pos)
        {
            WorldObjectItem instance = Instantiate(GenericWorldItem, pos, Quaternion.identity, null).GetComponent<WorldObjectItem>();
            instance.Init(item);
            return instance;
        }

        public static WorldObjectItem CreateInWorldStatic(Item item, Vector3 pos)
        {
            return Instance.CreateInWorld(item, pos);
        }

        public static EntityInstance Create(EntityBase template)
        {
            return Generate<EntityInstance>(template);
        }

        public static T Generate<T>(EntityBase template) where T : EntityInstance
        {
            if (ItemsParent == null)
            {
                ItemsParent = new GameObject("Entities");
            }

            GameObject go;
            T entity;

            if (template == null)
            {
                Debug.LogError("tried to create from null template.");
                return null;
            }
            if (template.WorldInstance != null)
            {
                go = Instantiate(template.WorldInstance).gameObject;
                entity = go.GetComponent<T>();
            }
            else
            {
                go = new GameObject();
                entity = go.AddComponent<T>();
            }

            entity.Base = template;
            go.name = template.Name + "_Instance";
            go.transform.parent = ItemsParent.transform;

            return entity;
        }
    }
}
