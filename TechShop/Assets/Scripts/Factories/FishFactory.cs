﻿using Fishy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

[Serializable]
public class FishFactory : Singleton<FishFactory>
{
    public GameObject FishPrefab;

    public Fish GenerateFish(EntityBase baseTemplate)
    {
        FishBase template = baseTemplate.GetComponentSO<FishBase>();
        if (template == null)
        {
            return null;
        }
        Fish fish = Instantiate(Instance.FishPrefab, Vector3.zero, Quaternion.identity, null).GetComponent<Fish>();

        fish.Base = baseTemplate;
        fish.FishBase = template;
        fish.Size = Random.Range(template.MinSize, template.MaxSize);

        fish.name = baseTemplate.Name + "_Instance";
        return fish.GetComponent<Fish>();
    }

    public static Fish GenerateFishItem(EntityBase baseTemplate)
    {
        FishBase template = baseTemplate.GetComponentSO<FishBase>();
        Fish fish = EntityFactory.Generate<Fish>(baseTemplate);
        fish.FishBase = template;
        fish.Size = Random.Range(template.MinSize, template.MaxSize);
        return fish.GetComponent<Fish>();
    }

    public static Fish GenerateFishStatic(EntityBase baseTemplate)
    {
        return Instance.GenerateFish(baseTemplate);
    }
}
