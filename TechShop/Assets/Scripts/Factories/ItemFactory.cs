﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFactory : Singleton<ItemFactory>
{
    public static Item Create(EntityInstance Entity)
    {
        if (Entity?.Base == null)
        {
            Debug.LogError("entity not given to create item from");
            return null;
        }

        ItemBase itemBase = Entity.Base.GetComponentSO<ItemBase>();
        if (itemBase == null)
        {
            Debug.LogError($"cannot make item out of {Entity}, missing ItemBase component.");
            return null;
        }
        Item i = new Item(Entity, itemBase);

        return i;
    }
}
