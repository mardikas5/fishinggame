﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class InteractionHighlightController : MonoBehaviour
{
    public int interactionLayer;

    public bool InteractionsEnabled => mouseInputHandler.InputActive;

    [Serializable]
    public class activeHightlight
    {
        public GameObject gameObject;
        public int originalLayer;
    }

    public activeHightlight ActiveHightlight = new activeHightlight();
    public MouseInputController mouseInputHandler;

    public PostProcessOutline outline;
    public PostProcessVolume outlineProcessVolume;

    public Color interactableColor;
    public Color nonInteractableColor;
    public Color tweenedColor;
    private TweenerCore<Color, Color, ColorOptions> colorTween;

    public void Awake()
    {
        mouseInputHandler = GetComponent<MouseInputController>();
    }

    public void Start()
    {
        outlineProcessVolume.profile.TryGetSettings(out outline);
    }

    public void Update()
    {
        UpdateHighlight();

        outline.color.value = tweenedColor;
    }

    public void UpdateHighlight()
    {
        if (!InteractionsEnabled)
        {
            SetHighlight(null);
            return;
        }

        if (mouseInputHandler.ActiveInputRequest.Count == 0)
        {
            SetHighlight(null);
            return;
        }

        MouseInputController.InputRequestUser highestPriority = mouseInputHandler?.ActiveInputRequest.OrderByDescending((x) => x.Priority)?.FirstOrDefault();
        SetHighlight(highestPriority);
    }

    public void RestoreLayers()
    {
        if (ActiveHightlight?.gameObject == null)
        {
            return;
        }


        ActiveHightlight.gameObject.layer = ActiveHightlight.originalLayer;
        //need to store original object and all layers.
        foreach (Transform child in ActiveHightlight.gameObject.GetComponentsInChildren<Transform>())
        {
            child.gameObject.layer = ActiveHightlight.originalLayer;
        }
    }

    public void SetHighlight(MouseInputController.InputRequestUser go)
    {
        GameObject gameObj = go?.obj as GameObject;

        if (gameObj == ActiveHightlight?.gameObject)
        {
            if (go != null)
            {
                SetHighlightType(go.interactable);
            }
            else
            {
                tweenedColor = Color.clear;
            }

            return;
        }
        else
        {
            tweenedColor = Color.clear;
        }

        RestoreLayers();

        if (go == null)
        {
            ActiveHightlight.gameObject = null;
            return;
        }

        setHighLight(gameObj);
    }

    private void setHighLight(GameObject go)
    {
        ActiveHightlight.gameObject = go;
        ActiveHightlight.originalLayer = go.layer;

        go.layer = interactionLayer;

        foreach (Transform child in go.GetComponentsInChildren<Transform>())
        {
            child.gameObject.layer = interactionLayer;
        }

    }

    private void SetHighlightType(bool interactable)
    {
        Color endColor = interactableColor;
        if (!interactable)
        {
            endColor = nonInteractableColor;
        }

        this.InvokeDelayed(.1f, () =>
        {
            colorTween?.Kill(false);
            colorTween = DOTween.To(() => tweenedColor, (x) => tweenedColor = x, endColor, .3f);
        });
    }
}
