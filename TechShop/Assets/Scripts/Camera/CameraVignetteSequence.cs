﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;

public class CameraVignetteSequence : MonoBehaviour
{
    public PostProcessVolume vignetteVolume;
    public Vignette vignette;

    public TweenerCore<float, float, FloatOptions> Fade;

    public void Awake()
    {
        vignetteVolume.profile.TryGetSettings(out Vignette vig);
        vignette = vig;
    }

    public void Start()
    {
        //this.InvokeDelayed(.5f, () =>
        //{
        //    FadeIn();
        //});
    }

    public void FadeTo(float intensity, float time)
    {
        CheckFade();
        Fade = DOTween.To(() => vignette.intensity.value, (x) => vignette.intensity.value = x, intensity, time);
    }

    public void FadeIn(float time = .3f)
    {
        FadeTo(1f, time);
    }

    public void FadeOut(float time = .4f)
    {
        FadeTo(0f, time);
    }

    private void CheckFade()
    {
        if (Fade != null)
        {
            Fade.Kill();
            Fade = null;
        }
    }
}
