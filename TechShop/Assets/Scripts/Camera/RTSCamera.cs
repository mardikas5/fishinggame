using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSCamera : MonoBehaviour
{
    public enum Axis
    {
        X = 0,
        Y = 1,
        Z = 2
    }


    public BoxCollider BoundsCollider;

    public float scrollSpeed;
    public float zoomSpeed;

    public float scrollDampening;
    public float zoomDampening;

    public Vector3 CameraMovement = Vector3.zero;

    public Axis VerticalAxis = Axis.Y;
    public bool OrthoCamera;

    private float ZoomScrolling = 0f;

    // Use this for initialization
    void Start()
    {

    }

    public Vector3 GetInput()
    {
        float h = Input.GetAxisRaw( "Horizontal" ) * scrollSpeed;
        float v = Input.GetAxisRaw( "Vertical" ) * scrollSpeed;
        float scroll = Input.GetAxisRaw( "Mouse ScrollWheel" ) * zoomSpeed * -100f;

        return new Vector3(h, v, scroll);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 input = GetInput();

        ZoomScrolling += input.z;

        ZoomScrolling *= zoomDampening;

        float t = DoCameraMove(OrthoCamera);

        float camHeight = 0f;

        if (!OrthoCamera)
        {
            camHeight = ZoomScrolling;
        }

        if ( VerticalAxis == Axis.Y )
        {
            CameraMovement += new Vector3(input.x, camHeight, input.y) * Time.deltaTime;
        }
        //keep default case z axis as vertical
        else
        {
            CameraMovement += new Vector3(input.x, input.y, camHeight) * Time.deltaTime;
        }

        CameraMovement *= scrollDampening;

        Vector3 ContainedPos = transform.position + CameraMovement * Mathf.Lerp( 1.0f, 5.0f, t);

        for ( int i = 0; i < 3; i++ )
        {
            ContainedPos[i] = LimitAxis( ContainedPos[i], (Axis)i );
        }

        transform.position = ContainedPos;
    }

    private float DoCameraMove(bool ortho)
    {
        if (ortho)
        {
            return DoCameraZoomOrtho();
        }
        return DoCameraZoomPerspective();
    }

    private float DoCameraZoomPerspective()
    {
        return Camera.main.transform.position.y / 2f;
    }

    private float DoCameraZoomOrtho()
    {
        float minZoom = 8.0f;
        float maxZoom = 40.0f;

        float newOrthoSize = Mathf.Clamp(Camera.main.orthographicSize + ZoomScrolling , minZoom, maxZoom);
        Camera.main.orthographicSize = newOrthoSize;

        return (newOrthoSize - minZoom) / (maxZoom - minZoom);
    }

    public float LimitAxis( float var, Axis axis )
    {
        Bounds b = BoundsCollider.bounds;

        Vector3 Min = BoundsCollider.bounds.center - BoundsCollider.bounds.extents;
        Vector3 Max = BoundsCollider.bounds.center + BoundsCollider.bounds.extents;

        return Mathf.Min( Max[(int)axis],
                Mathf.Max( Min[(int)axis], var ) );
    }
}
