﻿using Cinemachine;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public enum CameraStateEnum
    {
        DefaultPlayer,
        CloseUp,
        VeryCloseUp,
        Movie
    }

    public CinemachineTargetGroup targetGroup;

    public CinemachineVirtualCamera playerCamera;

    public CinemachineVirtualCamera closeUpCamera;

    public CinemachineVirtualCamera veryCloseCamera;

    public CinemachineVirtualCamera movieCamera;


    public Vector3 VeryCloseOffset = new Vector3(0,3f,1.7f);

    public CameraStateEnum state;

    public int priority = 10;

    public TweenerCore<Vector3, Vector3, VectorOptions> CloseUpFaceTween;

    public List<TweenerCore<float,float,FloatOptions>> activeTweens = new List<TweenerCore<float,float,FloatOptions>>();

    public Transform lastTarget;
    public Transform ConvoTransform;

    public float newCloseUpDelay;
    public float newCloseUpMoveTime;
    public float CloseUpRotTime;

    public BehindObjectMaskController maskController;
    

    private void RemoveEmptyTargets()
    {
        CinemachineTargetGroup.Target[] targets = targetGroup.m_Targets;
        List<CinemachineTargetGroup.Target> newTargets = new List<CinemachineTargetGroup.Target>(targets.Length);
        //cleanup
        for (int i = 0; i < targets.Length; i++)
        {
            if (targets[i].weight <= 0.01f)
            {
                continue;
            }
            newTargets.Add(targets[i]);
        }
        targetGroup.m_Targets = newTargets.ToArray();
    }

    public void SetFocus(Transform t)
    {
        if (t == null)
        {
            //should be able to focus on null.
            return;
        }
        SetFocus(t, 1f);
    }

    public void LateUpdate()
    {
        if (state == CameraStateEnum.VeryCloseUp)
        {
            OnVeryCloseCamera(lastTarget);
        }
    }

    public void SetFocus(Transform transform, float time)
    {
        KillTweens();

        RemoveEmptyTargets();

        targetGroup.AddMember(transform, 0f, 1f);
        int memberIndex = targetGroup.FindMember(transform);

        lastTarget = transform;

        TweenerCore<float, float, FloatOptions> newTarget = null;
        newTarget = DOTween.To(() => targetGroup.m_Targets[memberIndex].weight, (x) => targetGroup.m_Targets[memberIndex].weight = x, 1f, time)
            .SetEase(Ease.InSine).SetUpdate(UpdateType.Late, false)
            .OnComplete(() => activeTweens.Remove(newTarget));

        newTarget.onComplete += () => Debug.Log(" newtarget complete ");
        activeTweens.Add(newTarget);

        for (int i = 0; i < targetGroup.m_Targets.Length - 1; i++) //-1 assuming new object added to list is last.
        {
            TweenerCore<float, float, FloatOptions> tween = null;
            int index = i;
            tween = DOTween.To(() => targetGroup.m_Targets[index].weight, (x) => targetGroup.m_Targets[index].weight = x, 0f, time)
                .OnComplete(() => activeTweens.Remove(tween));

            activeTweens.Add(tween);
        }
    }

    private void KillTweens()
    {
        foreach (TweenerCore<float, float, FloatOptions> t in activeTweens)
        {
            t.Kill(false);
        }

        activeTweens.Clear();
    }

    public void SetState(CameraStateEnum stateEnum)
    {
        state = stateEnum;

        if (stateEnum == CameraStateEnum.DefaultPlayer)
        {
            playerCamera.Priority = priority;
            priority++;
        }
        if (stateEnum == CameraStateEnum.CloseUp)
        {
            closeUpCamera.Priority = priority;
            priority++;
        }
        if (stateEnum == CameraStateEnum.VeryCloseUp)
        {
            veryCloseCamera.Priority = priority;
            priority++;
        }
        if (stateEnum == CameraStateEnum.Movie)
        {
            movieCamera.Priority = priority;
            priority++;
        }
    }


    public void OnVeryCloseCamera(Transform target)
    {
        if (target == null)
        {
            return;
        }

        if (target.TryGetComponent(out HumanController controller))
        {
            target = controller.visuals;
        }

        this.InvokeDelayed(newCloseUpDelay, () =>
       {
           SetNewCloseUp(target, newCloseUpMoveTime, CloseUpRotTime);
       });
    }

    public void SetNewCloseUp(Transform target, float moveTime, float rotTime)
    {
        CinemachineTransposer transposer = veryCloseCamera.GetCinemachineComponent<CinemachineTransposer>();
        CinemachineComposer composer = veryCloseCamera.GetCinemachineComponent<CinemachineComposer>();

        Quaternion rot = Quaternion.LookRotation(-target.forward, Vector3.up);

        ConvoTransform.DOMove( GetConvoPos(target), moveTime).SetUpdate(UpdateType.Late, false);

        ConvoTransform.DORotate(rot.eulerAngles, rotTime).SetUpdate(UpdateType.Late, false);
      
    }

    public Vector3 GetConvoPos(Transform target)
    {
        return target.transform.position + (target.transform.forward * VeryCloseOffset.z)
            + Vector3.up * VeryCloseOffset.y;
    }

    public void SetToDefaultState()
    {
        SetState(CameraStateEnum.DefaultPlayer);
        SetFocus(GameLogic.Instance.Player.transform);
    }

}
