﻿using PixelCrushers.DialogueSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocusConversant : MonoBehaviour
{
    GameLogic logic;

    public float MoveToNewConversantTime = 2f;

    public float LastConvoStartTime = 0f;

    private void Awake()
    {
        logic = GameLogic.Instance;
    }
    // Start is called before the first frame update
    private void Start()
    {

    }

    private void OnConversationStart(Transform t)
    {
        logic.Player.EnterConversation(t);

        this.InvokeDelayed(.4f, () =>
        FaceTransform(t));
    }

    private void FaceTransform(Transform t)
    {
        logic.Player.StateController.HumanController.LookAt(t);
    }

    private void OnConversationLine(Subtitle subtitle)
    {
        if (subtitle.speakerInfo.transform == null)
        {
            return;
        }

        bool isNewState = logic.CameraController.state != CameraController.CameraStateEnum.VeryCloseUp;

        logic.CameraController.SetState(CameraController.CameraStateEnum.VeryCloseUp);

        if (Time.time - LastConvoStartTime < .2f)
        {
            isNewState = true;
        }

        float time = MoveToNewConversantTime;
        if (isNewState)
        {
            Debug.LogError(" new state. ");
            LastConvoStartTime = Time.time;
            logic.CameraController.SetNewCloseUp(getFace(subtitle.speakerInfo.transform), 0f, 0f);
            time *= 0f;
        }

        Debug.LogError(time);
        logic.CameraController.SetFocus(getFace(subtitle.speakerInfo.transform), time);
    }

    private Transform getFace(Transform t)
    {
        Transform face = t.GetComponent<HumanController>()?.Face;
        if (face == null)
        {
            return t;
        }
        return face;
    }

    private void OnConversationResponseMenu(Response[] t)
    {
        logic.CameraController.SetState(CameraController.CameraStateEnum.VeryCloseUp);
        if (transform != null)
        {
            logic.CameraController.SetFocus(getFace(transform), MoveToNewConversantTime);
        }
    }

    private void OnConversationEnd(Transform t)
    {
        logic.CameraController.SetToDefaultState();
        logic.Player.ExitConversation(t);
    }
}
