﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ChangeAnimSpeed : StateMachineBehaviour
{
    public float loopInterval;
    public float loopAnimSpeed;
    public string ChargeString;
    public string AnimLoopString;

    private float loopTimeEnd;
    private bool forwards;

    private Animator Animator;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Animator = animator;
        float charge = animator.GetFloat(ChargeString);
        float value = 0f;
        DOTween.To(
            () => value, 
            x => { value = x; animator.Play(0, 0, x); }, 
            .2f,
            .3f);
    }

     //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float charge = animator.GetFloat(ChargeString);
        
        if (charge >= .99f)
        {
            DoChargeLoop();
            return;
        }

        Animator.SetFloat(AnimLoopString, 1f);
    }

    private void DoChargeLoop()
    {
        if (loopTimeEnd < Time.time)
        {
            loopTimeEnd = Time.time + loopInterval;
            forwards = !forwards;
            Animator.SetFloat(AnimLoopString, loopAnimSpeed);
            
            if (!forwards)
            {
                loopAnimSpeed *= -1f;

            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
