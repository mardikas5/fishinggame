﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMovementAnimator : MonoBehaviour
{
    private Material fishMaterial;

    public string FishVectorName;
    public string FishAnimSpeedName;
    public int FishVectorInt;
    public int FishAnimSpeedInt;

    public float maxSpeedVector = .15f;
    public float minSpeedVector = 2f;

    public float minAnimSpeed = .1f;
    public float maxAnimSpeed = .2f;

    public float currentSpeed;
    public float currentNormalizedSpeed = 0f;
    public Vector4 defaultValues;
    private TweenerCore<float, float, FloatOptions> tween;

    public FishAI fishAI;

    public void Start()
    {
        fishAI.OnSpeedChanged += SetFishSpeed;
        FishVectorInt = Shader.PropertyToID(FishVectorName);
        FishAnimSpeedInt = Shader.PropertyToID(FishAnimSpeedName);

        fishMaterial = new Material(GetComponent<MeshRenderer>().sharedMaterial);
        fishMaterial.name = "mat_Inst_" + transform.root.name;
        GetComponent<MeshRenderer>().sharedMaterial = fishMaterial;
    }

    public void SetFishSpeed(float speed)
    {
        currentNormalizedSpeed = speed / fishAI.maxSpeed;
        float newSpeed = Mathf.Lerp(minSpeedVector, maxSpeedVector, currentNormalizedSpeed);
        if (tween != null)
        {
            tween.Kill();
            tween = null;
        }
        tween = DOTween.To(() => currentSpeed, SetValue, newSpeed, Mathf.Abs(currentSpeed - newSpeed) * .1f);
    }

    private void SetValue(float newSpeed)
    {
        currentSpeed = newSpeed;
        Vector4 newValue = defaultValues;
        newValue.w = newSpeed;
        fishMaterial.SetVector(FishVectorInt, newValue);
        fishMaterial.SetFloat(FishAnimSpeedInt, Mathf.Lerp(minAnimSpeed, maxAnimSpeed, currentNormalizedSpeed));
    }
}
