﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fishy
{
    public class TestingOnly : MonoBehaviour
    {
        public EntityBase template;
        public EntityBase saltTemplate;
        // Start is called before the first frame update
        void Start()
        {
            GiveFish();
            GiveFish();
            //GiveSalt();
        }

        [ContextMenu("giveFish")]
        public void GiveFish()
        {
            Fish f = FishFactory.GenerateFishItem(template);
            GetComponent<Inventory>().TryPlaceAnySlot(ItemFactory.Create(f));

            f = FishFactory.GenerateFishItem(template);
            GetComponent<Inventory>().TryPlaceAnySlot(ItemFactory.Create(f));
        }
    }
}
