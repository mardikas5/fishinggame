﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class SceneLoadController : Singleton<SceneLoadController>
{
    public TweenFadeHandler fadeHandler;
    public CanvasGroup canvasGroup;

    public int menuScene;
    public int gameScene;

    public bool isOpen = false;
    public bool isLoading = false;
    public AsyncOperation currentLoad;

    public int BufferFrames = 10;
    int BufferCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        fadeHandler.Init(canvasGroup, canvasGroup.gameObject, .5f, .5f);
    }

    public void Open()
    {
        if (isOpen)
        {
            return;
        }
        isOpen = true;
        fadeHandler.Open();
    }

    public void Close()
    {
        if (!isOpen)
        {
            return;
        }
        isOpen = false;
        fadeHandler.Close();
    }

    public void LoadToGame()
    {
        Load(gameScene);
    }

    public void LoadMenu()
    {
        Load(menuScene);
    }

    public void Load(int level)
    {
        if (isLoading)
        {
            return;
        }

        BufferCount = 0;

        Open();

        isLoading = true;

        this.InvokeDelayed(.5f, () =>
        {
            currentLoad = SceneManager.LoadSceneAsync(level, LoadSceneMode.Single);
            currentLoad.allowSceneActivation = false;
        });
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLoading();
    }

    void UpdateLoading()
    {
        if (currentLoad == null)
        {
            return;
        }

        if (!isLoading)
        {
            return;
        }

        if (currentLoad.progress >= .9f)
        {
            currentLoad.allowSceneActivation = true;

            if (BufferCount < BufferFrames)
            {
                BufferCount++;
                return;
            }

            isLoading = false;
            
            Close();
        }
    }
}
