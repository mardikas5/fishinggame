﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingMapController : MonoBehaviour
{
    public Vector3 NormalizedPlayerPos;

    public Transform PlayerFishingPosition;
    public Transform PlayerCoastPosition;

    public FishingSpot CurrentSpot;

    // Start is called before the first frame update
    void Start()
    {
        CurrentSpot = UI.Instance.FishingMapPanelUI.currentSpot.fishingSpot;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MovePlayerTowardsTarget()
    {

    }

    public void MoveTo(FishingSpot fishSpot)
    {

    }

    public void SetPlayerFishing()
    {
        GameLogic.Instance.Player.Teleport(PlayerFishingPosition.transform.position);
    }

    public void SetPlayerOnCoast()
    {
        GameLogic.Instance.Player.Teleport(PlayerCoastPosition.transform.position);
    }
}
