﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//weapon hittable?
public interface IHittable
{
    bool GetHit(IDamageSource source);
}

