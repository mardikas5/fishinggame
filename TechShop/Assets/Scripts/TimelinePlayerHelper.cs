﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelinePlayerHelper : MonoBehaviour
{
    public PlayableDirector director;

    public Camera filmCamera;

    public bool GameActive = true;

    public void DisableGame()
    {
        if (!GameActive) { return; }
        GameActive = false;

        UI.Instance.drawArea.gameObject.SetActive(false);
        //GameLogic.Instance.CameraController.gameObject.SetActive(false);

        GameLogic.Instance.Player.gameObject.SetActive(false);
        filmCamera.gameObject.SetActive(true);

        GameLogic.Instance.CameraController.SetState(CameraController.CameraStateEnum.Movie);
        GameLogic.Instance.CameraController.maskController.gameObject.SetActive(false);
    }

    public void EnableGame()
    {
        if (GameActive) { return; }
        GameActive = true;
        
        UI.Instance.drawArea.gameObject.SetActive(true);
        //GameLogic.Instance.CameraController.gameObject.SetActive(true);

        GameLogic.Instance.Player.gameObject.SetActive(true);
        GameLogic.Instance.Time.ResumeTime();
        filmCamera.gameObject.SetActive(false);

        GameLogic.Instance.CameraController.SetToDefaultState();
        GameLogic.Instance.CameraController.maskController.gameObject.SetActive(true);
    }

    void UpdatePlayer()
    {
        if (director.state == PlayState.Playing)
        {
            DisableGame();
        }
        else
        {
            EnableGame();
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlayer();
    }
}
