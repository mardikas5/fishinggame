using Fishy;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class Inventory : MonoBehaviour
{
    public delegate bool AddItemCondition(Item i);

    /// <summary>
    /// Generic conditions that can be added on top of default, existing ones.
    /// </summary>
    public List<AddItemCondition> InsertConditions = new List<AddItemCondition>();


    //dict might be better
    [JsonProperty, HideInInspector]
    public LinearMap2D<Item> Items;

    public string Title;


    public bool IgnoreItemSize = false;

    public bool Interactable { get; set; } = true;

    /// <summary>
    /// Signals the item that was just removed.
    /// </summary>
    public event Action<Item> ItemRemoved;
    public event Action<Item> ItemAdded;

    public event Action<List<Int2>> Blocked;
    public event Action<List<Int2>> UnBlocked;

    [Header("Temp")] // Used to add start items to player.
    public List<EntityBase> startItems = new List<EntityBase>();

    private bool IsInited = false;
    
    private HashSet<Int2> CurrentBlockingSlots = new HashSet<Int2>();

    private GameObject ItemsParent;

    [SerializeField]
    private Int2 initSize;

    public Int2 Size
    {
        get => new Int2(Items.size.x, Items.size.y);
        private set => Items.size = value;
    }

    public List<Item> ItemsList
    {
        get
        {
            HashSet<Item> items = new HashSet<Item>();

            foreach (Item i in Items.data)
            {
                if (i == null)
                {
                    continue;
                }
                items.Add(i);
            }

            return items.ToList();
        }
    }

    public void Awake()
    {
        if (initSize != Int2.zero)
        {
            Init(initSize);
        }
    }

    public void Init(Int2 initSize)
    {
        IsInited = true;

        Items = new LinearMap2D<Item>(initSize);
    }

    public void Start()
    {
        if (!IsInited)
        {
            Init(initSize);
        }

        foreach (EntityBase i in startItems)
        {
            Item ins = ItemFactory.Create(EntityFactory.Create(i));
            TryPlaceAnySlot(ins);
        }
    }

    public void Open()
    {
        Open(null);
    }

    // ui method.
    public void ShowUIInventory(Action<UIInventory> onCreated) {
        //reaching reference. -> very specific to game.
        UIFactories.Instance.InventoryFactory.CreateUIInventory(this, onCreated);
    }

    public void Open(Action<UIInventory> onCreated)
    {
        ShowUIInventory(onCreated);
    }

    public void Close()
    {
        //ContextHandler = null;
    }

    /// <summary>
    /// Check if an item can be placed into an container / inventory.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="container"></param>
    /// <param name="CornerPos"></param>
    /// <param name="occupies"></param>
    /// <returns></returns>
    private bool CanPlaceInContainer(Item item, Inventory container, out Int2 CornerPos, out List<Int2> occupies)
    {
        CornerPos = new Int2(-1, -1);

        occupies = new List<Int2>();

        if (item == null || container == null)
        {
            return false;
        }

        Inventory i = container;

        if (i?.CanPlaceAnySlot(item, out occupies, out CornerPos) == true)
        {
            if (CornerPos.x >= 0)
            {
                return true;
            }
        }

        return false;
    }

    public void RemoveAll()
    {
        if (Items == null)
        {
            return;
        }

        for (int i = 0; i < Items.data.Length; i++)
        {
            Remove(Items.data[i]);
        }
    }


    public void Remove(Item item)
    {
        if (item == null)
        {
            return;
        }

        if (Items[item.CornerPos] != item)
        {
            if (Items.data.FirstOrDefault((any) => any == item) == null)
            {
                Debug.LogError("could not find item.");
                return;
            }
        }

        item.OccupiedSlots.ToList().ForEach((slotIndex) => Items[slotIndex] = null);

        if (item.Container == this)
        {
            item.Container = null;
        }

        ItemRemoved?.Invoke(item);
    }

    /// <summary>
    /// Returns if an item can be placed into anywhere in the inventory.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="OccupiedPositions"></param>
    /// <param name="CornerPos"></param>
    /// <returns></returns>
    public bool CanPlaceAnySlot(Item item, out List<Int2> OccupiedPositions, out Int2 CornerPos)
    {
        ItemPlacementResult result;

        CornerPos = new Int2(-1, -1);
        OccupiedPositions = new List<Int2>();

        RectangleFillIterator rectFill = new RectangleFillIterator(Int2.zero, Items.size);

        foreach (Vector2i pos in rectFill)
        {
            if (ValidPlacement(item, pos, out result))
            {
                CornerPos = result.CornerPosition;
                OccupiedPositions = result.OccupiedSlots;

                return true;
            }
        }

        return false;
    }


    public bool CanCombineStack(Item stackInto, Item stackFrom, out ItemStackCombineResult StackCombineResult)
    {
        StackCombineResult = new ItemStackCombineResult();

        if (!stackInto.ItemBase.Stackable)
        {
            return false;
        }

        if (stackInto.ItemBase != stackFrom.ItemBase)
        {
            return false;
        }

        StackCombineResult.StackIntoItem = stackInto;
        StackCombineResult.StackFromItem = stackFrom;

        //add stacks together.
        StackCombineResult.StackIntoAmount = stackInto.CurrentStack + stackFrom.CurrentStack;
        StackCombineResult.StackFromAmount = 0;

        //if stack is larger than max stack size.
        if (StackCombineResult.StackIntoAmount > stackInto.ItemBase.StackSize)
        {
            StackCombineResult.StackFromAmount = StackCombineResult.StackIntoAmount - stackInto.ItemBase.StackSize;
            StackCombineResult.StackIntoAmount = stackInto.ItemBase.StackSize;
        }

        return true;
    }

    /// <summary>
    /// Tries to place the item into any slot in the inventory.
    /// </summary>
    /// <param name="t"></param>
    /// <param name="RemoveOldContainer"></param>
    /// <returns></returns>
    public bool TryPlaceAnySlot(Item t, bool RemoveOldContainer = true)
    {
        Int2 newCorner = new Int2(-1,-1);
        RectangleFillIterator i = new RectangleFillIterator(Int2.zero, Items.size);
        foreach (Vector2i pos in i)
        {
            if (Insert(t, pos, out ItemPlacementResult result, RemoveOldContainer))
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Tries to insert an item into inventory at a specific position.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="CornerPos">corner position of the item</param>
    /// <param name="AllowInsertInItem"></param>
    /// <returns></returns>
    public bool Insert(Item item, Int2 CornerPos, out ItemPlacementResult result, bool RemoveOldContainer = true)
    {
        result = null;

        if (item == null)
        {
            return false;
        }

        if (!Items.IsInside(CornerPos))
        {
            return false;
        }

        if (!ValidPlacement(item, CornerPos, out result))
        {
            return false;
        }

        if (result.IsCombiningStacks)
        {
            return CombineStacks(item, result);
        }

        if (RemoveOldContainer)
        {
            if (item.Container != null)
            {
                item.Container.Remove(item);
            }
        }

        SetItemToResult(result, item);

        result.Inventory.ItemAdded?.Invoke(item);

        return true;
    }


    /// <summary>
    /// Applies the placement result to an item -> moves it to according to placement result.
    /// </summary>
    /// <param name="result"></param>
    /// <param name="item"></param>
    private void SetItemToResult(ItemPlacementResult result, Item item)
    {
        if (result.Inventory.IgnoreItemSize)
        {
            item.Rotation = Rotation.Horizontal;
        }

        result.OccupiedSlots.ForEach(
        (slot) => (result.Inventory).Items[slot] = item);

        item.Container = result.Inventory;
        item.CornerPos = result.CornerPosition;
        item.SetContainer(result.OccupiedSlots);
    }


    /// <summary>
    /// Tries to combine stacks from a placement result from stack to into stack 
    /// </summary>
    /// <param name="item"> the item giving away its stack </param>
    /// <param name="result"></param>
    /// <returns>Was the stack fully combined.</returns>
    private bool CombineStacks(Item item, ItemPlacementResult result)
    {
        ItemStackCombineResult CombineStackResult = result.CombineStackResult;

        //finalize.
        CombineStackResult.StackFromItem.CurrentStack = CombineStackResult.StackFromAmount;
        CombineStackResult.StackIntoItem.CurrentStack = CombineStackResult.StackIntoAmount;

        //result.Inventory.InventoryChanged?.Invoke();

        //if stacks were not combined fully.
        if (item.CurrentStack > 0)
        {
            return false;
        }

        if (item.Container != null)
        {
            item.Container.Remove(item);
        }

        return true;
    }

    /// <summary>
    /// Checks if the item can be placed at a specific spot.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="cornerPos"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    public bool ValidPlacement(Item item, Int2 cornerPos, out ItemPlacementResult result)
    {
        result = new ItemPlacementResult();
        result.CornerPosition = cornerPos;
        result.OccupiedSlots = new List<Int2>();
        result.Inventory = this;

        if (item == null)
        {
            return false;
        }

        bool canPlace = TryPlaceItemInSlot(item, cornerPos, ref result, IgnoreItemSize);

        return canPlace;
    }

    /// <summary>
    /// Tries to place item at a specific spot.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="Pos"></param>
    /// <param name="result"></param>
    /// <param name="ignoreSize"></param>
    /// <returns></returns>
    private bool TryPlaceItemInSlot(Item item, Int2 Pos, ref ItemPlacementResult result, bool ignoreSize = false)
    {
        bool canPlace = true;

        // Do the check here instead in the slots but keep the result.
        foreach (AddItemCondition t in InsertConditions)
        {
            if (!t(item))
            {
                canPlace = false;
                break;
            }
        }

        if (ignoreSize)
        {
            Int2 TryPlaceSpot = new Int2(Pos.x, Pos.y);
            result.OccupiedSlots.Add(TryPlaceSpot);
            if (!canPlace)
            {
                return false;
            }
            return TryPlaceItemInSlotCheck(item, Pos, ref result);
        }

        for (int x = Pos.x; x < Pos.x + item.Size.x; x++)
        {
            for (int y = Pos.y; y < Pos.y + item.Size.y; y++)
            {
                Int2 TryPlaceSpot = new Int2(x,y);
                result.OccupiedSlots.Add(TryPlaceSpot);

                //should only check if all is good so far.
                if (!canPlace)
                {
                    continue;
                }

                canPlace = TryPlaceItemInSlotCheck(item, new Int2(x, y), ref result);
            }
        }

        return canPlace;
    }

    private bool TryPlaceItemInSlotCheck(Item item, Int2 TryPlaceSpot, ref ItemPlacementResult result)
    {
        if (!IsItemPlacableinSlot(item, TryPlaceSpot, ref result))
        {
            return false;
        }
        //eh..
        else
        {
            if (result?.IsCombiningStacks == true)
            {
                result.OccupiedSlots.Clear();
                result.OccupiedSlots.Add(TryPlaceSpot);
                return true;
            }
        }

        return true;
    }

    private bool IsItemPlacableinSlot(Item item, Int2 Pos, ref ItemPlacementResult result)
    {
        if (!Items.IsInside(Pos))
        {
            return false;
        }

        if (Items[Pos] == null || Items[Pos] == item)
        {
            return true;
        }

        //this is a "special" type of interaction more to do with inventory
        if (item.ItemBase.Stackable)
        {
            if (CanCombineStack(Items[Pos], item, out ItemStackCombineResult CombineStackResult))
            {
                result.IsCombiningStacks = true;
                result.CombineStackResult = CombineStackResult;
                return true;
            }
        }

        return false;
    }

    public Item this[Int2 index]    // Indexer declaration  
    => Items[index];
}
