﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Transaction
{
    public enum Type
    {
        Buying,
        Selling
    }

    public Type type;
    public EntityInstance entity;
    public float sellValue;

    public Transaction(Type type, EntityInstance i, float v)
    {
        this.type = type;
        this.entity = i;
        this.sellValue = v;
    }
}
