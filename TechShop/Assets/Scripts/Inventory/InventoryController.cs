﻿using Fishy;
using PixelCrushers.DialogueSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public Inventory Inventory;

    public List<Transaction> Transactions = new List<Transaction>();

    public event Action<EntityInstance> OnItemSold;

    //need checks that it belongs to player?
    public void SellItem(Item i)
    {
        Debug.LogError(i.Entity.name + ", " + i.Entity.Value());
        GameLogic.Instance.playerMoney.AddAmount(i.Entity.Value());
        Transactions.Add(new Transaction(Transaction.Type.Selling, i.Entity, i.Entity.Value()));
        Inventory.Remove(i);
        OnItemSold?.Invoke(i.Entity);
    }

    public void AddItem(Item fishItem)
    {
        if (!Inventory.TryPlaceAnySlot(fishItem))
        {
            EntityFactory.CreateInWorldStatic(fishItem, transform.position + Vector3.up);
        }
    }
}
