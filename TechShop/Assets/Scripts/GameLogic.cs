﻿using PixelCrushers.DialogueSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class GameLogic : Singleton<GameLogic>
{

    public GameController GameController;
    public TimeController Time;

    public InteractionHighlightController interactionHighlight;
    public PlayerMoney playerMoney;
    public PlayerController Player;
    public PlayerHousingController PlayerHousing;
    public PlayTimeController PlayTimeController;
    public TradeController TradeController;
    public TradeInteractionHandler TradeInteractionHandler;
    public FamilyController FamilyController;
    public FishingGamesCollection FishingGamesCollection;
    public CameraController CameraController;
    public FishingMapController FishingMapController;
    public PlayerReceiveItem PlayerRecieveItemHandler;
    public SimpleBeeper SimpleBeeper;
    public IslandEvents IslandEvents;
    public Transform LightHouseIslandPos;
    public Transform FishParent;

    public DialogueSystemController dialogueManager;
}
