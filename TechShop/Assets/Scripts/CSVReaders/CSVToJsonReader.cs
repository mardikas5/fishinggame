﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Object = UnityEngine.Object;
using System.IO;
using Newtonsoft.Json;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Designed to work with the <see cref="JsonUtility"/> class from <see cref="UnityEngine"/>™
/// </summary>
[Serializable]
public class CSVToJsonReader : Singleton<CSVToJsonReader>
{

#if UNITY_EDITOR
    public List<TextAsset> JsonAssets = new List<TextAsset>();

    public List<CustomReader> Readers = new List<CustomReader>();

    public static string defaultScriptableLocation = "Assets/ScriptableObjects/Entities/";

    /// <summary>
    /// Used to find or create new scriptableobjects.
    /// </summary>
    [Serializable]
    public class MetaData
    {
        public string Meta_Name;
    }

    public class CustomReader<T> : CustomReader where T : class
    {
        public override string Type => typeof(T).ToString();
    }

    public class GenericTypeReader<T> : CustomReader<T> where T : class
    {
        public string Path;
        public string Prefix;

        public override string CreateObjectPath => Path;

        public GenericTypeReader(string path, string prefix)
        {
            Path = path;
            Prefix = prefix;
            string cleanPath = path.Remove(path.Length - 1);
            if (!AssetDatabase.IsValidFolder(cleanPath))
            {
                Debug.LogError(cleanPath + " , doesnt exist. Please fix before importing.");
            }
        }

        public override void Parse(Dictionary<string, object> objectValuePairs)
        {
            MetaData meta = CreateMetaJson(objectValuePairs);
            string AssetName = CreateObjectPath + Prefix + meta.Meta_Name + ".asset";

            Object asset = BaseScriptableReader.GetAsset(AssetName, typeof(T));

            T entity = asset as T;
            string _JsonDerulo = CreateJson(objectValuePairs);

            //think you gotta do it double -> this doesnt set references correctly.
            JsonConvert.PopulateObject(_JsonDerulo, entity);
            //this one overwrites values its not supposed to? sumtims.
            JsonUtility.FromJsonOverwrite(_JsonDerulo, entity);

            EditorUtility.SetDirty(entity as Object);

            //Moved this up so you do it after every file not every item read...
            //AssetDatabase.SaveAssets();
            //AssetDatabase.Refresh();
        }
    }

    [Serializable]
    public class CustomReader
    {
        public Func<string, string> referenceProvider;

        public virtual string CreateObjectPath { get; }

        public virtual string Type { get; }

        //Make a out list of the inner nested objects that were read.
        //dont include them in the original jsun -> leave nested empty, apply them in later.
        private string DefaultJson(Dictionary<string, object> pairedCSVData, bool verbose = true)
        {
            string result = @"{";
            //dont need to wrap the type in {
            //result += $"\"{Type}\": " + @"{";

            bool isFirst = true;

            foreach (string header in pairedCSVData.Keys)
            {
                if (!isFirst)
                {
                    result += ",";
                }

                isFirst = false;

                if (CSVReader.IsObject(header, out string headerString, out string headerObjects))
                {
                    headerObjects += "\n" + CSVReader.CleanNestedContent(pairedCSVData[header].ToString());
                    List<Dictionary<string, object>> obj = CSVReader.ReadText(headerObjects);
                    string innerContent = "";
                    bool _nestedFirst = true;

                    foreach (Dictionary<string, object> k in obj)
                    {
                        if (!_nestedFirst)
                        {
                            innerContent += ",";
                        }

                        innerContent += DefaultJson(k);
                        _nestedFirst = false;
                    }

                    result += WrapContent(headerString, innerContent, true, false);

                    continue;
                }

                string content = pairedCSVData[header].ToString();
                result += WrapContent(header, content);
            }

            result += @"}";
            if (verbose)
            {
                Debug.Log(result);
            }
            return result;
        }

        protected virtual string WrapContent(string header, string content, bool forceValue = false, bool forceToLower = true)
        {
            bool isValue     = CSVReader.IsValue(content);
            bool isArray     = CSVReader.IsArray(header);
            bool isReference = CSVReader.IsReference(header);
            bool isLiteral   = CSVReader.IsLiteral(header);

            if (forceValue)
            {
                isValue = true;
            }

            string[] tags = header.Split('|');
            string cleanHeader = tags[0];

            string result = $"\"{cleanHeader}\": ";

            string value = TryParseString(header, content, isValue, isArray, isReference, isLiteral, forceToLower);

            result += value;

            return result;
        }

        protected virtual string LiteralHandler(string RawValue)
        {
            //google sheets specifically has double quotes when exporting...
            string doubleQuote = $"\"\"";
            RawValue = RawValue.Replace(doubleQuote, $"\"");
            RawValue = RawValue.Replace('|', ',');
            return $"{RawValue}";
        }

        ///
        protected virtual string TryParseString(string header, string content, bool isValue, bool isArray, bool isReference, bool isLiteral, bool forceValueToLower = false)
        {
            //note: cannot have a literal as reference...
            string value = "";

            if (isValue && forceValueToLower)
            {
                content = content.ToLower();
            }

            if (!isReference)
            {
                if (isLiteral)
                {
                    return LiteralHandler(content);
                }

                value = $"{content}";

                if (!isValue)
                {
                    value = $"\"{content}\"";
                }
            }

            if (isReference)
            {
                int[] objRef = TryGetReference(content, isArray);
                for (int i = 0; i < objRef.Length; i++)
                {
                    if (i > 0)
                    {
                        value += ",";
                    }
                    value += "{" + $"\"instanceID\":" + objRef[i] + "}";
                }
            }

            if (isArray)
            {
                value = "[" + value + "]";
            }

            return value;
        }

        protected virtual int[] TryGetReference(string valueRaw, bool isArray)
        {
            int[] returnIds = new int[1];
            string[] tryFind = new string[] { valueRaw };

            if (isArray)
            {
                tryFind = valueRaw.Split(',');
                returnIds = new int[tryFind.Length];
            }

            for (int i = 0; i < tryFind.Length; i++)
            {
                TryGetID(tryFind[i], out int id, out Object obj);
                returnIds[i] = id;
            }

            return returnIds;
        }

        public static bool TryGetID(string name, out int id, out Object obj)
        {
            string[] AssetIDs = AssetDatabase.FindAssets(name);
            id = -1;
            obj = null;

            if (AssetIDs.Length > 0)
            {
                string path = AssetDatabase.GUIDToAssetPath(AssetIDs[0]);

                foreach (string foundAssetID in AssetIDs)
                {
                    string pathName = AssetDatabase.GUIDToAssetPath(foundAssetID);
                    string[] assetDirPath = pathName.Split('/');

                    if (assetDirPath[assetDirPath.Length - 1] == name + ".asset")
                    {
                        path = pathName;
                        break;
                    }
                }

                obj = AssetDatabase.LoadAssetAtPath<Object>(path);

                if (obj == null)
                {
                    Debug.LogError(name + "found as: " + AssetIDs[0] + ", asset guid path: " + path + ", " + obj + ", loaded obj is null");
                    return false;
                }

                //try cast to correct type... -> need more types here.
                Sprite t = SpriteSheetLoader.LoadSingleFromSheet(name);

                id = obj.GetInstanceID();

                if (t != null)
                {
                    id = t.GetInstanceID();
                }
                else
                {
                    t = AssetDatabase.LoadAssetAtPath<Sprite>(path);
                    if (t != null)
                    {
                        id = t.GetInstanceID();
                    }
                }

                return true;
            }

            return false;
        }

        protected virtual string CreateJson(Dictionary<string, object> toJson)
        {
            return DefaultJson(toJson);
        }

        public virtual Dictionary<string, object> TryGetMeta(Dictionary<string, object> CSV)
        {
            Dictionary<string, object> MetaString = new Dictionary<string, object>();

            foreach (string header in CSV.Keys)
            {
                if (header.StartsWith("Meta", StringComparison.OrdinalIgnoreCase))
                {
                    MetaString.Add(header, CSV[header]);
                }
            }

            return MetaString;
        }

        protected virtual MetaData CreateMetaJson(Dictionary<string, object> MetaPairs)
        {
            return JsonConvert.DeserializeObject<MetaData>(DefaultJson(MetaPairs, true));
        }

        public virtual void Parse(Dictionary<string, object> objectValuePairs) { }
    }

    [ContextMenu("GetReaders")]
    public void GetReaders()
    {
        Readers = new List<CustomReader>();
        Readers.Add(new GenericTypeReader<EntityBase>(defaultScriptableLocation, "Entity_"));
        Readers.Add(new GenericTypeReader<FishBase>(defaultScriptableLocation + "FishBase/", "Fish_"));
        Readers.Add(new GenericTypeReader<ItemBase>(defaultScriptableLocation + "ItemBase/", "Item_"));
        Readers.Add(new GenericTypeReader<Recipe>(defaultScriptableLocation + "Recipies/", "Recipe_"));
    }

    [ContextMenu("ReadAsset")]
    public void ReadAsset()
    {
        GetReaders();

        foreach (TextAsset asset in JsonAssets)
        {
            List<Dictionary<string, object>> readObjects = CSVReader.ReadText(asset.text);

            foreach (Dictionary<string, object> t in readObjects)
            {
                TryParse(t);
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }

    private void TryParse(Dictionary<string, object> objectValuePairs)
    {
        foreach (string header in objectValuePairs.Keys)
        {
            //very special header.
            if (header == "Type")
            {
                CustomReader reader = TryGetReader((string)objectValuePairs[header]);
                if (reader == null)
                {
                    Debug.LogError(" no reader for type: " + (string)objectValuePairs[header]);
                    continue;
                }
                reader.Parse(objectValuePairs);
                return;
            }
        }
    }

    private CustomReader TryGetReader(string typeString)
    {
        return Readers.FirstOrDefault((x) => x.Type == typeString);
    }
#endif
}

