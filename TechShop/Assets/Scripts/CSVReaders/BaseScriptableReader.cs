﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static CSVToJsonReader;
using Object = UnityEngine.Object;

public static class BaseScriptableReader
{
    public static Object GetAsset(string AssetName, Type type)
    {
        Object asset = AssetDatabase.LoadAssetAtPath(AssetName, type);
        if (asset == null)
        {
            Debug.LogError("new asset: " + AssetName + ", " + type.ToString());
            asset = CreateNewAsset(AssetName, type);
        }

        return asset;
    }

    private static ScriptableObject CreateNewAsset(string path, Type type, bool explicitSave = false)
    {
        ScriptableObject t = ScriptableObject.CreateInstance(type.ToString());

        AssetDatabase.CreateAsset(t, path);

        if (explicitSave)
        {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        return t;
    }
}

//public class EntityBaseReader : CustomReader<EntityBase>
//{
//    public override string CreateObjectPath => "Assets/ScriptableObjects/Entities/";

//    public override void Parse(Dictionary<string, object> objectValuePairs)
//    {
//        MetaData meta = CreateMetaJson(objectValuePairs);
//        string AssetName = CreateObjectPath + "Entity_" + meta.Meta_Name;

//        Object asset = BaseScriptableReader.GetAsset(AssetName, typeof(EntityBase));

//        EntityBase entity = asset as EntityBase;
//        JsonUtility.FromJsonOverwrite(CreateJson(objectValuePairs), entity);
//    }
//}

//public class FishBaseReader : CustomReader<FishBase>
//{
//    public override string CreateObjectPath => "Assets/ScriptableObjects/Entities/FishBase";

//    public override void Parse(Dictionary<string, object> objectValuePairs)
//    {
//        MetaData meta = CreateMetaJson(objectValuePairs);
//        string AssetName = CreateObjectPath + "FishBase_" + meta.Meta_Name;

//        Object asset = BaseScriptableReader.GetAsset(AssetName, typeof(FishBase));

//        EntityBase entity = asset as EntityBase;
//        JsonUtility.FromJsonOverwrite(CreateJson(objectValuePairs), entity);
//    }
//}

#endif
