﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolEnergyUse : MonoBehaviour
{
    public float Amount;

    public int UseEnergyOnStage;

    public Action<int> ActiveListener;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Tool>().OnGetInteraction += GetInteractionHandler;
    }

    private void onEndToolUse()
    {
        ActiveListener = null;
    }

    private void onStartToolUse(ToolInteraction obj)
    {
        PlayerEnergyController energyController = obj.User.transform.parent.GetComponentInChildren<PlayerEnergyController>();

        ActiveListener = (x) =>
        {
            if (x == UseEnergyOnStage)
            {
                UseEnergy(energyController, Amount);
                ActiveListener = null;
            }
        };
    }

    private void GetEnergyUse(int stage)
    {
        ActiveListener?.Invoke(stage);
    }

    private void GetInteractionHandler(ToolInteraction obj)
    {
        PlayerEnergyController energyController = obj.User.transform.parent.GetComponentInChildren<PlayerEnergyController>();

        obj.ToolUseCondition.Add(() => energyController.hasEnergy(Mathf.CeilToInt(Amount)));
        onStartToolUse(obj);
        obj.OnCompleted += onEndToolUse;
        obj.StageChanged += GetEnergyUse;
    }

    private void UseEnergy(PlayerEnergyController energyController, float amount)
    {
        if (energyController != null)
        {
            energyController.UseEnergy(Mathf.CeilToInt(amount));
            return;
        }

        Debug.LogError(" could not use energy as no energy contrller was found.");
    }


}
