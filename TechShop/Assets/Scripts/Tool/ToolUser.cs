﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolUser : MonoBehaviour
{
    //transform to attach tool to.
    public Transform AttachToolParent;

    public GameObject activeVisuals;

    public Tool activeTool;

    public Stats Stats; // wip stats not done, wrong place to store them.

    private List<InputEnum> activeInputs = new List<InputEnum>();


    public bool UseTool(InputEnum attackInput, out ToolInteraction toolInteraction)
    {
        toolInteraction = null;

        if (activeTool == null)
        {
            return false;
        }

        if (activeTool.isBusy)
        {
            return false;
        }

        if (UI.Instance.MouseOnUI)
        {
            return false;
        }

        return activeTool.UseTool(attackInput, out toolInteraction);
    }

    public bool Equip(Tool tool)
    {
        if (tool == activeTool)
        {
            return true;
        }

        if (activeTool != null)
        {
            UnEquip();
        }

        if (tool == null)
        {
            //Debug.LogError("tried to equip null");
            return false;
        }

        GameObject visuals = tool.gameObject;

        visuals.transform.SetParent(AttachToolParent, false);
        visuals.transform.localPosition = tool.equipParams.localPos;
        visuals.transform.localRotation = Quaternion.identity;
        visuals.transform.localRotation = Quaternion.Euler(tool.equipParams.localEulers);
        visuals.transform.localScale = tool.equipParams.localScale;

        activeVisuals = visuals;
        activeTool = tool;
        tool.SetUser(this);

        return true;
    }

    public bool UnEquip()
    {
        if (activeVisuals != null)
        {
            Destroy(activeVisuals);
        }

        if (activeTool == null)
        {
            Debug.LogError("tool was null.");
            return true;
        }

        activeTool.SetUser(null);
        activeTool = null;

        return true;
    }
}
