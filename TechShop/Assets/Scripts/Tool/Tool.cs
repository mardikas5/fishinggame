﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tool : MonoBehaviour
{
    [Serializable]
    public class EquipParams
    {
        public Vector3 localPos;
        public Vector3 localEulers;
        public Vector3 localScale = Vector3.one;
    }

    public event Action<ToolInteraction> OnGetInteraction;
    public CustomEvent OnToolEquipped = new CustomEvent();

    public EquipParams equipParams;

    public ToolUser User { get; private set; }
    //if tool is currently being used > blocks other inputs?
    public bool isBusy;

    public virtual void SetUser(ToolUser user)
    {
        User = user;
        OnToolEquipped?.Invoke();
    }

    protected void SignalToolUse(ToolInteraction interaction)
    {
        OnGetInteraction?.Invoke(interaction);
    }

    public abstract bool ReleaseUseTool(InputEnum released, Action onComplete, bool isInterrupted);

    public abstract bool UseTool(InputEnum attackInput, out ToolInteraction interaction);
}
