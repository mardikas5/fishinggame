﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class ToolInteraction
{
    public float AnimTypeParam;

    public event Action OnCompleted;

    public ToolUser User;

    public Tool Tool;

    public int Stage;

    public List<Func<bool>> ToolUseCondition = new List<Func<bool>>();

    public event Action<int> StageChanged;

    public ToolInteraction(ToolInteraction copyFrom, Tool tool, ToolUser user)
    {
        AnimTypeParam = copyFrom.AnimTypeParam;
        User = user;
        Tool = tool;
    }

    public void SetStage(int stage)
    {
        Stage = stage;
        StageChanged?.Invoke(stage);
    }

    protected void SignalComplete()
    {
        OnCompleted?.Invoke();
    }

    public abstract void Execute(Action onComplete);

    public abstract void Stop();

    public abstract void Interrupt(Action onComplete = null);

    public virtual bool IsValid()
    {
        foreach (Func<bool> t in ToolUseCondition)
        {
            if (!t())
            {
                return false;
            }
        }
        return true;
    }
}
