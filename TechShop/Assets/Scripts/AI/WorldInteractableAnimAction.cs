﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

public class WorldIteractableAnimAction : AIAction
{
    protected override bool IsActive => true;

    public HumanController humanController;

    public string AnimName;

    public Transform Self;

    public HumanGenericAnimState AnimState;

    public SimpleAI AI;

    public Vector3 Offset;


    public WorldIteractableAnimAction(SimpleAI ai, Transform self, string animName, Vector3 offset)
    {
        humanController = ai.GetComponentInChildren<HumanController>();

        AnimState = humanController.GenericAnimState as HumanGenericAnimState;

        AI = ai;

        Self = self;

        AnimName = animName;

        Offset = offset;
        
        if (AnimState == null)
        {
            Debug.LogError(" no generic anim state found.");
        }

        IsComplete = false;
    }

    public void CheckRange()
    {
        if (IsComplete)
        {
            return;
        }
        if (Self?.transform == null || AI?.transform == null)
        {
            return;
        }
        if (Vector3.Distance(AI.transform.position, Self.transform.position) < .5f)
        {
            //MoveToAnimState();

            SetComplete(this);
        }
        else
        {
            AI.MoveTo(Self.transform.position);
        }
    }

    public void MoveToAnimState()
    {
        AI.transform.DOMove(Self.transform.position + Offset, .4f);
        humanController.transform.DORotate(Self.transform.eulerAngles, .3f);
        humanController.StateController.SignalAction<GenericAnimActionSignal>(getSignal());

        AI.navAgent.enabled = false;
    }

    public GenericAnimActionSignal.AnimParams getSignal()
    {
        return new GenericAnimActionSignal.AnimParams(AnimName, true);
    }

    public override bool UpdateAction()
    {
        CheckRange();

        return base.UpdateAction();
    }
}
