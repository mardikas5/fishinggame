﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class AIAction
{
    public Func<bool> isCompleteCheck;

    public event Action<AIAction> onComplete;

    protected bool _isActive;

    protected abstract bool IsActive { get; }

    public bool IsComplete = false;

    public bool Interrupted = false;

    private List<Action> invokeOnComplete = new List<Action>();


    protected void SetComplete(AIAction action)
    {
        IsComplete = true;

        foreach( var t in invokeOnComplete)
        {
            t?.Invoke();
        }

        onComplete?.Invoke(action);
    }

    public virtual void Interrupt()
    {
        Interrupted = true;
        SetComplete(this);
    }

    public virtual void Init()
    {
        IsComplete = false;
        _isActive = true;
    }

    public AIAction OnComplete(Action a)
    {
        invokeOnComplete.Add(a);

        return this;
    }

    public virtual void EndAction()
    {

    }

    public virtual bool UpdateAction()
    {
        return true;
    }

    public virtual void Update()
    {
        if (IsActive)
        {
            UpdateAction();
        }
    }
}
