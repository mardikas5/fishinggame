﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatAI : MonoBehaviour
{
    public List<Transform> currentPath = new List<Transform>();
    public int index;

    public Rigidbody rb;

    public float maxSpeed = 1300f;
    public float rotationPower = 5f;

    public Vector3 lastForwardDiff;

    public Transform rudder;
    public Transform sail;

    public Vector3 lastForceTowards;
    public Vector3 lastWantedDir;
    public Vector3 lastForce;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TravelWayPointsInner();

        rudder.transform.forward = Vector3.Lerp(rudder.transform.forward, transform.forward - lastForwardDiff, .4f);

        Vector3 sailTowards = (lastForce.normalized + lastWantedDir.normalized) / 2f;

        Debug.DrawLine(sail.transform.position, sail.transform.position + sailTowards, Color.cyan, .2f);

        sail.transform.forward = Vector3.Lerp(sail.transform.forward, sailTowards, 2f * Time.deltaTime);

        ///eh only allow y rot.
        sail.transform.localEulerAngles = new Vector3(0f, sail.transform.localEulerAngles.y, 0f);
    }

    public void TravelWaypoints(List<Transform> waypoints)
    {
        index = 0;
        currentPath = waypoints;
    }

    public void ApplyForceTowards(Vector3 pos, Rigidbody thisbody, float forceModifier = 1f)
    {
        Vector3 normalizedForce = pos.x0z() - thisbody.transform.position.x0z();
        normalizedForce.Normalize();

        lastForceTowards = normalizedForce;

        Vector3 movement = normalizedForce * Time.deltaTime * maxSpeed * forceModifier;

        thisbody.AddForce(movement, ForceMode.Force);

        Debug.DrawLine(thisbody.transform.position, thisbody.transform.position + thisbody.velocity, Color.magenta, .2f);
    }


    public void TravelWayPointsInner()
    {
        if (currentPath == null)
        {
            return;
        }

        if (index > currentPath.Count - 1)
        {
            currentPath = null;
            return;
        }

        Transform currentTarget = currentPath[index];

        Vector3 wantedDir = currentTarget.transform.position - transform.position;
        wantedDir.Normalize();

        lastWantedDir = wantedDir;

        RotateTowards(wantedDir);

        float forceModifier = 1f;
        forceModifier *= Mathf.Clamp(1f - Vector3.Distance(wantedDir, transform.forward), .6f, 1f);

        ApplyForceTowards((transform.position + transform.forward) + (wantedDir * .2f), rb, forceModifier);

        Debug.DrawLine(transform.position, currentTarget.transform.position, Color.yellow, .2f);

        float dist = Vector3.Distance(currentTarget.transform.position, transform.position);

        if (dist < 3f)
        {
            index++;
        }
    }

    public void RotateTowards(Vector3 wantedDir)
    {
        wantedDir.Normalize();

        lastForwardDiff = wantedDir - transform.forward;

        Vector3 fromToRot = Quaternion.FromToRotation(transform.forward.x0z(), wantedDir.x0z()).eulerAngles;

        if (fromToRot.y > 180f)
        {
            fromToRot.y -= 360f;
        }

        if (Mathf.Abs(fromToRot.y) < 2f)
        {
            fromToRot.y *= 0f;
        }

        float mag = Mathf.Clamp(fromToRot.y / 4f, -1f, 1f);

        Vector3 localPos = transform.position + transform.forward;
        Vector3 force = transform.right * mag;

        lastForce = force;

        rb.AddForceAtPosition(force * Time.deltaTime * rotationPower, localPos, ForceMode.Impulse);

        Debug.DrawLine(localPos, localPos + force, Color.red, .2f);
    }
}
