﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Fake ai surface...
public class FakeSurface : MonoBehaviour
{
    public PhysicsPlatform CollisionProvider;

    public Dictionary<NavMeshAgent, NavMeshAgent> fakeRealAgentPairs = new Dictionary<NavMeshAgent, NavMeshAgent>();

    private void CheckForNavAgents()
    {
        for (int i = 0; i < CollisionProvider.plantedChars.Count; i++)
        {
            NavMeshAgent objAgent = CollisionProvider.plantedChars[i].GetComponent<NavMeshAgent>();
            if (objAgent == null)
            {
                continue;
            }
            if (fakeRealAgentPairs.ContainsKey(objAgent))
            {
                continue;
            }

            fakeRealAgentPairs.Add(objAgent, CreateNewFakeAgent(CollisionProvider.transform, objAgent));
        }
    }

    public void Update()
    {
        //CheckForNavAgents();
        //UpdateFakeAgents();
    }

    public void UpdateFakeAgents()
    {
        foreach (NavMeshAgent objAgent in fakeRealAgentPairs.Keys)
        {
            if (objAgent.destination.magnitude > 10000f || objAgent.destination == Vector3.positiveInfinity)
            {
                Debug.Log("ignored: " + objAgent.pathPending + ", " + objAgent.path + ", " + objAgent.destination);
                continue;
            }

            NavMeshAgent fakeAgent = fakeRealAgentPairs[objAgent];

            Vector3 localDest = CollisionProvider.transform.InverseTransformPoint(objAgent.destination);
            objAgent.isStopped = true;

            Vector3 translateDest = transform.TransformDirection(localDest);

            Debug.Log(objAgent.destination);
            Debug.DrawLine(fakeAgent.transform.position, fakeAgent.transform.position + Vector3.up, Color.black, .2f);
            Debug.DrawLine(fakeAgent.transform.position, transform.position + translateDest, Color.yellow, .2f);

            fakeAgent.SetDestination(transform.position + translateDest);

            Vector3 agentLocalPos = CollisionProvider.transform.InverseTransformPoint(objAgent.transform.position);
            Vector3 fakePos = transform.TransformDirection(fakeAgent.transform.position);

            objAgent.Move(fakePos - agentLocalPos);
        }
    }

    private NavMeshAgent CreateNewFakeAgent(Transform realSurface, NavMeshAgent objAgent)
    {
        Vector3 currentLocalPos = realSurface.InverseTransformPoint(objAgent.transform.position);
        Debug.DrawLine(realSurface.transform.position, realSurface.transform.position + currentLocalPos, Color.magenta, 100000000f);

        GameObject FakeAgent = new GameObject("fakeai_" + objAgent.name);
        NavMeshAgent agent = FakeAgent.AddComponent<NavMeshAgent>();

        agent.speed = objAgent.speed;
        agent.height = objAgent.height;
        agent.acceleration = objAgent.acceleration;
        agent.radius = objAgent.radius;

        var test2 = transform.TransformDirection(currentLocalPos);
        Debug.DrawLine(transform.position, transform.position + test2, Color.magenta, 100000f);

        FakeAgent.transform.position = test2;

        return agent;
    }
}
