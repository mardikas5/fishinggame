﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WorldInteractableAI : MonoBehaviour
{
    public abstract AIAction GetInteraction(SimpleAI aiObj);
}


