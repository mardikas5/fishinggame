﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class IdleStateAI : GenericState
{
    public HumanController hController;

    public GenericState hGroundedState;
    private SimpleAI ai;

    public AIAction currentIdleAction;
    private GenericAnimInteraction currentOccupant;
    public bool currentlyIdling = false;

    public float minActionTime = 3f;
    public float maxActionTime;
    public float currentActionTime = 0f;
    public float lastUpdateTime = 0f;

    public bool movingToIdle = false;

    public override void Init(GenericStateController controller)
    {
        base.Init(controller);

        ai = transform.GetComponentInParent<SimpleAI>();

        hController = ai.GetComponent<HumanController>();
    }

    public override void EnterState()
    {
        base.EnterState();

        movingToIdle = false;
    }

    protected override void Update()
    {
        base.Update();
        if (movingToIdle)
        {
            return;
        }

        UpdateIdleState();
    }

    protected virtual void UpdateIdleState()
    {
        if (lastUpdateTime + currentActionTime < Time.time)
        {
            //Debug.LogError("getting new action. ");

            AIAction Action = GetAnotherThingToDo();

            if (Action == null)
            {
                return;
            }

            RemoveOccupant();

            currentIdleAction = null;
            currentlyIdling = false;

            if (hController.StateController.ActiveState != hGroundedState)
            {
                hController.StateController.SetState(hGroundedState);
            }

            this.InvokeDelayed(.5f, () =>
            {
                ai.SetSingleAction(Action);
            });

            movingToIdle = true;
            Action.onComplete += OnIdleActionComplete;
        }
    }

    private void OnIdleActionComplete(AIAction obj)
    {
        currentIdleAction = obj;

        currentlyIdling = true;

        movingToIdle = false;

        lastUpdateTime = Time.time;
        currentActionTime = Random.Range(minActionTime, maxActionTime);
    }

    public AIAction GetAnotherThingToDo()
    {
        float randChance = Random.Range(0f, 2f);

        if (ai.locProvider?.Location == null)
        {
            Debug.LogWarning("No location given for ai actions.");
            return null;
        }

        List<GenericAnimInteraction> animInteracts = ai.locProvider.Location.animInteractions;
        if (animInteracts.Count == 0)
        {
            return null;
        }

        GenericAnimInteraction randomInteraction = animInteracts.ElementAt(Random.Range(0, animInteracts.Count));
        AIAction actionFromRestPlace = randomInteraction.GetInteraction(ai);

        actionFromRestPlace.onComplete += (x) => { if (!TryOccupy(randomInteraction)) { GetAnotherThingToDo(); } };

        return actionFromRestPlace;
    }

    private bool TryOccupy(GenericAnimInteraction randomInteraction)
    {
        if (randomInteraction.occupant != ai && randomInteraction.occupant != null)
        {
            if (hController.StateController.ActiveState != hGroundedState)
            {
                hController.StateController.SetState(hGroundedState);
            }

            return false;
        }

        RemoveOccupant();

        currentOccupant = randomInteraction;

        randomInteraction.occupant = ai;

        return true;
    }

    public void RemoveOccupant()
    {
        Debug.Log(" removed occupant: " + currentOccupant);

        if (currentOccupant != null)
        {
            //Debug.DrawLine(ai.transform.position, currentOccupant.transform.position + Vector3.up * 2f, Color.green, 2f);

            currentOccupant.occupant = null;
        }

        currentOccupant = null;
    }

    public void GetRandomMove()
    {
        ai.navAgent.enabled = true;

        float rand = Random.Range(-5f, 5f);

        ai.MoveTo(transform.position + new Vector3(rand, 0f, rand));
    }

    public override void ExitState()
    {
        base.ExitState();

        RemoveOccupant();
    }
}
