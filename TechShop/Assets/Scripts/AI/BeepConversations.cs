﻿using PixelCrushers.DialogueSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeepConversations : MonoBehaviour
{
    AudioSource player;

    private Action m_stopSignal;

    // Start is called before the first frame update
    private void Start()
    {
        player = GetComponent<AudioSource>();
        DialogueManager.instance.GetComponent<DialogueSystemEvents>()
            .conversationEvents.onConversationLine.AddListener(OnGetLine);
    }

    private void OnGetLine(Subtitle arg0)
    {
        m_stopSignal?.Invoke();
        List<SimpleBeeper.TimedSound> sounds = GetComponent<SimpleBeeper>().getSentenceSounds(arg0.formattedText.text);
        playSounds(sounds, out m_stopSignal);
    }

    private void playSounds(List<SimpleBeeper.TimedSound> sounds, out Action stopSignal)
    {
        bool isStopped = false;
        stopSignal = () => isStopped = true;

        for (int i = 0; i < sounds.Count; i++)
        {
            int index = i;
            AudioClip sound = sounds[i].clip;
            float delay = sounds[i].delay;

            this.InvokeDelayed(delay, () =>
            {
                if (isStopped)
                {
                    return;
                }
                player.PlayOneShot(sound);
                player.pitch = UnityEngine.Random.Range(sounds[index].pitchMin, sounds[index].pitchMax);
            });
        }
    }

    // Update is called once per frame
    private void Update()
    {

    }
}
