﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIIdleAction : AIAction
{
    public float endTime;

    protected override bool IsActive => true;

    public AIIdleAction(float EndTime)
    {
        endTime = EndTime;
    }

    public override bool UpdateAction()
    {
        if (Time.time > endTime)
        {
            SetComplete(this);
            return true;
        }
        return true;
    }

}
