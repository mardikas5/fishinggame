﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveToDestination : AIAction
{
    public NavMeshAgent agent;
    public Vector3 dest;
    public NavMeshPath calcPath;
    public float destCooldown = .5f;
    public float destTime = 0f;
    public float relaxCooldown = .1f;
    public float relaxTime = 0f;


    protected override bool IsActive => _isActive;

    public List<Vector3> previousSuccessPath = new List<Vector3>();

    public MoveToDestination(NavMeshAgent agent, Vector3 destination)
    {
        this.agent = agent;
        dest = destination;

        IsComplete = false;
        _isActive = true;
    }

    public override void Init()
    {
        base.Init();
        agent.enabled = true;
        Debug.Log("new move action.");
        //agent.SetDestination(dest);
    }

    public override bool UpdateAction()
    {
        if (relaxTime + relaxCooldown > Time.time)
        {
            return false;
        }

        relaxTime = Time.time + Random.Range(.1f,.3f);
        Debug.DrawLine(agent.transform.position, dest, Color.white, .2f);

        if (!base.UpdateAction())
        {
            return false;
        }

        bool complete = Vector3.Distance(agent.transform.position, dest) < .4f;

        if (complete)
        {
            _isActive = false;
            IsComplete = true;
            SetComplete(this);
            return true;
        }

        if (!agent.isOnNavMesh)
        {
            Debug.DrawLine(agent.transform.position, agent.transform.position + Vector3.up, Color.white, .2f);
            return false;
        }

        if (Vector3.Distance(agent.destination, dest) < .2f)
        {
            Vector3 lastPos = agent.transform.position;

            //previousSuccessPath = new List<Vector3>();
            //previousSuccessPath.AddRange(agent.path.corners);

            foreach (Vector3 pos in agent.path?.corners)
            {
                Debug.DrawLine(lastPos, pos, Color.yellow, .5f);
                lastPos = pos;
            }
            //Debug.DrawLine(agent.transform.position, agent.destination, Color.yellow, .2f);
            return false;
        }

        //todo pending timeout?
        if (agent.pathPending)
        {
            //Debug.Log("path pending.");
            Debug.DrawLine(agent.transform.position, agent.transform.position + Vector3.up, Color.blue, .2f);
            return false;
        }

        if (agent.isPathStale)
        {
            //Debug.Log("stale path.");
            RecalculatePath(dest);
            return false;
        }

        if (!agent.hasPath)
        {
            //Debug.Log("no path");
            RecalculatePath(dest);
            return false;
        }

        Debug.DrawLine(agent.destination, dest, Color.magenta, .2f);

        return false;
    }

    public void RecalculatePath(Vector3 dest)
    {
        if (destCooldown + destTime < Time.time)
        {
            destTime = Time.time;
            //Debug.Log("reset dest pos: pendin: " + agent.pathPending + ", stale: " + agent.isPathStale + ", hasPath: " + agent.hasPath + ", status: " + agent.pathStatus + ", obj: " + agent.path + ", obj.count: " + agent.path?.corners?.Length);
            Debug.DrawLine(agent.transform.position, agent.transform.position + (Vector3.up * 5f), Color.magenta, .2f);

            //try set new path.
            if (!agent.SetDestination(dest))
            {
                //Debug.Log("could not set destination: " + dest);
                Debug.DrawLine(agent.transform.position, agent.transform.position + (Vector3.up * 15f), Color.magenta, .5f);
                Debug.DrawLine(agent.transform.position + Vector3.up, dest, Color.blue, .5f);
            }
            else
            {
                //Debug.Log("destination set: " + dest + ", variables: pendin: " +
                //    agent.pathPending + ", stale: " 
                //    + agent.isPathStale + ", hasPath: " 
                //    + agent.hasPath + ", status: " 
                //    + agent.pathStatus + ", obj: " 
                //    + agent.path + ", obj.count: " 
                //    + agent.path?.corners?.Length);
            }
        }
    }

}
