﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class SimpleAI : MonoBehaviour
{
    public Vector3 pos;

    public NavMeshAgent navAgent;

    public List<AIAction> activeActions = new List<AIAction>();

    public int AIActionCount;
    public int CompletedActions;

    public GenericStateController AIStateMachine;

    //public NavMeshAgent fakeSurfaceAgent;
    public Vector3 nextPos;
    public Vector3 acculmulatedOffset = Vector3.zero;
    //public bool isOnFakeSurface = false;

    public AILocationProvider locProvider;

    public void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();

        locProvider = GetComponent<AILocationProvider>();

        //navAgent.updatePosition = false;
    }

    public void AddAction(AIAction action)
    {
        action.Init();
        activeActions.Add(action);
    }

    private void OnEnable()
    {
        GetComponent<NavMeshAgent>().isStopped = false;
    }

    public AIAction MoveTo(Vector3 point)
    {
        //Debug.Log("moveto: " + point);
        Debug.DrawLine(transform.position, point, Color.black, .5f);

        if (Vector3.Distance(navAgent.destination, point) < .5f)
        {
            //Debug.Log("current destination target already close enough.");
            return null;
        }

        List<MoveToDestination> moveActions = activeActions.FindAll((x) => x.GetType() == typeof(MoveToDestination)).ConvertAll((x) => x as MoveToDestination);

        foreach (MoveToDestination move in moveActions)
        {
            if (Vector3.Distance(move.dest, point) < .2f)
            {
                //Debug.LogError("upcoming destination target already close enough.");
                return null;
            }
        }

        Debug.DrawLine(transform.position, point, Color.blue, 1f);

        MoveToDestination t = new MoveToDestination(navAgent, point);
        Debug.Log(" all movement interrupted. ");
        moveActions.ForEach((x) => x.Interrupt());

        AddAction(t);
        return t;
    }

    // Update is called once per frame
    private void Update()
    {
        AIActionCount = activeActions.Count;
        //CompletedActions = activeActions.FindAll((x) => x.IsComplete).ToArray().Length;

        UpdateActive();

        //transform.position = navAgent.nextPosition;
    }

    private void UpdateCorners(Vector3 acculmulatedOffset, NavMeshPath path)
    {
        for (int k = 0; k < path.corners.Length; k++)
        {
            for (int i = 0; i < 3; i++)
            {
                path.corners[k][i] += acculmulatedOffset[i];
            }
        }
    }

    public void SetSingleAction(AIAction focusAction)
    {
        foreach (AIAction t in activeActions.ToArray())
        {
            t.Interrupt();
        }

        activeActions.Add(focusAction);
    }

    private void UpdateActive()
    {
        foreach (AIAction t in activeActions.ToArray())
        {
            t.Update();
            if (t.IsComplete)
            {
                activeActions.Remove(t);
            }
        }
    }

    private void OnDisable()
    {
        GetComponent<NavMeshAgent>().isStopped = true;
    }

    //called in fixedupdate -> moving platforms / rigidbodies.
    public void Move(Vector3 offset)
    {
        //navAgent.autoRepath = false;
        //navAgent.updatePosition = false;
        Debug.DrawLine(transform.position, navAgent.nextPosition, Color.green, .2f);

        //acculmulatedOffset += offset;
        //GetComponent<CharacterController>().Move(offset);
    }
}
