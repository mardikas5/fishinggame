﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GenericAnimInteraction : WorldInteractableAI
{
    public string GenericAnimName;
    public Vector3 offset;
    public Vector3 randomOffset = Vector3.one;
    public SimpleAI occupant;

    public override AIAction GetInteraction(SimpleAI aiObj)
    {
        WorldIteractableAnimAction action = new WorldIteractableAnimAction(aiObj, transform, GenericAnimName, offset + getRandomOffset());

        action.onComplete += (x) => CheckOccupant(action, aiObj);

        return action as AIAction;
    }

    private void CheckOccupant(WorldIteractableAnimAction action, SimpleAI aiObj)
    {
        if (occupant == null || occupant == aiObj)
        {
            action.MoveToAnimState();
        }
    }

    public Vector3 getRandomOffset()
    {
        return new Vector3(Random.Range(-randomOffset.x, randomOffset.x), 0f, Random.Range(-randomOffset.z, randomOffset.z));
    }
}