﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AILocation : MonoBehaviour
{
    public Collider LocationCollider;

    public List<GenericAnimInteraction> animInteractions = new List<GenericAnimInteraction>();

    // Start is called before the first frame update
    private void Start()
    {
        GatherAnimInteractions();
    }

    public void GatherAnimInteractions()
    {
        GenericAnimInteraction[] interactions = FindObjectsOfType<GenericAnimInteraction>();
        foreach (GenericAnimInteraction t in interactions)
        {
            if (LocationCollider.bounds.Contains(t.transform.position))
            {
                animInteractions.Add(t);
            }
        }
    }
}
