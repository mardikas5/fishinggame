﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AILocationProvider : MonoBehaviour
{
    public AILocation Location;

    private void Start()
    {
        if (Location == null)
        {
            TryGetLocation();
        }
    }

    public void TryGetLocation()
    {
        AILocation[] k = FindObjectsOfType<AILocation>();

        foreach (AILocation t in k)
        {
            if (t.LocationCollider.bounds.Contains(transform.position))
            {
                Location = t;
                return;
            }
        }
    }
}
