﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//set the human state..
public class AISetAnimAction : MonoBehaviour
{
    public HumanController HumanController;

    public HumanStateBehaviour animState;

    public HumanStateBehaviour normalState;

    // Start is called before the first frame update
    void Start()
    {
        HumanController.StateController.SetState(animState);
    }

    //
    void Leave()
    {
        HumanController.StateController.SetState(normalState);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
