﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FamilyBox : MonoBehaviour
{
    public Inventory chestInventory;

    public void Interact()
    {
        UI.Instance.TransferPanelUI.Open(GameLogic.Instance.Player.InventoryController.Inventory, chestInventory);
    }
}
