﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FamilyNeed : MonoBehaviour
{
    public Inventory FamilyBox;

    public string Tag;

    public string Description;

    public float maxAmount = 100f;
    public float currentAmount = 100f;

    public float tickAmount;
    public float tickInterval;
    public float tickTimer;

    public string SuitableItemTag;

    public void Init(Inventory familyBox)
    {
        FamilyBox = familyBox;
    }

    public void TrySatisfy()
    {
        foreach (var item in FamilyBox.ItemsList)
        {
            //in items look for item with tag.
            ItemTag t = item.Entity.Base.GetTag<ItemTag>(SuitableItemTag);
            if (t == null)
            {
                continue;
            }

            //add item tag amount to satisfaction.
            AddSatisfaction(t.Amount);
            //remove item.
            FamilyBox.Remove(item);
        }
    }

    private void AddSatisfaction(float amount)
    {
        currentAmount += amount;
    }

    public void Tick(float secondsPassed)
    {
        tickInterval -= secondsPassed;

        if (tickInterval > 0f)
        {
            return;
        }

        float intervalsPassed = Mathf.Abs(tickInterval / tickTimer);
        int amount = Mathf.FloorToInt(intervalsPassed);
        float leftOver = intervalsPassed - amount;
        
        tickInterval = tickTimer - (leftOver * tickTimer);

        currentAmount -= tickAmount * (amount + 1);
    }

    public void UseItem(Item i)
    {
        ItemTag tag = TryGetTag(i);

        if (tag == null)
        {
            return;
        }

        currentAmount += tag.Amount;
    }

    public ItemTag TryGetTag(Item i)
    {
        return i.Entity.Base.GetTag<ItemTag>(Tag);
    }
}
