﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FamilyController : MonoBehaviour
{
    public List<FamilyNeed> Needs = new List<FamilyNeed>();

    public Inventory familyInventory;

    public DateTime lastUpdate;

    public void TickNeeds()
    {
        TimeSpan difference = GameLogic.Instance.PlayTimeController.TimeController.GameTime.Subtract(lastUpdate);
        lastUpdate = GameLogic.Instance.PlayTimeController.TimeController.GameTime;

        foreach (FamilyNeed t in Needs)
        {
            t.Tick((float)difference.TotalSeconds);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        lastUpdate = GameLogic.Instance.PlayTimeController.TimeController.GameTime;

        foreach (FamilyNeed t in Needs)
        {
            t.Init(familyInventory);
        }
    }

    // Update is called once per frame
    void Update()
    {
        TickNeeds();
        UseChestItems();
    }

    void UseChestItems()
    {
        foreach (Item i in familyInventory.ItemsList)
        {
            foreach (FamilyNeed t in Needs)
            {
                if(t.TryGetTag(i) != null)
                {
                    t.UseItem(i);
                    familyInventory.Remove(i);
                }
            }
        }
    }
}
