﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WaterAmbientColorSetter : MonoBehaviour
{
    public string materialProperty;

    public int materialPropertyID;

    public Material waterMaterial;

    public DayNightManager dayNightManager;

    

    // Start is called before the first frame update
    void Start()
    {
        if (dayNightManager == null)
        {
            dayNightManager = GetComponent<DayNightManager>();
        }
        dayNightManager.TimeController.NormalizedHoursPassed += OnTimeUpdated;
        materialPropertyID = Shader.PropertyToID(materialProperty);
    }

    private void OnTimeUpdated(float obj)
    {
        SetCurrentWorldVisualTime(dayNightManager.TimeController.NormalizedTimeOfDay());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetCurrentWorldVisualTime(float normalizedTime)
    {
        if (normalizedTime > 1)
        {
            normalizedTime = normalizedTime - (float)Math.Truncate(normalizedTime);
        }

        waterMaterial.SetColor(materialPropertyID, dayNightManager.GetWaterColor(normalizedTime));
    }
}
