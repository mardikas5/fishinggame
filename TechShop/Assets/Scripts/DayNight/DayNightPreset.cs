﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class DayNightPreset : ScriptableObject
{
    public AnimationCurve AmbientIntensity;

    public AnimationCurve SunIntensity;

    public AnimationCurve ShadowStrength;

    public Gradient SunColor;
    public Gradient SkyColor;
    public Gradient EquatorColor;
    public Gradient GroundColor;
    public Gradient SkyTintColor;

    [Header("Moon")]
    public AnimationCurve MoonLightIntensity;

    public AnimationCurve sunEulerRotationY;
    public AnimationCurve sunEulerRotationX;

    [Header("Fog")]
    public Gradient FogGradient;

    public AnimationCurve FogDensity;
    public AnimationCurve dayNightBalance;

    public Gradient WaterColor;

    [Header("Clouds")]
    public Gradient CloudTintColor;
    public AnimationCurve CloudBlend;
}
