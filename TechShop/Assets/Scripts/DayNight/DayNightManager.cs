﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightManager : MonoBehaviour
{
    public DayNightPreset dayPreset;
    public TimeController TimeController;

    public GameObject sun;
    public Light SunLight;
    public Light MoonLight;

    public Material skyMaterial;
    public Material cloudMaterial;

    public string dayNightSkyboxLerpName = "_TexLerpValue";
    public string skyColorName = "_TintColor";

    public string cloudColorName = "_CloudColor";
    public string cloudFogBlendName = "_FogMultiplier";

    private void Start()
    {
        if (TimeController == null)
        {
            TimeController = GameLogic.Instance.Time;
        }
        TimeController.NormalizedHoursPassed += OnTimeUpdate;
    }

    private void OnTimeUpdate(float timePassed)
    {
        SetCurrentWorldVisualTime(TimeController.NormalizedTimeOfDay());
    }

    /// <summary>
    /// sets the current world time.
    /// </summary>
    /// <param name="time">time in seconds</param>
    public void SetCurrentWorldVisualTime(float normalizedTime)
    {
        if (normalizedTime > 1)
        {
            normalizedTime = normalizedTime - (float)Math.Truncate(normalizedTime);
        }

        if (sun == null)
        {
            FindSun();
        }

        LerpMoon(normalizedTime);

        float scaledValue = normalizedTime * 24f;

        DayNightPreset p = dayPreset;

        RenderSettings.ambientIntensity = p.AmbientIntensity.Evaluate(scaledValue);
        SunLight.intensity      = p.SunIntensity.Evaluate(scaledValue);
        SunLight.shadowStrength = p.ShadowStrength.Evaluate(scaledValue);

        SunLight.color = p.SunColor.Evaluate(normalizedTime);

        RenderSettings.ambientSkyColor      = p.SkyColor.Evaluate(normalizedTime);
        RenderSettings.ambientEquatorColor  = p.EquatorColor.Evaluate(normalizedTime);
        RenderSettings.ambientGroundColor   = p.GroundColor.Evaluate(normalizedTime);

        Color FogColor = p.FogGradient.Evaluate(normalizedTime);

        RenderSettings.fogColor = FogColor;
        RenderSettings.fogDensity = p.FogDensity.Evaluate(scaledValue);

        Vector3 pos = sun.transform.rotation.eulerAngles;
        float nonMulti = p.sunEulerRotationX.Evaluate(scaledValue);

        float dayNightBalanceLerp = p.dayNightBalance.Evaluate(scaledValue);
        skyMaterial.SetFloat(dayNightSkyboxLerpName, dayNightBalanceLerp);
        skyMaterial.SetColor(skyColorName, p.SkyTintColor.Evaluate(normalizedTime));

        cloudMaterial.SetColor(cloudColorName, p.CloudTintColor.Evaluate(normalizedTime));
        cloudMaterial.SetFloat(cloudFogBlendName, p.CloudBlend.Evaluate(scaledValue));

        pos.y = p.sunEulerRotationY.Evaluate(scaledValue) * 360f;
        pos.x = p.sunEulerRotationX.Evaluate(scaledValue) * 360f;
        pos.z = 0f;

        sun.transform.eulerAngles = pos;
    }

    public Color GetWaterColor(float normalizedTime)
    {
        return dayPreset.WaterColor.Evaluate(normalizedTime);
    }

    public Color GetCombinedColor(AnimationCurve red, AnimationCurve green, AnimationCurve blue, float time)
    {
        return new Color(
            red.Evaluate(time),
            green.Evaluate(time),
            blue.Evaluate(time)
        );
    }


    private void LerpMoon(float normalizedTime)
    {
        float scaledValue = normalizedTime * 24f;
        MoonLight.intensity = dayPreset.MoonLightIntensity.Evaluate(scaledValue);

        if (MoonLight.intensity < 0.001f)
        {
            MoonLight.enabled = false;
        }
        else
        {
           MoonLight.enabled = true;
        }
    }

    private void FindSun()
    {
        List<Light> lights = new List<Light>();
        lights.AddRange(FindObjectsOfType<Light>());
        sun = lights.Find(x => x.GetComponent<Light>().type == LightType.Directional).gameObject;
        SunLight = sun.GetComponent<Light>();
    }
}