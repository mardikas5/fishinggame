﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Rotate : MonoBehaviour
{
    public Vector3 StartAngle;
    public Vector3 EndAngle;

    public float time;

    // Update is called once per frame
    void Start()
    {
        transform.localEulerAngles = StartAngle;

        transform.DOBlendableLocalRotateBy(EndAngle - StartAngle, time, RotateMode.LocalAxisAdd);
    }


}
