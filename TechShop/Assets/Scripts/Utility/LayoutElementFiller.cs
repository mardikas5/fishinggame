﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class LayoutElementFiller : MonoBehaviour
{
    LayoutElement element;
    RectTransform rectTrans;

    // Start is called before the first frame update
    void Start()
    {
        element = GetComponent<LayoutElement>();
        rectTrans = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        element.minWidth = rectTrans.sizeDelta.x;
        element.minHeight = rectTrans.sizeDelta.y;
    }
}
