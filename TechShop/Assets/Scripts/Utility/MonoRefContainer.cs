﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MonoRefContainer 
{
    [SerializeField]
    public MonoBehaviour serialRef;

    [SerializeField]
    public InterfaceContainer<object> gayboi = new InterfaceContainer<object>();
}

[Serializable]
public class InterfaceContainer<T> : NonGenericBaseHelper where T : class
{
    public T Interface(MonoBehaviour mono)
    {
        return mono as T;
    }
}