﻿using System;
using System.Collections;
using System.Collections.Generic;


public class CustomEvent : CustomEvent<object> {

    public override void Invoke(object Data = null) {
        base.Invoke(null);
    }

    public static CustomEvent operator +(CustomEvent a, Action b) {

        if (b == null) {
            return a;
        }

        a.Event += b;
        return a;
    }


    public static CustomEvent operator -(CustomEvent a, Action b) {

        if (b == null) {
            return a;
        }

        a.Event -= b;
        return a;
    }
}


/// <summary>
/// Replacement for a normal event, but this allows the reference to be passed around.
/// Do not forget to initialize or create a proper instance of this class.
/// </summary>
/// <typeparam name="T">Data <type> sent with the event.</typeparam>
public class CustomEvent<T> {

    private Dictionary<Action<T>, Action> lambdaStore = new Dictionary<Action<T>, Action>();
    private Dictionary<int, Action<T>> actions = new Dictionary<int, Action<T>>();
    private static int Index = 0;

    public T Data { get; private set; }

    public event Action Event;

    public CustomEvent() {
        Data = default(T);
        Event += () => InvokeStoredActions();
    }

    private void InvokeStoredActions() {

        Dictionary<int, Action<T>>.KeyCollection keys = actions.Keys;

        List<int> keyList = new List<int>();

        foreach (int i in keys) {
            if (actions.ContainsKey(i)) {
                keyList.Add(i);
            }
        }

        foreach (int i in keyList) {
            actions[i]?.Invoke(Data);
        }
    }

    /// <summary>
    /// Invokes the event with currently attached data. (to subscribers added via +=);
    /// </summary>
    public virtual void Invoke(T Data) {
        this.Data = Data;
        Event?.Invoke();
    }



    /// <summary>
    /// Use this in any case you want.
    /// </summary>
    /// <param name="eventAction"></param>
    /// <param name="attachToEvent"></param>
    private void SubUnsub(Action<T> attachToEvent) {

        if (attachToEvent == null) {
            return;
        }

        Index++;
        //cant use index as a reference > its constantly changing, assign local var.
        int localIndex = Index;

        attachToEvent += (x) => actions.Remove(localIndex);
        actions.Add(localIndex, attachToEvent);
    }

    /// <summary>
    /// Only use this if you do not want any data passed to your eventhandler/action.
    /// </summary>
    /// <param name="eventAction"></param>
    /// <param name="attachToEvent"></param>
    private void SubUnsub(ref Action eventAction, Action attachToEvent) {

        SubUnsub((x) => attachToEvent());
    }

    /// <summary>
    /// Attaches the action to the event for a single callback, then removes itself.
    /// <remarks>Only use this if you do not want any data passed to your eventhandler/action.</remarks>
    /// </summary>
    public void AttachOneShot(Action action) {

        if (action != null) {
            SubUnsub((x) => action());
        }
    }

    /// <summary>
    /// Attaches the action to the event for a single callback, then removes itself.
    /// </summary>
    public void AttachOneShot(Action<T> action) {
        if (action != null) {
            SubUnsub(action);
        }
    }

    /// <summary>
    /// not implemented.
    /// </summary>
    /// <param name="action"></param>
    public void RemoveOneShot(Action action) {
        //not implemented
        //if (action != null) {
        //    InvokedAction -= 
        //}
    }


    #region operators
    public static CustomEvent<T> operator +(CustomEvent<T> a, Action<T> b) {

        if (b == null) {
            return a;
        }

        Action exp = () => b.Invoke(a.Data);
        a.lambdaStore.Add(b, exp);

        a.Event += exp;
        return a;
    }


    public static CustomEvent<T> operator +(CustomEvent<T> a, Action b) {

        if (b == null) {
            return a;
        }

        a.Event += b;
        return a;
    }


    public static CustomEvent<T> operator -(CustomEvent<T> a, Action<T> b) {
        if (b == null) {
            return a;
        }

        Action exp = null;

        if (a.lambdaStore.TryGetValue(b, out exp)) {
            a.Event -= exp;
        }

        return a;
    }


    public static CustomEvent<T> operator -(CustomEvent<T> a, Action b) {
        if (b == null) {
            return a;
        }

        a.Event -= b;
        return a;
    }

    #endregion


}

