﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceUpright : MonoBehaviour
{
    public Transform Transform;
    public Vector3 Up;

    // Update is called once per frame
    private void LateUpdate()
    {
        Vector3 ang = transform.eulerAngles;
        transform.eulerAngles = new Vector3(ang.x * Up.x, ang.y * Up.y, ang.z * Up.z);
    }
}
