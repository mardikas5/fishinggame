﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipContainer : MonoBehaviour
{
    public List<AudioClip> clips = new List<AudioClip>();

    public bool PlayOnAwake = false;

    private void Awake()
    {
        PlayOnAwakeHandler();
    }

    private void PlayOnAwakeHandler()
    {
        if (!PlayOnAwake)
        {
            return;
        }

        AudioSource source = null;
        if (source = GetComponent<AudioSource>())
        {
            PlayRandom(source);
        }
    }

    public void PlayRandom()
    {
        GetComponent<AudioSource>()?.PlayOneShot(clips.Random());
    }

    public void PlayRandom(AudioSource source)
    {
        source?.PlayOneShot(clips.Random());
    }
}
