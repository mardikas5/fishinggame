using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public enum ClampMode
{
    ClampToDefault,
    ClampToBorder
}



[Serializable]
public class LinearMap2D<T> 
{
	public T[] data;
	
	public Vector2i size;

	[SerializeField]
    protected ClampMode clampMode;

    public T this[Vector2i index]
    {
        get { return GetValue( index ); }
        set { SetValue( index, value ); }
    }

	public T this[int x, int y]
	{
		get { return GetValue( x, y ); }
		set { SetValue( x, y, value ); }
	}

	protected LinearMap2D() { }
		
    public LinearMap2D( Vector2i size )
    {
        this.size = size;

        int numElements = size.x * size.y;

        data = new T[numElements];
    }

    public void SetAll( T value )
    {
        RectangleFillIterator iterator = new RectangleFillIterator( Vector2i.zero, size );
        foreach( Vector2i position in iterator )
        {
            data[IndexOf( position )] = value;
        }
    }



    public void SetClampMode( ClampMode mode )
    {
        clampMode = mode;
    }

    public bool IsInside( int x, int y )
    {
        return x >= 0 && y >= 0 && x < size.x && y < size.y;
    }

    public bool IsInside( Vector2i xy )
    {
        return IsInside( xy.x, xy.y );
    }

	public int IndexOf( int x, int y )
	{
        return x * size.y + y;
	}

    public int IndexOf( Vector2i index )
    {
        return index.x * size.y + index.y;
    }

	public T GetValue( int x, int y )
	{
		if( IsInside( x, y ) )
        {
            return data[IndexOf( x, y )];
        }

        switch( clampMode )
        {
        case ClampMode.ClampToBorder:
			x = Mathf.Clamp( x, 0, size.x - 1 );
			y = Mathf.Clamp( y, 0, size.y - 1 );

            return data[IndexOf( x, y )];            
        case ClampMode.ClampToDefault:
        default:
            return default( T );
        }
	}

    public T GetValue( Vector2i index )
    {
		return GetValue( index.x, index.y );
    }

	public void SetValue( int x, int y, T value )
	{
		if( IsInside( x, y ) )
        {
            data[IndexOf( x, y )] = value;
        }
	}

    public void SetValue( Vector2i index, T value )
    {
        SetValue( index.x, index.y, value );
    }

	public T[,] ToArray()
	{
		T[,] result = new T[size.x, size.y];

		for( int x = 0; x < size.x; ++x )
		{
			for( int y = 0; y < size.y; ++y )
			{
				result[x, y] = data[IndexOf( x, y )];
			}
		}

		return result;
	}
}