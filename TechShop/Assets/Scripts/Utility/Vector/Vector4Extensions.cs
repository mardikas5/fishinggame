﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector4Extensions
{
    //based from:
    /*
        {
        half3 offsets;

        half4 AB = steepness.xxyy * amp.xxyy * dirAB.xyzw;
        half4 CD = steepness.zzww * amp.zzww * dirCD.xyzw;

        half4 dotABCD = freq.xyzw * half4(dot(dirAB.xy, xzVtx), dot(dirAB.zw, xzVtx), dot(dirCD.xy, xzVtx), dot(dirCD.zw, xzVtx));
        half4 TIME = _Time.yyyy * speed;

        half4 COS = cos (dotABCD + TIME);
        half4 SIN = sin (dotABCD + TIME);

        offsets.x = dot(COS, half4(AB.xz, CD.xz));
        offsets.z = dot(COS, half4(AB.yw, CD.yw));
        offsets.y = dot(SIN, amp);

        return offsets;			
    }	

    */

    public static Vector4 add(this Vector4 first, Vector4 other)
    {
        return new Vector4(
            first.x + other.x,
            first.y + other.y,
            first.z + other.z,
            first.w + other.w);
    }

    public static float[] components(this Vector4 thisV4)
    {
        return new float[4] { thisV4.x, thisV4.y, thisV4.z, thisV4.w };
    }

    public static Vector4 fromComponents(this Vector4 thisV4, float[] values)
    {
        return new Vector4(values[0], values[1], values[2], values[3]);
    }
}
