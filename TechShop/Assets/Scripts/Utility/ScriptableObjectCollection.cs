﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ScriptableObjectCollection : ScriptableObject
{
    public List<ScriptableObject> objects = new List<ScriptableObject>();

#if UNITY_EDITOR
    [ContextMenu("Add to collection")]
    public void AddObjectsToCollection()
    {
        var destPath = AssetDatabase.GetAssetPath(GetInstanceID());

        AssetDatabase.SetMainObject(this, destPath);

        foreach ( var obj in objects)
        {
            var path = AssetDatabase.GetAssetPath(obj.GetInstanceID());

            AssetDatabase.RemoveObjectFromAsset(obj);
            AssetDatabase.AddObjectToAsset(obj, destPath);
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        objects.Clear();
    }

    [ContextMenu("ResetMain")]
    public void SetAsMain()
    {
        var destPath = AssetDatabase.GetAssetPath(GetInstanceID());

        AssetDatabase.SetMainObject(this, destPath);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    [ContextMenu("Deparent")]
    public void RemoveFromCollection()
    {
        var path = AssetDatabase.GetAssetPath(GetInstanceID());
        Object[] assets = AssetDatabase.LoadAllAssetsAtPath(path);
        string _name = name + ".asset";
        var newObjpath = path.Remove(path.Length - _name.Length);

        foreach (var t in assets)
        {
            if (t == this)
            {
                continue;
            }

            AssetDatabase.RemoveObjectFromAsset(t);
            AssetDatabase.CreateAsset(t, newObjpath + t.name + ".asset");
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
#endif

}
