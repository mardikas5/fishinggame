﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICallbackHandler<T> where T : Delegate
{
    Func<T> Handler { get; }
}
