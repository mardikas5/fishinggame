﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GenericState : MonoBehaviour
{
    private GenericStateController _controller { get; set; }

    public List<Func<bool>> EntryConditions = new List<Func<bool>>();

    public UnityEvent OnEnterState, OnExitState;


    public bool Active { get; private set; }


    protected virtual void Update() { }


    public virtual void EnterState() { OnEnterState?.Invoke(); }

    public virtual void ExitState() { OnExitState?.Invoke(); }


    public virtual void OnPreUpdate() { }

    public virtual void UpdateBehaviour() { }

    /// <summary>
    /// Init is called by the controller at awake/spawn of controller.
    /// </summary>
    /// <param name="controller"></param>
    public virtual void Init(GenericStateController controller)
    {
        _controller = controller;

        _controller.stateChange += OnStateChanged;
    }

    protected virtual void OnStateChanged(GenericState obj)
    {
        if (obj == this)
        {
            Active = true;
            return;
        }

        Active = false;
    }

    protected virtual bool SwitchState(GenericState newState, bool checkEntry = false)
    {
        if (checkEntry && !newState.CanEnter())
        {
            return false;
        }

        _controller.SetState(newState);

        return true;
    }

    private bool CanEnter()
    {
        foreach (Func<bool> t in EntryConditions)
        {
            if (!t.Invoke()) { return false; }
        }
        return true;
    }

    /// <summary>
    /// Register a handler for a trigger.
    /// </summary>
    /// <typeparam name="T">Handler signature</typeparam>
    /// <typeparam name="T1">Signal type</typeparam>
    /// <param name="handler"></param>
    protected virtual void RegisterTrigger<T, T1>(T handler) where T : Delegate 
    {
        //The signal is only triggered if the current state is active.
        Func<bool> preCheck = () =>
        {
            if (Active){ return true;}
            return false;
        };

        _controller.RegisterTrigger<T, T1>(handler, preCheck);
    }
}
