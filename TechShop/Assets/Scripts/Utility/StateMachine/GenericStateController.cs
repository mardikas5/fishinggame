﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Generic state machine
/// </summary>
public class GenericStateController : MonoBehaviour
{
    public GenericState ActiveState => _activeState;

    /// <summary>
    /// Signals generic events with int ID and object as param.
    /// </summary>
    public event Action<int, object> ActionSignal;

    public event Action<GenericState> stateChange;

    /// <summary>
    /// Stores all string, to int actions pairs. -> Used for faster lookup.
    /// </summary>
    public Dictionary<string, int> ActionInts = new Dictionary<string, int>();

    [SerializeField]
    protected GenericState _activeState;

    [SerializeField]
    protected GenericState startState;

    private int ActionCounter = -1;

    [Header("Debug view only"), SerializeField]
    private List<GenericState> stateBehaviours = new List<GenericState>();


    public void Awake() {
        stateBehaviours.Clear();

        GenericState[] states = transform.GetComponentsInChildren<GenericState>(true);

        stateBehaviours.AddRange(states);

        foreach (GenericState state in stateBehaviours) {
            state.Init(this); //Initialize all states.
        }

        SetState(startState);
    }


    public void Update() {
        foreach (GenericState state in stateBehaviours) {
            state.OnPreUpdate(); //wip not used.
        }

        //should always have activestate?
        ActiveState?.UpdateBehaviour();
    }


    /// <summary>
    /// Set state back to startstate.
    /// </summary>
    public void Restart() {
        SetState(startState);
    }

    /// <summary>
    /// Sets the current state in the statemachine.
    /// Use <see cref="GenericState.SwitchState(GenericState, bool)"/> to change states with entry conditions.
    /// </summary>
    /// <param name="newState"></param>
    public virtual void SetState(GenericState newState) {
        if (newState == _activeState) {
            Debug.LogWarning("!! state set to already active state.");
        }

        //if state needs exit time ?? ..
        if (_activeState != null) {
            _activeState.ExitState();
        }

        _activeState = newState;

        stateChange?.Invoke(newState);

        if (newState != null) {
            newState.EnterState();
        }
        else {
            Debug.LogWarning("!! set state to null. -> state is not changed.");
        }
    }


    #region GenericActionSignalling

    public int GetAction(string name)
    {
        if (ActionInts.TryGetValue(name, out int _value))
        {
            return _value;
        }
        else
        {
            return RegisterAction(name);
        }
    }

    /// <summary>
    /// Register trigger for a strongly typed action, to signal in a state.
    /// </summary>
    /// <typeparam name="T">Delegate signature type</typeparam>
    /// <typeparam name="T1">Action type, used to make sure the same signature can be used on different events.</typeparam>
    /// <param name="handler">Handler</param>
    /// <param name="preCheck"></param>
    public void RegisterTrigger<T, T1>(T handler, Func<bool> preCheck = null) where T : Delegate
    {
        int actionID = GetAction(GenericString<T1>());
        Action<int, object> attachedAction = (x, y) =>
        {
            if (x != actionID)
            {
                return;
            }

            if (preCheck != null)
            {
                if (preCheck?.Invoke() == false)
                {
                    return;
                }
            }

            handler.DynamicInvoke(y);
        };

        ActionSignal += attachedAction;
    }

    /// <summary>
    /// Register a strongly typed action.
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    public int RegisterAction<T>()
    {
        return RegisterAction(GenericString<T>());
    }

    /// <summary>
    /// Register a very generic action. -> no restrictions on input.
    /// String action, returns int for when that action is signalled.</summary>
    /// int is a faster comparison checking later than a string.
    /// <param name="action"></param>
    /// <returns></returns>
    public int RegisterAction(string action)
    {
        if (ActionInts.TryGetValue(action, out int _value))
        {
            return _value;
        }

        ActionCounter++;

        ActionInts.Add(action, ActionCounter);
        return ActionCounter;
    }

    public bool UnregisterAction(string action)
    {
        return ActionInts.Remove(action);
    }

    /// <summary>
    /// Signal generic action.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="param"></param>
    /// <returns></returns>
    public bool SignalAction<T>(object param)
    {
        if (ActionInts.TryGetValue(GenericString<T>(), out int _value))
        {
            ActionSignal?.Invoke(_value, param);
            return true;
        }
        
        Debug.LogError(" failed to signal: " );
        return false;
    }

    /// <summary>
    /// Signal string action with multiple inputs.
    /// </summary>
    /// <param name="action"></param>
    /// <param name="input"></param>
    /// <returns></returns>
    public bool SignalAction(string action, params object[] input)
    {
        if (ActionInts.TryGetValue(action, out int _value))
        {
            ActionSignal?.Invoke(_value, input);
            return true;
        }

        return false;
    }

    /// <summary>
    /// Get a generic string based on type hashcode.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    private string GenericString<T>()
    {
        int nr = typeof(T).GetHashCode();
        return nr.ToString();
    }

    #endregion
}
