﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class RectTransformExtensions
{
#if UNITY_EDITOR
    [MenuItem("CONTEXT/RectTransform/SetSize")]

    public static void SetAnchors(MenuCommand command)
    {
        ((RectTransform)command.context).SetAnchorsToCurrentSize();
    }
#endif
    public static RectTransform SetAnchorsToCurrentSize(this RectTransform rectTransform)
    {
        Transform transform = rectTransform.transform;

        Vector2 parentSize = transform.parent.GetComponent<RectTransform>().rect.size;
        rectTransform = transform.gameObject.GetComponent<RectTransform>();

        Vector3 actualPos = rectTransform.anchoredPosition3D;
        actualPos += new Vector3(parentSize.x * rectTransform.anchorMin.x, parentSize.y * rectTransform.anchorMin.y, 0f);

        Vector2 size = rectTransform.rect.size;

        rectTransform.anchorMin = Vector2.zero;
        rectTransform.anchorMax = Vector2.one;

        rectTransform.pivot = Vector2.zero;

        Vector2 parentPos = transform.parent.position;

        float anchorXSize = size.x / parentSize.x;
        float anchorYSize = size.y / parentSize.y;

        float anchorXPos = actualPos.x / parentSize.x;
        float anchorYPos = actualPos.y / parentSize.y;

        rectTransform.anchorMin = new Vector2(anchorXPos, anchorYPos);
        rectTransform.anchorMax = new Vector2(anchorXPos + anchorXSize, anchorYPos + anchorYSize);

        rectTransform.sizeDelta = Vector2.zero;
        rectTransform.anchoredPosition = Vector2.zero;

        return rectTransform;
    }
}
