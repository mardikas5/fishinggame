using System;
using UnityEngine;

[Serializable]
[DefaultExecutionOrder(-1000)]
public class Singleton<T> : SingletonBase where T : class
{
    private static T _instance;
    private static MonoBehaviour backing;

    public virtual void Awake()
    {
        TryCreateInstance();
    }

    protected void TryCreateInstance()
    {
        if (_instance != null)
        {
            bool hasNoInstance = true;
            try
            {
                if (backing.gameObject != null)
                {
                    hasNoInstance = false;
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Expected backing gameobject, got none. {e.Message.ToString()} ");
            }
            finally
            {
                if (hasNoInstance)
                {
                    backing = this;
                    _instance = this as T;
                    Debug.LogError($"No backing gameobject for {typeof(T).ToString()} found, allowing new instance");
                }
                else
                {
                    Debug.LogError($"Instance for {typeof(T).ToString()} already exists on {backing.name}, removing new instance.");
                    Destroy(this);
                }
            }
            if (!hasNoInstance)
            {
                Debug.LogError("had instance");
                return;
            }
        }

        backing = this;
        _instance = this as T;
    }

    public override void Initialize()
    {
        base.Initialize();
        TryCreateInstance();
    }

    public static T Instance => _instance;
}

public abstract class SingletonBase : MonoBehaviour
{
    public bool Initialized = false;

    public virtual void Initialize()
    {
        Initialized = true;
    }
}