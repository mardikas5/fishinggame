﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MonoUtility
{
    public static DelayedAction InvokeDelayedCB(this MonoBehaviour behaviour, float time, Action action)
    {
        return InvokeDelayedCB(behaviour, time, action, out Coroutine routine);
    }

    public static DelayedAction InvokeDelayedCB(this MonoBehaviour behaviour, float time, Action action, out Coroutine routine)
    {
        DelayedAction delayed = new DelayedAction();
        routine = behaviour.StartCoroutine(InvokeAfterTime(time, action));
        action += delayed.Invoke;
        return delayed;
    }

    public static Coroutine InvokeDelayed(this MonoBehaviour behaviour, float time, Action action)
    {
        return behaviour.StartCoroutine(InvokeAfterTime(time, action));
    }

    public static Coroutine InvokeRepeating(this MonoBehaviour behaviour, float time, Action action)
    {
        return behaviour.StartCoroutine(InvokeRepeating(time, action));
    }

    public static IEnumerator InvokeAfterTime(float time, Action action, bool useTimeScale = false)
    {
        if (useTimeScale)
        {
            yield return new WaitForSeconds(time);
        }
        else
        {
            yield return new WaitForSecondsRealtime(time);
        }

        action?.Invoke();
    }

    public static IEnumerator InvokeRepeating(float time, Action action)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke();
        }
    }

}
