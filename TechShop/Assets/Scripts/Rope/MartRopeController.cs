﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MartRopeController : MonoBehaviour
{
    public Transform launchObject;
    public MartRope rope;

    public float launchSpringStr;

    public float normalDist = 4f;
    public float thrownDist = 10f;

    public float normalMaxDist = .05f;
    public float thrownMaxDist = .5f;

    public float normalSpringStr = 25f;
    public float normalDamper;

    public float normalMass;
    public float thrownMass;

    public float normalAngularDrag;
    public float normalDrag;
    public float thrownAngularDrag;
    public float thrownDrag;

    public float thrownDamper;
    // Start is called before the first frame update
    void Start()
    {
        launchObject.SetParent(null);

        SetNormalizedPull(1f);

    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    ReelInRope();
        //}
        //
        //if (Input.GetKeyDown(KeyCode.Alpha2))
        //{
        //    rope.RemoveJoint(2);
        //}
        //if (Input.GetKeyDown(KeyCode.Alpha3))
        //{
        //    rope.AddJoint(2);
        //}
    }

    public void ReelInRope()
    {
        rope.limitVelocity = true;

        float nValue = 0f;
        int removed = 0;

        DOTween.To(() => rope.maxDistance, (x) => rope.maxDistance = x, normalDist, .2f);
        DOTween.To(() => nValue, (x) => { nValue = x; SetNormalizedPull(x, ref removed); }, 1f, 1f);
    }

    private void SetNormalizedPull(float normalizedPull)
    {
        float oneMinusNormal = (1f - normalizedPull);

        rope.springStr = (oneMinusNormal * launchSpringStr) + (normalizedPull * normalSpringStr);
        rope.capsuleHeight = (oneMinusNormal * .2f) + .1f;

        rope.Dist = (oneMinusNormal * thrownDist) + (normalizedPull * normalDist);

        rope.maxDistance = (oneMinusNormal * thrownMaxDist) + (normalizedPull * normalMaxDist);
        rope.ropeDrag = (oneMinusNormal * thrownDrag) + (normalizedPull * normalDrag);
        rope.damper = (oneMinusNormal * thrownDamper) + (normalizedPull * normalDamper);
        rope.ropeMass = (oneMinusNormal * thrownMass) + (normalizedPull * normalMass);
        rope.ropeAngluarDrag = (oneMinusNormal * thrownAngularDrag) + (normalizedPull * normalAngularDrag);
    }

    private void SetNormalizedPull(float normalizedPull, ref int removed)
    {
        SetNormalizedPull(normalizedPull);

        if (removed <= normalizedPull * 5f)
        {
            rope.RemoveJoint(2);
            removed++;
        }
    }


    public void ThrowLure(float launchForce, Vector3 forward)
    {

        throwLure(launchForce, forward);
    }

    private void throwLure(float launchForce, Vector3 forward)
    {
        float forceApprox = launchForce / 100f;
        float normalized = Mathf.Min(forceApprox, 1f);

        rope.limitVelocity = false;

        SetNormalizedPull(normalized);

        for (int i = 0; i < 5 * normalized; i++)
        {
            rope.AddJoint(2);
        }

        Rigidbody endBody = launchObject.GetComponent<Rigidbody>();

        endBody.isKinematic = false;
        endBody.mass = 4f;
        endBody.drag = thrownDrag;

        this.InvokeDelayed(.1f, () =>
        {
            endBody.velocity += forward * launchForce;
        });
    }
}
