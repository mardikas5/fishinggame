﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( LineRenderer ) )]
public class Rope : MonoBehaviour
{
    public GameObject m_RopePoint;

    protected LineRenderer m_LineRenderer;
    protected List<GameObject> m_Points;
    protected Vector3[] m_Positions;

    public void Initialize( Vector3 start, Vector3 end,  Rigidbody connectFrom, Rigidbody connectTo, float pointInterval = 4.0f )
    {
        m_LineRenderer = GetComponent<LineRenderer>();

        Vector3 diff = end - start;
        Vector3 dir = diff.normalized;


        int numPoints = Mathf.Max( 1, Mathf.CeilToInt( diff.magnitude / pointInterval ) );
        m_Points = new List<GameObject>( numPoints );
        m_Positions = new Vector3[numPoints];

        for( int i = 0; i < numPoints; ++i )
        {
            GameObject point = Instantiate( m_RopePoint, transform );

            Vector3 pos = start + dir * pointInterval * i;
            point.transform.position = pos;
            m_Positions[i] = pos;

            m_Points.Add( point );
        }

        m_LineRenderer.positionCount = numPoints;
        m_LineRenderer.SetPositions( m_Positions );

        
        for( int i = 0; i < numPoints - 1; ++i )
        {
            HingeJoint joint = m_Points[i].GetComponent<HingeJoint>();
            joint.connectedBody = m_Points[i + 1].GetComponent<Rigidbody>();
        }

        HingeJoint result = connectFrom.gameObject.AddComponent<HingeJoint>();
        result.connectedBody = m_Points[0].GetComponent<Rigidbody>();

        m_Points[numPoints - 1].GetComponent<HingeJoint>().connectedBody = connectTo;
        m_Points[numPoints - 1].GetComponent<HingeJoint>().connectedAnchor = end;
    }

    void Update()
    {
        for( int i = 0; i < m_Points.Count; ++i )
        {
            m_Positions[i] = m_Points[i].transform.position;
        }

        m_LineRenderer.SetPositions(m_Positions);
    }
}
