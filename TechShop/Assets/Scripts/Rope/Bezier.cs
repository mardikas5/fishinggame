﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(LineRenderer))]
public class Bezier : MonoBehaviour
{
    public Vector3[] controlPoints;
    public LineRenderer lineRenderer;

    private int curveCount = 0;
    public int numberOfPoints = 50;


    void Start()
    {
        if (!lineRenderer)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
    }

    void Update()
    {

        DrawCurve();

    }

    void DrawCurve()
    {
        curveCount = (int)controlPoints.Length / 3;
        int points = (controlPoints.Length - 2) * (numberOfPoints) + 1;

        if (points <= 1)
        {
            return;
        }

        lineRenderer.positionCount = points; //+2 is adding extra points to end and start.



        Vector3 p0, p1 ,p2;
        for (int j = 0; j < controlPoints.Length - 2; j++)
        {
            // check control points
            if (controlPoints[j] == null || controlPoints[j + 1] == null
            || controlPoints[j + 2] == null)
            {
                return;
            }
            // determine control points of segment
            p0 = 0.5f * (controlPoints[j]
            + controlPoints[j + 1]);
            p1 = controlPoints[j + 1];
            p2 = 0.5f * (controlPoints[j + 1]
            + controlPoints[j + 2]);

            // set points of quadratic Bezier curve
            Vector3 position;
            float t;
            float pointStep = 1.0f / numberOfPoints;
            if (j == controlPoints.Length - 3)
            {
                pointStep = 1.0f / (numberOfPoints - 1.0f);
                // last point of last segment should reach p2
            }
            for (int i = 0; i < numberOfPoints; i++)
            {
                t = i * pointStep;
                position = (1.0f - t) * (1.0f - t) * p0
                + 2.0f * (1.0f - t) * t * p1 + t * t * p2;
                lineRenderer.SetPosition(i + j * numberOfPoints, position);
            }

            lineRenderer.SetPosition(0, controlPoints[0]);
            lineRenderer.SetPosition(points-1, controlPoints[controlPoints.Length-1]);
        }

        //for (int j = 0; j < curveCount; j++)
        //{
        //    for (int i = 1; i <= SEGMENT_COUNT; i++)
        //    {
        //        float t = i / (float)SEGMENT_COUNT;
        //        int nodeIndex = j * 3;
        //        Vector3 pixel = CalculateCubicBezierPoint(t, controlPoints [nodeIndex], controlPoints [nodeIndex + 1], controlPoints [nodeIndex + 2], controlPoints [nodeIndex + 3]);
        //        lineRenderer.positionCount = (j * SEGMENT_COUNT) + i;
        //        lineRenderer.SetPosition((j * SEGMENT_COUNT) + (i - 1), pixel);
        //    }
        //
        //}
    }

    Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }
}