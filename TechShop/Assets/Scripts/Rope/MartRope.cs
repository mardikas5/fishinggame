﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MartRope : MonoBehaviour
{
    public float springStr;
    public float damper;

    public float minDistance;
    public float maxDistance;
    public float tolerance;
    public float stiffness;

    public float capsuleHeight;
    public float capsuleRadius;

    public float ropeMass;
    public float ropeDrag;

    public bool disableLeftOverRope;

    public Transform target;
    public Rigidbody pullIfExtendedBody;

    public LineRenderer line;

    public int maxJoints = 10;
    public int jointLayer;
    public int JointCount => joints.Length;

    private Vector3[] segmentPos;
    private GameObject[] joints;

    private int segments = 0;

    private List<Vector3> lastPos = new List<Vector3>();

    public Vector3 LocalAnchorJointPos;
    public float Dist;
    public int minJoints = 3;
    public float ropeAngluarDrag;
    public float ropeVelocityLimiter;
    public bool limitVelocity;
    public bool pullIfExtended;
    public float extension;

    private void OnDestroy()
    {
        ClearRope();
    }

    public GameObject[] getJoints()
    {
        return joints;
    }

    // Start is called before the first frame update
    private void Start()
    {
        //limitVelocity = true;
        line = GetComponent<LineRenderer>();
        this.InvokeDelayed(.1f, () => CreateRope(target));
    }

    // Update is called once per frame
    private void Update()
    {
        if (line != null)
        {
            DrawRope();
        }
    }

    private void FixedUpdate()
    {
        if (joints == null || joints.Length < 2)
        {
            return;
        }
        if (limitVelocity)
        {
            LimitVelocity();
        }
        if (pullIfExtended)
        {
            PullExtended();
        }
    }

    private void PullExtended()
    {
        Vector3 dir = joints[joints.Length - 1].transform.position - target.transform.position;

        extension = 0f;
        Color c = Color.green;
        if (dir.magnitude > maxDistance * 1.5f)
        {
            c = Color.red;
            extension = dir.magnitude;
            pullIfExtendedBody.AddForceAtPosition(dir * Time.fixedDeltaTime * 55f, target.transform.position);
        }

        Debug.DrawLine(
        target.transform.position,
        joints[joints.Length - 1].transform.position,
        c, .2f);
    }

    public void LimitVelocity()
    {
        foreach (GameObject t in joints)
        {
            t.GetComponent<Rigidbody>().velocity *= ropeVelocityLimiter;
        }
    }

    public GameObject CreateSegment(Vector3 pos, Rigidbody attachTo)
    {
        GameObject go = new GameObject();

        go.transform.position = pos;

        Rigidbody r = go.AddComponent<Rigidbody>();

        r.mass = ropeMass;
        r.drag = ropeDrag;

        CapsuleCollider capsuleCol = go.AddComponent<CapsuleCollider>();

        SetCapsuleSize(capsuleCol);
        AddJoint(go, attachTo);

        return go;
    }

    public void SetCapsuleSize(CapsuleCollider capsuleCol)
    {
        if (capsuleCol == null)
        {
            return;
        }

        capsuleCol.radius = capsuleRadius;
        capsuleCol.height = capsuleHeight;
        capsuleCol.direction = 2;
        capsuleCol.center = new Vector3(0, 0, (-capsuleHeight / 4f) - .1f);
    }

    public SpringJoint AddJoint(GameObject go, Rigidbody attachTo)
    {
        SpringJoint joint = go.AddComponent<SpringJoint>();

        joint.connectedBody = attachTo;

        joint.gameObject.layer = jointLayer;
        SetPhysicsParams(joint);

        return joint;
    }

    public void RemoveJoint(int index)
    {
        if (JointCount <= minJoints)
        {
            return;
        }

        GameObject joint = joints[index];
        joints[index + 1].GetComponent<SpringJoint>().connectedBody = joints[index - 1].GetComponent<Rigidbody>();
        GameObject.Destroy(joint);


        GameObject[] newArray = new GameObject[joints.Length - 1];

        Array.Copy(joints, 0, newArray, 0, index);
        newArray[index] = joints[index + 1];

        Array.Copy(joints, index + 2, newArray, index + 1, joints.Length - (index + 2));

        joints = newArray;
    }

    public void AddJoint(int index)
    {
        GameObject newJoint = CreateSegment(joints[index].transform.position, joints[index-1].GetComponent<Rigidbody>());

        GameObject[] newArray = new GameObject[joints.Length + 1];

        joints[index].GetComponent<SpringJoint>().connectedBody = newJoint.GetComponent<Rigidbody>();

        Array.Copy(joints, 0, newArray, 0, index);

        newArray[index] = newJoint;

        Array.Copy(joints, index, newArray, index + 1, joints.Length - index);

        newJoint.gameObject.name += "Joint_" + index;

        joints = newArray;
    }

    public void CreateRope(Transform target)
    {
        ClearRope();

        segments = maxJoints;

        segmentPos = new Vector3[maxJoints];
        joints = new GameObject[maxJoints];
        segmentPos[0] = transform.position;
        segmentPos[maxJoints - 1] = target.position;

        // Find the distance between each segment
        int segs = segments-1;
        Vector3 seperation = ((target.position - transform.position)/segs);

        joints[0] = gameObject;
        for (int s = 1; s < segments; s++)
        {
            // Find the each segments position using the slope from above
            Vector3 vector = (seperation*s) + transform.position;
            Debug.DrawLine(vector, vector + Vector3.up, Color.red, 5f);
            segmentPos[s] = vector;

            //Add Physics to the segments
            joints[s] = CreateSegment(vector, joints[s - 1].GetComponent<Rigidbody>());
            joints[s].gameObject.name += "Joint_" + s;

            joints[s].transform.LookAt(joints[s - 1].transform);
        }
        AddJoint(target.gameObject, joints[segments - 1].GetComponent<Rigidbody>());
    }

    private void ClearRope()
    {
        if (joints == null)
        {
            return;
        }

        //joints[0] is equal to this object.
        for (int i = 1; i < joints.Length; i++)
        {
            Destroy(joints[i]);
        }
    }

    private void DrawRope()
    {
        lastPos.Clear();

        if (joints == null)
        {
            return;
        }

        segments = joints.Length;

        LocalAnchorJointPos = Vector3.forward * (Dist / segments);

        for (int i = 0; i <= segments; i++)
        {
            if (i == 0)
            {
                lastPos.Add(transform.position);
            }
            else
            if (i == segments)
            {
                lastPos.Add(target.transform.position);
            }
            else
            {
                if (i >= 1 && i < segments)
                {
                    SetCapsuleSize(joints[i].GetComponent<CapsuleCollider>());

                    SetPhysicsParams(joints[i].GetComponent<SpringJoint>());

                    joints[i].name = "Joint_" + i;
                }

                lastPos.Add(joints[i].transform.position);
            }
        }

        GetComponent<Bezier>().controlPoints = lastPos.ToArray();
    }

    private Vector3 Stiffness(int i)
    {
        Vector3 target = joints[i-1].transform.position - joints[i].transform.position;
        Vector3 normalizedTargetForward = target.normalized;
        Vector3 currentForward = joints[i].transform.forward;

        Vector3 moveBy = normalizedTargetForward - currentForward;
        Vector3 deltaMoveBy = moveBy * Time.deltaTime * stiffness;

        if (deltaMoveBy.magnitude > moveBy.magnitude)
        {
            deltaMoveBy *= moveBy.magnitude / deltaMoveBy.magnitude;
        }

        return deltaMoveBy;
    }

    private void SetPhysicsParams(SpringJoint joint)
    {
        joint.autoConfigureConnectedAnchor = false;

        joint.damper = damper;
        joint.spring = springStr;
        joint.minDistance = minDistance;
        joint.maxDistance = maxDistance;
        joint.tolerance = tolerance;
        joint.enableCollision = true;
        joint.connectedAnchor = LocalAnchorJointPos;

        Rigidbody r = joint.GetComponent<Rigidbody>();
        r.mass = ropeMass;
        r.drag = ropeDrag;
        r.angularDrag = ropeAngluarDrag;
    }
}
