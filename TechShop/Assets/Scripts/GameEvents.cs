﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class GameEvents : Singleton<GameEvents>
{
    public enum EventEnum
    {
        OnItemsExchanged,
        OnDeliveryBoatArrived,
        OnDeliveryBoatLeft
    }

    public Dictionary<EventEnum, Func<Delegate>> eventHandlerPairs = new Dictionary<EventEnum,  Func<Delegate>>();

    public event Action<List<Item>> OnItemsExchanged;
    public event Action OnDeliveryBoatArrived;
    public event Action onDeliveryBoatLeft;

    public void Start()
    {
        InitDict();
    }

    public void InitDict()
    {
        eventHandlerPairs.Add(EventEnum.OnItemsExchanged, () => OnItemsExchanged);
        eventHandlerPairs.Add(EventEnum.OnDeliveryBoatArrived, () => OnDeliveryBoatArrived);
        eventHandlerPairs.Add(EventEnum.OnDeliveryBoatLeft, () => onDeliveryBoatLeft);

        this.InvokeDelayed(2f, () => DynamicInvokeEvent(EventEnum.OnDeliveryBoatArrived, null));
    }

    public void DynamicInvokeEvent(EventEnum Enum, object value)
    {
        if (eventHandlerPairs.TryGetValue(Enum, out Func<Delegate> eventHandler))
        {
            eventHandler?.Invoke()?.DynamicInvoke(value);
        }
    }
}

