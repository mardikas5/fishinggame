﻿using Fishy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Fishy
{
    public abstract class ItemProcessor : MonoBehaviour
    {
        [Serializable]
        public class Production
        {
            public List<Item> usedItems = new List<Item>();
            public List<Item> createdItems = new List<Item>();
            public Recipe usedRecipe;

            public void Clear()
            {
                usedItems.Clear();
                createdItems.Clear();
                usedRecipe = null;
            }
        }

        public enum ProcessState
        {
            Waiting,
            Processing,
            Completed
        }

        public ProcessState processorState;

        public List<Recipe> Recipies = new List<Recipe>();

        public Recipe ActiveRecipe;

        public Inventory ResultInventory;

        public List<Item> InputItems;

        public float normalizedProgress;

        public float HoursToProcess;
        public float normalizedDaysPassed;

        public event Action<ProcessState> stateChanged;

        public HumanController User;

        public Production activeProduction;

        public event Action OnRecipeNotSatisfied;

        public void SetState(ProcessState state)
        {
            processorState = state;

            stateChanged?.Invoke(state);
        }

        public void SetActiveRecipe(Recipe active)
        {
            if (!Recipies.Contains(active))
            {
                Debug.LogError(" set recipe to one not given in recipe list for this processor. ");
            }

            ActiveRecipe = active;
        }

        public void Update()
        {
            if (processorState == ProcessState.Completed)
            {
                if (ResultInventory.ItemsList.Count == 0)
                {
                    SetState(ProcessState.Waiting);
                }
            }
        }

        protected virtual void OnProcessingComplete()
        {
            foreach (Item i in activeProduction.createdItems)
            {
                ResultInventory.TryPlaceAnySlot(i);
            }

            SetState(ProcessState.Completed);

            activeProduction.Clear();

            activeProduction = null;
        }

        public bool StartProcessingRecipe(List<Item> orderedItems, Recipe recipe)
        {
            InputItems = orderedItems;

            HoursToProcess = recipe.ProductionTime;

            ActiveRecipe = recipe;
            //
            return StartProcessing(out activeProduction);
        }

        protected virtual bool StartProcessing(out Production production)
        {
            Debug.LogError("start processin");

            normalizedProgress = 0f;

            production = new Production() { usedRecipe = ActiveRecipe };

            List<EntityInstance> itemInstances = InputItems.ConvertAll((x) => x.Entity);

            if (!ActiveRecipe.ProduceOrdered(itemInstances, out List<EntityInstance> outputs, out List<EntityInstance> consumed))
            {
                Debug.LogError(" recipe was not satisfied! ");
                OnRecipeNotSatisfied?.Invoke();
                return false;
            }

            foreach (EntityInstance t in consumed)
            {
                itemInstances.Remove(t);
                Item item = InputItems.FirstOrDefault((x) => x.Entity == t);
                item?.Container?.Remove(item);
            }

            production.createdItems = outputs.ConvertAll((x) => ItemFactory.Create(x));

            foreach (Item t in InputItems)
            {
                if (t == null)
                {
                    continue;
                }

                if (consumed.Contains(t.Entity))
                {
                    continue;
                }

                t.Container?.Remove(t);
                production.createdItems.Add(t);
            }


            SetState(ProcessState.Processing);

            return true;
        }


        public virtual void AdvanceTime(float hoursPassed)
        {
            if (processorState != ProcessState.Processing)
            {
                return;
            }

            normalizedProgress += hoursPassed / HoursToProcess;

            if (normalizedProgress >= 1f)
            {
                OnProcessingComplete();
            }
        }
    }
}