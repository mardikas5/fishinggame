﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fishy
{
    public class ItemProcessorController : MonoBehaviour
    {
        private ItemProcessor processor;

        public void Start()
        {
            processor = GetComponent<ItemProcessor>();

            GameLogic.Instance.Time.NormalizedHoursPassed += processor.AdvanceTime;
        }

        public void Interact()
        {
            processor.User = GameLogic.Instance.Player.GetComponent<HumanController>();
            
            UI.Instance.ProcessPanelUI.Open(
                processor,
                GameLogic.Instance.Player.InventoryController.Inventory,
                processor.ResultInventory);
        }
    }
}