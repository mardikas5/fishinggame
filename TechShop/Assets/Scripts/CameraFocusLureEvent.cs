﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocusLureEvent : MonoBehaviour
{
    public void SignalFocus()
    {
        GameObject lure = GetComponent<FishingStateController>().activeRod.lure;

        GameLogic.Instance.CameraController.SetState(CameraController.CameraStateEnum.CloseUp);
        GameLogic.Instance.CameraController.SetFocus(lure.transform);
    }
}
