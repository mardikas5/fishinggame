using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Rotation
{
    Horizontal,
    Vertical
}

/// <summary>
/// Storable and later recoverable state for item placement.
/// </summary>
[Serializable]
public struct StartState
{
    public Int2 CornerPos;
    public Rotation Rotation;

    public StartState(Item i)
    {
        CornerPos = i.CornerPos;
        Rotation = i.Rotation;
    }

    public void Apply(Item i)
    {
        i.CornerPos = CornerPos;
        i.Rotation = Rotation;
    }
}

/// <summary>
/// Inventory item
/// </summary>
public class Item
{
    public Int2 CornerPos; //private?
    public List<Int2> OccupiedSlots = new List<Int2>(); //private?

    public Rotation Rotation;

    public Inventory Container; // get private set?


    public event Action ItemUpdated;
    public event Action<Item> ItemChangedSignal;
    public event Action<Item> OnDirty;

    public int StackSize => ItemBase.StackSize;

    /// <summary>
    /// Linked scriptableobject, static data.
    /// </summary>
    public ItemBase ItemBase { get; private set; }

    /// <summary>
    /// Instance of the underlying entity of the item.
    /// </summary>
    public EntityInstance Entity { get; private set; }

    public int CurrentStack { get => currentStack; set { currentStack = value; ItemUpdated?.Invoke(); } }

    //easy access to lower class properties.
    public Sprite Sprite => ItemBase.Sprite;
    public string Name => ItemBase.Name;

    //for draggin and placement. -> this doesnt really belong in the class -> more for UIItem, dragging?
    private StartState startState;
    private bool hasStartState = false;

    private int currentStack = 1;

    public Int2 Size
    {
        get
        {
            Int2 ItemSize = ItemBase.Size;

            if (Rotation == Rotation.Vertical) {
                ItemSize.x = ItemBase.Size.y;
                ItemSize.y = ItemBase.Size.x;
            }

            return ItemSize;
        }
    }

    //Constructor.
    public Item(EntityInstance entity, ItemBase itemBase)
    {
        entity.OnEntityUpdated += SignalChange;

        ItemComponentSlots components = entity.GetComponent<ItemComponentSlots>();

        if (components == null)
        {
            components = entity.gameObject.AddComponent<ItemComponentSlots>();
        }

        components.GenerateFromItemBase(itemBase);

        if (components != null)
        {
            components.OnComponentSlotChanged += SignalChange;
        }

        Entity = entity;
        ItemBase = itemBase;
    }


    public void MarkAsDirty()
    {
        ItemUpdated?.Invoke();
    }

    /// <summary>
    /// Sets the item container -> only insert validated data here.
    /// </summary>
    /// <param name="i"></param>
    /// <param name="occupied"></param>
    public void SetContainer(List<Int2> occupied)
    {
        OccupiedSlots = occupied;
        ItemUpdated?.Invoke();
    }

    public bool CanTakeOneFromStack()
    {
        if (!ItemBase.Stackable || currentStack < 1)
        {
            return false;
        }

        return true;
    }

    private void SignalChange() {
        ItemChangedSignal?.Invoke(this);
    }

    #region dragHandling -> this should be part of UI?
    public void SaveStartState() {
        if (!hasStartState) {
            hasStartState = true;
            startState = new StartState(this);
        }
    }

    public void FreeStartState() {
        hasStartState = false;
    }

    public void ApplyStartState() {
        startState.Apply(this);
        FreeStartState();
    }

    #endregion

}

