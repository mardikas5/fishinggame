
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;
using static PlayerStats;

public class ItemBase : ScriptableObject
{
    #region fields
    public List<ItemComponentSlotDefinition> SlotDefinitions = new List<ItemComponentSlotDefinition>();

    public List<ModifierTypePair> Modifiers = new List<ModifierTypePair>();

    public Int2 Size;

    public string Identifier;

    public string Name;

    public Sprite Sprite;

    public string SpriteName;

    public int StackSize;

    [TextArea]
    public string Description;

    #endregion

    public bool Stackable => StackSize > 1;

#if UNITY_EDITOR
    [ContextMenu("JsonExample")]
    public void GetJson()
    {
        Debug.LogError(JsonUtility.ToJson(this));
    }
#endif
}
