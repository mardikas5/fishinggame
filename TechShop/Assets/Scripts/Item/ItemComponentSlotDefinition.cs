﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ItemComponentSlotDefinition 
{
    public ItemTag[] tags;

    public bool IsValid(Item i)
    {
        foreach (ItemTag t in tags)
        {
            foreach (var item in i.Entity.Base.Tags)
            {
                if (t.MatchesTag(item))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
