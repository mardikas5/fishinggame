﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Slots that hold components for an item -> e.g. hook or line in a fishing rod.
/// </summary>
public class ItemComponentSlots : MonoBehaviour
{
    public List<ItemComponentSlot> ComponentSlots = new List<ItemComponentSlot>();

    public List<Inventory> ComponentSlotInventories = new List<Inventory>();

    public event Action OnComponentSlotChanged;

    public void GenerateFromItemBase(ItemBase item)
    {
        foreach (ItemComponentSlotDefinition slotDefinition in item.SlotDefinitions)
        {
            ItemComponentSlot itemSlot = new ItemComponentSlot();
            itemSlot.SlotDefinition = slotDefinition;
            ComponentSlots.Add(itemSlot);
        }

        GenerateInventories();
    }

    public static bool GetNestedItems(Item slotItem, ref List<ItemBase> modsList)
    {
        if (slotItem == null)
        {
            return false;
        }

        ItemBase itemBase = slotItem.ItemBase;

        ItemComponentSlots itemSlots = slotItem.Entity.GetComponent<ItemComponentSlots>();

        modsList.Add(itemBase);

        if (itemSlots == null)
        {
            return false;
        }

        foreach (Inventory t in itemSlots.ComponentSlotInventories)
        {
            if (t.ItemsList.Count == 0)
            {
                continue;
            }

            GetNestedItems(t.ItemsList[0], ref modsList);
        }

        return true;
    }

    private void GenerateInventories()
    {
        foreach (ItemComponentSlot t in ComponentSlots)
        {
            Inventory i = gameObject.AddComponent<Inventory>();

            i.ItemRemoved += OnItemRemoved;
            i.ItemAdded += OnItemAdded;

            i.Init(new Int2(1, 1));
            i.IgnoreItemSize = true;
            i.InsertConditions.Add(t.SlotDefinition.IsValid);
            ComponentSlotInventories.Add(i);
        }
    }

    private void OnItemRemoved(Item item)
    {
        ItemComponentSlots slots = item.Entity.GetComponent<ItemComponentSlots>();
        if (slots == null)
        {
            return;
        }
        slots.OnComponentSlotChanged -= SignalChange;
        SignalChange();
    }

    private void OnItemAdded(Item item)
    {
        //if a slot gets changed (if an item is removed or added) 
        //> then if the item that was added gets its slots changed this triggers again.
        ItemComponentSlots slots = item.Entity.GetComponent<ItemComponentSlots>();
        if (slots == null)
        {
            return;
        }
        slots.OnComponentSlotChanged += SignalChange;
        SignalChange();
    }

    private void SignalChange()
    {
        OnComponentSlotChanged?.Invoke();
    }
}
