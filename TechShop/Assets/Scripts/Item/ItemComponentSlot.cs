﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ItemComponentSlot
{
    public ItemComponentSlotDefinition SlotDefinition;

    public Item SlotItem;

    public bool SetItem(Item item)
    {
        if (SlotItem != null)
        {
            return false;
        }

        if (!SlotDefinition.IsValid(item))
        {
            return false;
        }

        SlotItem = item;

        return true;
    }

    public void RemoveItem(out Item item)
    {
        item = null;

        if (SlotItem != null)
        {
            item = SlotItem;
        }
    }
}
