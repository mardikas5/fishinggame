﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHousingController : MonoBehaviour
{
    public PayRent PayRentDuty;

    public int paymentIntervalDays;

    public DateTime lastPayDate;


    private void Start()
    {
        GameLogic.Instance.PlayTimeController.OnDayEnded += OnDayEnd;
    }

    private void OnDayEnd()
    {
        if (lastPayDate.AddDays(paymentIntervalDays) <= (GameLogic.Instance.Time.GameTime))
        {
            AddRentPayment();
            lastPayDate = GameLogic.Instance.Time.GameTime;
        }
    }

    private void AddRentPayment()
    {
        PayRent payRent = Instantiate(PayRentDuty);
        payRent.dueDate = GameLogic.Instance.Time.GameTime.AddDays(paymentIntervalDays);
        GameLogic.Instance.Player.DutyController.AddDuty(payRent);
    }
}
