﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRestArea : MonoBehaviour
{
    //could be a global in game settings.
    public float SleepTimeScale = 5f;



    public event Action OnRestAreaClicked;

    public event Action OnRestStarted;

    public event Action OnRestComplete;

    //Set by inspector.
    public Transform bedCenter;

    //serialized to expose to inspector.
    [SerializeField]
    private float SpeedUpTimeLeft = 0f;

    [SerializeField]
    private bool SlowingDown = false;

    private TimeController TimeController;
    private TweenerCore<float, float, FloatOptions> timeTween;
    private DateTime currentRestEndTime;

    // Start is called before the first frame update
    private void Start()
    {
        TimeController = GameLogic.Instance.PlayTimeController.TimeController;
        currentRestEndTime = new DateTime(0);
    }

    // Update is called once per frame
    private void Update()
    {
        UpdateResting();
    }

    private void UpdateResting()
    {
        CheckRestTime();

        if (SpeedUpTimeLeft > 0f)
        {
            SpeedUpTimeLeft -= Time.deltaTime;
        }

        if (SpeedUpTimeLeft < 3f && !SlowingDown)
        {
            SlowingDown = true;
            timeTween?.Kill(false);
            timeTween = DOTween.To(() => Time.timeScale, (x) => Time.timeScale = x, 1f, SpeedUpTimeLeft - (SpeedUpTimeLeft / 3f));
        }
    }

    private void CheckRestTime()
    {
        if (currentRestEndTime.Ticks > 0)
        {
            if (TimeController.GameTime > currentRestEndTime)
            {
                GameLogic.Instance.Player.EnergyController.OnEndRest();
                OnRestComplete?.Invoke();
                currentRestEndTime = new DateTime(0);
            }
        }
    }

    public void OnRestAreaClickedHandler()
    {
        UI.Instance.SleepPromptPanelUI.ShowRestArea(this);
    }

    public TimeSpan GetRestTimeLeft()
    {
        return (currentRestEndTime - TimeController.GameTime);
    }

    public void RestForHours(float hours)
    {
        StartSleepSequence(hours);
    }

    private void StartSleepSequence(float hours)
    {
        float realSecondsTime = TimeController.GameHoursToRealSeconds(hours);
        currentRestEndTime = TimeController.GameTime.AddHours(hours);

        SpeedUpTimeLeft = realSecondsTime;
        SlowingDown = false;

        this.InvokeDelayed(.8f, () =>
        {
            timeTween = DOTween.To(() => Time.timeScale, (x) => Time.timeScale = x, SleepTimeScale, 3f);
        });

        if (bedCenter == null)
        {
            bedCenter = transform;
        }

        //ugly lookup to player?
        GameLogic.Instance.Player.EnergyController.OnStartRest(bedCenter);

        OnRestStarted?.Invoke();
    }
}
