﻿using NCalc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PlayerStats.StatValue;

public class PlayerStats : MonoBehaviour
{
    #region StatThoughtsText
#if ReadOnly

    ///examples for some of the values that could be modified.
    
    public StatValue PullStrength; //Strength. -> the quality of being physically strong.
    public StatValue FishEscapeSpeed; //Tenacity. -> persistance, being able to grip something.

    public StatValue BaitAccuracySize; //Imitation. -> act of copying.
    public StatValue BaitMovementSpeed; //
    public StatValue BaitStrength;

    public StatValue SliderSize; //Steadiness. -> not moving or shaking.
    public StatValue SliderMovementUp; //Finesse. -> delicacy and skill.
    public StatValue SliderMovementDown;

#endif
    #endregion

    [Serializable]
    public class ModifierTypePair
    {
        public Modifier mod;
        public StatEnum stat;
    }

    public enum StatModifierTypeEnum
    {
        Add,
        MultiplyBase,
        MultiplyFinal,
        Custom // increases the size of capture area by 20%?
    }

    public enum StatEnum
    {
        Strength,   //pull str.
        Tenacity,   //how fast fish swims away.
        Imitation,  //how fast bait affects fish.
        Steadiness, //steadiness.
        Finesse     //finesse.
    }

    [Serializable]
    public class StatValue
    {
        public static Dictionary<StatModifierTypeEnum, Func<StatValue,float,float>> StatCalcHandlers = new Dictionary<StatModifierTypeEnum, Func<StatValue,float,float>>()
        {
            { StatModifierTypeEnum.Add, Additive },
            { StatModifierTypeEnum.MultiplyBase, MultiplyBase },
            { StatModifierTypeEnum.MultiplyFinal, MultiplyFinal }
        };

        [Serializable]
        public class Modifier
        {
            public StatModifierTypeEnum ModifierType;

            public float Value;

            public int Order;

            public Modifier() { }
        }

        public string Name;

        public float BaseValue;
        public float FinalValue;

        public event Action<float> ValueChanged;

        public List<Modifier> modifiers = new List<Modifier>();

        public float Evaluate()
        {
            FinalValue = BaseValue;

            foreach (Modifier m in modifiers)
            {
                FinalValue = StatCalcHandlers[m.ModifierType](this, m.Value);
            }

            ValueChanged?.Invoke(FinalValue);

            return FinalValue;
        }


        public void RemoveModifier(Modifier mod)
        {
            modifiers.Remove(mod);
        }

        public void AddModifier(Modifier mod)
        {
            modifiers.Add(mod);
        }

        public static string GetModifierString(Modifier mod)
        {
            if (mod.ModifierType == StatModifierTypeEnum.Add)
            {
                return " +" + mod.Value.ToString("F1");
            }

            if (mod.ModifierType == StatModifierTypeEnum.MultiplyBase)
            {
                return " base +" + (mod.Value * 100f) + "%";
            }

            if (mod.ModifierType == StatModifierTypeEnum.MultiplyFinal)
            {
                return " total +" + (mod.Value * 100f) + "%";
            }

            return " Unknown";
        }

        //Yauchers here.
        public static float Additive(StatValue m, float b)
        {
            return m.FinalValue + b;
        }

        public static float MultiplyBase(StatValue m, float b)
        {
            return m.BaseValue * (1f + b);
        }

        public static float MultiplyFinal(StatValue m, float b)
        {
            return m.FinalValue * (1f + b);
        }

        public static implicit operator float(StatValue value)
        {
            return value.FinalValue;
        }
    }

    //literal strength, how fast you can pull fish out of water.
    public StatValue Strength;
    //how easy it is to grasp things, stick to them ( fishing rod area ).
    public StatValue Tenacity;
    //how easy it is to bait.
    public StatValue Imitation; //baiting?
    //how accurate you can be with your tension / baiting ( increases speed of your tension area game, 
    public StatValue Steadiness;
    //slows down baiting game circle speed. speeds up fishing rod tension area game.
    public StatValue Finesse;

    public Dictionary<StatEnum, StatValue> StatEnumValues = new Dictionary<StatEnum, StatValue>();

    public List<StatValue> AllStats;

    private void GenerateDict()
    {
        StatEnumValues.Clear();

        StatEnumValues = new Dictionary<StatEnum, StatValue>()
        {
            { StatEnum.Strength, Strength },
            { StatEnum.Tenacity, Tenacity },
            { StatEnum.Imitation, Imitation },
            { StatEnum.Steadiness, Steadiness },
            { StatEnum.Finesse, Finesse },
        };
    }

    public void Awake()
    {
        GenerateDict();

        AllStats = new List<StatValue>()
        {
             Strength, Tenacity, Imitation, Steadiness, Finesse
        };

        AllStats.ForEach((x) => x.Evaluate());
    }

    public void AddModifier(StatEnum stat, Modifier mod)
    {
        GetValue(stat, out StatValue statValue);
        if (statValue != null)
        {
            statValue.AddModifier(mod);
        }
    }

    public object GetValue(StatEnum Stat, out StatValue stat)
    {
        //stat = null;

        if (StatEnumValues.TryGetValue(Stat, out stat))
        {
            return stat.Evaluate();
        }

        return 0f;
    }
}
