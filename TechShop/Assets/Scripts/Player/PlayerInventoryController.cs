﻿using PixelCrushers.DialogueSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class PlayerInventoryController : MonoBehaviour
{
    InventoryController invController;

    private void Awake()
    {
        invController = GetComponent<InventoryController>();

        ///Compares the itembase name.
        Lua.RegisterFunction("PlayerHasItem", this, typeof(PlayerInventoryController).GetMethod("PlayerHasItem"));
        Lua.RegisterFunction("PlayerRemoveItem", this, typeof(PlayerInventoryController).GetMethod("PlayerRemoveItem"));

        QuestLog.AddQuestStateObserver("Fishing1", LuaWatchFrequency.EndOfConversation, OnConvoEnd);
    }

    private void OnConvoEnd(string questName, QuestState newState)
    {
        Debug.LogError(questName);
        if (newState == QuestState.Success)
        {
            UI.Instance.ShowDemoCompleted();
        }
    }

    private bool _playerHasItems(string itemName, int count, out Item[] items)
    {
        items = new Item[count];

        List<Item> itemsList = invController.Inventory.ItemsList;

        int nrFound = 0;

        for (int i = 0; i < itemsList.Count; i++)
        {
            if (itemsList[i].ItemBase.Name == itemName)
            {
                items[nrFound] = itemsList[i];
                nrFound++;

                if (count == nrFound)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Compares the itembase name.
    /// </summary>
    /// <param name="itemName"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public bool PlayerHasItem(string itemName, float count)
    {
        return _playerHasItems(itemName, (int)count, out Item[] items);
    }

    public bool PlayerRemoveItem(string itemName, float count)
    {
        if (_playerHasItems(itemName, (int)count, out Item[] items))
        {
            for (int i = 0; i < items.Length;i++)
            {
                invController.Inventory.Remove(items[i]);
            }
        }

        return false;
    }

}
