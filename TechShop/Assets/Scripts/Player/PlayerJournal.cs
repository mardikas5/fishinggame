﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJournal : MonoBehaviour
{
    public List<Notice> ActiveNotices = new List<Notice>();

    public void AddNotice(Notice n)
    {
        ActiveNotices.Add(n);
    }
}
