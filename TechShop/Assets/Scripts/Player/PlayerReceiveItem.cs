﻿using Fishy;
using PixelCrushers.DialogueSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//test script for the first conversation and lua trigger for getting an item.
public class PlayerReceiveItem : MonoBehaviour
{
    public EntityBase StartingRod;

    private void Awake()
    {
        Lua.RegisterFunction("GiveItemStringRod", this, typeof(PlayerReceiveItem).GetMethod("GiveStartingRod"));
    }

    public void GiveStartingRod()
    { 
        Item i = ItemFactory.Create(EntityFactory.Create(StartingRod));

        OnItemReceived(i);
    }

    public void OnItemReceived(Item i)
    {
        GameLogic.Instance.Player.InventoryController.Inventory.TryPlaceAnySlot(i);

        UI.Instance.PlayerRecievedItemUI.OnItemRecieved(i.ItemBase);
    }
}
