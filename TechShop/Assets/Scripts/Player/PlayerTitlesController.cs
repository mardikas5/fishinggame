﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;

public class PlayerTitlesController : MonoBehaviour
{
    public bool hasCitizenship;

    // Start is called before the first frame update
    void Start()
    {
        Lua.RegisterFunction("BuyCitizenShip", this, typeof(PlayerTitlesController).GetMethod("BuyCitizenShip"));
        Lua.RegisterFunction("PushVariables", this, typeof(PlayerTitlesController).GetMethod("PushVariables"));

    }

    public void UpdateMoney()
    {
        float money = GameLogic.Instance.playerMoney.Amount;
        //Lua.Result t = DialogueLua.GetVariable("playerMoney");
        DialogueLua.SetVariable("playerMoney", money);
    }

    public void UpdateCitizenShip()
    {
        bool isCitizen = hasCitizenship;
        //Lua.Result t = DialogueLua.GetVariable("isCitizen");
        DialogueLua.SetVariable("isCitizen", isCitizen);
    }

    public void PushVariables()
    {
        UpdateMoney();
        UpdateCitizenShip();
    }


    public void BuyCitizenShip()
    {
        GameLogic.Instance.playerMoney.RemoveAmount(15f);
        hasCitizenship = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
