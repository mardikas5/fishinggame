﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonetaryDuty : PlayerDuty
{
    public float AmountRequired;

    public override bool TryFill(PlayerController controller)
    {
        if (GameLogic.Instance.playerMoney.RemoveAmount(AmountRequired, false))
        {
            //
            return true;
        }

        return true;
    }
}
