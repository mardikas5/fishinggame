﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDutyController : MonoBehaviour
{
    public List<PlayerDuty> duties = new List<PlayerDuty>();


    public event Action<PlayerDuty> AddedDuty;

    public event Action<PlayerDuty> RemovedDuty;

    public EndOfDayDutyResolver DutyResolver;


    public void ResolveDuties(List<PlayerDuty> fillDuties, Action OnDutiesResolved)
    {
        //OnDutiesResolved?.Invoke();

        DutyResolver.Resolve(fillDuties, OnDutiesResolved);
    }

    public List<PlayerDuty> DayDuties()
    {
        return CheckDuties();
    }

    public void RemoveDuty(PlayerDuty duty)
    {
        if (duties.Remove(duty))
        {
            RemovedDuty?.Invoke(duty);
        }
    }

    public void AddDuty(PlayerDuty duty)
    {
        duties.Add(duty);

        duty.UpdateResponsibility();

        AddedDuty?.Invoke(duty);
    }

    public List<PlayerDuty> CheckDuties()
    {
        List<PlayerDuty> needsSolving = new List<PlayerDuty>();

        foreach(PlayerDuty t in duties)
        {
            if (t.dueDate < GameLogic.Instance.Time.GameTime)
            {
                needsSolving.Add(t);
                //t.TryFill(GameLogic.Instance.Player);
            }
        }

        return needsSolving;
    }
}
