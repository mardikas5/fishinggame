﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfDayDutyResolver : MonoBehaviour
{
    public UIPlayerDuties UIDuties;
    public PlayerDutyController Controller;

    public void Resolve(List<PlayerDuty> duties, Action onComplete)
    {
        UIDuties.StartEndOfDayEvaluation();

        int index = 0;
        List<Action> doActions = new List<Action>();

        //for loop action.
        Action onSingleResolved = () =>
        {
            if (index >= doActions.Count)
            {
                UIDuties.EndDayEvaluation();
                UI.Instance.InvokeDelayed(.3f,
                    () => {onComplete?.Invoke();});
                return;
            }

            doActions[index].Invoke();
            index++;
        };

        Vector2 middle = new Vector2 (
                    UI.Instance.drawArea.GetComponent<RectTransform>().rect.size.x * .5f,
                    UI.Instance.drawArea.GetComponent<RectTransform>().rect.size.y * .85f);

        //setting up actions.
        for (int i = 0; i < duties.Count; i++)
        {
            PlayerDuty t = duties[i];

            //getting ui object.
            if (UIDuties.uiPairs.TryGetValue(t, out UIDuty uiObj))
            {
                //frame delay due to layout group update delay.
                this.InvokeDelayed(.01f, () =>
                {
                    //remove parent from all due to snapping on layout group.
                    uiObj.transform.SetParent(UI.Instance.drawArea.transform);
                });

                //adding anim.
                doActions.Add(() =>
                {
                    //animation.
                    uiObj.GetComponent<RectTransform>().DOMove(middle, .3f).OnComplete(() =>
                    {
                        uiObj.transform.DOLocalMove(Vector3.up * 300f, 2f);
                        t.TryFill(GameLogic.Instance.Player);
                    });

                    //finishing.
                    this.InvokeDelayed(.4f, () =>
                    {
                        uiObj.Resolve(() =>
                        {
                            Controller.RemoveDuty(t);
                            onSingleResolved();
                        });
                    });
                });
            }
            else
            {
                //if no ui, just fill the condition.
                t.TryFill(GameLogic.Instance.Player);
            }
        }

        this.InvokeDelayed(1f, () =>
        {
            //start the animation for loop.
            onSingleResolved?.Invoke();
        });
    }
}
