﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//TODO
[Serializable]
public abstract class PlayerDuty : MonoBehaviour
{
    [TextArea]
    public string Description;

    public PlayerController Controller;

    public event Action OnValueUpdate;

    public DateTime dueDate;

    public int NormalizedHoursToPay;


    protected virtual void Start()
    {
        dueDate = GameLogic.Instance.Time.GameTime.AddHours(NormalizedHoursToPay);

        Controller = GameLogic.Instance.Player;
    }

    protected void SignalUpdate()
    {
        OnValueUpdate?.Invoke();
    }

    public abstract void UpdateResponsibility();

    public abstract bool TryFill(PlayerController controller);
}
