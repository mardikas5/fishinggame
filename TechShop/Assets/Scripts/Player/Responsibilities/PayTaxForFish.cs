﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PayTaxForFish : MonetaryDuty
{
    //redo?
    public float TaxRatePercent;

    public void Start()
    {
        //GameLogic.Instance.Player.InventoryController.OnItemSold += (x) => UpdateResponsibility();
    }

    public override bool TryFill(PlayerController controller)
    {
        UpdateResponsibility();

        return base.TryFill(controller);
    }

    public override void UpdateResponsibility()
    {
        AmountRequired = 0f;
        
        foreach (Transaction soldItem in Controller.InventoryController.Transactions)
        {
            Fish fish = soldItem.entity as Fish;
            if (fish == null)
            {
                continue;
            }

            AmountRequired += soldItem.sellValue * TaxRatePercent;
        }

        SignalUpdate();
    }
}
