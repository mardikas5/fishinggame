﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoney : MonoBehaviour
{
    public float Amount;

    public event Action<float> AmountUpdated;

    [ContextMenu("Money")]
    public bool AddAmount(float amount)
    {
        if (TransferWithLimits(amount))
        {
            Amount += amount;
            AmountUpdated?.Invoke(Amount);
            return true;
        }
        return false;
    }

    public bool RemoveAmount(float amount, bool limited = true)
    {
        if (limited)
        {
            if (TransferWithLimits(-amount))
            {
                Amount -= amount;
                AmountUpdated?.Invoke(Amount);
                return true;
            }
            else
            {
                return false;
            }
        }

        Amount -= amount;
        AmountUpdated?.Invoke(Amount);

        return true;
    }

    public bool TransferWithLimits(float amount)
    {
        //if removing
        if (amount < 0f)
        {
            //if removing will leave less than 0.
            if (amount + Amount < 0f)
            {
                return false;
            }
        }
        // can always add.
        return true;
    }
}
