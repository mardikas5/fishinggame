﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Modifies the cutoffmask values to adjust for player height.
/// </summary>
[ExecuteInEditMode]
public class PlayerBoatInsideViewer : MonoBehaviour
{
    public float HeightOffset;

    public float fade = 0f;
    public float fadeDist = 10f;

    public TweenerCore<float, float, FloatOptions> activeTween;

    public BehindObjectMaskController maskController;

    GlobalShaderSettings ShaderSettings;

    
    // Start is called before the first frame update
    private void Start()
    {
        ShaderSettings = GlobalShaderSettings.Instance;
    }


    // Update is called once per frame
    private void LateUpdate()
    {
        UpdateShader();
    }

    private void UpdateShader() {
        if (ShaderSettings == null) {
            return;
        }

        if (maskController != null) {
            float endValue = fadeDist;
            bool cutOffEnabled = false;
            if (maskController.isActive) {
                cutOffEnabled = true;
                ShaderSettings.SetCutoffActive(true);
                endValue = 0f;
            }
            lerpValue(endValue)?.OnComplete(() => { if (!cutOffEnabled) { ShaderSettings.SetCutoffActive(false); }; });
        }
        else {
            ShaderSettings.SetCutoffActive(false);
        }

        ShaderSettings.UpdateShaderCutoffHeight(transform.position.y + HeightOffset + fade);
    }

    private Tween lerpValue(float endValue)
    {
        if (activeTween != null)
        {
            if (activeTween.endValue == endValue)
            {
                return activeTween;
            }

            activeTween.Kill();
            activeTween = null;
        }

        activeTween = DOTween.To(() => fade, (x) => fade = x, endValue, 0.5f).SetEase(Ease.OutCubic);
        return activeTween;
    }

}
