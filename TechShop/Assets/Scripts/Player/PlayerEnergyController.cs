﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEnergyController : MonoBehaviour {
    public int MaxEnergy;

    public int CurrentEnergy;

    public event Action<int> OnAmountUpdated;

    public bool IsResting = false;

    public float cumulatedRest;

    public float EnergyPerHour = 2f;

    //Temp rest animation name.
    public string AnimName;

    private PlayerController player;
    private TimeController TimeController;

    public void Start() {
        player = GameLogic.Instance.Player;

        TimeController = GameLogic.Instance.Time;

        GameLogic.Instance.GameController.OnStartNewDay += OnNewDayStart;

        OnNewDayStart();
    }

    public void Update() {
        if (IsResting) {
            Rest();
        }
    }

    //
    private void Rest() {
        if (TimeController.TimeStopped) { return; }

        cumulatedRest += EnergyPerHour * TimeController.HoursPassedLastFrame();

        //since player energy is set as an int -> floor to int and leave the remainder.
        int energyRecovered = Mathf.FloorToInt(cumulatedRest);
        cumulatedRest -= energyRecovered;

        //if recovered energy was 0 no need to update.
        if (energyRecovered > 0f) { SetEnergy(CurrentEnergy + energyRecovered); }
    }

    //
    public void OnStartRest(Transform restPos) {
        IsResting = true;

        player.StateController.SignalAction<GenericAnimActionSignal>(new GenericAnimActionSignal.AnimParams(AnimName, true));

        player.StateController.SignalAction<TargetPosAction>(restPos);
    }

    public void OnEndRest() {
        IsResting = false;

        player.StateController.SignalAction<GenericAnimActionSignal>(new GenericAnimActionSignal.AnimParams(AnimName, false));
    }

    public void OnNewDayStart() {
        SetEnergy(MaxEnergy);
    }

    public void SetEnergy(int amount) {
        CurrentEnergy = amount;

        if (CurrentEnergy > MaxEnergy) {
            CurrentEnergy = MaxEnergy;
        }

        OnAmountUpdated?.Invoke(CurrentEnergy);
    }

    public bool hasEnergy(int amount) {
        if (CurrentEnergy - amount < 0) {
            //Temp tutorial stuff shouldnt be here.
            UI.Instance.tutorialTiredObj.SetActive(true);

            return false;
        }
        return true;
    }

    public bool UseEnergy(int amount) {
        if (!hasEnergy(amount)) {
            return false;
        }
        SetEnergy(CurrentEnergy - amount);
        return true;
    }

    public int GetEnergyRestored(float hours) {
        return Mathf.FloorToInt(EnergyPerHour * hours);
    }
}
