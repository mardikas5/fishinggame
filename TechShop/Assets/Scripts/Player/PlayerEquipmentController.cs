﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipmentController : MonoBehaviour
{
    [Serializable]
    public class PlayerEquipmentSlot
    {
        public ItemComponentSlot Slot;
        public Inventory Inventory;

        public PlayerEquipmentSlot(ItemComponentSlot slot, Inventory inventory)
        {
            Slot = slot;
            Inventory = inventory;
        }
    }

    public PlayerEquipmentSlot ToolSlot, UpperGarments, LowerGarments, Belt, Hat, Boots, Charms0, Charms1, Charms2, Charms3;

    public PlayerStats PlayerStats;

    public event Action StatsChanged;

    [NonSerialized]
    public List<PlayerEquipmentSlot> EquipmentSlots = new List<PlayerEquipmentSlot>();

    public Dictionary<Item, List<PlayerStats.ModifierTypePair>> StatModItemLinks = new Dictionary<Item, List<PlayerStats.ModifierTypePair>>();

    private void Awake()
    {
        Init();
    }

    private void Start()
    {
        PlayerStats = GameLogic.Instance.Player.GetComponent<PlayerStats>();
    }

    private void AddModifiers(Item obj)
    {
        AddModifiersFromItem(obj, out List<PlayerStats.ModifierTypePair> modifiers);

        StatModItemLinks.Add(obj, modifiers);
    }

    private void RemoveModifiers(Item obj)
    {
        if (StatModItemLinks.TryGetValue(obj, out List<PlayerStats.ModifierTypePair> mods))
        {
            foreach (PlayerStats.ModifierTypePair modifier in mods)
            {
                RemoveModifier(modifier);
            }
        }

        StatModItemLinks.Remove(obj);
    }

    private void GetModifiersFromItem(Item item, List<PlayerStats.ModifierTypePair> modsList)
    {
        if (item == null)
        {
            return;
        }

        List<PlayerStats.ModifierTypePair> mods = item.ItemBase.Modifiers;

        if (mods != null)
        {
            modsList.AddRange(mods);
        }

        ItemComponentSlots nestedItemSlots = item.Entity.GetComponent<ItemComponentSlots>();

        if (nestedItemSlots != null)
        {
            foreach (Inventory slotInv in nestedItemSlots.ComponentSlotInventories)
            {
                if (slotInv.ItemsList.Count > 0)
                {
                    GetModifiersFromItem(slotInv.ItemsList[0], modsList);
                }
            }
        }
    }

    private void ItemChangedHandler(Item slotItem)
    {
        OnItemRemoved(slotItem, false);
        OnItemAdded(slotItem);
    }

    private void AddModifiersFromItem(Item item, out List<PlayerStats.ModifierTypePair> modPairs)
    {
        modPairs = new List<PlayerStats.ModifierTypePair>();

        GetModifiersFromItem(item, modPairs);

        for (int p = 0; p < modPairs.Count; p++)
        {
            PlayerStats.GetValue(modPairs[p].stat, out PlayerStats.StatValue _stat);

            _stat.AddModifier(modPairs[p].mod);

            object value = _stat.Evaluate();
        }
    }

    private void RemoveModifier(PlayerStats.ModifierTypePair modStatPair)
    {
        PlayerStats.GetValue(modStatPair.stat, out PlayerStats.StatValue _stat);

        _stat.RemoveModifier(modStatPair.mod);

        object value = _stat.Evaluate();
    }

    private void InitEquipmentSlots()
    {
        EquipmentSlots.Clear();
        EquipmentSlots.AddRange(new PlayerEquipmentSlot[] { ToolSlot, UpperGarments, LowerGarments, Belt, Hat, Boots, Charms0, Charms1, Charms2, Charms3 });

        foreach (PlayerEquipmentSlot equipSlot in EquipmentSlots)
        {
            if (equipSlot.Inventory == null)
            {
                GameObject slotInventory = new GameObject();
                Inventory t = slotInventory.AddComponent<Inventory>();
                t.Init(new Int2(1, 1));
                t.IgnoreItemSize = true;
                slotInventory.name = "EquipSlot";
                slotInventory.transform.SetParent(gameObject.transform);
                equipSlot.Inventory = t;
            }

            equipSlot.Inventory.InsertConditions.Add(equipSlot.Slot.SlotDefinition.IsValid);
        }

        foreach (PlayerEquipmentSlot t in EquipmentSlots)
        {
            t.Inventory.ItemAdded += (x) => OnItemAdded(x);
            t.Inventory.ItemRemoved += (x) => OnItemRemoved(x);
        }

        ToolSlot.Inventory.ItemAdded += OnToolChanged;
        ToolSlot.Inventory.ItemRemoved += OnToolRemoved;
    }

    private void Init()
    {
        InitEquipmentSlots();
    }

    private void OnItemAdded(Item obj, bool signalChange = true)
    {
        obj.ItemChangedSignal += ItemChangedHandler;

        AddModifiers(obj);

        StatsChanged?.Invoke();
    }

    private void OnItemRemoved(Item obj, bool signalChange = true)
    {
        obj.ItemChangedSignal -= ItemChangedHandler;

        RemoveModifiers(obj);

        StatsChanged?.Invoke();
    }

    private void OnToolRemoved(Item obj)
    {
        GameLogic.Instance.Player.ToolUser.UnEquip();
    }

    private void OnToolChanged(Item obj)
    {
        this.InvokeDelayed(.01f, () =>
            {
                //dangerous lookups.
                GameObject p = obj.Entity.GetVisualEntity().gameObject;
                GameLogic.Instance.Player.ToolUser.Equip(p.GetComponent<Tool>());
            });
    }


    // Update is called once per frame
    private void Update()
    {

    }
}
