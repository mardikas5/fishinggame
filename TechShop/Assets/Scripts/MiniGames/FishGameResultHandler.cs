﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class FishGameResultHandler : MonoBehaviour
{
    public GameObject successPanel;
    public GameObject failPanel;

    private GameObject activePanel;
    private CustomEvent onInput = new CustomEvent();

    public UIFishCaptureResult successResultHandler;

    // Update is called once per frame
    void Update()
    {
        if (activePanel != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                onInput?.Invoke();
            }
        }
    }


    public void Hide()
    {
        var panel = activePanel;

        activePanel.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 300f), .3f);
        activePanel.GetComponent<CanvasGroup>().DOFade(0f, .3f).OnComplete(() => panel.SetActive(false));
        activePanel = null;
        //onInput = null;
    }

    public void Show(bool isCaptured, Fish capturedFish, Action p, string failReason = "")
    {
        if (activePanel != null)
        {
            return;
        }

        this.InvokeDelayed(.3f, () =>
        {
            onInput.AttachOneShot(() => { Hide(); p?.Invoke(); });
        });

        successPanel.SetActive(false);
        failPanel.SetActive(false);
        failPanel.GetComponentInChildren<TextMeshProUGUI>().text = failReason;

        activePanel = isCaptured ? successPanel : failPanel;

        activePanel.SetActive(true);
        activePanel.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 300f);
        activePanel.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, .3f);
        activePanel.GetComponent<CanvasGroup>().DOFade(.98f, .3f);

        if (isCaptured)
        {
            this.InvokeDelayed(.3f, () =>
            {
                onInput.AttachOneShot(() => { this.InvokeDelayed(1.2f, () => successResultHandler.DisplayResult(capturedFish)); });
            });
        }
    }
}
