﻿using NCalc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//IFishBaitGameVarProvider or whatever.
public class FishingBaitGameVarsController : MonoBehaviour
{
    public PlayerStats playerStats;
    public FishingStateController stateController;
    public FishingBaitGame baitingGame;
    public FishBalanceGame balanceGame;

    // Start is called before the first frame update
    private void Start()
    {
        playerStats = GameLogic.Instance.Player.Stats;

        baitingGame.GameStarting += UpdateBaitGameStats;
        baitingGame.GameStarting += UpdateCaptureGameStats;
    }

    public void UpdateBaitGameStats()
    {
        float baitGameErrorValue = .1f + (playerStats.Tenacity * .01f + playerStats.Imitation * 0.05f);
        baitingGame.errorAllowed = Mathf.Clamp(baitGameErrorValue, .05f, .3f);

        float baitGameCircleSpeed = 1.2f - (playerStats.Finesse * .02f);
        float baitGameSpeedDiff = .8f - (playerStats.Steadiness * .03f);

        baitingGame.MinMaxSpeed = new Vector2(baitGameCircleSpeed, baitGameCircleSpeed + baitGameSpeedDiff);
        //.2 - .5
        float baitGameMinMaxSpeedDelta = .2f - (playerStats.Imitation * 0.05f);
        float baitGameMinMaxSpeedDiff = .3f - (playerStats.Imitation * 0.05f);

        baitingGame.MinMaxSpeedDelta = new Vector2(baitGameMinMaxSpeedDelta, baitGameMinMaxSpeedDiff);
        //-.3, .4
        float baitGameProgress = .4f + (playerStats.Imitation * 0.05f);
        float baitGameProgressLoss = -.3f + (playerStats.Imitation * 0.05f);
        baitingGame.MinMaxClampProgress = new Vector2(baitGameProgressLoss, baitGameProgress);
    }

    public void UpdateCaptureGameStats()
    {
        float fishNormalSize = 0f;
        float fishMoveMultiplier = 0f;
        float fishPullStrength = 0f;

        if (stateController.activeFish != null)
        {
            Fish fish = stateController.activeFish;

            fishNormalSize = fish.FishBase.CaptureDifficulty + (fish.Size * .06f);
            fishNormalSize *= .1f;
            fishMoveMultiplier = fish.FishBase.CaptureDifficulty + (fish.Size * .01f);
            fishMoveMultiplier *= .1f;
            fishPullStrength = fish.FishBase.CaptureDifficulty + (fish.Size * .02f);
            fishPullStrength *= 0.1f;

            balanceGame.fishNormalizedSize.x = stateController.activeFish.NormalizedSize.x;
            balanceGame.fishNormalizedSize.y = stateController.activeFish.NormalizedSize.y;

            Debug.LogError("new fish: " + fish.Size + ", " + fishNormalSize + ", " + fishMoveMultiplier + ", " + fishPullStrength);
        }

        float playerSizePositive = .02f + (playerStats.Tenacity * 0.015f + playerStats.Finesse * 0.01f);
        float playerSizeNegative = 0f;// fishNormalSize;

        balanceGame.playerNormalizedSize.x = Mathf.Max(playerSizePositive / 2f, playerSizePositive - playerSizeNegative);
        balanceGame.playerNormalizedSize.y = balanceGame.playerNormalizedSize.x;

        if (balanceGame.GetGameType(stateController.activeFish) == FishBalanceGame.BalanceGameType.oneDGame)
        {
            balanceGame.playerNormalizedSize.y = 1f;
            balanceGame.fishNormalizedSize.y = 1f;
        }

        //.5f
        float moveMulti = .04f + (playerStats.Finesse * 0.07f + playerStats.Steadiness * 0.02f);
        float moveMultiNegative = 0f;// fishMoveMultiplier;
        balanceGame.moveMultiplier = Mathf.Max(moveMulti / 2f, moveMulti - moveMultiNegative);

        //1f
        float pullStrength = .01f + (playerStats.Strength * 0.05f);
        float pullStrengthNegative = 0f;// fishPullStrength;
        balanceGame.playerPullStrength = Mathf.Max(pullStrength / 2f, pullStrength - pullStrengthNegative);
    }
}
