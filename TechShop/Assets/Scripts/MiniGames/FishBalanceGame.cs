﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fishing game controller for the fish capture game. -> aligning the player and fish boxes.
/// </summary>
public class FishBalanceGame : FishGameLayer
{
    public enum BalanceGameType
    {
        oneDGame,
        twoDGame
    }

    //should remove this from here? -> UI stuff
    public FishingPullInCatchMeter captureMeter;

    /// <summary>
    /// Current position of the player box.
    /// </summary>
    public Vector2 PlayerNormalizedPos = Vector2.zero;

    /// <summary>
    /// Size of player box in normalized terms compared to play area.
    /// </summary>
    public Vector2 playerNormalizedSize = new Vector2(.1f,.1f);

    public Vector2 fishNormalizedSize = new Vector2(.2f, .2f);
    /// <summary>
    /// Current movement direction.
    /// </summary>
    public Vector2 cumulativeTension = Vector2.zero;

    /// <summary>
    /// How fast the box moves when player holds down button.
    /// </summary>
    public float moveMultiplier = .5f;

    /// <summary>
    /// How fast the box starts moving to the start.
    /// </summary>
    public float fallMultiplier => moveMultiplier * 2f;
    
    /// <summary>
    /// Amount the fish moves towards the player when boxes overlap.
    /// </summary>
    public float playerPullStrength = 1f;

    /// <summary>
    /// Multiplier to movement when box hits the edge of the play area.
    /// </summary>
    public float edgeTensionBounce = -.6f;

    public BalanceGameType GameType;

    private event Action<bool> OnCaptured;



    public override void StartGame(Fish activeFish, FishGameController controller)
    {
        base.StartGame(activeFish, controller);

        OnCaptured = null;

        ActiveFish = activeFish;

        PlayerNormalizedPos = Vector2.zero;

        GameType = GetGameType(activeFish);

        captureMeter.StartGame(this, OnStartSignalHandler);

        UpdateFishing();
    }

    private void OnStartSignalHandler()
    {
        IsActive = true;
        PlayerNormalizedPos = captureMeter.GetNormalizedPlayerPos(captureMeter.getPlayAreaPosFish());
        //PlayerNormalizedPos = ActiveFishBehaviour.NormalizedPos;
        Debug.LogError(PlayerNormalizedPos + ", " + ActiveFishBehaviour.NormalizedPos);
    }

    public void Update()
    {
        if (!IsActive)
        {
            return;
        }

        UpdateFishing();
    }

    public void UpdateFishing()
    {
        if (Input.GetKey(KeyCode.D))
        {
            OnPullX();
        }
        else
        {
            OnRestX();
        }

        if (Input.GetKey(KeyCode.W))
        {
            OnPullY();
        }
        else
        {
            OnRestY();
        }

        UpdatePlayerBox();

        CheckFishOverlap();

        MoveFish();
    }


    private void MoveFish()
    {
        if (Controller.FishProgress >= 1f)
        {
            EscapeFish();
        }

        if (Controller.FishProgress <= 0f)
        {
            CaptureFish();
        }
    }

    /// <summary>
    /// Check if the player and fish boxes are overlapping, apply action accordingly.
    /// </summary>
    private void CheckFishOverlap()
    {
        //float dist = Mathf.Abs(PlayerNormalizedPos - ActiveFishBehaviour.normalizedPos.x);
        //ui dictating stuff not gucci. -> todo.
        if (captureMeter.BoxesOverlap)
        {
            ActiveFishBehaviour.Pull(playerPullStrength);
        }
        else
        {
            ActiveFishBehaviour.Evade();
        }
    }

    /// <summary>
    /// Gets the game type depending on the fish.
    /// </summary>
    /// <param name="activeFish"></param>
    /// <returns></returns>
    public BalanceGameType GetGameType(Fish activeFish)
    {
        Debug.LogError(activeFish?.name + ", " + activeFish.FishBase.CaptureDifficulty);
        if (activeFish.FishBase.CaptureDifficulty > .2f)
        {
           return BalanceGameType.twoDGame;
        }
        else
        {
           return BalanceGameType.oneDGame;
        }
    }

    /// <summary>
    /// Updates the player box according to move direction.
    /// </summary>
    private void UpdatePlayerBox()
    {
        PlayerNormalizedPos += cumulativeTension * Time.deltaTime;

        Vector2 rawPosValue = PlayerNormalizedPos;

        PlayerNormalizedPos.x = Mathf.Clamp01(PlayerNormalizedPos.x);
        PlayerNormalizedPos.y = Mathf.Clamp01(PlayerNormalizedPos.y);

        //if any edge was hit.
        if (rawPosValue.x != PlayerNormalizedPos.x)
        {
            cumulativeTension.x *= edgeTensionBounce;
        }
        if (rawPosValue.y != PlayerNormalizedPos.y)
        {
            cumulativeTension.y *= edgeTensionBounce;
        }
    }

    private void OnRestY()
    {
        cumulativeTension.y -= Time.deltaTime * fallMultiplier;
    }

    private void OnPullY()
    {
        cumulativeTension.y += Time.deltaTime * moveMultiplier;
    }

    private void OnRestX()
    {
        cumulativeTension.x -= Time.deltaTime * fallMultiplier;
    }

    private void OnPullX()
    {
        cumulativeTension.x += Time.deltaTime * moveMultiplier;
    }

    public void CaptureFish()
    {
        EndGame(true);
    }

    public void EscapeFish()
    {
        EndGame(false);
    }

    public override void EndGame(bool winner)
    {
        Success = winner;

        Completed = true;

        ///wrong way around. shouldnt signal to ui stuff.
        captureMeter.EndGame(winner, () => OnCaptured?.Invoke(winner));

        IsActive = false;
    }
}
