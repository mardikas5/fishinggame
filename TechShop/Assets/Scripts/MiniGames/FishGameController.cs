﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Controls the whole fishing game.
/// </summary>
public class FishGameController : MonoBehaviour
{
    /// <summary>
    /// The layer to complete in order to win the game.
    /// </summary>
    public FishGameLayer MainLayer;

    /// <summary>
    /// Intermediary or other games that can be triggered.
    /// </summary>
    public List<FishGameLayer> GameLayers = new List<FishGameLayer>();


    public Fish ActiveFish;
    public FishCaptureGameBehaviour FishCaptureBehaviour;

    public FishingRod ActiveRod;


    public float FishProgress { get; private set; }

    //Exposed to inspector.
    public float FishStartProgress;

    private bool GameActive = false;

    private event Action<bool> onLayersComplete;

    private void Awake()
    {
        GameActive = false;
    }

    /// <summary>
    /// Starts a new game with set fish, rod and handler for completion.
    /// </summary>
    /// <param name="activeFish"></param>
    /// <param name="activeRod"></param>
    /// <param name="captureEndHandler"></param>
    public void StartGame(Fish activeFish, FishingRod activeRod, Action<bool> captureEndHandler) {
        GameActive = false;

        ActiveFish = activeFish;
        FishCaptureBehaviour = ActiveFish.GetComponent<FishCaptureGameBehaviour>();

        if (FishCaptureBehaviour == null) {
            Debug.LogError(" Fish has no capture game behaviour!...");
            ActiveFish.gameObject.AddComponent<FishCaptureGameBehaviour>();
        }

        ActiveRod = activeRod;

        FishProgress = FishStartProgress;

        FishCaptureBehaviour.Progress = FishProgress;

        onLayersComplete = null;
        onLayersComplete += captureEndHandler;

        foreach (FishGameLayer layer in GameLayers) {
            layer.StartGame(activeFish, this);
        }

        this.InvokeDelayed(.5f, () => GameActive = true);
    }


    private void Update()
    {
        if (!GameActive)
        {
            return;
        }

        CheckVictory();

        if (MainLayer.IsActive)
        {
            UpdateFish();
        }
    }


    private void UpdateFish()
    {
        FishCaptureBehaviour.UpdateBehaviour();

        FishProgress = FishCaptureBehaviour.Progress;
    }


    public void CheckVictory()
    {
        if (!MainLayer.Completed)
        {
            return;
        }

        onLayersComplete?.Invoke(MainLayer.Success);

        EndGame();
    }


    public void EndGame()
    {
        foreach (FishGameLayer layer in GameLayers)
        {
            layer.EndGame(false);
        }

        GameActive = false;
    }


    public void AddFishCaptureProgress(float v)
    {
        FishCaptureBehaviour.Progress += v;
    }

    /// <summary>
    /// Sets an active layer for the game to focus on, pausing others.
    /// </summary>
    /// <param name="layer"></param>
    public void Focus(FishGameLayer layer)
    {
        foreach (FishGameLayer L in GameLayers)
        {
            if (L != layer)
            {
                L.IsActive = false;
            }
        }
    }

    /// <summary>
    /// Lets every layer run as usual.
    /// </summary>
    public void UnFocus()
    {
        foreach (FishGameLayer L in GameLayers)
        {
            L.IsActive = true;
        }
    }
}
