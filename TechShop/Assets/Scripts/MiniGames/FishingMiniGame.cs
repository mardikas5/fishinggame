﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FishingMiniGame : MonoBehaviour, IMiniGame
{
    private FishingRod activeRod;
    private HumanController activeChar;

    public FishingStateController fishingController;

    public FishingState StartState;

    public FishingGamesCollection.FishingGameEnum GameEnum => FishingGamesCollection.FishingGameEnum.Beginner;

    public event Action<int> StageChanged;

    public bool GameActive = false;

    public void Awake()
    {
        fishingController.enabled = false;

        fishingController.stateChange += OnStateChange;
    }

    public void StartGame(FishingRod useRod, Action<bool> onComplete, HumanController playerChar)
    {
        GameActive = true;

        fishingController.activeChar = playerChar;
        fishingController.activeRod = useRod;

        fishingController.enabled = true;
        fishingController.StartFishing(StartState, onComplete);
        //this.InvokeDelayed(.1f, () => );
    }

    private void OnStateChange(GenericState obj)
    {
        FishingState current = obj as FishingState;
        if (current == null)
        {
            return;
        }
        StageChanged?.Invoke(current.Stage);
    }

    public void End()
    {
        GameActive = false;

        fishingController.SetState(null);

        fishingController.enabled = false;
    }
}
