﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Controller for the letter typing mini-game for the fishing capture game.
/// </summary>
public class FishComboGame : FishGameLayer
{
    /// <summary>
    /// List of all the triggers currently set in the game.
    /// </summary>
    public List<FishingComboTrigger> activeComboTriggers;

    /// <summary>
    /// Currenly active trigger.
    /// </summary>
    [NonSerialized]
    public FishingComboTrigger activeTrigger; //get private set?

    /// <summary>
    /// Keys that could come up as part of the combo game.
    /// </summary>
    public List<KeyCode> possibleKeys = new List<KeyCode>();

    /// <summary>
    /// Time allocated to press a key.
    /// </summary>
    public float timePerChar = .1f;

    public event Action onTriggersGenerated;

    public float timeTaken = 0f;

    private void Awake()
    {
        activeTrigger = null;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!IsActive)
        {
            return;
        }

        CheckFishComboTriggers();
    }
    
    public override void StartGame(Fish activeFish, FishGameController controller)
    {
        base.StartGame(activeFish, controller);

        int nrOfTriggers = Random.Range(activeFish.FishBase.MinLines, activeFish.FishBase.MaxLines); // nr of lines, difficulty depends on fish.

        GenerateComboTriggers(nrOfTriggers);

        IsActive = true;
    }

    /// <summary>
    /// Extracts characters from a list of KeyCodes 
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    public char[] charsFromKeyCodes(List<KeyCode> keys)
    {
        char[] allowed = new char[keys.Count];
        for (int i = 0; i < allowed.Length; i++)
        {
            allowed[i] = (char)keys[i];
        }
        return allowed;
    }

    /// <summary>
    /// Generates a new set of combo triggers for the combo game.
    /// </summary>
    public void GenerateComboTriggers(int nrOfTriggers)
    {
        activeComboTriggers.Clear();

        float start = Random.Range(.4f, .6f);
        float dist = start / nrOfTriggers;

        for (int i = 0; i < nrOfTriggers; i++)
        {
            FishingComboTrigger trigger = new FishingComboTrigger() { NormalizedPos = start - (i * dist) };
            ResetTrigger(trigger);
            activeComboTriggers.Add(trigger);
        }

        onTriggersGenerated?.Invoke();
    }

    /// <summary>
    /// Resets a combo trigger
    /// </summary>
    /// <param name="trigger"></param>
    private void ResetTrigger(FishingComboTrigger trigger)
    {
        trigger.Started = false;
        trigger.Completed = false;
        trigger.ComboIndex = 0;

        trigger.GenerateCombo(Random.Range(3, 7), charsFromKeyCodes(possibleKeys));
        trigger.TimeToSolve = timePerChar * trigger.keyCombo.Length;
    }

    /// <summary>
    /// Gets the currently active keycode from a preset keycodes
    /// </summary>
    /// <returns></returns>
    private KeyCode getActiveInput()
    {
        foreach (KeyCode k in possibleKeys)
        {
            if (Input.GetKeyDown(k))
            {
                return k;
            }
        }
        return KeyCode.None;
    }

    /// <summary>
    /// Updates the currently active combo (key-press game) trigger
    /// </summary>
    private void UpdateActiveTrigger()
    {
        timeTaken += Time.deltaTime;
        KeyCode input = getActiveInput();

        if (timeTaken > activeTrigger.TimeToSolve)
        {
            activeTrigger.FailCombo();
            activeTrigger = null;
            return;
        }

        if (!activeTrigger.EnterCombo(input))
        {
            timeTaken += timePerChar;
        }

        return;
    }

    /// <summary>
    /// Checks if a point has been reached where a combo game should start.
    /// </summary>
    private void CheckFishComboTriggers()
    {
        if (activeTrigger != null)
        {
            UpdateActiveTrigger();
            return;
        }

        foreach (FishingComboTrigger trigger in activeComboTriggers)
        {
            if (trigger.Completed)
            {
                continue;
            }

            if (trigger.NormalizedPos > FishProgress)
            {
                StartTrigger(trigger);
            }
        }
    }

    /// <summary>
    /// Starts the set trigger, sets it as active.
    /// </summary>
    /// <param name="trigger"></param>
    private void StartTrigger(FishingComboTrigger trigger)
    {
        timeTaken = 0f;
        Controller.Focus(this);
        activeTrigger = trigger;
        trigger.Started = true;
        Action triggerOnce = null;

        triggerOnce = () =>
        {
            OnTriggerComplete(trigger);
            trigger.onComboComplete -= triggerOnce;
        };

        trigger.onComboComplete += triggerOnce;
    }

    /// <summary>
    /// Handler for a combo game trigger that has been completed.
    /// </summary>
    /// <param name="trigger"></param>
    private void OnTriggerComplete(FishingComboTrigger trigger)
    {
        if (trigger.TimeToSolve > timeTaken)
        {
            OnTriggerSuccess(trigger);
        }
        else
        {
            OnTriggerFail(trigger);
        }

        Controller.UnFocus();
    }

    /// <summary>
    /// Handler for fail
    /// </summary>
    /// <param name="trigger"></param>
    private void OnTriggerFail(FishingComboTrigger trigger)
    {
        trigger.SignalResult(false);
        Controller.AddFishCaptureProgress(.15f);
        ResetTrigger(trigger);
    }

    /// <summary>
    /// Handler for success
    /// </summary>
    /// <param name="trigger"></param>
    private void OnTriggerSuccess(FishingComboTrigger trigger)
    {
        Controller.AddFishCaptureProgress(-.01f);
        trigger.SignalResult(true);

        trigger.Completed = true;
        trigger.Started = false;
        activeTrigger = null;

        Completed = true;

        foreach (FishingComboTrigger t in activeComboTriggers)
        {
            if (!t.Completed)
            {
                Completed = false;
                break;
            }
        }
    }
}
