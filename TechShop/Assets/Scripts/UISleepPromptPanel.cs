﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UISleepPromptPanel : MonoBehaviour, IGenericUIPanel
{
    public Slider slider;

    public TextMeshProUGUI timeText;
    public TextMeshProUGUI timeLeftText;
    public TextMeshProUGUI energyRestoreText;

    public PlayerRestArea restAreaListener;

    public TweenFadeHandler mainPanelFade;
    public TweenFadeHandler restingPanelFade;

    public GameObject selectionPanel;
    public GameObject restingPanel;

    public event Action<IGenericUIPanel> PanelClosed;
    public event Action<IGenericUIPanel> PanelOpened;

    private bool isResting = false;
    private DateTime endTime;

    // Start is called before the first frame update
    private void Awake()
    {
        selectionPanel.GetComponent<CanvasGroup>().alpha = 0f;

        mainPanelFade.Init(selectionPanel.GetComponent<CanvasGroup>(), gameObject, .5f, .6f);
        restingPanelFade.Init(restingPanel.GetComponent<CanvasGroup>(), restingPanel, .5f, .6f);

        slider.onValueChanged.AddListener(OnSliderValueChanged);
        OnSliderValueChanged(0f);
    }

    public void ShowRestArea(PlayerRestArea restArea)
    {
        restAreaListener = restArea;
        Show();
    }

    public void Update()
    {
        if (isResting)
        {
            UpdateRestTime();
        }
    }

    private void UpdateRestTime()
    {
        TimeSpan timeLeft = restAreaListener.GetRestTimeLeft();

        if (timeLeft.TotalHours <= 0f)
        {
            isResting = false;
            restingPanelFade.DoFade(0f, .3f).OnComplete(() => Close());
        }

        timeLeftText.text =
            "Resting..." +
            " Hours: " + timeLeft.TotalHours.ToString("F1");
    }

    public void OnAcceptClicked()
    {
        mainPanelFade.DoFade(0f, .4f).OnComplete(() => selectionPanel.SetActive(false));
        restingPanelFade.Open(true);

        restAreaListener.RestForHours(slider.value * 24f);
        isResting = true;
    }

    private void Show()
    {
        gameObject.SetActive(true);

        mainPanelFade.Open(true);

        selectionPanel.SetActive(true);
        restingPanel.SetActive(false);

        PanelOpened?.Invoke(this);
        mainPanelFade.Open(false);
    }

    private void OnSliderValueChanged(float normalized)
    {
        float hours = normalized * 24f;
        timeText.text = (hours).ToString("F1") + " Hours";
        energyRestoreText.text = GameLogic.Instance.Player.EnergyController.GetEnergyRestored(hours).ToString("F0") + " Energy";
    }

    public void Close()
    {
        PanelClosed?.Invoke(this);
        mainPanelFade.Close();
    }
}
