﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FishingSpot
{
    public WaterBody waterBody;
    public bool Known;
    public bool Unlocked;

    public float CostToUnlock;

    public event Action OnUnlocked;

    public void Unlock()
    {
        Unlocked = true;
        OnUnlocked?.Invoke();
    }

    // Start is called before the first frame update
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {

    }
}
